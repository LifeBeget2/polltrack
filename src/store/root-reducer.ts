import { combineReducers } from '@reduxjs/toolkit';
import { reducer as navSectionReducer } from '../slices/navSection';
import { reducer as testHistoryReducer } from '../slices/testHistory';
import { reducer as waterTestHistoryReducer } from '../slices/waterTestHistory';
import { reducer as waterTestReducer } from '../slices/waterTest';
import { reducer as waterTestPoolsReducer } from '../slices/waterTestPools';
import { reducer as waterTestCalculatorReducer } from '../slices/waterTestCalculator';
import { reducer as calendarReducer } from '../slices/calendar';
import { reducer as accountReducer } from '../slices/account';
import { reducer as contactReducer } from '../slices/contact';
import { reducer as dashboardAnalyticsReducer } from '../slices/dashboardAnalytics';
import { reducer as dashboardFinanceReducer } from '../slices/dashboardFinance';
import { reducer as contactInvoiceReducer } from '../slices/contactInvoice';
import { reducer as batchInvoiceReducer } from '../slices/batchInvoice';
import { reducer as batchPreviewReducer } from '../slices/batchPreview';
import { reducer as batchQueueReducer } from '../slices/batchQueue';
import { reducer as resendBatchInvoiceReducer } from '../slices/resendBatchInvoice';
import { reducer as resendInvoiceReducer } from '../slices/resendInvoice';
import { reducer as poolReducer } from '../slices/pool';
import { reducer as poolSpecificationsReducer } from '../slices/poolSpecifications';
import { reducer as poolEquipmentReducer } from '../slices/poolEquipment';
import { reducer as productReducer } from '../slices/product';
import { reducer as chatReducer } from '../slices/chat';
import { reducer as kanbanReducer } from '../slices/kanban';
import { reducer as quickScheduleReducer } from '../slices/quickSchedule';
import { reducer as mailReducer } from '../slices/mail';
import { reducer as sentSmsReducer } from '../slices/sentSms';
import { reducer as smsTierReducer } from '../slices/smsTier';
import { reducer as openedQuoteReducer } from '../slices/openedQuote';
import { reducer as archivedQuoteReducer } from '../slices/archivedQuote';
import { reducer as chemicalGroupReducer } from '../slices/chemicalGroup';
import { reducer as observationGroupReducer } from '../slices/observationGroup';
import { reducer as contactDetailReducer } from '../slices/contactDetail';
import { reducer as poolDetailReducer } from '../slices/poolDetail';
import { reducer as userReducer } from '../slices/user';
import { reducer as jobDetailReducer } from '../slices/jobDetail';
import { reducer as jobTemplateReducer } from '../slices/jobTemplate';
import { reducer as jobCalendarReducer } from '../slices/jobCalendar';
import { reducer as jobToInvoiceReducer } from '../slices/jobToInvoice';
import { reducer as poolPhotosReducer } from '../slices/poolPhotos';
import { reducer as deduplicatePoolToolReducer } from '../slices/deduplicatePool';
import { reducer as deduplicateContactToolReducer } from '../slices/deduplicateContactTool';
import { reducer as deduplicateContactToolAutomaticReducer } from '../slices/deduplicateContactToolAutomatic';
import { reducer as fileImporterReducer } from '../slices/fileImporter';

import { reducer as campaignReducer } from '../slices/campaign';
import { reducer as chemicalTestReducer } from '../slices/chemicalTest';
import { reducer as observationTestReducer } from '../slices/observationTest';
import { reducer as invoiceCreateReducer } from '../slices/invoiceCreate';
import { reducer as batchInvoiceDetailsReducer } from '../slices/batchInvoiceDetails';

import { reducer as vendIntegrationReducer } from '../slices/vendIntegration';
import { reducer as vendIntegrationSyncContactsReducer } from '../slices/vendIntegrationSyncContacts';
import { reducer as vendIntegrationSyncProductsReducer } from '../slices/vendIntegrationSyncProducts';
import { reducer as vendIntegrationMatchUsersReducer } from '../slices/vendIntegrationMatchUsers';

import { apiReducer } from 'src/api';

export const rootReducer = combineReducers({
  ...apiReducer,
  navSection: navSectionReducer,
  testHistory: testHistoryReducer,
  waterTestHistory: waterTestHistoryReducer,
  waterTest: waterTestReducer,
  waterTestPools: waterTestPoolsReducer,
  waterTestCalculator: waterTestCalculatorReducer,
  account: accountReducer,
  contact: contactReducer,
  contactInvoice: contactInvoiceReducer,
  batchInvoice: batchInvoiceReducer,
  batchQueue: batchQueueReducer,
  batchPreview: batchPreviewReducer,
  batchInvoiceDetails: batchInvoiceDetailsReducer,
  resendBatchInvoice: resendBatchInvoiceReducer,
  resendInvoice: resendInvoiceReducer,
  dashboardAnalytics: dashboardAnalyticsReducer,
  dashboardFinance: dashboardFinanceReducer,
  pool: poolReducer,
  poolSpecifications: poolSpecificationsReducer,
  poolEquipment: poolEquipmentReducer,
  product: productReducer,
  calendar: calendarReducer,
  chat: chatReducer,
  kanban: kanbanReducer,
  quickSchedule: quickScheduleReducer,
  openedQuote: openedQuoteReducer,
  archivedQuote: archivedQuoteReducer,
  chemicalGroup: chemicalGroupReducer,
  observationGroup: observationGroupReducer,
  contactDetail: contactDetailReducer,
  poolDetail: poolDetailReducer,
  user: userReducer,
  jobDetail: jobDetailReducer,
  jobTemplate: jobTemplateReducer,
  jobCalendar: jobCalendarReducer,
  jobToInvoice: jobToInvoiceReducer,
  poolPhotos: poolPhotosReducer,
  fileImporter: fileImporterReducer,
  campaign: campaignReducer,
  chemicalTest: chemicalTestReducer,
  observationTest: observationTestReducer,
  invoiceCreate: invoiceCreateReducer,
  mail: mailReducer,
  deduplicatePoolTool: deduplicatePoolToolReducer,
  sentSms: sentSmsReducer,
  smsTier: smsTierReducer,
  deduplicateContactTool: deduplicateContactToolReducer,
  deduplicateContactToolAutomatic: deduplicateContactToolAutomaticReducer,
  vendIntegration: vendIntegrationReducer,
  vendIntegrationSyncContacts: vendIntegrationSyncContactsReducer,
  vendIntegrationSyncProducts: vendIntegrationSyncProductsReducer,
  vendIntegrationMatchUsers: vendIntegrationMatchUsersReducer,
});
