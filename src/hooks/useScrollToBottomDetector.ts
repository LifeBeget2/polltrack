import { useEffect } from 'react';

export default function useScrollToBottomDetector(targetNode: HTMLElement | null, callback: () => void) {
  useEffect(() => {
    if (targetNode == null) {
      return null;
    }

    const options = {
      root: null,
      rootMargin: '0px',
      threshold: 0.5,
    };

    const observer: IntersectionObserver = new IntersectionObserver((entries) => {
      if (entries.length === 0) {
        return;
      }

      const [entry] = entries;

      if (entry.isIntersecting) {
        callback();
      }
    }, options);

    observer.observe(targetNode);

    return () => observer.unobserve(targetNode);
  }, [targetNode, callback]);
}
