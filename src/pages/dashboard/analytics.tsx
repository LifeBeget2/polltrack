import {
  Box,
  Button,
  Container,
  Grid,
} from '@mui/material';
import {
  AnalyticsOverview,
  AnalyticsMostRequestedJobs,
  AnalyticsPerformedWaterTests,
  AnalyticsStoreStats,
} from '../../components/dashboard/analytics';
import { useSettings } from '../../hooks/use-settings';
import ChevronDownIcon from '../../icons/ChevronDown';
import Page from '../../components/Page';
import Breadcrumbs from '../../components/Breadcrumbs';
import type { BreadcrumbItem } from '../../types/common';
import { AuthGuard } from '../../components/authentication/auth-guard';
import { MainLayout } from '../../layouts/main-layout';
import { NextPage } from 'src/../next';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Operations',
    routeTo: '/dashboard/overview'
  },
  {
    title: 'Dashboard',
    routeTo: '/dashboard/overview'
  },
];

const Analytics: NextPage = () => {
  const { settings } = useSettings();

  return (
    <Page
      title="Dashboard | Analytics"
    >
      <Breadcrumbs
        title="Analytics"
        items={breadcrumbItems}
      >
        <Button
          color="primary"
          endIcon={<ChevronDownIcon fontSize="small" />}
          sx={{ ml: 2 }}
          variant="contained"
        >
          Last month
        </Button>
      </Breadcrumbs>
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Box sx={{ py: 3 }}>
          <AnalyticsOverview />
        </Box>
        <Box sx={{ pb: 3 }}>
          <AnalyticsStoreStats />
        </Box>
        <Grid
          container
          spacing={3}
        >
          <Grid
            item
            xl={9}
            md={8}
            xs={12}
          >
            <AnalyticsMostRequestedJobs />
          </Grid>
          <Grid
            item
            xl={3}
            md={4}
            xs={12}
          >
            <AnalyticsPerformedWaterTests />
          </Grid>
        </Grid>
      </Container>
    </Page>
  );
};

Analytics.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default Analytics;
