/* eslint-disable */
import type { NextPage } from 'next';
import {
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  Container,
  Grid,
  Typography
} from '@mui/material';
import {
  OverviewPrivateWallet,
  OverviewTotalBalance,
  OverviewTotalTransactions,
  OverviewWeeklyEarnings
} from '../../components/dashboard/overview';
import { useSettings } from '../../hooks/use-settings';
import ExternalLinkIcon from '../../icons/ExternalLink';
import InformationCircleIcon from '../../icons/InformationCircle';
import Page from '../../components/Page';
import useAuth from '../../hooks/useAuth';
import Breadcrumbs from '../../components/Breadcrumbs';
import type { BreadcrumbItem } from '../../types/common';
import {AuthGuard} from "src/components/authentication/auth-guard";
import {MainLayout} from "src/layouts/main-layout";

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Operations',
    routeTo: '/'
  },
  {
    title: 'Dashboard',
    routeTo: '/'
  },
];

const Overview: NextPage = () => {
  const { user } = useAuth();
  const { settings } = useSettings();

  return (
    <Page
      title="Dashboard | Overview"
    >
      <Breadcrumbs
        title="Overview"
        items={breadcrumbItems}
      />
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Grid
          container
          spacing={3}
        >
          <Grid
            alignItems="center"
            container
            justifyContent="space-between"
            spacing={3}
            item
            xs={12}
          >
            <Grid item>
              <Typography
                color="textSecondary"
                variant="overline"
              >
                {`Greetings, ${user.first_name} ${user.last_name}`}
              </Typography>
              <Typography
                color="textSecondary"
                variant="subtitle2"
              >
                Here&apos;s what&apos;s happening with your organisation today
              </Typography>
            </Grid>
          </Grid>
          {/*<Grid*/}
          {/*  item*/}
          {/*  md={6}*/}
          {/*  xs={12}*/}
          {/*>*/}
          {/*  <OverviewWeeklyEarnings />*/}
          {/*</Grid>*/}
          {/*<Grid*/}
          {/*  item*/}
          {/*  md={6}*/}
          {/*  xs={12}*/}
          {/*>*/}
          {/*  <OverviewPrivateWallet />*/}
          {/*</Grid>*/}
          {/*<Grid*/}
          {/*  item*/}
          {/*  md={8}*/}
          {/*  xs={12}*/}
          {/*>*/}
          {/*  <OverviewTotalTransactions />*/}
          {/*</Grid>*/}
          {/*<Grid*/}
          {/*  item*/}
          {/*  md={4}*/}
          {/*  xs={12}*/}
          {/*>*/}
          {/*  <OverviewTotalBalance />*/}
          {/*</Grid>*/}
          <Grid
            item
            md={6}
            xs={12}
          >
            <Card>
              <CardHeader
                disableTypography
                subheader={(
                  <Typography
                    color="textPrimary"
                    variant="h6"
                  >
                    Ask our stuff
                  </Typography>
                )}
                title={(
                  <Box
                    sx={{
                      alignItems: 'center',
                      display: 'flex',
                      pb: 2
                    }}
                  >
                    <InformationCircleIcon color="primary" />
                    <Typography
                      color="textPrimary"
                      sx={{ pl: 1 }}
                      variant="h6"
                    >
                      Help desk
                    </Typography>
                  </Box>
                )}
                sx={{ pb: 0 }}
              />
              <CardContent sx={{ pt: '8px' }}>
                <Typography
                  color="textSecondary"
                  variant="body2"
                >
                  Contact our support team to assist you.
                </Typography>
              </CardContent>
              <CardActions
                sx={{
                  backgroundColor: 'background.default',
                  p: 2
                }}
              >
                <Button
                  color="primary"
                  endIcon={<ExternalLinkIcon fontSize="small" />}
                  href="https://pooltrackrassist.freshdesk.com/support/tickets/new"
                  target="_blank"
                  variant="text"
                >
                  Contact help desk
                </Button>
              </CardActions>
            </Card>
          </Grid>
          <Grid
            item
            md={6}
            xs={12}
          >
            <Card>
              <CardHeader
                disableTypography
                subheader={(
                  <Typography
                    color="textPrimary"
                    variant="h6"
                  >
                    Need help figuring things out?
                  </Typography>
                )}
                title={(
                  <Box
                    sx={{
                      alignItems: 'center',
                      display: 'flex',
                      pb: 2
                    }}
                  >
                    <InformationCircleIcon color="primary" />
                    <Typography
                      color="textPrimary"
                      sx={{ pl: 1 }}
                      variant="h6"
                    >
                      Knowledge base
                    </Typography>
                  </Box>
                )}
                sx={{ pb: 0 }}
              />
              <CardContent sx={{ pt: '8px' }}>
                <Typography
                  color="textSecondary"
                  variant="body2"
                >
                  Check our knowledge base for hints and most common scenarios descriptions.
                </Typography>
              </CardContent>
              <CardActions
                sx={{
                  backgroundColor: 'background.default',
                  p: 2
                }}
              >
                <Button
                  color="primary"
                  endIcon={<ExternalLinkIcon fontSize="small" />}
                  href="https://pooltrackrassist.freshdesk.com/support/home"
                  target="_blank"
                  variant="text"
                >
                  Knowledge base
                </Button>
              </CardActions>
            </Card>
          </Grid>
        </Grid>
      </Container>
    </Page>
  );
};

Overview.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default Overview;
