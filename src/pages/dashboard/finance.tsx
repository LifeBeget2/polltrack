import type { NextPage } from 'next';
import {
  Box,
  Button,
  Container,
  Grid,
} from '@mui/material';
import {
  FinanceOverview,
  FinancePerformanceStats,
  FinanceSales
} from '../../components/dashboard/finance';
import { useSettings } from '../../hooks/use-settings';
import ChevronDownIcon from '../../icons/ChevronDown';
import Page from '../../components/Page';
import Breadcrumbs from '../../components/Breadcrumbs';
import type { BreadcrumbItem } from '../../types/common';
import { AuthGuard } from '../../components/authentication/auth-guard';
import { MainLayout } from '../../layouts/main-layout';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Operations',
    routeTo: '/'
  },
  {
    title: 'Dashboard',
    routeTo: '/'
  },
];

const Finance: NextPage = () => {
  const { settings } = useSettings();

  return (
    <Page
      title="Dashboard | Finance"
    >
      <Breadcrumbs
        title="Finance"
        items={breadcrumbItems}
      >
        <Button
          color="primary"
          endIcon={<ChevronDownIcon fontSize="small" />}
          sx={{ ml: 2 }}
          variant="contained"
        >
          Last month
        </Button>
      </Breadcrumbs>
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Box sx={{ mt: 3 }}>
          <Grid
            container
            spacing={3}
          >
            <Grid
              item
              xs={12}
            >
              <FinanceOverview />
            </Grid>
            <Grid
              item
              xs={12}
            >
              <FinanceSales />
            </Grid>
            <Grid
              item
              xs={12}
            >
              <FinancePerformanceStats />
            </Grid>
          </Grid>
        </Box>
      </Container>
    </Page>
  );
};

Finance.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default Finance;
