import React, { memo } from 'react';
import NextLink from 'next/link';
import type { NextPage } from 'src/../next';

import {
  Box,
  Button,
  Container,
} from '@mui/material';

import FileImporter from 'src/components/file-importer/index';
import Page from 'src/components/Page';
import { useSettings } from 'src/hooks/use-settings';
import { Column } from 'src/types/fileImporter';
import Breadcrumbs from 'src/components/Breadcrumbs';
import type { BreadcrumbItem } from 'src/types/common';
import ArrowLeftIcon from 'src/icons/ArrowLeft';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Operations',
    routeTo: '/dashboard/overview'
  },
  {
    title: 'Pools',
    routeTo: '/pools'
  },
];

const EXPECTED_COLUMNS: Column[] = [
  {
    type: 'string',
    matchHeaderName: 'id',
    id: 'id',
    displayName: 'ID',
    description: 'Identifier of a pool',
    required: false,
  },
  {
    type: 'string',
    matchHeaderName: 'pool_type',
    id: 'pool_type',
    displayName: 'Pool type',
    description: 'Type of a pool',
    required: true,
  },
  {
    type: 'string',
    matchHeaderName: 'surface_type',
    id: 'surface_type',
    displayName: 'Surface type',
    description: 'Surface type of a pool',
    required: true,
  },
  {
    type: 'string',
    matchHeaderName: 'classification',
    id: 'classification',
    displayName: 'Classification',
    description: 'Classification of a pool',
    required: false,
  },
  {
    type: 'string',
    matchHeaderName: 'ground_level',
    id: 'ground_level',
    displayName: 'Ground level',
    description: 'Ground level of a pool',
    required: false,
  },
  {
    type: 'string',
    matchHeaderName: 'location',
    id: 'location',
    displayName: 'Location',
    description: 'Location of a pool',
    required: false,
  },
  {
    type: 'string',
    matchHeaderName: 'filter',
    id: 'filter',
    displayName: 'Filter',
    description: 'Filter of a pool',
    required: false,
  },
  {
    type: 'string',
    matchHeaderName: 'sanitiser',
    id: 'sanitiser',
    displayName: 'Sanitiser',
    description: 'Sanitiser of a pool',
    required: false,
  },
  {
    type: 'float',
    matchHeaderName: 'volume',
    id: 'volume',
    displayName: 'Volume',
    description: 'Volume of a pool',
    required: true,
  },
  {
    type: 'string',
    matchHeaderName: 'address_street_one',
    id: 'address_street_one',
    displayName: 'Address street one',
    description: '',
    required: true,
  },
  {
    type: 'string',
    matchHeaderName: 'address_street_two',
    id: 'address_street_two',
    displayName: 'Address street two',
    description: '',
    required: false,
  },
  {
    type: 'string',
    matchHeaderName: 'address_city',
    id: 'address_city',
    displayName: 'Address city',
    description: '',
    required: false,
  },
  {
    type: 'string',
    matchHeaderName: 'address_state',
    id: 'address_state',
    displayName: 'Address state',
    description: '',
    required: true,
  },
  {
    type: 'string',
    matchHeaderName: 'address_postcode',
    id: 'address_postcode',
    displayName: 'Address postcode',
    description: '',
    required: true,
  },
  {
    type: 'string',
    matchHeaderName: 'notes',
    id: 'notes',
    displayName: 'Notes',
    description: '',
    required: false,
  },
];

const PoolImport: NextPage = memo(() => {
  const { settings } = useSettings();

  return (
    <Page title="Pool | Import">
      <Breadcrumbs
        title="Import pools"
        items={breadcrumbItems}
      >
        <NextLink
          href="/pools"
          passHref
        >
          <Button
            color="primary"
            startIcon={<ArrowLeftIcon fontSize="small" />}
            sx={{ mt: 1 }}
            variant="outlined"
          >
            Cancel
          </Button>
        </NextLink>
      </Breadcrumbs>
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Box py={3}>
          <FileImporter
            title="Drag and drop a file (CSV or XLSX) anywhere, or browse your files."
            expectedColumns={EXPECTED_COLUMNS}
            onConfirm={() => {}}
          />
        </Box>
      </Container>
    </Page>
  );
});

PoolImport.propTypes = {};

PoolImport.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default PoolImport;
