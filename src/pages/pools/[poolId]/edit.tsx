import React, { useEffect } from 'react';
import type { NextPage } from 'src/../next';
import { useRouter } from 'next/router';
import NextLink from 'next/link';
import {
  Box,
  Button,
  Container,
} from '@mui/material';
import { PoolForm } from 'src/components/pool';
import { useSettings } from 'src/hooks/use-settings';
import { useDispatch, useSelector } from 'src/store';
import Page from 'src/components/Page';
import { getPool, reset } from 'src/slices/poolDetail';
import Breadcrumbs from 'src/components/Breadcrumbs';
import type { BreadcrumbItem } from 'src/types/common';
import ArrowLeftIcon from 'src/icons/ArrowLeft';
import LoadingScreen from 'src/components/LoadingScreen';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Operations',
    routeTo: '/dashboard/overview'
  },
  {
    title: 'Pools',
    routeTo: '/pools'
  },
];

const PoolEdit: NextPage = () => {
  const router = useRouter();
  const { poolId } = router.query;
  const { settings } = useSettings();
  const { organisation } = useSelector((state) => state.account);
  const { pool, contacts } = useSelector((state) => state.poolDetail);
  const dispatch = useDispatch();

  useEffect(() => () => dispatch(reset()), []);

  useEffect(() => {
    // @ts-ignore
    dispatch(getPool(organisation.id, parseInt(poolId, 10)));
  }, [organisation]);

  return (
    <Page
      title="Pool | Edit"
    >
      <Breadcrumbs
        title="Edit pool"
        items={breadcrumbItems}
      >
        <NextLink
          href="/pools"
          passHref
        >
          <Button
            color="primary"
            startIcon={<ArrowLeftIcon fontSize="small" />}
            sx={{ mt: 1 }}
            variant="outlined"
          >
            Cancel
          </Button>
        </NextLink>
      </Breadcrumbs>
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Box mt={3}>
          {!pool
            ? (<LoadingScreen />)
            : (
              <PoolForm
                pool={pool}
                contacts={contacts}
                onEditComplete={() => router.push('/pools')}
              />
            )}
        </Box>
      </Container>
    </Page>
  );
};

PoolEdit.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default PoolEdit;
