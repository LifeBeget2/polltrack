import React, { useState, useEffect } from 'react';
import type { ChangeEvent } from 'react';
import type { NextPage } from 'src/../next';
import { useRouter } from 'next/router';
import NextLink from 'next/link';
import {
  Box,
  Button,
  Container,
  Divider,
  Grid,
  Tab,
  Tabs,
} from '@mui/material';
import PoolPhotos from 'src/components/pool/pool-photos/index';
import { WaterTests } from 'src/components/pool/water-tests';
import {
  PoolForm,
  PoolDataManagement,
  PoolInvoices,
  PoolJobs,
  PoolSummary,
  PoolLogs
} from 'src/components/pool';
import { useSettings } from 'src/hooks/use-settings';
import Page from 'src/components/Page';
import { useDispatch, useSelector } from 'src/store';
import { getPool, reset } from 'src/slices/poolDetail';
import { NotesList } from 'src/components/note';
import Breadcrumbs from 'src/components/Breadcrumbs';
import type { BreadcrumbItem } from 'src/types/common';
import LoadingScreen from 'src/components/LoadingScreen';
import ArrowLeftIcon from 'src/icons/ArrowLeft';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Operations',
    routeTo: '/dashboard/overview'
  },
  {
    title: 'Pools',
    routeTo: '/pools'
  },
];

const DETAILS_TAB = 'details';
const JOBS_TAB = 'jobs';
const QUOTES_TAB = 'quotes';
const INVOICES_TAB = 'invoices';
const WATER_TESTS_TAB = 'water-tests';
const JOB_PHOTOS_TAB = 'job-photos';
const LOGS_TAB = 'logs';

const tabs = [
  { label: 'Details', value: DETAILS_TAB },
  { label: 'Jobs', value: JOBS_TAB },
  { label: 'Quotes', value: QUOTES_TAB },
  { label: 'Invoices', value: INVOICES_TAB },
  { label: 'Water tests', value: WATER_TESTS_TAB },
  { label: 'Job photos', value: JOB_PHOTOS_TAB },
  { label: 'Logs', value: LOGS_TAB },
];

const PoolDetails: NextPage = () => {
  const router = useRouter();
  const { poolId, tab } = router.query;
  const { settings } = useSettings();
  // @ts-ignore
  const [currentTab, setCurrentTab] = useState<string>(tab);
  const { organisation } = useSelector((state) => state.account);
  const { pool, contacts, notes } = useSelector((state) => state.poolDetail);
  const dispatch = useDispatch();

  useEffect(() => () => dispatch(reset()), []);

  useEffect(() => {
    // @ts-ignore
    dispatch(getPool(organisation.id, parseInt(poolId, 10)));
  }, [organisation, getPool, poolId]);

  if (!pool) {
    return <LoadingScreen />;
  }

  const handleTabsChange = (event: ChangeEvent<{}>, value: string): void => {
    setCurrentTab(value);
  };

  return (
    <Page
      title="Pool | Details"
    >
      <Breadcrumbs
        title="Pool details"
        items={breadcrumbItems}
      >
        <NextLink
          href="/pools"
          passHref
        >
          <Button
            color="primary"
            startIcon={<ArrowLeftIcon fontSize="small" />}
            variant="outlined"
          >
            Cancel
          </Button>
        </NextLink>
      </Breadcrumbs>
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Box sx={{ mt: 3 }}>
          <Tabs
            indicatorColor="primary"
            onChange={handleTabsChange}
            scrollButtons="auto"
            textColor="primary"
            value={currentTab}
            variant="scrollable"
          >
            {tabs.map((tab) => (
              <Tab
                key={tab.value}
                label={tab.label}
                value={tab.value}
                onClick={() => router.push(`/pools/${poolId}/${tab.value}`)}
              />
            ))}
          </Tabs>
        </Box>
        <Divider />
        <Box sx={{ mt: 3 }}>
          {currentTab === DETAILS_TAB && (
            <Grid
              container
              spacing={3}
            >
              <Grid
                item
                md={8}
                xs={12}
              >
                <PoolForm
                  pool={pool}
                  contacts={contacts}
                />
              </Grid>
              <Grid
                item
                md={4}
                xs={12}
              >
                <PoolSummary />
                <Box
                  mt={3}
                >
                  <NotesList notes={notes} />
                </Box>
                <Box
                  mt={3}
                >
                  <PoolDataManagement />
                </Box>
              </Grid>
            </Grid>
          )}
          {currentTab === JOBS_TAB && <PoolJobs />}
          {currentTab === QUOTES_TAB && <PoolJobs />}
          {currentTab === INVOICES_TAB && <PoolInvoices />}
          {currentTab === WATER_TESTS_TAB && <WaterTests />}
          {currentTab === JOB_PHOTOS_TAB && <PoolPhotos />}
          {currentTab === LOGS_TAB && <PoolLogs />}
        </Box>
      </Container>
    </Page>
  );
};

PoolDetails.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default PoolDetails;
