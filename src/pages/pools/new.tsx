import React, { useEffect } from 'react';
import type { NextPage } from 'src/../next';
import { useRouter } from 'next/router';
import NextLink from 'next/link';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';
import {
  Box,
  Button,
  Container,
} from '@mui/material';
import { PoolForm } from 'src/components/pool';
import { useSettings } from 'src/hooks/use-settings';
import ArrowLeftIcon from 'src/icons/ArrowLeft';
import { useDispatch, useSelector } from 'src/store';
import Page from 'src/components/Page';
import { getPool } from 'src/slices/poolDetail';
import Breadcrumbs from 'src/components/Breadcrumbs';
import type { BreadcrumbItem } from 'src/types/common';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Operations',
    routeTo: '/dashboard/overview'
  },
  {
    title: 'Pools',
    routeTo: '/pools'
  },
];

const PoolNew: NextPage = () => {
  const { settings } = useSettings();
  const { pool, contacts, isLoaded } = useSelector((state) => state.poolDetail);
  const dispatch = useDispatch();
  const router = useRouter();

  useEffect(() => {
    dispatch(getPool());
  }, []);

  if (!isLoaded) {
    return null;
  }

  return (
    <Page
      title="Pool | New"
    >
      <Breadcrumbs
        title="New pool"
        items={breadcrumbItems}
      >
        <NextLink
          href="/pools"
          passHref
        >
          <Button
            color="primary"
            startIcon={<ArrowLeftIcon fontSize="small" />}
            sx={{ mt: 1 }}
            variant="outlined"
          >
            Cancel
          </Button>
        </NextLink>
      </Breadcrumbs>
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Box mt={3}>
          <PoolForm
            pool={pool}
            contacts={contacts}
            onAddComplete={() => router.push('/pools')}
          />
        </Box>
      </Container>
    </Page>
  );
};

PoolNew.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default PoolNew;
