import React, { useEffect } from 'react';
import type { NextPage } from 'src/../next';
import NextLink from 'next/link';
import {
  Box,
  Button,
  Container,
} from '@mui/material';
import { PoolListTable } from '../../components/pool';
import PlusIcon from '../../icons/Plus';
import { getPools } from '../../slices/pool';
import Page from '../../components/Page';
import { useDispatch, useSelector } from '../../store';
import { useSettings } from '../../hooks/use-settings';
import Breadcrumbs from '../../components/Breadcrumbs';
import type { BreadcrumbItem } from '../../types/common';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Operations',
    routeTo: '/dashboard/overview'
  },
  {
    title: 'Pools',
    routeTo: '/pools'
  },
];

const PoolList: NextPage = () => {
  const { settings } = useSettings();
  const { organisation } = useSelector((state) => state.account);
  const { pools, limit, page, searchText, order, orderBy } = useSelector((state) => state.pool);
  const dispatch = useDispatch();

  useEffect(() => {
    const sortBy = orderBy ? `${order === 'desc' ? '-' : ''}${orderBy}` : '';
    dispatch(getPools(organisation.id, limit, page, searchText, sortBy));
  }, [organisation, limit, page, searchText, order, orderBy]);

  return (
    <Page
      title="Pool | List"
    >
      <Breadcrumbs
        title="Pools list"
        items={breadcrumbItems}
      >
        <NextLink
          href="/pools/new"
          passHref
        >
          <Button
            color="primary"
            startIcon={<PlusIcon fontSize="small" />}
            variant="contained"
          >
            New pool
          </Button>
        </NextLink>
      </Breadcrumbs>
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Box sx={{ mt: 3 }}>
          <PoolListTable pools={pools} />
        </Box>
      </Container>
    </Page>
  );
};

PoolList.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default PoolList;
