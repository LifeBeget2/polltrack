import {
  Box,
  Container,
} from '@mui/material';
import React, { useEffect } from 'react';
import Page from '../../components/Page';
import { useDispatch, useSelector } from '../../store';
import { useSettings } from '../../hooks/use-settings';
import { getHistory } from '../../slices/testHistory';
import Breadcrumbs from '../../components/Breadcrumbs';
import TestHistoryTable from '../../components/lab/TestHistoryTable';
import type { BreadcrumbItem } from '../../types/common';
import type { NextPage } from '../../../next';
import { AuthGuard } from '../../components/authentication/auth-guard';
import { MainLayout } from '../../layouts/main-layout';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Operations',
    routeTo: '/dashboard/overview'
  },
  {
    title: 'Lab',
    routeTo: '/lab/water-test/pool-list'
  }
];

const TestHistory: NextPage = () => {
  const { settings } = useSettings();
  const { organisation } = useSelector((state) => state.account);
  const { selectedRange, history, limit, page, searchText, order, orderBy } = useSelector((state) => state.testHistory);
  const dispatch = useDispatch();

  useEffect(() => {
    const sortBy = orderBy ? `${order === 'desc' ? '-' : ''}${orderBy}` : '';
    dispatch(getHistory(organisation.id, selectedRange, limit, page, searchText, sortBy));
  }, [organisation, selectedRange, limit, page, searchText, order, orderBy]);

  return (
    <Page
      title="Lab | Test history"
    >
      <Breadcrumbs
        title="Test history"
        items={breadcrumbItems}
      />
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Box sx={{ mt: 3 }}>
          <TestHistoryTable history={history} />
        </Box>
      </Container>
    </Page>
  );
};

TestHistory.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default TestHistory;
