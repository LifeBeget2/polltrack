import React, { useEffect } from 'react';
import {
  Box,
  Button,
  Container,
} from '@mui/material';
import NextLink from 'next/link';
import PoolListTable from 'src/components/lab/PoolListTable';
import { getPools } from 'src/slices/waterTestPools';
import Page from 'src/components/Page';
import PlusIcon from 'src/icons/Plus';
import { useDispatch, useSelector } from 'src/store';
import { useSettings } from 'src/hooks/use-settings';
import type { BreadcrumbItem } from 'src/types/common';
import Breadcrumbs from 'src/components/Breadcrumbs';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';
import type { NextPage } from 'src/../next';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Operations',
    routeTo: '/dashboard/overview'
  },
  {
    title: 'Lab',
    routeTo: '/lab/water-test/pool-list'
  }
];

const PoolList: NextPage = () => {
  const { settings } = useSettings();
  const { organisation } = useSelector((state) => state.account);
  const { pools, limit, page, searchText, order, orderBy } = useSelector((state) => state.waterTestPools);
  const dispatch = useDispatch();

  useEffect(() => {
    const sortBy = orderBy ? `${order === 'desc' ? '-' : ''}${orderBy}` : '';
    dispatch(getPools(organisation.id, limit, page, searchText, sortBy));
  }, [organisation, limit, page, searchText, order, orderBy]);

  return (
    <Page
      title="Lab | Pools list"
    >
      <Breadcrumbs
        title="Pools list"
        items={breadcrumbItems}
      >
        <NextLink
          href="/pools/new"
          passHref
        >
          <Button
            color="primary"
            startIcon={<PlusIcon fontSize="small" />}
            variant="contained"
          >
            New pool
          </Button>
        </NextLink>
      </Breadcrumbs>
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Box sx={{ mt: 3 }}>
          <PoolListTable pools={pools} />
        </Box>
      </Container>
    </Page>
  );
};

PoolList.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default PoolList;
