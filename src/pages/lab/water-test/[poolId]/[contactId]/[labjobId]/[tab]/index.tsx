/* eslint-disable */
import React, { useState, useEffect, useMemo } from 'react';
import type { ChangeEvent } from 'react';
import {
  Alert,
  Box,
  Container,
  Divider,
  Grid,
  Tab,
  Tabs
} from '@mui/material';
import { useSettings } from 'src/hooks/use-settings';
import toast from 'react-hot-toast';
import Page from 'src/components/Page';
import useAuth from 'src/hooks/useAuth';
import { useDispatch, useSelector } from 'src/store';
import {getLabJob, reset, updateWaterTest, saving} from 'src/slices/waterTest';
import { WaterTestCalculator, WaterTestDeviceConnect, WaterTestSummary, WaterTestHistoryTable } from 'src/components/lab';
import LoadingScreen from 'src/components/LoadingScreen';
import Breadcrumbs from 'src/components/Breadcrumbs';
import type { BreadcrumbItem } from 'src/types/common';
import {useTheme} from "@mui/material/styles";
import type {PdfInstance} from "src/types/common";
import {AuthGuard} from "src/components/authentication/auth-guard";
import {MainLayout} from "src/layouts/main-layout";
import type {NextPage} from "src/../next";
import {useRouter} from "next/router";

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Operations',
    routeTo: '/dashboard/overview'
  },
  {
    title: 'Lab',
    routeTo: '/lab/water-test/pool-list'
  }
];

const WATER_TEST_TAB = 'detail';
const WATER_TEST_REPORT_TAB = 'report';
const WATER_TEST_HISTORY_TAB = 'history';

const WaterTest: NextPage = () => {
  const router = useRouter();
  const { poolId, contactId, labjobId, tab } = router.query;
  const { settings } = useSettings();
  const { user } = useAuth();
  const [tabs, setTabs] = useState<any[]>([]);
  // @ts-ignore
  const [currentTab, setCurrentTab] = useState<string>(tab);
  const { organisation, logo } = useSelector((state) => state.account);
  const { labJob, pool, contact, isLoading, isSaving } = useSelector((state) => state.waterTest);
  const { activeChemicalTestResults, activeObservationTestResults } = useSelector((state) => state.waterTestCalculator);
  const dispatch = useDispatch();

  useEffect(() => {
    // @ts-ignore
    dispatch(getLabJob(organisation.id, user.id, parseInt(poolId, 10), parseInt(contactId, 10), labjobId === 'new' ? null : parseInt(labjobId, 10)));
    return dispatch(reset())
  }, []);

  useEffect(() => {
    if (labJob && labjobId === 'new') {
      router.push(`/lab/water-test/${labJob.pool_id}/${labJob.contact_id}/${labJob.id}/detail`);
    }
  }, [organisation, labJob, labjobId]);

  const handleGenerateReport = async (pdfInstance: PdfInstance) => {
    try {
      await dispatch(saving(true));
      await dispatch(updateWaterTest(
        organisation.id,
        labJob.id,
        activeChemicalTestResults,
        activeObservationTestResults,
        pdfInstance.blob
      ));
      toast.success('The report has been successfully generated');
    } catch (e) {
      await dispatch(saving(false));
      toast.error(e.message);
    }
  };

  useEffect(() => {
    let tabs = [
      { label: 'Water test', value: WATER_TEST_TAB },
      { label: 'Test history', value: WATER_TEST_HISTORY_TAB },
    ];

    // if (labJob && labJob.pdf_location) {
    //   tabs = [
    //     { label: 'Water test', value: WATER_TEST_TAB },
    //     { label: 'Water test report', value: WATER_TEST_REPORT_TAB },
    //     { label: 'Test history', value: TEST_HISTORY_TAB },
    //   ];
    // }

    setTabs(tabs);
  }, [labJob]);

  const poolHasEnoughInfo: boolean = useMemo(() => {
    let poolIsValid = false;
    if (pool) {
      poolIsValid = pool.pool_volume > 0 && pool.surface_type_id && pool.pool_sanitisers?.length > 0;
    }

    return poolIsValid;
  }, [pool]);

  if (!labJob || isLoading) {
    return <LoadingScreen />;
  }

  const handleTabsChange = (event: ChangeEvent<{}>, value: string): void => {
    setCurrentTab(value);
  };

  return (
    <Page
      title="Lab | Water test calculator"
    >
      <Breadcrumbs
        title="Water test"
        items={breadcrumbItems}
      />
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Box sx={{ mt: 3 }}>
          <Tabs
            indicatorColor="primary"
            onChange={handleTabsChange}
            scrollButtons="auto"
            textColor="primary"
            value={currentTab}
            variant="scrollable"
          >
            {tabs.map((tab) => (
              <Tab
                key={tab.value}
                label={tab.label}
                value={tab.value}
                // onClick={() => navigate(`${location.pathname.split('/').slice(0, -1).join('/')}/${tab.value}`)}
              />
            ))}
          </Tabs>
        </Box>
        <Divider />
        <Box sx={{ mt: 3 }}>
          {currentTab === WATER_TEST_TAB && (
            <Grid
              container
              spacing={3}
            >
              {poolHasEnoughInfo && (
                <Grid
                  item
                  xs={12}
                  md={8}
                >
                  <WaterTestDeviceConnect
                    pool={pool}
                  />
                  <Box
                    mt={3}
                  >
                    <WaterTestCalculator
                      job={labJob}
                      pool={pool}
                      contact={contact}
                      disabled={isSaving}
                      onGenerateReport={handleGenerateReport}
                    />
                  </Box>
                </Grid>
              )}
              {!poolHasEnoughInfo && (
                <Grid
                  item
                  xs={12}
                  md={8}
                >
                  <Alert severity="warning">
                    <div>
                      Please update the pool information to view the chemical calculator.
                    </div>
                  </Alert>
                </Grid>
              )}
              <Grid
                item
                xs={12}
                md={4}
                display={{ md: 'block', xs: 'none' }}
              >
                <WaterTestSummary
                  labJob={labJob}
                  pool={pool}
                  contact={contact}
                />
              </Grid>
            </Grid>
          )}
          {currentTab === WATER_TEST_HISTORY_TAB && (
            <WaterTestHistoryTable
              // @ts-ignore
              poolId={poolId}
              // @ts-ignore
              contactId={contactId}
            />
          )}
        </Box>
      </Container>
    </Page>
  );
};

WaterTest.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default WaterTest;
