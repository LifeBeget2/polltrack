import React, { useCallback, useEffect } from 'react';
import type { NextPage } from 'src/../next';
import NextLink from 'next/link';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';
import {
  Box,
  Button,
  Container,
} from '@mui/material';
import { saveAs } from 'file-saver';
import { ProductListTable } from 'src/components/product';
import { useSettings } from 'src/hooks/use-settings';
import DownloadIcon from 'src/icons/Download';
import UploadIcon from 'src/icons/Upload';
import PlusIcon from 'src/icons/Plus';
import { useDispatch, useSelector } from '../../store';
import Page from '../../components/Page';
import Breadcrumbs from '../../components/Breadcrumbs';
import type { BreadcrumbItem } from '../../types/common';
import { useGetProductsQuery, useExportProductMutation } from 'src/api/product';
import { ALL_CAT_FILTER } from '../../types/product';
import { setTotal } from '../../slices/product';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Operations',
    routeTo: '/dashboard/overview'
  },
  {
    title: 'Products',
    routeTo: '/products'
  },
];

const ProductList: NextPage = () => {
  const { settings } = useSettings();
  const dispatch = useDispatch();
  const { organisation } = useSelector((state) => state.account);
  const { limit, page, categoryFilter, searchText, order, orderBy } = useSelector((state) => state.product);
  const [exportRequest] = useExportProductMutation();
  const { data, isLoading, isSuccess } = useGetProductsQuery({
    organisationId: organisation.id,
    limit,
    page,
    catFilter: (Boolean(categoryFilter) && categoryFilter !== ALL_CAT_FILTER) ? categoryFilter : '',
    filter: searchText,
    sortBy: orderBy ? `${order === 'desc' ? '-' : ''}${orderBy}` : ''
  });

  useEffect(() => {
    if (isSuccess && data?.meta) {
      dispatch(setTotal(data.meta.total));
    }
  }, [data, isSuccess]);

  const exportProducts = useCallback(async () => {
    try {
      // TODO: move to the slice
      const response:any = await exportRequest({ organisationId: organisation.id });
      const csv = new Blob([response.data], {
        type: 'application/csv'
      });
      const fileName = `Products_export_${new Date().toLocaleDateString()}.csv`;
      saveAs(csv, fileName);
    } catch (err) {
      console.error(err);
    }
  }, [organisation]);

  return (
    <Page
      title="Product | List"
    >
      <Breadcrumbs
        title="Products list"
        items={breadcrumbItems}
      >
        <NextLink
          href="/products/import"
          passHref
        >
          <Button
            color="primary"
            startIcon={<UploadIcon fontSize="small" />}
            variant="text"
          >
            Import
          </Button>
        </NextLink>
        <Button
          color="primary"
          startIcon={<DownloadIcon fontSize="small" />}
          sx={{ ml: 2 }}
          variant="text"
          onClick={exportProducts}
        >
          Export
        </Button>
        <NextLink
          href="/products/new"
          passHref
        >
          <Button
            color="primary"
            startIcon={<PlusIcon fontSize="small" />}
            sx={{ ml: 2 }}
            variant="contained"
          >
            New product
          </Button>
        </NextLink>
      </Breadcrumbs>
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Box sx={{ mt: 3 }}>
          <ProductListTable
            products={data?.data ? data.data : []}
            isLoading={isLoading}
          />
        </Box>
      </Container>
    </Page>
  );
};

ProductList.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default ProductList;
