import React from 'react';
import type { NextPage } from 'src/../next';
import NextLink from 'next/link';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';
import {
  Box,
  Button,
  Container,
} from '@mui/material';
import { ProductCreateForm } from 'src/components/product';
import { useSettings } from 'src/hooks/use-settings';
import ArrowLeftIcon from 'src/icons/ArrowLeft';
import Page from 'src/components/Page';
import Breadcrumbs from 'src/components/Breadcrumbs';
import type { BreadcrumbItem } from 'src/types/common';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Operations',
    routeTo: '/dashboard/overview'
  },
  {
    title: 'Products',
    routeTo: '/products'
  },
];

const ProductNew: NextPage = () => {
  const { settings } = useSettings();

  return (
    <Page
      title="Product | New"
    >
      <Breadcrumbs
        title="New product"
        items={breadcrumbItems}
      >
        <NextLink
          href="/products"
          passHref
        >
          <Button
            color="primary"
            startIcon={<ArrowLeftIcon fontSize="small" />}
            variant="outlined"
          >
            Cancel
          </Button>
        </NextLink>
      </Breadcrumbs>
      <Container maxWidth={settings.compact ? 'md' : false}>
        <Box sx={{ mt: 3 }}>
          <ProductCreateForm />
        </Box>
      </Container>
    </Page>
  );
};

ProductNew.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default ProductNew;
