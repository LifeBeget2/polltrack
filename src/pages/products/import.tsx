import React, { memo } from 'react';
import type { NextPage } from 'src/../next';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';
import {
  Box,
  Container,
} from '@mui/material';

import FileImporter from 'src/components/file-importer/index';
import Page from 'src/components/Page';
import { useSettings } from 'src/hooks/use-settings';
import { Column } from 'src/types/fileImporter';
import Breadcrumbs from '../../components/Breadcrumbs';
import type { BreadcrumbItem } from '../../types/common';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Operations',
    routeTo: '/dashboard/overview'
  },
  {
    title: 'Products',
    routeTo: '/products'
  },
];

interface ProductImportProps {}

const EXPECTED_COLUMNS: Column[] = [
  {
    type: 'string',
    matchHeaderName: 'id',
    id: 'id',
    displayName: 'ID',
    description: 'Identifier of a product',
    required: false,
  },
  {
    type: 'string',
    matchHeaderName: 'name',
    id: 'name',
    displayName: 'Name',
    description: 'Name of a product',
    required: true,
  },
  {
    type: 'string',
    matchHeaderName: 'category',
    id: 'category',
    displayName: 'Category',
    description: 'Category of a product',
    required: false,
  },
  {
    type: 'float',
    matchHeaderName: 'price',
    id: 'price',
    displayName: 'Price',
    description: 'Price of a product',
    required: true,
  },
  {
    type: 'float',
    matchHeaderName: 'volume',
    id: 'volume',
    displayName: 'Volume',
    description: 'Volume of a product',
    required: false,
  },
  {
    type: 'string',
    matchHeaderName: 'barcode',
    id: 'barcode',
    displayName: 'Barcode',
    description: '',
    required: false,
  },
  {
    type: 'string',
    matchHeaderName: 'sku',
    id: 'sku',
    displayName: 'SKU',
    description: 'A stock-keeping unit',
    required: false,
  },
  {
    type: 'string',
    matchHeaderName: 'units',
    id: 'units',
    displayName: 'Units',
    description: '',
    required: false,
  },
  {
    type: 'string',
    matchHeaderName: 'brand',
    id: 'brand',
    displayName: 'Brand',
    description: '',
    required: false,
  },
  {
    type: 'boolean',
    matchHeaderName: 'gst included',
    id: 'gst_included',
    displayName: 'GST included',
    description: '',
    required: false,
  },
];

const ProductImport: NextPage<ProductImportProps> = memo(() => {
  const { settings } = useSettings();

  return (
    <Page title="Product | Import">
      <Breadcrumbs
        title="Import products"
        items={breadcrumbItems}
      />
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Box py={3}>
          <FileImporter
            title="Drag and drop a file (CSV or XLSX) anywhere, or browse your files."
            expectedColumns={EXPECTED_COLUMNS}
            onConfirm={() => {}}
          />
        </Box>
      </Container>
    </Page>
  );
});

ProductImport.propTypes = {};

ProductImport.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default ProductImport;
