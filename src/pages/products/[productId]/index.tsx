import React, { useState, useEffect } from 'react';
import type { NextPage } from 'src/../next';
import NextLink from 'next/link';
import { useRouter } from 'next/router';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';
import {
  Box,
  Button,
  Container,
} from '@mui/material';
import type { Product } from 'src/types/product';
import { ProductEditForm } from 'src/components/product';
import useIsMountedRef from 'src/hooks/useIsMountedRef';
import { useSettings } from 'src/hooks/use-settings';
import { useSelector } from 'src/store';
import Page from 'src/components/Page';
import Breadcrumbs from 'src/components/Breadcrumbs';
import type { BreadcrumbItem } from 'src/types/common';
import LoadingScreen from 'src/components/LoadingScreen';
import ArrowLeftIcon from 'src/icons/ArrowLeft';
import { useGetProductQuery } from 'src/api/product';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Operations',
    routeTo: '/dashboard/overview'
  },
  {
    title: 'Products',
    routeTo: '/products'
  },
];

const ProductEdit: NextPage = () => {
  const router = useRouter();
  const { productId } = router.query;
  const isMountedRef = useIsMountedRef();
  const { settings } = useSettings();
  const { organisation } = useSelector((state) => state.account);
  const [product, setProduct] = useState<Product | null>(null);
  const { data, isLoading } = useGetProductQuery({ organisationId: organisation.id, id: parseInt(productId.toString(), 10) });

  useEffect(() => {
    if (!isLoading) {
      if (isMountedRef.current) {
        setProduct(data);
      }
    }
  }, [isMountedRef, data, isLoading]);

  return (
    <Page
      title="Product | Edit"
    >
      <Breadcrumbs
        title="Edit product"
        items={breadcrumbItems}
      >
        <NextLink
          href="/products"
          passHref
        >
          <Button
            color="primary"
            startIcon={<ArrowLeftIcon fontSize="small" />}
            variant="outlined"
          >
            Cancel
          </Button>
        </NextLink>
      </Breadcrumbs>
      <Container maxWidth={settings.compact ? 'md' : false}>
        <Box mt={3}>
          {!product
            ? (
              <LoadingScreen />
            )
            : (
              <ProductEditForm product={product} />
            )}
        </Box>
      </Container>
    </Page>
  );
};

ProductEdit.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default ProductEdit;
