import React, { useEffect } from 'react';
import type { NextPage } from 'src/../next';
import NextLink from 'next/link';
import {
  Box,
  Button,
  Container,
} from '@mui/material';
import { ContactListTable } from 'src/components/contact';
import PlusIcon from 'src/icons/Plus';
import { getContacts } from 'src/slices/contact';
import Page from 'src/components/Page';
import { useDispatch, useSelector } from 'src/store';
import { useSettings } from 'src/hooks/use-settings';
import Breadcrumbs from 'src/components/Breadcrumbs';
import type { BreadcrumbItem } from 'src/types/common';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Operations',
    routeTo: '/dashboard/overview'
  },
  {
    title: 'Contacts',
    routeTo: '/contacts'
  },
];

const ContactList: NextPage = () => {
  const { settings } = useSettings();
  const { organisation } = useSelector((state) => state.account);
  const { contacts, limit, page, searchText, order, orderBy } = useSelector((state) => state.contact);
  const dispatch = useDispatch();

  useEffect(() => {
    const sortBy = orderBy ? `${order === 'desc' ? '-' : ''}${orderBy}` : '';
    dispatch(getContacts(organisation.id, limit, page, searchText, sortBy));
  }, [organisation, limit, page, searchText, order, orderBy]);

  return (
    <Page
      title="Contact | List"
    >
      <Breadcrumbs
        title="Contacts list"
        items={breadcrumbItems}
      >
        <NextLink
          href="/contacts/new"
          passHref
        >
          <Button
            color="primary"
            startIcon={<PlusIcon fontSize="small" />}
            sx={{ ml: 2 }}
            variant="contained"
          >
            New contact
          </Button>
        </NextLink>
      </Breadcrumbs>
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Box sx={{ mt: 3 }}>
          <ContactListTable contacts={contacts} />
        </Box>
      </Container>
    </Page>
  );
};

ContactList.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default ContactList;
