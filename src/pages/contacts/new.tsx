import React, { useEffect } from 'react';
import type { NextPage } from 'src/../next';
import { useRouter } from 'next/router';
import NextLink from 'next/link';
import {
  Box,
  Button,
  Container,
} from '@mui/material';
import { ContactForm } from 'src/components/contact';
import { useSettings } from 'src/hooks/use-settings';
import ArrowLeftIcon from 'src/icons/ArrowLeft';
import { useDispatch, useSelector } from 'src/store';
import Page from 'src/components/Page';
import { getContact } from 'src/slices/contactDetail';
import Breadcrumbs from 'src/components/Breadcrumbs';
import type { BreadcrumbItem } from 'src/types/common';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Operations',
    routeTo: '/dashboard/overview'
  },
  {
    title: 'Contacts',
    routeTo: '/contacts'
  },
];

const ContactNew: NextPage = () => {
  const { settings } = useSettings();
  const router = useRouter();
  const { contact, pools } = useSelector((state) => state.contactDetail);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getContact());
  }, []);

  return (
    <Page
      title="Contact | New"
    >
      <Breadcrumbs
        title="New contact"
        items={breadcrumbItems}
      >
        <NextLink
          href="/contacts"
          passHref
        >
          <Button
            color="primary"
            startIcon={<ArrowLeftIcon fontSize="small" />}
            variant="outlined"
          >
            Cancel
          </Button>
        </NextLink>
      </Breadcrumbs>
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Box mt={3}>
          <Container maxWidth={settings.compact ? 'md' : false}>
            <ContactForm
              contact={contact}
              pools={pools}
              onAddComplete={() => router.push('/contacts')}
            />
          </Container>
        </Box>
      </Container>
    </Page>
  );
};

ContactNew.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default ContactNew;
