import React, { memo } from 'react';
import type { NextPage } from 'src/../next';
import NextLink from 'next/link';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';

import {
  Box,
  Button,
  Container,
} from '@mui/material';

import FileImporter from 'src/components/file-importer/index';
import Page from 'src/components/Page';
import { useSettings } from 'src/hooks/use-settings';
import { Column } from 'src/types/fileImporter';
import Breadcrumbs from 'src/components/Breadcrumbs';
import type { BreadcrumbItem } from 'src/types/common';
import ArrowLeftIcon from 'src/icons/ArrowLeft';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Operations',
    routeTo: '/dashboard/overview'
  },
  {
    title: 'Contacts',
    routeTo: '/contacts'
  },
];

interface ContactImportProps {}

const EXPECTED_COLUMNS: Column[] = [
  {
    type: 'string',
    matchHeaderName: 'id',
    id: 'id',
    displayName: 'ID',
    description: 'Identifier of a contact',
    required: false,
  },
  {
    type: 'string',
    matchHeaderName: 'first_name',
    id: 'first_name',
    displayName: 'First name',
    description: '',
    required: true,
  },
  {
    type: 'string',
    matchHeaderName: 'last_name',
    id: 'last_name',
    displayName: 'Last name',
    description: '',
    required: true,
  },
  {
    type: 'number',
    matchHeaderName: 'type',
    id: 'type',
    displayName: 'Type',
    description: '',
    required: true,
  },
  {
    type: 'string',
    matchHeaderName: 'company_name',
    id: 'company_name',
    displayName: 'Company name',
    description: '',
    required: false,
  },
  {
    type: 'string',
    matchHeaderName: 'email',
    id: 'email',
    displayName: 'Email',
    description: '',
    required: false,
  },
  {
    type: 'string',
    matchHeaderName: 'mobile',
    id: 'mobile',
    displayName: 'Mobile phone',
    description: '',
    required: false,
  },
  {
    type: 'string',
    matchHeaderName: 'phone',
    id: 'phone',
    displayName: 'Phone',
    description: '',
    required: false,
  },
  {
    type: 'string',
    matchHeaderName: 'address_street_one',
    id: 'address_street_one',
    displayName: 'Address street one',
    description: '',
    required: true,
  },
  {
    type: 'string',
    matchHeaderName: 'address_street_two',
    id: 'address_street_two',
    displayName: 'Address street two',
    description: '',
    required: false,
  },
  {
    type: 'string',
    matchHeaderName: 'address_city',
    id: 'address_city',
    displayName: 'Address city',
    description: '',
    required: false,
  },
  {
    type: 'string',
    matchHeaderName: 'address_state',
    id: 'address_state',
    displayName: 'Address state',
    description: '',
    required: true,
  },
  {
    type: 'string',
    matchHeaderName: 'address_postcode',
    id: 'address_postcode',
    displayName: 'Address postcode',
    description: '',
    required: true,
  },
  {
    type: 'string',
    matchHeaderName: 'notes',
    id: 'notes',
    displayName: 'Notes',
    description: '',
    required: false,
  },
  {
    type: 'number',
    matchHeaderName: 'invoice_pref',
    id: 'invoice_pref',
    displayName: 'Invoice preference',
    description: '',
    required: true,
  },
];

const ContactImport: NextPage<ContactImportProps> = memo(() => {
  const { settings } = useSettings();

  return (
    <Page title="Contact | Import">
      <Breadcrumbs
        title="Import contacts"
        items={breadcrumbItems}
      >
        <NextLink
          href="/contacts"
          passHref
        >
          <Button
            color="primary"
            startIcon={<ArrowLeftIcon fontSize="small" />}
            sx={{ mt: 1 }}
            variant="outlined"
          >
            Cancel
          </Button>
        </NextLink>
      </Breadcrumbs>
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Box py={3}>
          <FileImporter
            title="Drag and drop a file (CSV or XLSX) anywhere, or browse your files."
            expectedColumns={EXPECTED_COLUMNS}
            onConfirm={() => {}}
          />
        </Box>
      </Container>
    </Page>
  );
});

ContactImport.propTypes = {};

ContactImport.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default ContactImport;
