import React, { useState, useEffect } from 'react';
import type { ChangeEvent } from 'react';
import type { NextPage } from 'src/../next';
import { useRouter } from 'next/router';
import NextLink from 'next/link';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';
import {
  Box,
  Button,
  Container,
  Divider,
  Grid,
  Tab,
  Tabs,
} from '@mui/material';
import {
  ContactDataManagement,
  ContactInvoices,
  ContactLogs,
  ContactSummary,
  ContactForm
} from 'src/components/contact';
import {
  NotesList
} from 'src/components/note';
import { useSettings } from 'src/hooks/use-settings';
import Page from 'src/components/Page';
import { useDispatch, useSelector } from 'src/store';
import { getContact, reset } from 'src/slices/contactDetail';
import ArrowLeftIcon from 'src/icons/ArrowLeft';
import Breadcrumbs from 'src/components/Breadcrumbs';
import type { BreadcrumbItem } from 'src/types/common';
import LoadingScreen from 'src/components/LoadingScreen';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Operations',
    routeTo: '/dashboard/overview'
  },
  {
    title: 'Contacts',
    routeTo: '/contacts'
  },
];

const DETAILS_TAB = 'details';
const INVOICES_TAB = 'invoices';
const LOGS_TAB = 'logs';

const tabs = [
  { label: 'Details', value: DETAILS_TAB },
  { label: 'Invoices', value: INVOICES_TAB },
  { label: 'Logs', value: LOGS_TAB },
];

const ContactDetails: NextPage = () => {
  const router = useRouter();
  const { contactId, tab } = router.query;
  const { settings } = useSettings();
  // @ts-ignore
  const [currentTab, setCurrentTab] = useState<string>(tab);
  const { organisation } = useSelector((state) => state.account);
  const { contact, pools, notes } = useSelector((state) => state.contactDetail);
  const dispatch = useDispatch();

  useEffect(() => () => dispatch(reset()), []);

  useEffect(() => {
    // @ts-ignore
    dispatch(getContact(organisation.id, parseInt(contactId, 10)));
  }, [organisation, getContact, contactId]);

  const handleTabsChange = (event: ChangeEvent<{}>, value: string): void => {
    setCurrentTab(value);
  };

  if (!contact) {
    return <LoadingScreen />;
  }

  return (
    <Page
      title="Contact | Details"
    >
      <Breadcrumbs
        title="Contact details"
        items={breadcrumbItems}
      >
        <NextLink
          href="/contacts"
          passHref
        >
          <Button
            color="primary"
            startIcon={<ArrowLeftIcon fontSize="small" />}
            variant="outlined"
          >
            Cancel
          </Button>
        </NextLink>
      </Breadcrumbs>
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Box sx={{ mt: 3 }}>
          <Tabs
            indicatorColor="primary"
            onChange={handleTabsChange}
            scrollButtons="auto"
            textColor="primary"
            value={currentTab}
            variant="scrollable"
          >
            {tabs.map((tab) => (
              <Tab
                key={tab.value}
                label={tab.label}
                value={tab.value}
                onClick={() => router.push(`/contacts/${contactId}/${tab.value}`)}
              />
            ))}
          </Tabs>
        </Box>
        <Divider />
        <Box sx={{ mt: 3 }}>
          {currentTab === 'details' && (
            <Grid
              container
              spacing={3}
            >
              <Grid
                item
                md={8}
                xs={12}
              >
                <ContactForm
                  contact={contact}
                  pools={pools}
                />
              </Grid>
              <Grid
                item
                md={4}
                xs={12}
              >
                <ContactSummary />
                <Box
                  mt={3}
                >
                  <NotesList notes={notes} />
                </Box>
                <Box
                  mt={3}
                >
                  <ContactDataManagement />
                </Box>
              </Grid>
            </Grid>
          )}
          {currentTab === 'invoices' && <ContactInvoices />}
          {currentTab === 'logs' && <ContactLogs />}
        </Box>
      </Container>
    </Page>
  );
};

ContactDetails.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default ContactDetails;
