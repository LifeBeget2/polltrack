import React, { useEffect } from 'react';
import type { NextPage } from 'src/../next';
import { useRouter } from 'next/router';
import NextLink from 'next/link';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';
import {
  Box,
  Button,
  Container,
} from '@mui/material';
import { ContactForm } from 'src/components/contact';
import { useSettings } from 'src/hooks/use-settings';
import { useDispatch, useSelector } from 'src/store';
import Page from 'src/components/Page';
import { getContact, reset } from 'src/slices/contactDetail';
import Breadcrumbs from 'src/components/Breadcrumbs';
import type { BreadcrumbItem } from 'src/types/common';
import ArrowLeftIcon from 'src/icons/ArrowLeft';
import LoadingScreen from 'src/components/LoadingScreen';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Operations',
    routeTo: '/dashboard/overview'
  },
  {
    title: 'Contacts',
    routeTo: '/contacts'
  },
];

const ContactEdit: NextPage = () => {
  const router = useRouter();
  const { contactId } = router.query;
  const { settings } = useSettings();
  const { organisation } = useSelector((state) => state.account);
  const { contact, pools } = useSelector((state) => state.contactDetail);
  const dispatch = useDispatch();

  useEffect(() => () => dispatch(reset()), []);

  useEffect(() => {
    // @ts-ignore
    dispatch(getContact(organisation.id, parseInt(contactId, 10)));
  }, [organisation]);

  if (!contact) {
    return <LoadingScreen />;
  }

  return (
    <Page
      title="Contact | Edit"
    >
      <Breadcrumbs
        title="Edit contact"
        items={breadcrumbItems}
      >
        <NextLink
          href="/contacts"
          passHref
        >
          <Button
            color="primary"
            startIcon={<ArrowLeftIcon fontSize="small" />}
            variant="outlined"
          >
            Cancel
          </Button>
        </NextLink>
      </Breadcrumbs>
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Box mt={3}>
          <ContactForm
            contact={contact}
            pools={pools}
            onEditComplete={() => router.push('/contacts')}
          />
        </Box>
      </Container>
    </Page>
  );
};

ContactEdit.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default ContactEdit;
