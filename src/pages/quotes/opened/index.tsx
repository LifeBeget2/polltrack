import type { NextPage } from 'src/../next';
import {
  Box,
  Button,
  Container,
} from '@mui/material';
import React, { useEffect } from 'react';
import PlusIcon from 'src/icons/Plus';
import Page from 'src/components/Page';
import { useDispatch, useSelector } from 'src/store';
import { useSettings } from 'src/hooks/use-settings';
import { getOpenedQuotes } from 'src/slices/openedQuote';
import QuoteOpenedListTable from 'src/components/quote/QuoteOpenedListTable';
import Breadcrumbs from 'src/components/Breadcrumbs';
import type { BreadcrumbItem } from 'src/types/common';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Operations',
    routeTo: '/dashboard/overview'
  },
  {
    title: 'Quotes',
    routeTo: '/quotes/opened'
  },
];

const QuoteOpenedList: NextPage = () => {
  const { settings } = useSettings();
  const { organisation } = useSelector((state) => state.account);
  const { quotes, limit, page, searchText, order, orderBy } = useSelector((state) => state.openedQuote);
  const dispatch = useDispatch();

  useEffect(() => {
    const sortBy = orderBy ? `${order === 'desc' ? '-' : ''}${orderBy}` : '';
    dispatch(getOpenedQuotes(organisation.id, limit, page, searchText, sortBy));
  }, [organisation, limit, page, searchText, order, orderBy]);

  return (
    <Page
      title="Quote | Opened list"
    >
      <Breadcrumbs
        title="Opened"
        items={breadcrumbItems}
      >
        <Button
          color="primary"
          startIcon={<PlusIcon fontSize="small" />}
          variant="contained"
        >
          Add quote
        </Button>
      </Breadcrumbs>
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Box sx={{ mt: 3 }}>
          <QuoteOpenedListTable quotes={quotes} />
        </Box>
      </Container>
    </Page>
  );
};

QuoteOpenedList.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default QuoteOpenedList;
