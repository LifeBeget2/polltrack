import type { NextPage } from 'src/../next';
import {
  Box,
  Button,
  Container,
} from '@mui/material';
import React, { useEffect } from 'react';
import PlusIcon from 'src/icons/Plus';
import Page from 'src/components/Page';
import { useDispatch, useSelector } from 'src/store';
import { useSettings } from 'src/hooks/use-settings';
import QuoteArchivedListTable from 'src/components/quote/QuoteArchivedListTable';
import { getArchivedQuotes } from 'src/slices/archivedQuote';
import Breadcrumbs from 'src/components/Breadcrumbs';
import type { BreadcrumbItem } from 'src/types/common';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Operations',
    routeTo: '/dashboard/overview'
  },
  {
    title: 'Quotes',
    routeTo: '/quotes/opened'
  },
];

const QuoteArchivedList: NextPage = () => {
  const { settings } = useSettings();
  const { organisation } = useSelector((state) => state.account);
  const { quotes, limit, page, searchText, order, orderBy } = useSelector((state) => state.archivedQuote);
  const dispatch = useDispatch();

  useEffect(() => {
    const sortBy = orderBy ? `${order === 'desc' ? '-' : ''}${orderBy}` : '';
    dispatch(getArchivedQuotes(organisation.id, limit, page, searchText, sortBy));
  }, [organisation, limit, page, searchText, order, orderBy]);

  return (
    <Page
      title="Quote | Archived list"
    >
      <Breadcrumbs
        title="Archived"
        items={breadcrumbItems}
      >
        <Button
          color="primary"
          startIcon={<PlusIcon fontSize="small" />}
          variant="contained"
        >
          Add quote
        </Button>
      </Breadcrumbs>
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Box sx={{ mt: 3 }}>
          <QuoteArchivedListTable quotes={quotes} />
        </Box>
      </Container>
    </Page>
  );
};

QuoteArchivedList.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default QuoteArchivedList;
