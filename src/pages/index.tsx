import { useEffect } from 'react';
import type { NextPage } from 'next';
import Head from 'next/head';
import { MainLayout } from '../layouts/main-layout';
import { gtm } from '../lib/gtm';
import { AuthGuard } from '../components/authentication/auth-guard';

const Home: NextPage = () => {
  useEffect(() => {
    gtm.push({ event: 'page_view' });
  }, []);

  return (
    <>
      <Head>
        <title>
          Pooltrackr
        </title>
      </Head>
      <main />
    </>
  );
};

Home.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default Home;
