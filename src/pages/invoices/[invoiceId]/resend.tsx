import React, { memo, useEffect } from 'react';
import type { NextPage } from 'src/../next';
import NextLink from 'next/link';
import {
  Container,
  Grid,
  Box,
  Button,
} from '@mui/material';
import ArrowLeftIcon from 'src/icons/ArrowLeft';

import { useSettings } from 'src/hooks/use-settings';
import Page from 'src/components/Page';
import { InvoiceResend as InvoiceResendCard } from 'src/components/invoice';
import InvoiceResendSummary from 'src/components/invoice/invoiceResend/InvoiceResendSummary';
import { useDispatch, useSelector } from 'src/store';
import Breadcrumbs from 'src/components/Breadcrumbs';
import type { BreadcrumbItem } from 'src/types/common';
import LoadingScreen from 'src/components/LoadingScreen';
import { initialize, reset } from 'src/slices/resendInvoice';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';
import { useRouter } from 'next/router';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Operations',
    routeTo: '/dashboard/overview'
  },
  {
    title: 'Invoices',
    routeTo: '/invoices'
  },
];

const InvoiceResend: NextPage = memo(() => {
  const router = useRouter();
  const { invoiceId } = router.query;
  const { settings } = useSettings();
  const dispatch = useDispatch();
  const { isLoading } = useSelector((state) => state.resendInvoice);
  const { id: organisationId } = useSelector(
    (state) => state.account.organisation
  );

  useEffect(() => {
    // @ts-ignore
    dispatch(initialize(organisationId, invoiceId));
    return () => {
      dispatch(reset());
    };
  }, [dispatch, invoiceId, organisationId]);

  if (isLoading) {
    return <LoadingScreen />;
  }

  return (
    <Page title="Invoice | Resend">
      <Breadcrumbs
        title="Resend"
        items={breadcrumbItems}
      >
        <NextLink
          href="/invoices"
          passHref
        >
          <Button
            color="primary"
            startIcon={<ArrowLeftIcon fontSize="small" />}
            variant="outlined"
          >
            Cancel
          </Button>
        </NextLink>
      </Breadcrumbs>
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Box sx={{ mt: 3 }}>
          <Grid
            container
            spacing={3}
          >
            <Grid
              item
              md={7}
              xs={12}
            >
              <InvoiceResendCard />
            </Grid>
            <Grid
              item
              md={5}
              sx={{
                display: { xs: 'none', md: 'block' }
              }}
            >
              <InvoiceResendSummary />
            </Grid>
          </Grid>
        </Box>
      </Container>
    </Page>
  );
});

InvoiceResend.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default InvoiceResend;
