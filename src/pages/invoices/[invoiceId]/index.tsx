import React, { useState, useEffect, useCallback } from 'react';
import type { NextPage } from 'src/../next';
import NextLink from 'next/link';
import { PDFViewer } from '@react-pdf/renderer';
import {
  Box,
  Button,
  Container,
  Dialog,
} from '@mui/material';
import axios from 'axios';
import { InvoicePDF, InvoicePreview } from 'src/components/invoice';
import useIsMountedRef from 'src/hooks/useIsMountedRef';
import { useSettings } from 'src/hooks/use-settings';
import ArrowLeftIcon from 'src/icons/ArrowLeft';
import type { Invoice } from 'src/types/contactInvoice';
import Page from 'src/components/Page';
import { useSelector } from 'src/store';
import { apiConfig } from 'src/config';
import Breadcrumbs from 'src/components/Breadcrumbs';
import type { BreadcrumbItem } from 'src/types/common';
import LoadingScreen from 'src/components/LoadingScreen';
import { useRouter } from 'next/router';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Operations',
    routeTo: '/dashboard/overview'
  },
  {
    title: 'Invoices',
    routeTo: '/invoices'
  },
];

const InvoiceDetails: NextPage = () => {
  const router = useRouter();
  const { invoiceId } = router.query;
  const { settings } = useSettings();
  const isMountedRef = useIsMountedRef();
  const [invoice, setInvoice] = useState<Invoice | null>(null);
  const [viewPDF, setViewPDF] = useState<boolean>(false);
  const { organisation } = useSelector((state) => state.account);

  const getInvoice = useCallback(async () => {
    try {
      // TODO: move to the slice
      const response = await axios.get(`${apiConfig.apiV1Url}/organisations/${organisation.id}/contactinvoice/${invoiceId}`);

      if (isMountedRef.current) {
        setInvoice(response.data);
      }
    } catch (err) {
      console.error(err);
    }
  }, [isMountedRef]);

  useEffect(() => {
    getInvoice();
  }, [getInvoice]);

  if (!invoice) {
    return <LoadingScreen />;
  }

  return (
    <Page
      title="Invoice | Details"
    >
      <Breadcrumbs
        title="Pool details"
        items={breadcrumbItems}
      >
        <Button
          color="primary"
          href={invoice.invoice_pdf_sheet}
          target="_blank"
          variant="contained"
        >
          Preview PDF
        </Button>
        <NextLink
          href="/invoices"
          passHref
        >
          <Button
            color="primary"
            startIcon={<ArrowLeftIcon fontSize="small" />}
            sx={{ ml: 2 }}
            variant="outlined"
          >
            Cancel
          </Button>
        </NextLink>
      </Breadcrumbs>
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Box sx={{ mt: 3 }}>
          <InvoicePreview invoice={invoice} />
        </Box>
      </Container>
      <Dialog
        fullScreen
        open={viewPDF}
      >
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'column',
            height: '100%'
          }}
        >
          <Box
            sx={{
              backgroundColor: 'background.default',
              p: 2
            }}
          >
            <Button
              color="primary"
              startIcon={<ArrowLeftIcon fontSize="small" />}
              onClick={(): void => setViewPDF(false)}
              variant="contained"
            >
              Back
            </Button>
          </Box>
          <Box sx={{ flexGrow: 1 }}>
            <PDFViewer
              height="100%"
              style={{ border: 'none' }}
              width="100%"
            >
              <InvoicePDF invoice={invoice} />
            </PDFViewer>
          </Box>
        </Box>
      </Dialog>
    </Page>
  );
};

InvoiceDetails.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default InvoiceDetails;
