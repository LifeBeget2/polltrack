import React, { useEffect } from 'react';
import type { NextPage } from 'src/../next';
import {
  Box,
  Container,
} from '@mui/material';
import { InvoiceListTable } from 'src/components/invoice';
import { getContactInvoices } from 'src/slices/contactInvoice';
import Page from 'src/components/Page';
import { useDispatch, useSelector } from 'src/store';
import { useSettings } from 'src/hooks/use-settings';
import Breadcrumbs from 'src/components/Breadcrumbs';
import type { BreadcrumbItem } from 'src/types/common';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Operations',
    routeTo: '/dashboard/overview'
  },
];

const InvoicesList: NextPage = () => {
  const { settings } = useSettings();
  const { organisation } = useSelector((state) => state.account);
  const { selectedRange, invoices, limit, page, searchText, statusFilter, order, orderBy } = useSelector((state) => state.contactInvoice);
  const dispatch = useDispatch();

  useEffect(() => {
    const sortBy = orderBy ? `${order === 'desc' ? '-' : ''}${orderBy}` : '';
    dispatch(getContactInvoices(organisation.id, selectedRange, limit, page, searchText, statusFilter, sortBy));
  }, [organisation, selectedRange, limit, page, searchText, statusFilter, order, orderBy]);

  return (
    <Page
      title="Invoice | List"
    >
      <Breadcrumbs
        title="Invoices list"
        items={breadcrumbItems}
      />
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Box sx={{ mt: 3 }}>
          <InvoiceListTable invoices={invoices} />
        </Box>
      </Container>
    </Page>
  );
};

InvoicesList.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default InvoicesList;
