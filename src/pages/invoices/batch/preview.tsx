import React, { memo, useEffect } from 'react';
import type { NextPage } from 'src/../next';
import { useLocation } from 'react-router';
import NextLink from 'next/link';
import {
  Container,
  Grid,
  Box,
  Button,
} from '@mui/material';

import Page from 'src/components/Page';
import BatchPreviewMessageToCustomer from 'src/components/invoice/batchPreview/BatchPreviewMessageToCustomer';
import BatchPreviewSummary from 'src/components/invoice/batchPreview/BatchPreviewSummary';
import BatchPreviewActions from 'src/components/invoice/batchPreview/BatchPreviewActions';
import BatchPreviewIssueTo from 'src/components/invoice/batchPreview/BatchPreviewIssueTo';
import BatchPreviewInvoices from 'src/components/invoice/batchPreview/BatchPreviewInvoices';
import ArrowLeftIcon from 'src/icons/ArrowLeft';
import { useSettings } from 'src/hooks/use-settings';
import { useDispatch, useSelector } from 'src/store';
import { initialize, reset } from 'src/slices/batchPreview';
import type { BreadcrumbItem } from 'src/types/common';
import Breadcrumbs from 'src/components/Breadcrumbs';
import LoadingScreen from 'src/components/LoadingScreen';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';
import { useRouter } from 'next/router';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Operations',
    routeTo: '/dashboard/overview'
  },
  {
    title: 'Batch queue',
    routeTo: '/invoices/batch/queue'
  },
];

const BatchPreview: NextPage = memo(() => {
  const dispatch = useDispatch();
  const { settings } = useSettings();
  const router = useRouter();
  const { contact_id: contactId, invoices_ids: invoicesIds } = router.query;

  const organisationId = useSelector((state) => state.account.organisation.id);
  const { batchSettings } = useSelector((state) => state.account);
  const { isLoading, markedAsInvoiced } = useSelector((state) => state.batchPreview);

  useEffect(() => {
    if (markedAsInvoiced) {
      router.push('/invoices/batch/queue');
    }
  }, [markedAsInvoiced]);

  useEffect((): VoidFunction => {
    // @ts-ignore
    const contactInvoiceIds = (invoicesIds || '').split(',').map((id) => Number(id));

    if (
      Number.isNaN(contactId)
      || contactInvoiceIds.some((id) => Number.isNaN(id))
    ) {
      router.push('/invoices/batch/queue');
      return null;
    }

    dispatch(
      initialize(organisationId, Number(contactId), contactInvoiceIds, batchSettings)
    );
    return () => dispatch(reset());
  }, [organisationId, contactId, invoicesIds]);

  if (isLoading) {
    return <LoadingScreen />;
  }

  return (
    <Page title="Invoice | Batch preview">
      <Breadcrumbs
        title="Batch invoice preview"
        items={breadcrumbItems}
      >
        <NextLink
          href="/invoices/batch/queue"
          passHref
        >
          <Button
            color="primary"
            startIcon={<ArrowLeftIcon fontSize="small" />}
            variant="outlined"
          >
            Cancel
          </Button>
        </NextLink>
      </Breadcrumbs>
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Box mt={3}>
          <Grid
            container
            spacing={3}
          >
            <Grid
              item
              md={8}
              xs={12}
            >
              <Box>
                <BatchPreviewIssueTo />
              </Box>
              <Box mt={3}>
                <BatchPreviewMessageToCustomer />
              </Box>
              <Box mt={3}>
                <BatchPreviewInvoices />
              </Box>
              <Box mt={3}>
                <BatchPreviewActions />
              </Box>
            </Grid>
            <Grid
              item
              md={4}
              xs={12}
              sx={{
                display: {
                  xs: 'none',
                  md: 'block'
                }
              }}
            >
              <BatchPreviewSummary />
            </Grid>
          </Grid>
        </Box>
      </Container>
    </Page>
  );
});

BatchPreview.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default BatchPreview;
