import React, { memo, useCallback, useEffect, useMemo, useState } from 'react';
import { useDispatch } from 'react-redux';
import type { NextPage } from 'src/../next';
import {
  Container,
  Box,
  Card,
  CardContent,
  TextField,
  InputAdornment,
  IconButton,
} from '@mui/material';

import Page from 'src/components/Page';
import { useSettings } from 'src/hooks/use-settings';
import { getBatchQueue, setSearchQuery } from 'src/slices/batchQueue';
import { useSelector } from 'src/store';
import BatchQueueItem from 'src/components/invoice/batchQueue/BatchQueueAccordion';
import SearchIcon from 'src/icons/Search';
import RefreshIcon from 'src/icons/Refresh';
import { ClipLoader } from 'react-spinners';
import { useTheme } from '@mui/material/styles';
import { BatchQueue as BatchQueueType } from 'src/types/batchQueue';
import type { BreadcrumbItem } from 'src/types/common';
import Breadcrumbs from 'src/components/Breadcrumbs';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Operations',
    routeTo: '/dashboard/overview'
  },
];

const BatchQueue: NextPage = memo(() => {
  const { settings } = useSettings();
  const dispatch = useDispatch();
  const theme = useTheme();
  const [expandedContactQueueId, setExpandedContactQueueId] = useState<number | null>(null);

  const { id: organisationId } = useSelector(
    (state) => state.account.organisation
  );
  const { batchQueue, searchQuery, isLoading } = useSelector((state) => state.batchQueue);

  const batchQueueFiltered = useMemo<BatchQueueType[]>(() => {
    const query = searchQuery.trim().toLowerCase();

    if (query.length === 0) {
      return batchQueue;
    }

    return batchQueue.filter((q) => q.first_name.toLowerCase().includes(query) || q.last_name.toLowerCase().includes(query));
  }, [batchQueue, searchQuery]);

  const handleChangeSearchQuery = useCallback((event: React.ChangeEvent<HTMLInputElement>) => {
    dispatch(setSearchQuery({ searchQuery: event.target.value }));
  }, []);

  const handleRefresh = useCallback(() => {
    dispatch(getBatchQueue(organisationId));
  }, [organisationId]);

  useEffect(() => {
    dispatch(getBatchQueue(organisationId));
  }, [organisationId]);

  return (
    <Page title="Invoice | Batch queue">
      <Breadcrumbs
        title="Batch queue"
        items={breadcrumbItems}
      />
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Box mt={3}>
          <Card>
            <CardContent>
              <Box sx={{ display: 'flex' }}>
                <TextField
                  InputProps={{
                    startAdornment: (
                      <InputAdornment position="start">
                        {isLoading
                          ? (
                            <ClipLoader
                              size={20}
                              color={theme.palette.primary.main}
                            />
                          )
                          : <SearchIcon />}
                      </InputAdornment>
                    ),
                    endAdornment: (
                      <InputAdornment position="end">
                        <IconButton
                          onClick={handleRefresh}
                        >
                          <RefreshIcon />
                        </IconButton>
                      </InputAdornment>
                    )
                  }}
                  onChange={handleChangeSearchQuery}
                  placeholder="Search..."
                  value={searchQuery}
                />
              </Box>
            </CardContent>
            <Box mt={1}>
              {batchQueueFiltered.map((q) => (
                <BatchQueueItem
                  key={q.contact_id}
                  queue={q}
                  expanded={q.contact_id === expandedContactQueueId}
                  onExpand={() => setExpandedContactQueueId(q.contact_id)}
                  onCollapse={() => setExpandedContactQueueId(null)}
                />
              ))}
            </Box>
          </Card>
        </Box>
      </Container>
    </Page>
  );
});

BatchQueue.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default BatchQueue;
