import React, { memo, useEffect } from 'react';
import type { NextPage } from 'src/../next';
import NextLink from 'next/link';

import {
  Box,
  Button,
  Container,
  Grid,
} from '@mui/material';

import { getBatchInvoice } from 'src/slices/batchInvoiceDetails';

import BatchInvoiceContent from 'src/components/invoice/batchInvoiceDetails/BatchInvoiceContent';
import BatchInvoiceSummary from 'src/components/invoice/batchInvoiceDetails/BatchInvoiceSummary';
import Page from 'src/components/Page';
import { useSettings } from 'src/hooks/use-settings';
import { useDispatch, useSelector } from 'src/store';

import type { BreadcrumbItem } from 'src/types/common';
import LoadingScreen from 'src/components/LoadingScreen';
import Breadcrumbs from 'src/components/Breadcrumbs';
import ArrowLeftIcon from 'src/icons/ArrowLeft';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';
import { useRouter } from 'next/router';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Operations',
    routeTo: '/dashboard/overview'
  },
  {
    title: 'Batch invoices',
    routeTo: '/invoices/batch'
  },
];

const BatchInvoiceDetails: NextPage = memo(() => {
  const router = useRouter();
  const { invoiceId } = router.query;
  const { settings } = useSettings();
  const dispatch = useDispatch();
  const { organisation } = useSelector((state) => state.account);
  const { isLoading } = useSelector((state) => state.batchInvoiceDetails);

  useEffect(() => {
    // @ts-ignore
    dispatch(getBatchInvoice(organisation.id, invoiceId));
  }, [invoiceId]);

  if (isLoading) {
    return <LoadingScreen />;
  }

  return (
    <Page title="Invoice | Batch details">
      <Breadcrumbs
        title="Batch invoice details"
        items={breadcrumbItems}
      >
        <NextLink
          href="/invoices/batch"
          passHref
        >
          <Button
            color="primary"
            startIcon={<ArrowLeftIcon fontSize="small" />}
            variant="outlined"
          >
            Cancel
          </Button>
        </NextLink>
      </Breadcrumbs>
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Box sx={{ mt: 3 }}>
          <Grid
            container
            spacing={3}
          >
            <Grid
              item
              xs={12}
              md={8}
            >
              <BatchInvoiceContent />
            </Grid>
            <Grid
              item
              xs={12}
              md={4}
              display={{ md: 'block', xs: 'none' }}
            >
              <BatchInvoiceSummary />
            </Grid>
          </Grid>
        </Box>
      </Container>
    </Page>
  );
});

BatchInvoiceDetails.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default BatchInvoiceDetails;
