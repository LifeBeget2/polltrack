import React, { memo, useEffect } from 'react';
import type { NextPage } from 'src/../next';
import NextLink from 'next/link';
import {
  Container,
  Grid,
  Button,
  Box,
} from '@mui/material';
import ArrowLeftIcon from 'src/icons/ArrowLeft';

import { useSettings } from 'src/hooks/use-settings';
import Page from 'src/components/Page';

import BatchInvoiceResendContent from 'src/components/invoice/batchInvoice/BatchInvoiceResendContent';
import BatchInvoiceResendSummary from 'src/components/invoice/batchInvoice/BatchInvoiceResendSummary';
import { useDispatch, useSelector } from 'src/store';
import LoadingScreen from 'src/components/LoadingScreen';
import { initialize, reset } from 'src/slices/resendBatchInvoice';
import Breadcrumbs from 'src/components/Breadcrumbs';
import type { BreadcrumbItem } from 'src/types/common';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';
import { useRouter } from 'next/router';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Operations',
    routeTo: '/dashboard/overview'
  },
  {
    title: 'Batch invoices',
    routeTo: '/invoices/batch'
  },
];

const BatchInvoiceResend: NextPage = memo(() => {
  const { settings } = useSettings();
  const dispatch = useDispatch();
  const router = useRouter();
  const { batchInvoiceId } = router.query;
  const { id: organisationId } = useSelector(
    (state) => state.account.organisation
  );
  const { isLoading } = useSelector((state) => state.resendBatchInvoice);

  useEffect(() => {
    // @ts-ignore
    dispatch(initialize(organisationId, batchInvoiceId));
    return () => {
      dispatch(reset());
    };
  }, [dispatch, batchInvoiceId, organisationId]);

  if (isLoading) {
    return <LoadingScreen />;
  }

  return (
    <Page title="Invoice | Batch resend">
      <Breadcrumbs
        title="Resend"
        items={breadcrumbItems}
      >
        <NextLink
          href="/invoices/batch"
          passHref
        >
          <Button
            color="primary"
            startIcon={<ArrowLeftIcon fontSize="small" />}
            variant="outlined"
          >
            Cancel
          </Button>
        </NextLink>
      </Breadcrumbs>
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Box sx={{ mt: 3 }}>
          <Grid
            container
            spacing={3}
          >
            <Grid
              item
              md={8}
              xs={12}
            >
              <BatchInvoiceResendContent />
            </Grid>
            <Grid
              item
              md={4}
              sx={{
                display: { xs: 'none', md: 'block' }
              }}
            >
              <BatchInvoiceResendSummary />
            </Grid>
          </Grid>
        </Box>
      </Container>
    </Page>
  );
});

BatchInvoiceResend.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default BatchInvoiceResend;
