import React, { useEffect } from 'react';
import type { NextPage } from 'src/../next';
import {
  Box,
  Container,
} from '@mui/material';
import { BatchInvoiceListTable } from 'src/components/invoice';
import { getBatchInvoices } from 'src/slices/batchInvoice';
import Page from 'src/components/Page';
import { useDispatch, useSelector } from 'src/store';
import { useSettings } from 'src/hooks/use-settings';
import Breadcrumbs from 'src/components/Breadcrumbs';
import type { BreadcrumbItem } from 'src/types/common';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Operations',
    routeTo: '/dashboard/overview'
  },
];

const BatchInvoiceList: NextPage = () => {
  const { settings } = useSettings();
  const { organisation } = useSelector((state) => state.account);
  const { selectedRange, invoices, limit, page, searchText, statusFilter, order, orderBy } = useSelector((state) => state.batchInvoice);
  const dispatch = useDispatch();

  useEffect(() => {
    const sortBy = orderBy ? `${order === 'desc' ? '-' : ''}${orderBy}` : '';
    dispatch(getBatchInvoices(organisation.id, selectedRange, limit, page, searchText, statusFilter, sortBy));
  }, [organisation, selectedRange, limit, page, searchText, statusFilter, order, orderBy]);

  return (
    <Page
      title="Invoice | Batch list"
    >
      <Breadcrumbs
        title="Batch invoices list"
        items={breadcrumbItems}
      />
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Box sx={{ mt: 3 }}>
          <BatchInvoiceListTable invoices={invoices} />
        </Box>
      </Container>
    </Page>
  );
};

BatchInvoiceList.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default BatchInvoiceList;
