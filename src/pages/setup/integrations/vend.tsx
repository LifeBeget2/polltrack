import React, { useEffect } from 'react';
import type { NextPage } from 'src/../next';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';

import {
  Box,
  Container,
} from '@mui/material';

import Page from 'src/components/Page';
import { IntegrationStepper } from 'src/components/setup/integrations/vend-integration';
import { useSettings } from 'src/hooks/use-settings';
import {
  connectionResetHandled,
  initialize,
  reset as resetVendIntegration,
} from 'src/slices/vendIntegration';
import { reset as resetVendIntegrationSyncContacts } from 'src/slices/vendIntegrationSyncContacts';
import { reset as resetVendIntegrationSyncProducts } from 'src/slices/vendIntegrationSyncProducts';

import { useDispatch, useSelector } from 'src/store';
import LoadingScreen from 'src/components/LoadingScreen';
import Breadcrumbs from 'src/components/Breadcrumbs';
import type { BreadcrumbItem } from 'src/types/common';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Setup',
    routeTo: '/setup/organisation'
  },
];

const VendIntegration: NextPage = () => {
  const { settings } = useSettings();
  const dispatch = useDispatch();
  const { id: organisationId } = useSelector(
    (state) => state.account.organisation
  );
  const { initializing, connectionResetSuccess } = useSelector(
    (state) => state.vendIntegration
  );

  useEffect(() => {
    dispatch(initialize(organisationId));
  }, [organisationId]);

  useEffect(() => {
    if (connectionResetSuccess) {
      dispatch(connectionResetHandled());

      dispatch(resetVendIntegration());
      dispatch(resetVendIntegrationSyncContacts());
      dispatch(resetVendIntegrationSyncProducts());
      dispatch(initialize(organisationId));
    }
  }, [connectionResetSuccess]);

  if (initializing) {
    return <LoadingScreen />;
  }

  return (
    <Page title="Setup | Vend integration">
      <Breadcrumbs
        title="Vend integration"
        items={breadcrumbItems}
      />
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Box
          sx={{
            mt: 3
          }}
        >
          <IntegrationStepper />
        </Box>
      </Container>
    </Page>
  );
};

VendIntegration.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default VendIntegration;
