import React from 'react';
import type { NextPage } from 'src/../next';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';
import {
  Box,
  Container,
} from '@mui/material';
import { useSettings } from 'src/hooks/use-settings';
import Page from 'src/components/Page';
import { StripeIntegration as Stripe } from 'src/components/integration';
import Breadcrumbs from 'src/components/Breadcrumbs';
import type { BreadcrumbItem } from 'src/types/common';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Setup',
    routeTo: '/setup/organisation'
  },
];

const StripeIntegration: NextPage = () => {
  const { settings } = useSettings();

  return (
    <Page
      title="Setup | Stripe integration"
    >
      <Breadcrumbs
        title="Stripe integration"
        items={breadcrumbItems}
      />
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Box
          sx={{
            mt: 3
          }}
        >
          <Stripe />
        </Box>
      </Container>
    </Page>
  );
};

StripeIntegration.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default StripeIntegration;
