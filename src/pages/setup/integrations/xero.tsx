import React from 'react';
import type { NextPage } from 'src/../next';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';
import {
  Container,
} from '@mui/material';
import { useSettings } from 'src/hooks/use-settings';
import Page from 'src/components/Page';
import Breadcrumbs from 'src/components/Breadcrumbs';
import type { BreadcrumbItem } from 'src/types/common';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Setup',
    routeTo: '/setup/organisation'
  },
];

const XeroIntegration: NextPage = () => {
  const { settings } = useSettings();

  return (
    <Page
      title="Setup | Xero integration"
    >
      <Breadcrumbs
        title="Xero integration"
        items={breadcrumbItems}
      />
      <Container maxWidth={settings.compact ? 'xl' : false} />
    </Page>
  );
};

XeroIntegration.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default XeroIntegration;
