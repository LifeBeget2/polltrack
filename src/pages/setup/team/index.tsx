import React, { useEffect } from 'react';
import type { NextPage } from 'src/../next';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';
import NextLink from 'next/link';
import {
  Box,
  Button,
  Container,
} from '@mui/material';
import { useSettings } from 'src/hooks/use-settings';
import PlusIcon from 'src/icons/Plus';
import { getUsers } from 'src/slices/user';
import { useDispatch, useSelector } from 'src/store';
import Page from 'src/components/Page';
import { UserListTable } from 'src/components/account/team';
import Breadcrumbs from 'src/components/Breadcrumbs';
import type { BreadcrumbItem } from 'src/types/common';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Setup',
    routeTo: '/setup/organisation'
  },
];

const UserList: NextPage = () => {
  const { settings } = useSettings();
  const { organisation } = useSelector((state) => state.account);
  const { users } = useSelector((state) => state.user);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getUsers(organisation.id));
  }, [organisation]);

  return (
    <Page
      title="Account | Team list"
    >
      <Breadcrumbs
        title="Team list"
        items={breadcrumbItems}
      >
        <NextLink
          href="/team/new"
          passHref
        >
          <Button
            color="primary"
            startIcon={<PlusIcon fontSize="small" />}
            variant="contained"
          >
            New user
          </Button>
        </NextLink>
      </Breadcrumbs>
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Box sx={{ mt: 3 }}>
          {users && <UserListTable users={users} />}
        </Box>
      </Container>
    </Page>
  );
};

UserList.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default UserList;
