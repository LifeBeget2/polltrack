import React, { useEffect } from 'react';
import type { NextPage } from 'src/../next';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';
import {
  Box,
  Container,
} from '@mui/material';
import { getChemicalTests } from 'src/slices/chemicalTest';
import Page from 'src/components/Page';
import { useDispatch, useSelector } from 'src/store';
import { useSettings } from 'src/hooks/use-settings';
import { ChemicalTestListTable } from 'src/components/setup/water-testing';
import Breadcrumbs from 'src/components/Breadcrumbs';
import type { BreadcrumbItem } from 'src/types/common';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Setup',
    routeTo: '/setup/organisation'
  },
  {
    title: 'Water testing',
    routeTo: '/setup/water-testing/chemical-tests'
  },
];

const ChemicalTestList: NextPage = () => {
  const { settings } = useSettings();
  const { organisation } = useSelector((state) => state.account);
  const { chemicalTests, searchText, order, orderBy } = useSelector((state) => state.chemicalTest);
  const dispatch = useDispatch();

  useEffect(() => {
    const sortBy = orderBy ? `${order === 'desc' ? '-' : ''}${orderBy}` : '';
    dispatch(getChemicalTests(organisation.id, searchText, sortBy));
  }, [dispatch, organisation, searchText, order, orderBy]);

  return (
    <Page
      title="Setup | Chemical tests list"
    >
      <Breadcrumbs
        title="Chemical tests list"
        items={breadcrumbItems}
      />
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Box sx={{ mt: 3 }}>
          <ChemicalTestListTable chemicalTests={chemicalTests} />
        </Box>
      </Container>
    </Page>
  );
};

ChemicalTestList.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default ChemicalTestList;
