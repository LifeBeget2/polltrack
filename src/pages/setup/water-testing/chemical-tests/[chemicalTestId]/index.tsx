import React, { useEffect } from 'react';
import type { NextPage } from 'src/../next';
import NextLink from 'next/link';
import { useRouter } from 'next/router';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';
import {
  Box,
  Button,
  Container,
} from '@mui/material';
import { useSettings } from 'src/hooks/use-settings';
import ArrowLeftIcon from 'src/icons/ArrowLeft';
import Page from 'src/components/Page';
import { ChemicalTestForm } from 'src/components/setup/water-testing';
import { useDispatch, useSelector } from 'src/store';
import { getChemicalTest, resetChemicalTest } from 'src/slices/chemicalTest';
import LoadingScreen from 'src/components/LoadingScreen';
import Breadcrumbs from 'src/components/Breadcrumbs';
import type { BreadcrumbItem } from 'src/types/common';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Setup',
    routeTo: '/setup/organisation'
  },
  {
    title: 'Water testing',
    routeTo: '/setup/water-testing/chemical-tests'
  },
  {
    title: 'Chemical tests',
    routeTo: '/setup/water-testing/chemical-tests'
  },
];

const ChemicalTestEdit: NextPage = () => {
  const router = useRouter();
  const { chemicalTestId } = router.query;
  const { settings } = useSettings();
  const dispatch = useDispatch();
  const { organisation } = useSelector((state) => state.account);
  const { chemicalTest, isLoadingChemicalTest } = useSelector((state) => state.chemicalTest);

  useEffect(() => () => dispatch(resetChemicalTest()), []);

  useEffect(() => {
    // @ts-ignore
    dispatch(getChemicalTest(organisation.id, parseInt(chemicalTestId, 10)));
  }, [organisation, chemicalTestId]);

  return (
    <Page
      title="Setup | Edit chemical test"
    >
      <Breadcrumbs
        title="Edit chemical test"
        items={breadcrumbItems}
      >
        <NextLink
          href="/setup/water-testing/chemical-tests"
          passHref
        >
          <Button
            color="primary"
            startIcon={<ArrowLeftIcon fontSize="small" />}
            variant="outlined"
          >
            Cancel
          </Button>
        </NextLink>
      </Breadcrumbs>
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Box mt={3}>
          {isLoadingChemicalTest ? (<LoadingScreen />) : (<ChemicalTestForm chemicalTest={chemicalTest} />)}
        </Box>
      </Container>
    </Page>
  );
};

ChemicalTestEdit.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default ChemicalTestEdit;
