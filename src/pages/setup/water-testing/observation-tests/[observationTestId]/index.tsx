import React, { useCallback, useEffect, useState } from 'react';
import type { NextPage } from 'src/../next';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';
import NextLink from 'next/link';
import { useRouter } from 'next/router';
import {
  Box,
  Button,
  Container,
} from '@mui/material';
import { useSettings } from 'src/hooks/use-settings';
import ArrowLeftIcon from 'src/icons/ArrowLeft';
import Page from 'src/components/Page';
import { ObservationTestForm } from 'src/components/setup/water-testing';
import axios from 'axios';
import { apiConfig } from 'src/config';
import { ObservationTest } from 'src/types/chemical';
import { useSelector } from 'src/store';
import useIsMountedRef from 'src/hooks/useIsMountedRef';
import Breadcrumbs from 'src/components/Breadcrumbs';
import type { BreadcrumbItem } from 'src/types/common';
import LoadingScreen from 'src/components/LoadingScreen';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Setup',
    routeTo: '/setup/organisation'
  },
  {
    title: 'Water testing',
    routeTo: '/setup/water-testing/chemical-tests'
  },
  {
    title: 'Observation tests',
    routeTo: '/setup/water-testing/observation-tests'
  },
];

const ObservationTestEdit: NextPage = () => {
  const router = useRouter();
  const { observationTestId } = router.query;
  const isMountedRef = useIsMountedRef();
  const { settings } = useSettings();
  const { organisation } = useSelector((state) => state.account);
  const [observationTest, setObservationTest] = useState<ObservationTest | null>(null);

  const getObservationTest = useCallback(async () => {
    try {
      // TODO: move to the slice
      const response = await axios.get(`${apiConfig.apiV2Url}/organisations/${organisation.id}/observation-test/${observationTestId}`);

      if (isMountedRef.current) {
        setObservationTest(response.data);
      }
    } catch (err) {
      console.error(err);
    }
  }, [isMountedRef, organisation, observationTestId]);

  useEffect(() => {
    getObservationTest();
  }, [getObservationTest]);

  if (!observationTest) {
    return <LoadingScreen />;
  }

  return (
    <Page
      title="Setup | Edit observation test"
    >
      <Breadcrumbs
        title="Edit observation test"
        items={breadcrumbItems}
      >
        <NextLink
          href="/setup/water-testing/observation-tests"
          passHref
        >
          <Button
            color="primary"
            startIcon={<ArrowLeftIcon fontSize="small" />}
            variant="outlined"
          >
            Cancel
          </Button>
        </NextLink>
      </Breadcrumbs>
      <Container maxWidth={settings.compact ? 'lg' : false}>
        <Box mt={3}>
          <ObservationTestForm observationTest={observationTest} />
        </Box>
      </Container>
    </Page>
  );
};

ObservationTestEdit.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default ObservationTestEdit;
