import React, { useEffect } from 'react';
import type { NextPage } from 'src/../next';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';
import NextLink from 'next/link';
import {
  Box,
  Button,
  Container,
} from '@mui/material';
import PlusIcon from 'src/icons/Plus';
import { getObservationTests } from 'src/slices/observationTest';
import Page from 'src/components/Page';
import { useDispatch, useSelector } from 'src/store';
import { useSettings } from 'src/hooks/use-settings';
import { ObservationTestListTable } from 'src/components/setup/water-testing';
import Breadcrumbs from 'src/components/Breadcrumbs';
import type { BreadcrumbItem } from 'src/types/common';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Setup',
    routeTo: '/setup/organisation'
  },
  {
    title: 'Water testing',
    routeTo: '/setup/water-testing/chemical-tests'
  },
  {
    title: 'Observation tests',
    routeTo: '/setup/water-testing/observation-tests'
  },
];

const ObservationTestList: NextPage = () => {
  const { settings } = useSettings();
  const { organisation } = useSelector((state) => state.account);
  const { observationTests, searchText, page, limit, order, orderBy } = useSelector((state) => state.observationTest);
  const dispatch = useDispatch();

  useEffect(() => {
    const sortBy = orderBy ? `${order === 'desc' ? '-' : ''}${orderBy}` : '';
    dispatch(getObservationTests(organisation.id, page, limit, searchText, sortBy));
  }, [dispatch, limit, page, organisation, searchText, order, orderBy]);

  console.log(observationTests);

  return (
    <Page
      title="Setup | Observation tests list"
    >
      <Breadcrumbs
        title="Observation tests list"
        items={breadcrumbItems}
      >
        <NextLink
          href="/setup/water-testing/observation-tests/new"
          passHref
        >
          <Button
            color="primary"
            startIcon={<PlusIcon fontSize="small" />}
            variant="contained"
          >
            New observation test
          </Button>
        </NextLink>
      </Breadcrumbs>
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Box sx={{ mt: 3 }}>
          <ObservationTestListTable observationTests={observationTests} />
        </Box>
      </Container>
    </Page>
  );
};

ObservationTestList.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default ObservationTestList;
