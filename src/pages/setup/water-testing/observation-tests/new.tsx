import React from 'react';
import type { NextPage } from 'src/../next';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';
import NextLink from 'next/link';
import {
  Box,
  Button,
  Container,
} from '@mui/material';
import { useSettings } from 'src/hooks/use-settings';
import ArrowLeftIcon from 'src/icons/ArrowLeft';
import Page from 'src/components/Page';
import { ObservationTestForm } from 'src/components/setup/water-testing';
import Breadcrumbs from 'src/components/Breadcrumbs';
import type { BreadcrumbItem } from 'src/types/common';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Setup',
    routeTo: '/setup/organisation'
  },
  {
    title: 'Water testing',
    routeTo: '/setup/water-testing/chemical-tests'
  },
  {
    title: 'Observation tests',
    routeTo: '/setup/water-testing/observation-tests'
  },
];

const ObservationTestNew: NextPage = () => {
  const { settings } = useSettings();

  return (
    <Page
      title="Setup | New observation test"
    >
      <Breadcrumbs
        title="New observation test"
        items={breadcrumbItems}
      >
        <NextLink
          href="/setup/water-testing/observation-tests"
          passHref
        >
          <Button
            color="primary"
            startIcon={<ArrowLeftIcon fontSize="small" />}
            variant="outlined"
          >
            Cancel
          </Button>
        </NextLink>
      </Breadcrumbs>
      <Container maxWidth={settings.compact ? 'lg' : false}>
        <Box mt={3}>
          <ObservationTestForm />
        </Box>
      </Container>
    </Page>
  );
};

ObservationTestNew.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default ObservationTestNew;
