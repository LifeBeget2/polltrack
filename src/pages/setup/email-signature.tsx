import React from 'react';
import type { NextPage } from 'src/../next';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';
import {
  Box,
  Container,
} from '@mui/material';
import { useSettings } from '../../hooks/use-settings';
import Page from '../../components/Page';
import { EmailSignatureForm } from '../../components/account/settings';
import { BreadcrumbItem } from 'src/types/common';
import Breadcrumbs from '../../components/Breadcrumbs';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Setup',
    routeTo: '/setup/organisation'
  },
];

const EmailSignature: NextPage = () => {
  const { settings } = useSettings();

  return (
    <Page
      title="Setup | Email signature"
    >
      <Breadcrumbs
        title="Email signature"
        items={breadcrumbItems}
      />
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Box sx={{ mt: 3 }}>
          <EmailSignatureForm />
        </Box>
      </Container>
    </Page>
  );
};

EmailSignature.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default EmailSignature;
