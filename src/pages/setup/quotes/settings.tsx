import React from 'react';
import type { NextPage } from 'src/../next';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';
import {
  Box,
  Container,
} from '@mui/material';
import {
  QuoteSettings as QuoteSettingsForm,
} from 'src/components/setup/quotes';
import { useSettings } from 'src/hooks/use-settings';
import Page from 'src/components/Page';
import Breadcrumbs from 'src/components/Breadcrumbs';
import type { BreadcrumbItem } from 'src/types/common';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Setup',
    routeTo: '/setup/organisation'
  },
  {
    title: 'Lab',
    routeTo: '/setup/lab/settings'
  },
];

const QuoteSettings: NextPage = () => {
  const { settings } = useSettings();

  return (
    <Page
      title="Setup | Quote settings"
    >
      <Breadcrumbs
        title="Quote settings"
        items={breadcrumbItems}
      />
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Box sx={{ mt: 3 }}>
          <QuoteSettingsForm />
        </Box>
      </Container>
    </Page>
  );
};

QuoteSettings.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default QuoteSettings;
