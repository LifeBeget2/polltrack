import React, { useEffect } from 'react';
import type { NextPage } from 'src/../next';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';
import {
  Box,
  Button,
  Container,
} from '@mui/material';
import { ChemicalGroupListTable } from 'src/components/setup/dosage-groups';
import PlusIcon from 'src/icons/Plus';
import { getChemicalGroups } from 'src/slices/chemicalGroup';
import Page from 'src/components/Page';
import { useDispatch, useSelector } from 'src/store';
import { useSettings } from 'src/hooks/use-settings';
import Breadcrumbs from 'src/components/Breadcrumbs';
import type { BreadcrumbItem } from 'src/types/common';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Setup',
    routeTo: '/setup/organisation'
  },
  {
    title: 'Dosage groups',
    routeTo: '/setup/dosage-groups/chemical-groups'
  },
];

const ChemicalGroupList: NextPage = () => {
  const { settings } = useSettings();
  const { organisation } = useSelector((state) => state.account);
  const { chemicalGroups, limit, page, searchText, order, orderBy } = useSelector((state) => state.chemicalGroup);
  const dispatch = useDispatch();

  useEffect(() => {
    const sortBy = orderBy ? `${order === 'desc' ? '-' : ''}${orderBy}` : '';
    dispatch(getChemicalGroups(organisation.id, limit, page, searchText, sortBy));
  }, [organisation, limit, page, searchText, order, orderBy]);

  return (
    <Page
      title="Setup | Chemical groups list"
    >
      <Breadcrumbs
        title="Chemical groups list"
        items={breadcrumbItems}
      >
        <Button
          color="primary"
          startIcon={<PlusIcon fontSize="small" />}
          variant="contained"
        >
          Add chemical group
        </Button>
      </Breadcrumbs>
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Box sx={{ mt: 3 }}>
          <ChemicalGroupListTable chemicalGroups={chemicalGroups} />
        </Box>
      </Container>
    </Page>
  );
};

ChemicalGroupList.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default ChemicalGroupList;
