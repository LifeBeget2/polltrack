import React, { useEffect } from 'react';
import type { NextPage } from 'src/../next';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';
import {
  Box,
  Button,
  Container,
} from '@mui/material';
import { ObservationGroupListTable } from 'src/components/setup/dosage-groups';
import PlusIcon from 'src/icons/Plus';
import { getObservationGroups } from 'src/slices/observationGroup';
import Page from 'src/components/Page';
import { useDispatch, useSelector } from 'src/store';
import { useSettings } from 'src/hooks/use-settings';
import Breadcrumbs from 'src/components/Breadcrumbs';
import type { BreadcrumbItem } from 'src/types/common';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Setup',
    routeTo: '/setup/organisation'
  },
  {
    title: 'Dosage groups',
    routeTo: '/setup/dosage-groups/chemical-groups'
  },
];

const ObservationGroupList: NextPage = () => {
  const { settings } = useSettings();
  const { organisation } = useSelector((state) => state.account);
  const { observationGroups, limit, page, searchText, order, orderBy } = useSelector((state) => state.observationGroup);
  const dispatch = useDispatch();

  useEffect(() => {
    const sortBy = orderBy ? `${order === 'desc' ? '-' : ''}${orderBy}` : '';
    dispatch(getObservationGroups(organisation.id, limit, page, searchText, sortBy));
  }, [organisation, limit, page, searchText, order, orderBy]);

  return (
    <Page
      title="Setup | Observation groups list"
    >
      <Breadcrumbs
        title="Observation groups list"
        items={breadcrumbItems}
      >
        <Button
          color="primary"
          startIcon={<PlusIcon fontSize="small" />}
          variant="contained"
        >
          Add observation group
        </Button>
      </Breadcrumbs>
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Box sx={{ mt: 3 }}>
          <ObservationGroupListTable observationGroups={observationGroups} />
        </Box>
      </Container>
    </Page>
  );
};

ObservationGroupList.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default ObservationGroupList;
