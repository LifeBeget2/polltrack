import React, { ChangeEvent, useState } from 'react';
import type { NextPage } from 'src/../next';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';
import { useRouter } from 'next/router';
import { useSettings } from 'src/hooks/use-settings';

import {
  Box,
  Container,
  Divider,
  Tab,
  Tabs,
} from '@mui/material';

import Page from 'src/components/Page';
import DataCleaningPoolDeduplicateTab from 'src/components/setup/data-cleaning/pool/DataCleaningPoolDeduplicateTab';
import Breadcrumbs from 'src/components/Breadcrumbs';
import type { BreadcrumbItem } from 'src/types/common';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Setup',
    routeTo: '/setup/organisation'
  },
];

const DEDUPLICATE_TAB = 'deduplicate';

const tabs = [{ label: 'Deduplicate', value: DEDUPLICATE_TAB }];

const DataCleaningPool: NextPage = () => {
  const { settings } = useSettings();
  const router = useRouter();
  const { tab } = router.query;
  // @ts-ignore
  const [currentTab, setCurrentTab] = useState<string>(tab);

  const handleTabsChange = (event: ChangeEvent<{}>, value: string): void => {
    setCurrentTab(value);
  };

  return (
    <Page
      title="Setup | Deduplicate pools"
    >
      <Breadcrumbs
        title="Deduplicate pools"
        items={breadcrumbItems}
      />
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Box sx={{ mt: 3 }}>
          <Tabs
            indicatorColor="primary"
            onChange={handleTabsChange}
            scrollButtons="auto"
            textColor="primary"
            value={currentTab}
            variant="scrollable"
          >
            {tabs.map((tab) => (
              <Tab
                key={tab.value}
                label={tab.label}
                value={tab.value}
                onClick={() => router.push(`/setup/data-cleaning/pool/${tab.value}`)}
              />
            ))}
          </Tabs>
        </Box>
        <Divider />
        <Box sx={{ mt: 3 }}>
          {currentTab === DEDUPLICATE_TAB && (
            <DataCleaningPoolDeduplicateTab />
          )}
        </Box>
      </Container>
    </Page>
  );
};

DataCleaningPool.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default DataCleaningPool;
