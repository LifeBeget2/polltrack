import React from 'react';
import type { NextPage } from 'src/../next';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';
import {
  Box,
  Container,
} from '@mui/material';
import {
  BatchSettings as BatchSettingsForm,
} from 'src/components/setup/batch-billing';
import { useSettings } from 'src/hooks/use-settings';
import Page from 'src/components/Page';
import Breadcrumbs from 'src/components/Breadcrumbs';
import type { BreadcrumbItem } from 'src/types/common';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Setup',
    routeTo: '/setup/organisation'
  },
  {
    title: 'Batch billing',
    routeTo: '/setup/batch-billing/settings'
  },
];

const BatchSettings: NextPage = () => {
  const { settings } = useSettings();

  return (
    <Page
      title="Setup | Batch billing settings"
    >
      <Breadcrumbs
        title="Batch billing settings"
        items={breadcrumbItems}
      />
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Box sx={{ mt: 3 }}>
          <BatchSettingsForm />
        </Box>
      </Container>
    </Page>
  );
};

BatchSettings.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default BatchSettings;
