import React, { useState } from 'react';
import type { ChangeEvent } from 'react';
import { useRouter } from 'next/router';
import type { NextPage } from 'src/../next';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';
import {
  Box,
  Container,
  Divider,
  Tab,
  Tabs,
} from '@mui/material';
import {
  SurfaceListTable,
  SanitiserListTable,
  CustomExceptionListTable,
  BrandListTable,
  EquipmentTypeListTable,
} from 'src/components/setup/pool-characteristics';
import { useSettings } from 'src/hooks/use-settings';
import Page from 'src/components/Page';
import Breadcrumbs from 'src/components/Breadcrumbs';
import type { BreadcrumbItem } from 'src/types/common';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Setup',
    routeTo: '/setup/organisation'
  },
];

const SURFACE_TAB = 'surface';
const SANITISER_TAB = 'sanitiser';
const CUSTOM_TAB = 'custom-exception';
const BRAND_TAB = 'brand';
const EQUIPMENT_TAB = 'equipment-type';

const tabs = [
  { label: 'Surfaces', value: SURFACE_TAB },
  { label: 'Sanitisers', value: SANITISER_TAB },
  { label: 'Custom exceptions', value: CUSTOM_TAB },
  { label: 'Brands', value: BRAND_TAB },
  { label: 'Equipment types', value: EQUIPMENT_TAB },
];

const PoolCharacteristics: NextPage = () => {
  const { settings } = useSettings();
  const router = useRouter();
  const { tab } = router.query;
  // @ts-ignore
  const [currentTab, setCurrentTab] = useState<string>(tab);

  const handleTabsChange = (event: ChangeEvent<{}>, value: string): void => {
    setCurrentTab(value);
  };

  return (
    <Page
      title="Setup | Pool characteristics"
    >
      <Breadcrumbs
        title="Pool characteristics"
        items={breadcrumbItems}
      />
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Box sx={{ mt: 3 }}>
          <Tabs
            indicatorColor="primary"
            onChange={handleTabsChange}
            scrollButtons="auto"
            textColor="primary"
            value={currentTab}
            variant="scrollable"
          >
            {tabs.map((tab) => (
              <Tab
                key={tab.value}
                label={tab.label}
                value={tab.value}
                onClick={() => router.push(`/setup/pool-characteristics/${tab.value}`)}
              />
            ))}
          </Tabs>
        </Box>
        <Divider />
        <Box sx={{ mt: 3 }}>
          {currentTab === SURFACE_TAB && <SurfaceListTable />}
          {currentTab === SANITISER_TAB && <SanitiserListTable />}
          {currentTab === CUSTOM_TAB && <CustomExceptionListTable />}
          {currentTab === BRAND_TAB && <BrandListTable />}
          {currentTab === EQUIPMENT_TAB && <EquipmentTypeListTable />}
        </Box>
      </Container>
    </Page>
  );
};

PoolCharacteristics.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default PoolCharacteristics;
