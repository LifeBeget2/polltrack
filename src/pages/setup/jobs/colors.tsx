import type { NextPage } from 'src/../next';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';
import {
  Box,
  Container,
} from '@mui/material';
import ColorListTable from 'src/components/setup/color/ColorListTable';
import { useSettings } from 'src/hooks/use-settings';
import Page from 'src/components/Page';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'src/store';
import { getColors } from 'src/slices/account';
import Breadcrumbs from 'src/components/Breadcrumbs';
import type { BreadcrumbItem } from 'src/types/common';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Setup',
    routeTo: '/setup/organisation'
  },
  {
    title: 'Jobs',
    routeTo: '/setup/jobs/job-templates'
  },
];

const ColorsList: NextPage = () => {
  const { settings } = useSettings();
  const { organisation } = useSelector((state) => state.account);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getColors(organisation.id));
  }, [organisation]);

  return (
    <Page
      title="Setup | Job colors"
    >
      <Breadcrumbs
        title="Colors"
        items={breadcrumbItems}
      />
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Box sx={{ mt: 3 }}>
          <ColorListTable />
        </Box>
      </Container>
    </Page>
  );
};

ColorsList.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default ColorsList;
