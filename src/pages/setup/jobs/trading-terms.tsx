import type { NextPage } from 'src/../next';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';
import {
  Box,
  Container,
} from '@mui/material';
import TradingTermListTable from 'src/components/setup/trading-term/TradingTermListTable';
import { useSettings } from 'src/hooks/use-settings';
import Page from 'src/components/Page';
import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'src/store';
import { getTradingTerms } from 'src/slices/account';
import Breadcrumbs from 'src/components/Breadcrumbs';
import type { BreadcrumbItem } from 'src/types/common';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Setup',
    routeTo: '/setup/organisation'
  },
  {
    title: 'Jobs',
    routeTo: '/setup/jobs/job-templates'
  },
];

const TradingTermsList: NextPage = () => {
  const { settings } = useSettings();
  const { organisation } = useSelector((state) => state.account);
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getTradingTerms(organisation.id));
  }, [organisation]);

  return (
    <Page
      title="Setup | Trading Terms"
    >
      <Breadcrumbs
        title="Trading Terms"
        items={breadcrumbItems}
      />
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Box sx={{ mt: 3 }}>
          <TradingTermListTable />
        </Box>
      </Container>
    </Page>
  );
};

TradingTermsList.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default TradingTermsList;
