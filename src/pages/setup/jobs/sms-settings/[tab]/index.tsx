import React, { ChangeEvent, useState } from 'react';
import type { NextPage } from 'src/../next';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';
import { useRouter } from 'next/router';

import {
  Box,
  Container,
  Divider,
  Tab,
  Tabs,
} from '@mui/material';

import Page from 'src/components/Page';
import SmsManagementConfigureTab from 'src/components/setup/jobs/SmsManagement/SmsManagementConfigureTab';
import SmsManagementSentTab from 'src/components/setup/jobs/SmsManagement/SmsManagementSentTab';
import { useSettings } from 'src/hooks/use-settings';
import Breadcrumbs from 'src/components/Breadcrumbs';
import type { BreadcrumbItem } from 'src/types/common';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Setup',
    routeTo: '/setup/organisation'
  },
  {
    title: 'Jobs',
    routeTo: '/setup/jobs/job-templates'
  },
];

const CONFIGURE_SMS_TAB = 'configure';
const SENT_SMS_TAB = 'sent';

const tabs = [
  { label: 'Configure SMS messages', value: CONFIGURE_SMS_TAB },
  { label: 'View sent messages', value: SENT_SMS_TAB },
];

const PoolDetails: NextPage = () => {
  const { settings } = useSettings();
  const router = useRouter();
  const { tab } = router.query;
  // @ts-ignore
  const [currentTab, setCurrentTab] = useState<string>(tab);

  const handleTabsChange = (event: ChangeEvent<{}>, value: string): void => {
    setCurrentTab(value);
  };

  return (
    <Page title="Setup | Job SMS">
      <Breadcrumbs
        title="SMS"
        items={breadcrumbItems}
      />
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Box sx={{ mt: 3 }}>
          <Tabs
            indicatorColor="primary"
            onChange={handleTabsChange}
            scrollButtons="auto"
            textColor="primary"
            value={currentTab}
            variant="scrollable"
          >
            {tabs.map((tab) => (
              <Tab
                key={tab.value}
                label={tab.label}
                value={tab.value}
                onClick={() => router.push(`/setup/jobs/sms-settings/${tab.value}`)}
              />
            ))}
          </Tabs>
        </Box>
        <Divider />
        <Box sx={{ mt: 3 }}>
          {currentTab === CONFIGURE_SMS_TAB && <SmsManagementConfigureTab />}
          {currentTab === SENT_SMS_TAB && <SmsManagementSentTab />}
        </Box>
      </Container>
    </Page>
  );
};

PoolDetails.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default PoolDetails;
