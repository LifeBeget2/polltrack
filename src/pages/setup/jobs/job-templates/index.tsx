import React, { useEffect } from 'react';
import type { NextPage } from 'src/../next';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';
import NextLink from 'next/link';
import {
  Box,
  Button,
  Container,
} from '@mui/material';
import { JobTemplateListTable } from 'src/components/setup/job-template';
import { useSettings } from 'src/hooks/use-settings';
import PlusIcon from 'src/icons/Plus';
import { getTemplates } from 'src/slices/jobTemplate';
import { useDispatch, useSelector } from 'src/store';
import Page from 'src/components/Page';
import Breadcrumbs from 'src/components/Breadcrumbs';
import type { BreadcrumbItem } from 'src/types/common';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Setup',
    routeTo: '/setup/organisation'
  },
  {
    title: 'Jobs',
    routeTo: '/setup/jobs/job-templates'
  },
];

const JobTemplateList: NextPage = () => {
  const { settings } = useSettings();
  const { organisation } = useSelector((state) => state.account);
  const { jobTemplates, limit, page, jobTypeFilter, searchText, order, orderBy } = useSelector((state) => state.jobTemplate);
  const dispatch = useDispatch();

  useEffect(() => {
    const sortBy = orderBy ? `${order === 'desc' ? '-' : ''}${orderBy}` : '';
    dispatch(getTemplates(organisation.id, limit, page, jobTypeFilter, searchText, sortBy));
  }, [organisation, limit, page, jobTypeFilter, searchText, order, orderBy]);

  return (
    <Page
      title="Setup | Job templates list"
    >
      <Breadcrumbs
        title="Job templates list"
        items={breadcrumbItems}
      >
        <NextLink
          href="/setup/jobs/job-templates/new"
          passHref
        >
          <Button
            color="primary"
            startIcon={<PlusIcon fontSize="small" />}
            variant="contained"
          >
            New template
          </Button>
        </NextLink>
      </Breadcrumbs>
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Box sx={{ mt: 3 }}>
          {jobTemplates && <JobTemplateListTable jobTemplates={jobTemplates} />}
        </Box>
      </Container>
    </Page>
  );
};

JobTemplateList.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default JobTemplateList;
