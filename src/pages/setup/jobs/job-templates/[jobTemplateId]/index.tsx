import type { NextPage } from 'src/../next';
import { useRouter } from 'next/router';
import NextLink from 'next/link';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';
import {
  Box,
  Button,
  Container,
} from '@mui/material';
import { useSettings } from 'src/hooks/use-settings';
import ArrowLeftIcon from 'src/icons/ArrowLeft';
import Page from 'src/components/Page';
import { JobTemplateForm } from 'src/components/setup/job-template';
import React, { useCallback, useEffect, useState } from 'react';
import axios from 'axios';
import { apiConfig } from 'src/config';
import { JobTemplate } from 'src/types/jobTemplate';
import { useSelector } from 'src/store';
import useIsMountedRef from 'src/hooks/useIsMountedRef';
import Breadcrumbs from 'src/components/Breadcrumbs';
import type { BreadcrumbItem } from 'src/types/common';
import LoadingScreen from 'src/components/LoadingScreen';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Setup',
    routeTo: '/setup/organisation'
  },
  {
    title: 'Jobs',
    routeTo: '/setup/jobs/job-templates'
  },
  {
    title: 'Job templates',
    routeTo: '/setup/jobs/job-templates'
  },
];

const JobTemplateEdit: NextPage = () => {
  const router = useRouter();
  const { jobTemplateId } = router.query;
  const isMountedRef = useIsMountedRef();
  const { settings } = useSettings();
  const { organisation } = useSelector((state) => state.account);
  const [jobTemplate, setJobTemplate] = useState<JobTemplate | null>(null);

  const getJobTemplate = useCallback(async () => {
    try {
      // TODO: move to the slice
      const response = await axios.get(`${apiConfig.apiV2Url}/organisations/${organisation.id}/job-template/${jobTemplateId}`);

      if (isMountedRef.current) {
        setJobTemplate(response.data);
      }
    } catch (err) {
      console.error(err);
    }
  }, [isMountedRef]);

  useEffect(() => {
    getJobTemplate();
  }, [getJobTemplate]);

  if (!jobTemplate) {
    return <LoadingScreen />;
  }

  return (
    <Page
      title="Setup | Job template edit"
    >
      <Breadcrumbs
        title="Edit job template"
        items={breadcrumbItems}
      >
        <NextLink
          href="/setup/jobs/job-templates"
          passHref
        >
          <Button
            color="primary"
            startIcon={<ArrowLeftIcon fontSize="small" />}
            sx={{ mt: 1 }}
            variant="outlined"
          >
            Cancel
          </Button>
        </NextLink>
      </Breadcrumbs>
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Box mt={3}>
          <JobTemplateForm jobTemplate={jobTemplate} />
        </Box>
      </Container>
    </Page>
  );
};

JobTemplateEdit.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default JobTemplateEdit;
