import React from 'react';
import type { NextPage } from 'src/../next';
import NextLink from 'next/link';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';
import {
  Box,
  Button,
  Container,
} from '@mui/material';
import { useSettings } from 'src/hooks/use-settings';
import ArrowLeftIcon from 'src/icons/ArrowLeft';
import Page from 'src/components/Page';
import { JobTemplateForm } from 'src/components/setup/job-template';
import Breadcrumbs from 'src/components/Breadcrumbs';
import type { BreadcrumbItem } from 'src/types/common';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Setup',
    routeTo: '/setup/organisation'
  },
  {
    title: 'Jobs',
    routeTo: '/setup/jobs/job-templates'
  },
  {
    title: 'Job templates',
    routeTo: '/setup/jobs/job-templates'
  },
];

const JobTemplateNew: NextPage = () => {
  const { settings } = useSettings();

  return (
    <Page
      title="Setup | New job template"
    >
      <Breadcrumbs
        title="New job template"
        items={breadcrumbItems}
      >
        <NextLink
          href="/setup/jobs/job-templates"
          passHref
        >
          <Button
            color="primary"
            startIcon={<ArrowLeftIcon fontSize="small" />}
            sx={{ mt: 1 }}
            variant="outlined"
          >
            Cancel
          </Button>
        </NextLink>
      </Breadcrumbs>
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Box mt={3}>
          <JobTemplateForm />
        </Box>
      </Container>
    </Page>
  );
};

JobTemplateNew.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default JobTemplateNew;
