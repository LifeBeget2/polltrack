import React from 'react';
import type { NextPage } from 'src/../next';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';
import {
  Box,
  Container,
} from '@mui/material';
import {
  InvoiceSettings as InvoiceSettingsForm,
} from 'src/components/setup/jobs';
import { useSettings } from 'src/hooks/use-settings';
import Page from 'src/components/Page';
import Breadcrumbs from 'src/components/Breadcrumbs';
import type { BreadcrumbItem } from 'src/types/common';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Setup',
    routeTo: '/setup/organisation'
  },
  {
    title: 'Jobs',
    routeTo: '/setup/jobs/job-templates'
  },
];

const InvoiceSettings: NextPage = () => {
  const { settings } = useSettings();

  return (
    <Page
      title="Setup | Invoice"
    >
      <Breadcrumbs
        title="Invoice"
        items={breadcrumbItems}
      />
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Box sx={{ mt: 3 }}>
          <InvoiceSettingsForm />
        </Box>
      </Container>
    </Page>
  );
};

InvoiceSettings.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default InvoiceSettings;
