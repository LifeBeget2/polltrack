import React, { useState } from 'react';
import type { ChangeEvent } from 'react';
import type { NextPage } from 'src/../next';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';
import {
  Box,
  Container,
  Divider,
  Tab,
  Tabs,
} from '@mui/material';
import {
  AccountBillingSettings,
  OrganisationGeneralSettings,
  AccountBrandSettings
} from '../../components/account/organisation';
import { useSettings } from '../../hooks/use-settings';
import Page from '../../components/Page';
import Breadcrumbs from '../../components/Breadcrumbs';
import type { BreadcrumbItem } from '../../types/common';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Setup',
    routeTo: '/setup/organisation'
  },
];

const tabs = [
  { label: 'General', value: 'general' },
  { label: 'Billing', value: 'billing' },
  { label: 'Brand', value: 'brand' }
];

const Account: NextPage = () => {
  const { settings } = useSettings();
  const [currentTab, setCurrentTab] = useState<string>('general');

  const handleTabsChange = (event: ChangeEvent<{}>, value: string): void => {
    setCurrentTab(value);
  };

  return (
    <Page
      title="Organisation | Details"
    >
      <Breadcrumbs
        title="Organisation details"
        items={breadcrumbItems}
      />
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Box sx={{ mt: 3 }}>
          <Tabs
            indicatorColor="primary"
            onChange={handleTabsChange}
            scrollButtons="auto"
            textColor="primary"
            value={currentTab}
            variant="scrollable"
          >
            {tabs.map((tab) => (
              <Tab
                key={tab.value}
                label={tab.label}
                value={tab.value}
              />
            ))}
          </Tabs>
        </Box>
        <Divider />
        <Box sx={{ mt: 3 }}>
          {currentTab === 'general' && <OrganisationGeneralSettings />}
          {currentTab === 'billing' && <AccountBillingSettings />}
          {currentTab === 'brand' && <AccountBrandSettings />}
        </Box>
      </Container>
    </Page>
  );
};

Account.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default Account;
