import React from 'react';
import type { NextPage } from 'src/../next';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';
import {
  Container,
  Grid,
} from '@mui/material';
import { PricingPlan } from 'src/components/account/plan-and-payment';
import Page from '../../components/Page';
import Breadcrumbs from '../../components/Breadcrumbs';
import type { BreadcrumbItem } from '../../types/common';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Setup',
    routeTo: '/setup/organisation'
  },
];

const PlanAndPayment: NextPage = () => (
  <Page title="Plan & payment">
    <Breadcrumbs
      title="Plan & payment"
      items={breadcrumbItems}
    />
    <Container
      maxWidth="lg"
      sx={{ py: 6 }}
    >
      <Grid
        container
        spacing={4}
      >
        <Grid
          item
          md={4}
          xs={12}
        >
          <PricingPlan
            cta="Start Free Trial"
            currency="$"
            description="Water testing for your store."
            features={[
              'In-store water testing',
              'Chemical calculator',
              'Works with all products',
              'Generates comprehensive reports',
              'Stores graphs and records',
            ]}
            image="/static/pricing/plan1.svg"
            name="Lab"
            price="45"
            sx={{
              height: '100%',
              maxWidth: 460,
              mx: 'auto'
            }}
          />
        </Grid>
        <Grid
          item
          md={4}
          xs={12}
        >
          <PricingPlan
            cta="Start Free Trial"
            currency="$"
            description="Perfect for your pool servicing business."
            features={[
              'In-field water testing',
              'Job management with pool industry templates',
              'Job scheduling',
              'GPS address linked to owner and pool details',
              'GPS tech/van tracking in real time',
              'Quoting, invoicing',
              'Credit card payments',
            ]}
            image="/static/pricing/plan2.svg"
            name="Field"
            popular
            price="59"
            sx={{
              height: '100%',
              maxWidth: 460,
              mx: 'auto'
            }}
          />
        </Grid>
        <Grid
          item
          md={4}
          xs={12}
        >
          <PricingPlan
            cta="Start Free Trial"
            currency="$"
            description="You have a shop and service team."
            features={[
              'All previous',
            ]}
            image="/static/pricing/plan3.svg"
            name="Pro"
            price="104"
            sx={{
              height: '100%',
              maxWidth: 460,
              mx: 'auto'
            }}
          />
        </Grid>
      </Grid>
    </Container>
  </Page>
);

PlanAndPayment.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default PlanAndPayment;
