import React from 'react';
import type { NextPage } from 'src/../next';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';
import {
  Box,
  Container,
} from '@mui/material';
import {
  SmsSettings as SmsSettingsForm,
} from 'src/components/setup/lab';
import { useSettings } from 'src/hooks/use-settings';
import Page from 'src/components/Page';
import Breadcrumbs from 'src/components/Breadcrumbs';
import type { BreadcrumbItem } from 'src/types/common';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Setup',
    routeTo: '/setup/organisation'
  },
  {
    title: 'Lab',
    routeTo: '/setup/lab/settings'
  },
];

const SmsSettings: NextPage = () => {
  const { settings } = useSettings();

  return (
    <Page
      title="Setup | Lab SMS"
    >
      <Breadcrumbs
        title="Lab SMS"
        items={breadcrumbItems}
      />
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Box sx={{ mt: 3 }}>
          <SmsSettingsForm />
        </Box>
      </Container>
    </Page>
  );
};

SmsSettings.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default SmsSettings;
