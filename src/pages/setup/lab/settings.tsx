import React from 'react';
import type { NextPage } from 'src/../next';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';
import {
  Box,
  Container,
} from '@mui/material';
import {
  LabSettings as LabSettingsForm,
} from 'src/components/setup/lab';
import { useSettings } from 'src/hooks/use-settings';
import Page from 'src/components/Page';
import Breadcrumbs from 'src/components/Breadcrumbs';
import type { BreadcrumbItem } from 'src/types/common';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Setup',
    routeTo: '/setup/organisation'
  },
  {
    title: 'Lab',
    routeTo: '/setup/lab/settings'
  },
];

const LabSettings: NextPage = () => {
  const { settings } = useSettings();

  return (
    <Page
      title="Setup | Lab settings"
    >
      <Breadcrumbs
        title="Lab settings"
        items={breadcrumbItems}
      />
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Box sx={{ mt: 3 }}>
          <LabSettingsForm />
        </Box>
      </Container>
    </Page>
  );
};

LabSettings.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default LabSettings;
