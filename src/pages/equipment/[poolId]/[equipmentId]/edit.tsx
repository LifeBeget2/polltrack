import React, { useEffect } from 'react';
import { useRouter } from 'next/router';
import type { NextPage } from 'src/../next';
import {
  Box,
  Button,
  Container,
  Grid,
} from '@mui/material';
import toast from 'react-hot-toast';

import Page from 'src/components/Page';
import SectionEquipmentInfo from 'src/components/pool-equipment/SectionEquipmentInfo';
import SectionEquipmentWarranty from 'src/components/pool-equipment/SectionEquipmentWarranty';
import SectionEquipmentAdditionalInfo from 'src/components/pool-equipment/SectionEquipmentAdditionalInfo';
import SectionEquipmentImages from 'src/components/pool-equipment/SectionEquipmentImages';
import SectionEquipmentActions from 'src/components/pool-equipment/SectionEquipmentActions';

import { useSettings } from 'src/hooks/use-settings';
import ArrowLeftIcon from 'src/icons/ArrowLeft';
import { initialize, reset } from 'src/slices/poolEquipment';
import { useDispatch, useSelector } from 'src/store';
import Breadcrumbs from 'src/components/Breadcrumbs';
import type { BreadcrumbItem } from 'src/types/common';
import LoadingScreen from 'src/components/LoadingScreen';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';
import { useTranslation } from 'react-i18next';

const EditEquipment: NextPage = () => {
  const dispatch = useDispatch();
  const { settings } = useSettings();
  const router = useRouter();
  const { t } = useTranslation();
  const { poolId, equipmentId } = router.query;

  const organisationId = useSelector((state) => state.account.organisation.id);
  const {
    isLoading,
    isCreatingOrUpdatingEquipmentSuccess,
    equipmentNotFound,
  } = useSelector((state) => state.poolEquipment);

  const breadcrumbItems: BreadcrumbItem[] = [
    {
      title: t('Operations'),
      routeTo: '/dashboard/overview'
    },
    {
      title: t('Pools'),
      routeTo: '/pools'
    },
    {
      title: t('Pool edit'),
      routeTo: `/pools/${poolId}/details`
    }
  ];

  useEffect(() => {
    // @ts-ignore
    dispatch(initialize(organisationId, equipmentId, poolId));
    return () => {
      dispatch(reset());
    };
  }, [organisationId, poolId, equipmentId]);

  useEffect(() => {
    // watching for `isCreatingOrUpdatingEquipmentSuccess`
    // after success, the route will be changed and state will be reset by hook above
    if (isCreatingOrUpdatingEquipmentSuccess) {
      toast.success(t('Equipment has been updated'));

      router.back();
    }
  }, [isCreatingOrUpdatingEquipmentSuccess]);

  useEffect(() => {
    if (equipmentNotFound) {
      router.back();
    }
  }, [equipmentNotFound]);

  if (isLoading) {
    return <LoadingScreen />;
  }

  return (
    <Page title={t('Edit equipment')}>
      <Breadcrumbs
        title={t('Edit equipment')}
        items={breadcrumbItems}
      >
        <Button
          color="primary"
          startIcon={<ArrowLeftIcon fontSize="small" />}
          sx={{
            mt: 1,
          }}
          variant="outlined"
          onClick={() => router.back()}
        >
          {t('Cancel')}
        </Button>
      </Breadcrumbs>
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Box mt={3}>
          <Grid
            container
            spacing={3}
          >
            <Grid
              item
              lg={6}
              xs={12}
            >
              <SectionEquipmentInfo />
            </Grid>
            <Grid
              item
              lg={6}
              xs={12}
            >
              <SectionEquipmentWarranty />
            </Grid>
            <Grid
              item
              lg={6}
              xs={12}
            >
              <SectionEquipmentAdditionalInfo />
            </Grid>
            <Grid
              lg={6}
              xs={12}
              sx={{
                display: {
                  lg: 'flex',
                  xs: 'none'
                },
              }}
            />
            <Grid
              item
              lg={6}
              xs={12}
            >
              <SectionEquipmentImages />
            </Grid>
            <Grid
              item
              lg={6}
              xs={12}
            />
            <Grid
              item
              lg={6}
              xs={12}
            >
              <SectionEquipmentActions />
            </Grid>
          </Grid>
        </Box>
      </Container>
    </Page>
  );
};

EditEquipment.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default EditEquipment;
