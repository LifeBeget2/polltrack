/* eslint-disable */
import React, { useState, useRef, useEffect, MouseEvent } from 'react';
import type { FC } from 'react';
import {Link as RouterLink, useNavigate} from 'react-router-dom';
import FullCalendar from '@fullcalendar/react';
import rrulePlugin from '@fullcalendar/rrule';
import dayGridPlugin from '@fullcalendar/daygrid';
import interactionPlugin from '@fullcalendar/interaction';
import listPlugin from '@fullcalendar/list';
import timeGridPlugin from '@fullcalendar/timegrid';
import timelinePlugin from '@fullcalendar/timeline';
import moment from 'moment/moment';
import momentTimezonePlugin from '@fullcalendar/moment-timezone';
import momentPlugin from '@fullcalendar/moment';
import {
  Box,
  Breadcrumbs,
  Button,
  Card, CardHeader,
  Container,
  Grid,
  Link,
  Popover,
  Typography
} from '@mui/material';
import type { Theme } from '@mui/material';
import {alpha, styled, useTheme} from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';
import { CalendarToolbar } from 'src/components/dashboard/calendar/calendar-toolbar';
import ChevronRightIcon from '../../icons/ChevronRight';
import PlusIcon from '../../icons/Plus';
import {
  getEvents,
  selectEvent,
  updateEvent
} from '../../slices/calendar';
import {
  getJobs
} from '../../slices/jobCalendar';
import { useDispatch, useSelector } from '../../store';
// import type { RootState } from '../../store';
import type { CalendarEvent, CalendarView } from '../../types/calendar';
import Page from '../../components/Page';
import {useRouter} from "next/router";
// import { Job } from '../../types/job';

// const selectedEventSelector = (state: RootState): CalendarEvent | null => {
//   const { events, selectedEventId } = state.calendar;
//
//   if (selectedEventId) {
//     return events.find((_event) => _event.id === selectedEventId);
//   }
//
//   return null;
// };

/**
 * https://github.com/fullcalendar/fullcalendar/blob/495d925436e533db2fd591e09a0c887adca77053/packages/common/src/common/StandardEvent.tsx#L79
 */
const renderInnerContent = (innerProps) => (
  <div className="fc-event-main-frame">
    { innerProps.timeText
      && <div className="fc-event-time">{ innerProps.timeText }</div>}
    <div className="fc-event-title-container">
      <div className="fc-event-title fc-sticky">
        { innerProps.event.title }
      </div>
    </div>
  </div>
);

const FullCalendarWrapper = styled('div')(
  ({ theme }) => (
    {
      // '& .fc-license-message': {
      //   display: 'none'
      // },
      '& .fc': {
        '--fc-bg-event-opacity': 1,
        '--fc-border-color': theme.palette.divider,
        '--fc-daygrid-event-dot-width': '10px',
        // '--fc-event-text-color': theme.palette.text.primary,
        '--fc-list-event-hover-bg-color': theme.palette.background.default,
        '--fc-neutral-bg-color': theme.palette.background.default,
        '--fc-page-bg-color': theme.palette.background.default,
        '--fc-today-bg-color': alpha(theme.palette.primary.main, 0.25),
        color: theme.palette.text.primary,
        fontFamily: theme.typography.fontFamily
      },
      '& .fc .fc-col-header-cell-cushion': {
        paddingBottom: '10px',
        paddingTop: '10px'
      },
      '& .fc .fc-day-other .fc-daygrid-day-top': {
        color: theme.palette.text.secondary
      },
      '& .fc-daygrid-event': {
        padding: '5px'
      }
    }
  )
);

const JobCalendarBackup: FC = () => {
  const dispatch = useDispatch();
  const router = useRouter();
  const theme = useTheme();
  const calendarRef = useRef<FullCalendar | null>(null);
  const mobileDevice = useMediaQuery((theme: Theme) => theme.breakpoints.down('sm'));
  const { organisation } = useSelector((state) => state.account);
  const {
    jobs,
    fromDate,
    toDate
  } = useSelector((state) => state.jobCalendar);
  // const selectedEvent = useSelector(selectedEventSelector);
  const [date, setDate] = useState<Date>(new Date());
  const [events, setEvents] = useState<CalendarEvent[]>([]);
  const [view, setView] = useState<CalendarView>(mobileDevice
    ? 'listWeek'
    : 'dayGridMonth');
  const [anchorEl, setAnchorEl] = useState<HTMLElement | null>(null);

  const handlePopoverOpen = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handlePopoverClose = () => {
    setAnchorEl(null);
  };

  const open = Boolean(anchorEl);

  useEffect(() => {
    dispatch(getEvents());
    dispatch(getJobs(organisation.id, fromDate, toDate));
  }, []);

  useEffect(() => {
    const transformedJobs = jobs.map((job) => ({
      id: job.id ? job.id.toString(10) : job.virtual_id,
      allDay: false,
      color: theme.palette.primary.main,
      description: job.job_template.name,
      // rrule?: string;
      start: moment(job.start_time).toDate(),
      end: moment(job.end_time).toDate(),
      title: job.job_template.name,
      extendedProps: {
        ...job
      }
    }));
    setEvents(transformedJobs);
  }, [jobs]);

  useEffect(() => {
    const calendarEl = calendarRef.current;

    if (calendarEl) {
      const calendarApi = calendarEl.getApi();
      const newView = mobileDevice ? 'listWeek' : 'dayGridMonth';

      calendarApi.changeView(newView);
      setView(newView);
    }
  }, [mobileDevice]);

  const handleDateToday = (): void => {
    const calendarEl = calendarRef.current;

    if (calendarEl) {
      const calendarApi = calendarEl.getApi();

      calendarApi.today();
      setDate(calendarApi.getDate());
    }
  };

  const handleViewChange = (newView: CalendarView): void => {
    const calendarEl = calendarRef.current;

    if (calendarEl) {
      const calendarApi = calendarEl.getApi();

      calendarApi.changeView(newView);
      setView(newView);
    }
  };

  const handleDatePrev = (): void => {
    const calendarEl = calendarRef.current;

    if (calendarEl) {
      const calendarApi = calendarEl.getApi();

      calendarApi.prev();
      setDate(calendarApi.getDate());
    }
  };

  const handleDateNext = (): void => {
    const calendarEl = calendarRef.current;

    if (calendarEl) {
      const calendarApi = calendarEl.getApi();

      calendarApi.next();
      setDate(calendarApi.getDate());
    }
  };

  const handleEventSelect = (arg: any): void => {
    dispatch(selectEvent(arg.event.id));
  };

  const handleEventResize = async ({ event }: any): Promise<void> => {
    try {
      await dispatch(updateEvent(event.id, {
        allDay: event.allDay,
        start: event.start,
        end: event.end
      }));
    } catch (err) {
      console.error(err);
    }
  };

  const handleEventDrop = async ({ event }: any): Promise<void> => {
    try {
      await dispatch(updateEvent(event.id, {
        allDay: event.allDay,
        start: event.start,
        end: event.end
      }));
    } catch (err) {
      console.error(err);
    }
  };

  // const handleEventDataTransform = (job: any): CalendarEvent => ({
  //   id: job.id.toString(10),
  //   allDay: false,
  //   color: '#31a6f4',
  //   description: job.job_template.name,
  //   // rrule?: string;
  //   start: moment(job.start_time).toDate(),
  //   end: moment(job.end_time).toDate(),
  //   title: job.job_template.name,
  //   extendedProps: {
  //     ...job
  //   }
  // });

  const handleEventMouseEnter = (mouseEnterInfo: any): void => {
    console.log(mouseEnterInfo);
  };

  return (
    <Page
      title="Job calendar"
    >
      <Container maxWidth={false}>
        <Grid
          container
          justifyContent="space-between"
          spacing={3}
        >
          <Grid item>
            <Typography
              color="textPrimary"
              variant="h5"
            >
              Jobs
            </Typography>
            <Breadcrumbs
              aria-label="breadcrumb"
              separator={<ChevronRightIcon fontSize="small" />}
              sx={{ mt: 1 }}
            >
              <Link
                color="textPrimary"
                variant="subtitle2"
              >
                Dashboard
              </Link>
              <Typography
                color="textSecondary"
                variant="subtitle2"
              >
                Calendar
              </Typography>
            </Breadcrumbs>
          </Grid>
          <Grid item>
            <Box sx={{ m: -1 }}>
              <Button
                color="primary"
                startIcon={<PlusIcon fontSize="small" />}
                sx={{ m: 1 }}
                variant="contained"
              >
                New job
              </Button>
            </Box>
          </Grid>
        </Grid>
        <Box sx={{ mt: 3 }}>
          <CalendarToolbar
            date={date}
            onDateNext={handleDateNext}
            onDatePrev={handleDatePrev}
            onDateToday={handleDateToday}
            onViewChange={handleViewChange}
            view={view}
          />
        </Box>
        <Card
          sx={{
            mt: 3,
            p: 2
          }}
        >
          <FullCalendarWrapper>
            <FullCalendar
              allDayMaintainDuration
              dayMaxEventRows={3}
              droppable
              editable
              schedulerLicenseKey="CC-Attribution-NonCommercial-NoDerivatives"
              eventClick={handleEventSelect}
              eventDisplay="block"
              eventDrop={handleEventDrop}
              // eventDataTransform={handleEventDataTransform}
              eventMouseEnter={handleEventMouseEnter}
              eventResizableFromStart
              eventResize={handleEventResize}
              events={events}
              headerToolbar={false}
              height={800}
              initialDate={date}
              initialView={view}
              plugins={[
                rrulePlugin,
                dayGridPlugin,
                interactionPlugin,
                listPlugin,
                momentPlugin,
                momentTimezonePlugin,
                timeGridPlugin,
                timelinePlugin
              ]}
              businessHours={[ // specify an array instead
                {
                  daysOfWeek: [1, 2, 3], // Monday, Tuesday, Wednesday
                  startTime: '08:00', // 8am
                  endTime: '18:00' // 6pm
                },
                {
                  daysOfWeek: [4, 5], // Thursday, Friday
                  startTime: '10:00', // 10am
                  endTime: '16:00' // 4pm
                }
              ]}
              eventContent={(arg) => (
                <div>
                  <Box
                    aria-owns={open ? 'mouse-over-popover' : undefined}
                    aria-haspopup="true"
                    onMouseEnter={handlePopoverOpen}
                    onMouseLeave={handlePopoverClose}
                    onClick={() => router.push(`/jobs/${arg.event.id}`)}
                  >
                    { renderInnerContent(arg) }
                  </Box>
                  <Popover
                    id="mouse-over-popover"
                    open={open}
                    anchorEl={anchorEl}
                    sx={{
                      pointerEvents: 'none',
                    }}
                    anchorOrigin={{
                      vertical: 'bottom',
                      horizontal: 'left',
                    }}
                    transformOrigin={{
                      vertical: 'top',
                      horizontal: 'left',
                    }}
                    onClose={handlePopoverClose}
                    keepMounted
                    PaperProps={{
                      sx: { width: 240, p: 1 }
                    }}
                  >
                    <Card>
                      <CardHeader title="Job details" />
                    </Card>
                  </Popover>
                </div>
              )}
              titleFormat="{MMMM {D}}, YYYY"
              timeZone={organisation.timezone}
              ref={calendarRef}
              rerenderDelay={10}
              weekends
            />
          </FullCalendarWrapper>
        </Card>
      </Container>
    </Page>
  );
};

export default JobCalendarBackup;
