import React, { ChangeEvent, useCallback, useEffect, useState } from 'react';
import type { NextPage } from 'src/../next';
import { useRouter } from 'next/router';
import NextLink from 'next/link';
import {
  Box,
  Button,
  Container,
  Divider,
  Tab,
  Tabs,
} from '@mui/material';
import { useSettings } from 'src/hooks/use-settings';
import ArrowLeftIcon from 'src/icons/ArrowLeft';
import { useSelector } from 'src/store';
import useIsMountedRef from 'src/hooks/useIsMountedRef';
import axios from 'axios';
import { apiConfig } from 'src/config';
import type { Job } from 'src/types/job';
import Page from 'src/components/Page';
import {
  JobActivities,
  JobDetail as JobOverview,
} from 'src/components/job';
import ReceiptIcon from '@mui/icons-material/Receipt';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';
import Breadcrumbs from 'src/components/Breadcrumbs';
import type { BreadcrumbItem } from 'src/types/common';

const tabs = [
  { label: 'Detail', value: 'detail' },
  { label: 'Activity', value: 'activity' },
];

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Operations',
    routeTo: '/dashboard/overview'
  },
  {
    title: 'Jobs',
    routeTo: '/jobs/calendar'
  },
];

const JobDetail: NextPage = () => {
  const router = useRouter();
  const { jobId } = router.query;
  const { settings } = useSettings();
  const isMountedRef = useIsMountedRef();
  const [job, setJob] = useState<Job | null>(null);
  const [currentTab, setCurrentTab] = useState<string>('detail');
  const { organisation } = useSelector((state) => state.account);

  const getJob = useCallback(async () => {
    try {
      const response = await axios.get(`${apiConfig.apiV1Url}/organisations/${organisation.id}/jobs/${jobId}`);

      if (isMountedRef.current) {
        setJob(response.data);
      }
    } catch (err) {
      console.error(err);
    }
  }, [isMountedRef]);

  useEffect(() => {
    getJob();
  }, [getJob]);

  const handleTabsChange = (event: ChangeEvent<{}>, value: string): void => {
    setCurrentTab(value);
  };

  if (!job) {
    return null;
  }

  return (
    <Page
      title="Job detail"
    >
      <Breadcrumbs
        title="Job detail"
        items={breadcrumbItems}
      >
        <NextLink
          href={`/jobs/${jobId}/invoice`}
          passHref
        >
          <Button
            color="primary"
            startIcon={<ReceiptIcon fontSize="small" />}
            sx={{ m: 1 }}
            variant="contained"
          >
            Invoice
          </Button>
        </NextLink>
        <NextLink
          href="/jobs/calendar"
          passHref
        >
          <Button
            color="primary"
            startIcon={<ArrowLeftIcon fontSize="small" />}
            variant="outlined"
          >
            Back
          </Button>
        </NextLink>
      </Breadcrumbs>
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Box sx={{ mt: 3 }}>
          <Tabs
            indicatorColor="primary"
            onChange={handleTabsChange}
            scrollButtons="auto"
            textColor="primary"
            value={currentTab}
            variant="scrollable"
          >
            {tabs.map((tab) => (
              <Tab
                key={tab.value}
                label={tab.label}
                value={tab.value}
              />
            ))}
          </Tabs>
        </Box>
        <Divider />
        <Box sx={{ mt: 3 }}>
          {currentTab === 'detail'
          && <JobOverview job={job} />}
          {currentTab === 'activity'
          && <JobActivities activities={[]} />}
        </Box>
      </Container>
    </Page>
  );
};

JobDetail.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default JobDetail;
