import React, { useEffect } from 'react';
import type { NextPage } from 'src/../next';
import { useRouter } from 'next/router';
import NextLink from 'next/link';
import {
  Box,
  Button,
  Container,
} from '@mui/material';
import { useSettings } from 'src/hooks/use-settings';
import ArrowLeftIcon from 'src/icons/ArrowLeft';
import { useDispatch, useSelector } from 'src/store';
import Page from 'src/components/Page';
import { InvoiceCreate } from 'src/components/invoice';
import { getJob } from 'src/slices/invoiceCreate';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';
import Breadcrumbs from 'src/components/Breadcrumbs';
import { BreadcrumbItem } from 'src/types/common';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Operations',
    routeTo: '/dashboard/overview'
  },
  {
    title: 'Jobs',
    routeTo: '/jobs/calendar'
  },
];

const JobInvoice: NextPage = () => {
  const router = useRouter();
  const { jobId } = router.query;
  const { settings } = useSettings();
  const dispatch = useDispatch();
  const { organisation } = useSelector((state) => state.account);
  const { job, isLoading } = useSelector((state) => state.invoiceCreate);

  useEffect(() => {
    // @ts-ignore
    dispatch(getJob(organisation.id, parseInt(jobId, 10)));
  }, [organisation, jobId]);

  if (isLoading || !job) {
    return null;
  }

  return (
    <Page
      title="Job invoice"
    >
      <Breadcrumbs
        title="Job invoice"
        items={breadcrumbItems}
      >
        <NextLink
          href={`/jobs/${jobId}`}
          passHref
        >
          <Button
            color="primary"
            startIcon={<ArrowLeftIcon fontSize="small" />}
            variant="outlined"
          >
            Back
          </Button>
        </NextLink>
      </Breadcrumbs>
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Box mt={3}>
          <InvoiceCreate job={job} />
        </Box>
      </Container>
    </Page>
  );
};

JobInvoice.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default JobInvoice;
