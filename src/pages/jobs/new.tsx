import React, { useCallback, useEffect, useState } from 'react';
import type { NextPage } from 'src/../next';
import NextLink from 'next/link';
import {
  Box,
  Button,
  Container,
  Grid,
} from '@mui/material';
import { RRule } from 'rrule';

import { useSettings } from '../../hooks/use-settings';
import ArrowLeftIcon from '../../icons/ArrowLeft';
import { useDispatch, useSelector } from '../../store';
import { getJob } from '../../slices/jobDetail';
import { JobNewForm, JobSummary } from '../../components/job';
import { useTheme } from '@mui/material/styles';
import Page from '../../components/Page';
import Breadcrumbs from '../../components/Breadcrumbs';
import type { BreadcrumbItem } from '../../types/common';
import LoadingScreen from '../../components/LoadingScreen';
import { AuthGuard } from '../../components/authentication/auth-guard';
import { MainLayout } from '../../layouts/main-layout';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Operations',
    routeTo: '/dashboard/overview'
  },
  {
    title: 'Calendar',
    routeTo: '/jobs/calendar'
  },
];

const JobNew: NextPage = () => {
  const { settings } = useSettings();
  const { isLoaded } = useSelector((state) => state.jobDetail);
  const [schedule, setSchedule] = useState<string | null>();
  const theme = useTheme();
  const dispatch = useDispatch();

  const handleChangeSchedule = useCallback((rRule: RRule) => setSchedule(rRule?.toText() || null), []);

  useEffect(() => {
    dispatch(getJob());
  }, []);

  if (!isLoaded) {
    return <LoadingScreen />;
  }

  return (
    <Page
      title="Job | New"
    >
      <Breadcrumbs
        title="Job"
        items={breadcrumbItems}
      >
        <NextLink
          href="/jobs/calendar"
          passHref
        >
          <Button
            color="primary"
            startIcon={<ArrowLeftIcon fontSize="small" />}
            sx={{ mt: 1 }}
            variant="outlined"
          >
            Cancel
          </Button>
        </NextLink>
      </Breadcrumbs>
      <Container
        maxWidth={settings.compact ? 'xl' : false}
        sx={{
          backgroundColor: 'background.default',
          display: 'flex',
          flexDirection: 'column',
          flexGrow: 1
        }}
      >
        <Box sx={{ mt: 3 }}>
          <Grid
            container
            spacing={3}
          >
            <Grid
              item
              md={7}
              xs={12}
            >
              <JobNewForm onChangeRecurrence={handleChangeSchedule} />
            </Grid>
            <Grid
              item
              md={5}
              sx={{
                display: { xs: 'none', md: 'block' }
              }}
            >
              <JobSummary
                schedule={schedule}
                sx={{
                  position: 'sticky',
                  top: theme.spacing(3),
                  minWidth: '275',
                }}
              />
            </Grid>
          </Grid>
        </Box>
      </Container>
    </Page>
  );
};

JobNew.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default JobNew;
