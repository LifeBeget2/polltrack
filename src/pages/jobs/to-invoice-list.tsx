import React, { useEffect } from 'react';
import type { NextPage } from 'src/../next';
import {
  Box,
  Container,
} from '@mui/material';
import { JobToInvoiceListTable } from 'src/components/job';
import { getJobsToInvoice } from 'src/slices/jobToInvoice';
import Page from 'src/components/Page';
import { useDispatch, useSelector } from 'src/store';
import { useSettings } from 'src/hooks/use-settings';
import Breadcrumbs from 'src/components/Breadcrumbs';
import type { BreadcrumbItem } from 'src/types/common';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Operations',
    routeTo: '/dashboard/overview'
  },
];

const JobToInvoiceList: NextPage = () => {
  const { settings } = useSettings();
  const { organisation } = useSelector((state) => state.account);
  const { selectedRange, jobsToInvoice, statusFilter, limit, page, searchText, order, orderBy } = useSelector((state) => state.jobToInvoice);
  const dispatch = useDispatch();

  useEffect(() => {
    const sortBy = orderBy ? `${order === 'desc' ? '-' : ''}${orderBy}` : '';
    dispatch(getJobsToInvoice(organisation.id, selectedRange, limit, page, statusFilter, searchText, sortBy));
  }, [organisation, selectedRange, statusFilter, limit, page, searchText, order, orderBy]);

  return (
    <Page
      title="Job | To invoice"
    >
      <Breadcrumbs
        title="Jobs to invoice"
        items={breadcrumbItems}
      />
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Box sx={{ mt: 3 }}>
          <JobToInvoiceListTable jobsToInvoice={jobsToInvoice} />
        </Box>
      </Container>
    </Page>
  );
};

JobToInvoiceList.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default JobToInvoiceList;
