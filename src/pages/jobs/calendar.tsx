import React, { useEffect } from 'react';
import type { NextPage } from 'src/../next';
import {
  Box,
  Button,
} from '@mui/material';
import Page from 'src/components/Page';
import { useDispatch, useSelector } from 'src/store';
import { init } from 'src/slices/jobCalendar';
import { JobCalendar as JobCalendarComponent } from 'src/components/job';
import Breadcrumbs from 'src/components/Breadcrumbs';
import type { BreadcrumbItem } from 'src/types/common';
import LoadingScreen from 'src/components/LoadingScreen';
import NextLink from 'next/link';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Operations',
    routeTo: '/dashboard/overview'
  },
];
const JobCalendar: NextPage = () => {
  const { organisation } = useSelector((state) => state.account);
  console.log(organisation);
  const { isInitialised } = useSelector(
    (state) => state.jobCalendar
  );
  const dispatch = useDispatch();

  useEffect(
    () => {
      if (organisation) {
        dispatch(init(organisation.id));
      }
    },
    []
  );

  if (!isInitialised) {
    return <LoadingScreen />;
  }

  return (
    <Page
      title="Job | Calendar"
    >
      <Breadcrumbs
        title="Calendar"
        items={breadcrumbItems}
      >
        <NextLink
          href="/jobs/new"
          passHref
        >
          <Button
            color="primary"
            sx={{
              textTransform: 'uppercase'
            }}
            variant="contained"
          >
            Create job
          </Button>
        </NextLink>
      </Breadcrumbs>
      <Box sx={{ m: 3 }}>
        <JobCalendarComponent />
      </Box>
    </Page>
  );
};

JobCalendar.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default JobCalendar;
