import type { FC } from 'react';
import type { NextPage } from 'src/../next';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';
import NextLink from 'next/link';
import { useRouter } from 'next/router';
import {
  Box,
  Button,
  Container,
} from '@mui/material';
import { useSettings } from 'src/hooks/use-settings';
import ArrowLeftIcon from 'src/icons/ArrowLeft';
import Page from 'src/components/Page';
import { CampaignForm } from 'src/components/campaign';
import React, { useCallback, useEffect, useState } from 'react';
import axios from 'axios';
import { apiConfig } from 'src/config';
import type { Campaign } from 'src/types/campaign';
import { useSelector } from 'src/store';
import useIsMountedRef from 'src/hooks/useIsMountedRef';
import Breadcrumbs from 'src/components/Breadcrumbs';
import type { BreadcrumbItem } from 'src/types/common';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Operations',
    routeTo: '/dashboard/overview'
  },
  {
    title: 'Campaigns',
    routeTo: '/campaigns'
  },
];

const CampaignEdit: NextPage = () => {
  const router = useRouter();
  const { campaignId } = router.query;
  const isMountedRef = useIsMountedRef();
  const { settings } = useSettings();
  const { organisation } = useSelector((state) => state.account);
  const [campaign, setCampaign] = useState<Campaign | null>(null);

  const getCampaign = useCallback(async () => {
    try {
      // TODO: move to the slice
      const response = await axios.get(`${apiConfig.apiV2Url}/organisations/${organisation.id}/campaign/${campaignId}`);

      if (isMountedRef.current) {
        setCampaign(response.data);
      }
    } catch (err) {
      console.error(err);
    }
  }, [isMountedRef]);

  useEffect(() => {
    getCampaign();
  }, [getCampaign]);

  if (!campaign) {
    return null;
  }

  return (
    <Page
      title="Campaign | Edit"
    >
      <Breadcrumbs
        title="Edit campaign"
        items={breadcrumbItems}
      >
        <NextLink
          href="/campaigns"
          passHref
        >
          <Button
            color="primary"
            startIcon={<ArrowLeftIcon fontSize="small" />}
            variant="outlined"
          >
            Cancel
          </Button>
        </NextLink>
      </Breadcrumbs>
      <Container maxWidth={settings.compact ? 'lg' : false}>
        <Box mt={3}>
          <CampaignForm campaign={campaign} />
        </Box>
      </Container>
    </Page>
  );
};

CampaignEdit.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default CampaignEdit;
