import React, { useEffect } from 'react';
import type { NextPage } from 'src/../next';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';
import NextLink from 'next/link';
import {
  Box,
  Button,
  Container,
} from '@mui/material';
import { useSettings } from 'src/hooks/use-settings';
import PlusIcon from 'src/icons/Plus';
import { CampaignList as CampaignCardsList } from 'src/components/campaign';
import { getCampaigns } from '../../slices/campaign';
import { useDispatch, useSelector } from '../../store';
import Page from '../../components/Page';
import Breadcrumbs from '../../components/Breadcrumbs';
import type { BreadcrumbItem } from '../../types/common';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Operations',
    routeTo: '/dashboard/overview'
  },
];

const CampaignList: NextPage = () => {
  const { settings } = useSettings();
  const { organisation } = useSelector((state) => state.account);
  const { campaigns, limit, page, searchText, order, orderBy } = useSelector((state) => state.campaign);
  const dispatch = useDispatch();

  useEffect(() => {
    const sortBy = orderBy ? `${order === 'desc' ? '-' : ''}${orderBy}` : '';
    dispatch(getCampaigns(organisation.id, limit, page, searchText, sortBy));
  }, [organisation, limit, page, searchText, order, orderBy]);

  return (
    <Page
      title="Campaign | List"
    >
      <Breadcrumbs
        title="Campaigns list"
        items={breadcrumbItems}
      >
        <NextLink
          href="/campaigns/new"
          passHref
        >
          <Button
            color="primary"
            startIcon={<PlusIcon fontSize="small" />}
            variant="contained"
          >
            New campaign
          </Button>
        </NextLink>
      </Breadcrumbs>
      <Container maxWidth={settings.compact ? 'xl' : false}>
        <Box sx={{ mt: 3 }}>
          <CampaignCardsList campaigns={campaigns} />
        </Box>
      </Container>
    </Page>
  );
};

CampaignList.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default CampaignList;
