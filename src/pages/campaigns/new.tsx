import React from 'react';
import type { NextPage } from 'src/../next';
import { AuthGuard } from 'src/components/authentication/auth-guard';
import { MainLayout } from 'src/layouts/main-layout';
import NextLink from 'next/link';
import {
  Box,
  Button,
  Container,
} from '@mui/material';
import { useSettings } from 'src/hooks/use-settings';
import ArrowLeftIcon from 'src/icons/ArrowLeft';
import Page from 'src/components/Page';
import { CampaignForm } from 'src/components/campaign';
import Breadcrumbs from 'src/components/Breadcrumbs';
import type { BreadcrumbItem } from 'src/types/common';

const breadcrumbItems: BreadcrumbItem[] = [
  {
    title: 'Operations',
    routeTo: '/dashboard/overview'
  },
  {
    title: 'Campaigns',
    routeTo: '/campaigns'
  },
];

const CampaignNew: NextPage = () => {
  const { settings } = useSettings();

  return (
    <Page
      title="Campaign | New"
    >
      <Breadcrumbs
        title="New campaign"
        items={breadcrumbItems}
      >
        <NextLink
          href="/campaigns"
          passHref
        >
          <Button
            color="primary"
            startIcon={<ArrowLeftIcon fontSize="small" />}
            variant="outlined"
          >
            Cancel
          </Button>
        </NextLink>
      </Breadcrumbs>
      <Container maxWidth={settings.compact ? 'lg' : false}>
        <Box mt={3}>
          <CampaignForm />
        </Box>
      </Container>
    </Page>
  );
};

CampaignNew.getLayout = (page) => (
  <AuthGuard>
    <MainLayout>
      {page}
    </MainLayout>
  </AuthGuard>
);

export default CampaignNew;
