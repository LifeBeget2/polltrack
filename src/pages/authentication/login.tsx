import { useEffect } from 'react';
import type { NextPage } from 'next';
import { GuestGuard } from 'src/components/authentication/guest-guard';
import { JWTLogin } from 'src/components/authentication/jwt-login';
import {
  Box,
  Card,
  CardContent,
  Container,
  Typography
} from '@mui/material';
import AppLogo from 'src/components/AppLogo';
import Head from 'next/head';
import NextLink from 'next/link';
import { gtm } from 'src/lib/gtm';

const Login: NextPage = () => {
  useEffect(() => {
    gtm.push({ event: 'page_view' });
  }, []);

  return (
    <>
      <Head>
        <title>
          Authentication | Login
        </title>
      </Head>
      <Box
        component="main"
        sx={{
          backgroundColor: 'background.default',
          display: 'flex',
          flexDirection: 'column',
          minHeight: '100vh'
        }}
      >
        <Container
          maxWidth="sm"
          sx={{ py: '80px' }}
        >
          <Box
            sx={{
              display: 'flex',
              justifyContent: 'center',
              mb: 8
            }}
          >
            <NextLink
              href="/"
              passHref
            >
              <a>
                <AppLogo />
              </a>
            </NextLink>
          </Box>
          <Card>
            <CardContent
              sx={{
                display: 'flex',
                flexDirection: 'column',
                p: 4
              }}
            >
              <Box
                sx={{
                  alignItems: 'center',
                  display: 'flex',
                  justifyContent: 'space-between',
                  mb: 3
                }}
              >
                <div>
                  <Typography
                    color="textPrimary"
                    gutterBottom
                    variant="h4"
                  >
                    Log in
                  </Typography>
                </div>
              </Box>
              <Box
                sx={{
                  flexGrow: 1,
                  mt: 3
                }}
              >
                <JWTLogin />
              </Box>
            </CardContent>
          </Card>
        </Container>
      </Box>
    </>
  );
};

Login.getLayout = (page) => (
  <GuestGuard>
    {page}
  </GuestGuard>
);

export default Login;
