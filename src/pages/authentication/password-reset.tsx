import { useEffect } from 'react';
import type { NextPage } from 'next';
import { GuestGuard } from 'src/components/authentication/guest-guard';
import {
  Box,
  Card,
  CardContent,
  Container,
  Typography
} from '@mui/material';
import AppLogo from 'src/components/AppLogo';
import PasswordReset from 'src/components/authentication/password-reset';
import Head from 'next/head';
import NextLink from 'next/link';
import { gtm } from 'src/lib/gtm';

const PasswordResetPage: NextPage = () => {
  useEffect(() => {
    gtm.push({ event: 'page_view' });
  }, []);

  return (
    <>
      <Head>
        <title>
          Authentication | Password reset
        </title>
      </Head>
      <Box
        sx={{
          backgroundColor: 'background.default',
          display: 'flex',
          flexDirection: 'column',
          minHeight: '100vh'
        }}
      >
        <Container
          maxWidth="sm"
          sx={{ py: 10 }}
        >
          <Box
            sx={{
              display: 'flex',
              justifyContent: 'center'
            }}
          >
            <NextLink
              href="/"
              passHref
            >
              <a>
                <AppLogo />
              </a>
            </NextLink>
          </Box>
          <Box
            sx={{
              display: 'flex',
              justifyContent: 'center',
              mb: 8
            }}
          />
          <Card>
            <CardContent
              sx={{
                display: 'flex',
                flexDirection: 'column',
                p: 4
              }}
            >
              <Box
                sx={{
                  alignItems: 'center',
                  display: 'flex',
                  justifyContent: 'space-between',
                  mb: 3
                }}
              >
                <div>
                  <Typography
                    color="textPrimary"
                    gutterBottom
                    variant="h4"
                  >
                    Password reset
                  </Typography>
                </div>
              </Box>
              <Box
                sx={{
                  flexGrow: 1,
                  mt: 3
                }}
              >
                <PasswordReset />
              </Box>
            </CardContent>
          </Card>
        </Container>
      </Box>
    </>
  );
};

PasswordResetPage.getLayout = (page) => (
  <GuestGuard>
    {page}
  </GuestGuard>
);

export default PasswordResetPage;
