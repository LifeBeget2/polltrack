export const SMS_STATUS_DELIVERED = 'delivered';
export const SMS_STATUS_UNDELIVERED = 'undelivered';
export const SMS_STATUS_QUEUED = 'queued';
export const SMS_STATUS_SENT = 'sent';
export const SMS_STATUS_ERROR = 'error';
export const SMS_STATUS_LOW_BALANCE = 'low_balance';
export const SMS_STATUS_FAILED = 'failed';

export const SUCCESS_STATUSES = [SMS_STATUS_DELIVERED, SMS_STATUS_SENT];
export const NEUTRAL_STATUSES = [SMS_STATUS_QUEUED];
export const ERROR_STATUSES = [
  SMS_STATUS_UNDELIVERED,
  SMS_STATUS_LOW_BALANCE,
  SMS_STATUS_ERROR,
  SMS_STATUS_FAILED,
];
