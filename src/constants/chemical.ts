export const CLASSIFICATION_TYPE = 'classification';
export const CUSTOM_TYPE = 'custom';
export const GROUND_LEVEL_TYPE = 'ground_level';
export const LOCATION_TYPE = 'location';
export const POOL_TYPE = 'pool_type';
export const SANITISER_TYPE = 'sanitiser';
export const SURFACE_TYPE = 'surface';
export const TEMPERATURE_TYPE = 'temperature';

export const LOW_TEMPERATURE = '< 28 degrees';
export const HIGH_TEMPERATURE = '>= 28 degrees';

export const EXCEPTION_TYPES = [
  {
    value: CLASSIFICATION_TYPE,
    label: 'Classification',
  },
  {
    value: CUSTOM_TYPE,
    label: 'Custom',
  },
  {
    value: GROUND_LEVEL_TYPE,
    label: 'Ground Lvl',
  },
  {
    value: LOCATION_TYPE,
    label: 'Location',
  },
  {
    value: POOL_TYPE,
    label: 'Pool Type',
  },
  {
    value: SANITISER_TYPE,
    label: 'Sanitiser',
  },
  {
    value: SURFACE_TYPE,
    label: 'Surface',
  },
  {
    value: TEMPERATURE_TYPE,
    label: 'Temperature',
  }
];
