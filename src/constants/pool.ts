export const POOL_TYPE = 1;
export const SPA_TYPE = 2;
export const SWIM_SPA_TYPE = 3;

export const POOL_LABEL = 'Pool';
export const SPA_LABEL = 'Spa';
export const SWIM_SPA_LABEL = 'Swim Spa';

export const POOL_TYPES_LIST = [
  {
    value: POOL_TYPE,
    label: POOL_LABEL,
  },
  {
    value: SPA_TYPE,
    label: SPA_LABEL,
  },
  {
    value: SWIM_SPA_TYPE,
    label: SWIM_SPA_LABEL,
  },
];

export const AFM_FILTER = 'afm';
export const CARTRIDGE_FILTER = 'cartridge';
export const DE_FILTER = 'de';
export const GLASS_FILTER = 'glass';
export const SAND_FILTER = 'sand';

export const AFM_FILTER_LABEL = 'AFM';
export const CARTRIDGE_FILTER_LABEL = 'Cartridge';
export const DE_FILTER_LABEL = 'DE';
export const GLASS_FILTER_LABEL = 'Glass';
export const SAND_FILTER_LABEL = 'Sand';

export const FILTERS_LIST = [
  {
    value: AFM_FILTER,
    label: AFM_FILTER_LABEL,
  },
  {
    value: CARTRIDGE_FILTER,
    label: CARTRIDGE_FILTER_LABEL,
  },
  {
    value: DE_FILTER,
    label: DE_FILTER_LABEL,
  },
  {
    value: GLASS_FILTER,
    label: GLASS_FILTER_LABEL,
  },
  {
    value: SAND_FILTER,
    label: SAND_FILTER_LABEL,
  },
];
