export const LAB_EMAIL_MERGE_TAGS = {
  ORG_NAME: '{org_name}',
  ORG_ADDRESS: '{org_address}',
  ORG_EMAIL: '{org_email}',
  ORG_PHONE: '{org_phone}',
  ORG_WEBSITE: '{org_website}',
  CONTACT_FIRST_NAME: '{contact_first_name}',
  CONTACT_LAST_NAME: '{contact_last_name}',
  CONTACT_ADDRESS: '{contact_address}',
  SYSTEM_ADDRESS: '{system_address}',
  TEST_RESULTS_URL: '{test_results_url}',
  NEXT_TEST_DATE: '{next_test_date}',
  TEST_DATE: '{test_date}',
};
