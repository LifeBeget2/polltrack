import { ASSISTANT, GUARD, HOME, MOBILE, OFFICE, PHONE } from 'src/constants/contact';
import type { Pool } from './pool';
import type { Note } from './note';

export interface Contact {
  id?: number;
  organisation_id: number;
  first_name: string | null;
  last_name: string | null;
  full_name?: string;
  customer_code?: string;
  type: number;
  invoice_pref: number;
  email: string | null;
  contact_name: string | null;
  company_name: string | null;
  cc_emails: string | null;
  notes: string | null;
  contact_notes?: Note[];
  full_address?: string | null;
  address_street_one: string | null;
  address_street_two: string | null;
  address_postcode: string | null;
  address_state: string | null;
  address_city?: string | null;
  address_country: string | null;
  accounting_sync_message?: string | null;
  accounting_sync_status?: string | null;
  is_vend_contact_deleted?: boolean;
  xero_contact_id?: string;
  vend_contact_id?: string;
  pronto_contact_id?: string;
  // TODO: add vend; xero and pronto contact interfaces
  visibility: boolean;
  is_batch: boolean;
  trading_term_id?: number;
  trading_term?: TradingTerm;
  bgw_comms_pref?: CommsPref[];
  pools: Pool[];
  phones: Phone[];
  pivot?: PoolPivot;
  updated_at?: string;
  created_at?: string;
  submit?: boolean;
}

export interface VendContact {
  id: number;
  first_name: string;
  last_name: string;
  name: string;
  email: string;
  contact_id: number;
  company_name: string;
  customer_code: string;
  customer_group_id: string;
  physical_address_1: string;
  physical_address_2: string;
  physical_city: string;
  physical_postcode: string;
  physical_state: string;
  physical_suburb: string;
  postal_address_1: string;
  postal_address_2: string;
  postal_country_id: string;
  postal_postcode: string;
  postal_city: string;
  postal_state: string;
  postal_suburb: string;
  vend_contact_id: string;
  balance: string;
  mobile: string;
  phone: string;
  invoice_pref: number;
}

export interface CommsPref {
  id: string;
  name: string;
  value: boolean;
}

export interface Phone {
  id?: number;
  phone_type_id?: PhoneType;
  phone_type?: PhoneTypeInt;
  phone_number: string;
  label?: string | null;
}

export interface PhoneOption {
  value: PhoneType;
  label: string;
}

export interface TradingTerm {
  id?: number;
  organisation_id?: number;
  name: string;
  descriptive_name?: string;
  from_type: string;
  type: string;
  // from_type: 'from_invoice' | 'from_month';
  // type: 'weeks' | 'days' | 'months';
  quantity: number;
  is_default: boolean;
  updated_at?: string;
  created_at?: string;
  submit: boolean;
}

export interface PhoneTypeInt {
  id?: number;
  name: string;
}

export type PhoneType =
  | typeof PHONE
  | typeof MOBILE
  | typeof HOME
  | typeof OFFICE
  | typeof ASSISTANT
  | typeof GUARD;

export type TradingTermType =
  | 'weeks'
  | 'days'
  | 'months';

export type TradingTermFromType =
  | 'from_invoice'
  | 'from_month';

export interface PoolPivot {
  address_type_id: number;
  contact_id: number;
  pool_id: number;
}
