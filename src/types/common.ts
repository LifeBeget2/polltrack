export interface BreadcrumbItem {
  title: string;
  routeTo?: string;
}

export interface SelectListItem {
  id?: number;
  name: string;
  enabled: boolean;
}

export interface PdfInstance {
  loading: boolean;
  blob: Blob | null;
  url: string | null;
  error: string | null;
}
