export interface ClassicMetric {
  current_month: number;
  is_currency: boolean;
  last_month: number;
  ratio: string;
  title: string;
  metricType: string;
  today: number;
  yesterday: number;
}

export interface ChartData {
  chart_type: ChartType,
  data_interval: ChartDataInterval;
  from_date: string;
  from_date_utc: string;
  jobs?: ChartDataSet;
  waterTests?: ChartDataSet;
  revenue?: ChartDataSet;
  avgInvoice?: ChartDataSet;
  xaxis: string[] | number[];
}

export interface ChartDataInterval {
  start_month: string;
  end_month: string;
  date1: string;
  date2: string;
}

export interface ChartDataSet {
  title: string;
  label: string;
  data_set: number[],
  left_val: number;
  right_val: number;
}

export interface MostRequestedJob {
  job_template_name: string;
  count: number;
  total_hours: number;
}

export interface ProductTabCategory {
  name :string,
  total_sales: number,
}

export interface ProductTabProduct {
  brand: string,
  name :string,
  total_sales: number,
}

export interface ProductTabData {
  products: ProductTabProduct[],
  categories: ProductTabCategory[],
}

export interface StaffTabItem {
  id: number,
  name: string,
  number_of_jobs: number,
  number_of_tests: number,
  total_invoiced: number,
  hours_worked: number | string,
}

export interface CustomerTabItem {
  id: number,
  most_requested_job: string,
  name: string,
  number_of_jobs: number,
  pool_address: string,
  total_invoiced: number,
}

export type ChartType =
  | 'bar'
  | 'linear';

export type ChartServiceType =
  | 'jobs'
  | 'revenue'
  | 'avgInvoice'
  | 'waterTests';

export type RangeType =
  | 100
  | 200
  | 300
  | 400;
