import type { ObservationTestResult } from './waterTest';

export interface ChemicalTest {
  id: number;
  organisation_id?: number;
  name: string;
  low_pdf_msg: string;
  high_pdf_msg: string;
  minimum_value: number;
  maximum_value: number;
  minimum: RangeType,
  maximum: RangeType,
  target_value: number;
  is_default: boolean;
  chemical_groups: ChemicalGroup[] | [];
  exceptions: ChemicalTestException[] | [];
  updated_at: string;
  created_at: string;
  submit?: boolean;
}

export interface ChemicalTestException {
  id?: number | string;
  chemical_test_id: number;
  exception_name: string;
  exception_type?: ChemicalTestExceptionType;
  exception_id?: number;
  min_value?: number | null;
  max_value?: number | null;
  target_value?: number | null;
  updated_at?: string;
  created_at?: string;
}

export interface ObservationTest {
  id?: number;
  organisation_id?: number;
  name: string;
  report_message: string;
  observation_logical_schema?: string;
  is_default: boolean;
  on_by_default?: boolean;
  is_note?: boolean;
  is_maintenance?: boolean;
  is_lab_only?: boolean;
  pool_types?: PoolType[] | [];
  order: number;
  observation_groups?: ObservationGroup[] | [];
  updated_at?: string;
  created_at?: string;
  submit?: string | null;
  result?: ObservationTestResult;
}

export interface ChemicalGroup {
  id: number;
  organisation_id: number;
  name: string;
  chemical_test: ChemicalTest;
  updated_at: string;
  created_at: string;
}

export interface ObservationGroup {
  id: number;
  organisation_id: number;
  name: string;
  observation_test: ObservationTest;
  updated_at: string;
  created_at: string;
}

export interface PoolType {
  id: number;
  name: string;
}

export interface RangeType {
  value: string;
}

export type ChemicalTestExceptionType =
  | 'classification'
  | 'custom'
  | 'ground_level'
  | 'location'
  | 'sanitiser'
  | 'surface'
  | 'temperature'
  | 'pool_type';
