import type { Pool } from './pool';
import type { Contact } from './contact';
import type { User } from './user';

export interface TestHistory {
  id: number;
  organisation_id: number;
  contact: Contact | null;
  contact_id: number;
  email: string | null;
  email_reminder_sent: boolean;
  is_s3_moved: boolean | number;
  pdf_compact_location: string | null;
  pdf_location: string | null;
  pool: Pool;
  pool_id: number;
  sms_reminder_sent: boolean;
  start_time: string;
  user: User;
  user_id: number;
  updated_at: string;
  created_at: string;
}

export interface CombinedTestHistory {
  id: number;
  organisation_id: number;
  contact_id: number;
  pool_id: number;
  contact: Contact;
  results: TestHistoryResult[];
  analysis_date: string;
  start_time: string;
  is_from_lab: boolean;
  pdf_location: string;
  end_time: string;
  updated_at: string;
  created_at: string;
}

export interface TestHistoryResult {
  name: string,
  status: TestHistoryResultStatus,
  value: string | number,
}

export type TestHistoryResultStatus =
  | 'GOOD'
  | 'HIGH'
  | 'LOW';
