import {
  SMS_STATUS_DELIVERED,
  SMS_STATUS_ERROR,
  SMS_STATUS_FAILED,
  SMS_STATUS_LOW_BALANCE,
  SMS_STATUS_QUEUED,
  SMS_STATUS_SENT,
} from 'src/constants/sms';

import { Contact } from './contact';

export type SmsStatus =
  | typeof SMS_STATUS_DELIVERED
  | typeof SMS_STATUS_DELIVERED
  | typeof SMS_STATUS_QUEUED
  | typeof SMS_STATUS_SENT
  | typeof SMS_STATUS_ERROR
  | typeof SMS_STATUS_LOW_BALANCE
  | typeof SMS_STATUS_FAILED;

interface SmsContact {
  data: Contact;
}

export interface Sms {
  id: number;

  organisation_id: number;
  job_id: number;
  contact_id: number;

  created_at: string;
  send_date: string;

  status: SmsStatus;
  type: string;

  phone_number: string;

  message: string;
  error_message?: string;

  contact: SmsContact;
}

export interface SmsTier {
  endingUnit: number;
  object: string;
  price: number;
  startingUnit: number;
}
