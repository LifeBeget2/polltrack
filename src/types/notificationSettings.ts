export interface NotificationSettings {
  auto_buy_enabled: boolean;
  created_at: string;
  id: number,
  low_sms_balance_notification_sent: number;
  low_sms_limit: number;
  organisation_id: number;
  purchased_sms_count: number;
  remaining_sms_count: number;
  sms_notifications_enabled: boolean;
  sms_pack: number;
  sms_pack_price: number;
  updated_at: string;
}
