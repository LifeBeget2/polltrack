export interface ChartData {
  title: string;
  labels: string[];
  color: string;
  series: string[] | number[];
}
