import type { Phone, VendContact } from './contact';
import type { VendProduct } from './product';

export interface ContactMatch {
  id: number;
  address_city?: string;
  address_country?: string;
  address_postcode?: string;
  address_state?: string;
  address_street_one?: string;
  address_street_two?: string;
  cc_emails?: string;
  company_name?: string;
  contact_name?: string;
  email?: string;
  first_name?: string;
  full_address?: string;
  full_name?: string;
  phones?: Phone[];
  matchable: {
    id: number;
    is_matched: boolean;
    probability: number;
    matched_fields: {
      address_street_one: boolean;
      company_name: boolean;
      email: boolean;
      first_name: boolean;
      last_name: boolean;
    },
    vend_contact: VendContact,
  }[],
}

export interface ProductMatch {
  id: number;
  name: string;
  sku: string;
  unit_price: number;
  matchable: {
    id: number;
    is_matched: boolean;
    probability: number;
    matchable_id: number;
    match: VendProduct;
    matched_fields: {
      name: boolean;
      sku: boolean;
    },
  }[],
}

export interface Register {
  id: number;
  name: string;
}

export interface UserRegister {
  id: number;
  register_id: number;
  user_id: number;
  vend_user_id: number;
}
