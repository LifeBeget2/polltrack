export interface Meta {
  current_page: number;
  from: number;
  last_page: number;
  per_page: string;
  to: number;
  total: number;
}
