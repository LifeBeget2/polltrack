export enum DuplicationPoolSwitcher {
  linkedContact = 'by_linked_contact',
  address = 'by_address',
  poolType = 'by_pool_type',
  surface = 'by_surface',
  sanitiser = 'by_sanitiser',
}
