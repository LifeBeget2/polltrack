export interface InvoiceSettings {
  id: number;
  organisation_id: number;
  cc_email: string | null;
  disclaimer: string | null;
  email_signature: string | null;
  intro_message: string | null;
  invoice_due_days: number;
  invoice_prefix: string | null;
  payment_message: string | null;
  reply_to: string | null;
  settings: Settings;
  subject_message: string | null;
  theme_color: string | null;
  created_at: string | null;
  updated_at: string | null;
  deleted_at: string | null;
}

export interface Settings {
  chemical_actions: boolean | string;
  chemical_history: boolean | string;
  tasks_list: boolean | string;
  use_invoice_sign_off: boolean | string;
  display_invoice_notes: boolean | string;
  water_result: boolean | string;
}
