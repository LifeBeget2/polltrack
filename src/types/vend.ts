export interface VendOrganisation {
  is_synced: boolean;

  is_syncing_contacts: boolean;
  is_syncing_products: boolean;

  is_matching_contacts: number;
  is_matching_products: number;

  last_contact_updated_at: string;
  last_product_updated_at: string;

  domain_prefix: string;
}
