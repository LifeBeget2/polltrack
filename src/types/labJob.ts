import type { Pool } from './pool';
import type { Contact } from './contact';
import type { User } from './user';
import type { ChemicalTestResult, ObservationTestResult } from './waterTest';

export interface LabJob {
  id?: number;
  organisation_id?: number;
  user_id: string | number | null;
  user?: User | null;
  pool_id: number | null;
  contact_id: number | null;
  pool?: Pool | null,
  contact?: Contact | null,
  start_time?: string,
  end_time?: string,
  pdf_location?: string,
  pdf_compact_location?: string,
  chemical_results?: ChemicalTestResult[],
  observation_results?: ObservationTestResult[],
  created_at?: string,
}
