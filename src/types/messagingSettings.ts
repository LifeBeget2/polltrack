export interface MessagingSettings {
  id: number;
  organisation_id: number;

  created_at: string;
  updated_at: string;
  deleted_at?: string;

  booking_reminder_sms_before_hours: number;
  booking_reminder_sms_enabled: boolean;
  booking_reminder_sms_message: string;

  booking_sms_enabled: boolean;
  booking_sms_message: string;

  job_complete_sms_enabled: boolean;
  job_complete_sms_message: string;
}
