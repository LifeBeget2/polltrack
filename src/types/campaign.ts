export interface Campaign {
  id?: number;
  organisation_id?: number;
  start_date: string;
  expiry_date?: string;
  name: string;
  banner_name?: string;
  banner_url?: string;
  url: string;
  email_enabled: boolean;
  invoice_enabled: boolean;
  job_sheet_enabled: boolean;
  water_test_enabled: boolean;
  created_at?: string;
  updated_at?: string;
  submit: boolean;
}
