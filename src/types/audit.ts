import type { User } from './user';

export interface Audit {
  id: number;
  user_id: number;
  event: AuditEvent;
  auditable_type: string;
  auditable_id: number;
  old_values?: object;
  new_values?: object;
  url: string;
  ip_address: string;
  user_agent: string;
  tags: string | null;
  description?: string | null;
  created_at: string;
  updated_at: string;
  user: User;
}

export type AuditEvent =
  | 'created'
  | 'updated'
  | 'deleted'
  | 'related';
