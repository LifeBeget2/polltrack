import type { ChemicalGroup, ChemicalTest, ObservationGroup, ObservationTest } from './chemical';

export interface ChemicalTestResult {
  id?: number;
  value: string;
  min_value: number | null;
  max_value: number | null;
  target_value: number | null;
  name: string;
  status: ResultStatus;
  action: string;
  description: string;
  short_description?: string;
  show_on_report: boolean;
  enabled: boolean;
  is_saved: boolean;
  chemical_test_id: number;
  chemical_test: ChemicalTest;
  chemical_group_id?: number | null;
  chemical_group?: ChemicalGroup | null;
}

export interface ObservationTestResult {
  id?: number;
  value: boolean;
  name: string;
  action: string;
  description: string;
  short_description?: string;
  show_on_report: boolean;
  enabled: boolean;
  is_saved: boolean;
  observation_test_id: number;
  observation_test: ObservationTest;
  observation_group_id?: number | null;
  observation_group?: ObservationGroup | null;
}

export type ResultStatus =
  | 'high'
  | 'low'
  | 'ok';
