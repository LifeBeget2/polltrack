import type { Contact } from './contact';
import type { JobTemplate } from './jobTemplate';

export interface Quote {
  id: number,
  organisation_id: number,
  contact_id: number,
  contact: Contact,
  job_template_id: number,
  job_template: JobTemplate,
  amount: number,
  updated_at: string,
  created_at: string,
}

export interface ArchivedQuote {
  id: number,
  organisation_id: number,
  contact_id: number,
  contact: Contact,
  amount: number,
  job_template_name: string | null,
  contact_first_name: string | null,
  contact_last_name: string | null,
  pdf_location: string | null,
  status: ArchivedQuoteStatus,
  updated_at: string,
  created_at: string,
}

export type ArchivedQuoteStatus =
  | 'scheduled'
  | 'sent';
