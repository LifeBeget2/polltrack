import type { Invoice as Product } from './job';
import type { Contact } from './contact';

interface InvoiceCustomer {
  address?: string;
  company?: string;
  email: string;
  name: string;
  taxId?: string;
}

interface InvoiceItem {
  id: string;
  currency: string;
  description: string;
  unitAmount: number;
}

export type InvoiceStatus =
  | 'canceled'
  | 'paid'
  | 'pending';

export interface Invoice {
  id: string;
  currency: string;
  customer: InvoiceCustomer;
  dueDate?: number;
  issueDate?: number;
  items?: InvoiceItem[];
  number?: string;
  status: InvoiceStatus;
  subtotalAmount?: number;
  taxAmount?: number;
  totalAmount?: number;
}

export interface BatchSettings {
  id: number;
  organisation_id: number;
  cc_emails: string | null;
  intro_message: string | null;
  prefix: string | null;
  reply_to: string | null;
  attach_job_sheets: boolean;
  attach_individual_invoices: boolean;
  send_job_sheet_on_complete: boolean;
  subject_message: string | null;
  created_at: string | null;
  updated_at: string | null;
  deleted_at: string | null;
}

export interface QuoteSettings {
  id: number;
  organisation_id: number;
  cc_email: string | null;
  email_signature: string | null;
  intro_message: string | null;
  reply_to: string | null;
  resend_after: number;
  resend_days: number;
  resend_quote: boolean;
  subject_message: string | null;
  settings: QuoteSettingsParams;
  created_at: string | null;
  updated_at: string | null;
  deleted_at: string | null;
}

export interface QuoteSettingsParams {
  invoice_sign_off: boolean | string;
  tasks_list: boolean | string;
}

export interface InvoiceRecipient {
  number?: number;
  job_id: number;
  contact_id: number;
  contact: Contact;
  subject_message: string;
  contact_intro: string;
  email?: string;
  products?: Product[];
  display_invoice: boolean;
  display_tasks: boolean;
  display_water_tests: boolean;
  display_chemical_history: boolean;
  display_chemical_actions: boolean;
}
