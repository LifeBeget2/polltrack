import type { User } from './user';

export interface Note {
  id: number;
  user_id: number;
  event: AuditEvent;
  notable_type: string;
  notable_id: number;
  user_agent: string;
  tags?: string[];
  title: string;
  description?: string | null;
  created_at: string;
  updated_at: string;
  user: User;
  files?: NoteFile[];
}

export interface NoteFile {
  id: string;
  mimeType: string;
  name: string;
  size: number;
  url: string;
}

export type AuditEvent =
  | 'created'
  | 'updated'
  | 'deleted'
  | 'related';
