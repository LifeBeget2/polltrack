import type { Pool } from './pool';
import type { Contact } from './contact';
import type { JobTemplate } from './jobTemplate';
import type { User } from './user';
import type { Product } from './product';
import type { Color } from './organisation';
import type { ChemicalTestResult, ObservationTestResult } from './waterTest';

export const ALL_JOBS_TO_INVOICE_FILTER = 'all';

export interface Job {
  id?: number;
  organisation_id?: number;
  recurrence_id?: number;
  user_id: string | number | null;
  user?: User | null;
  job_template_id: number | null;
  job_template?: JobTemplate | null;
  pool_id: number | null;
  contact_id: number | null;
  colorcode_id: number | null;
  org_color?: Color | null;
  pool?: Pool | null,
  contact?: Contact | null,
  tasks?: Task[] | [],
  photos?: Photo[] | [],
  invoices?: Invoice[] | [],
  activities?: JobActivity[] | [],
  start_time?: string,
  end_time?: string,
  job_notes?: string,
  notes?: string,
  virtual_id?: string,
  is_job_invoice_visible?: boolean,
  created_at?: string,
  job_status_id: number,
  submit?: boolean;
  job_recurrence?: JobRecurrence;
  chemical_results?: ChemicalTestResult[],
  observation_results?: ObservationTestResult[],
  booking_enabled?: boolean;
  complete_enabled?: boolean;
  reminder_enabled?: boolean;
}

export interface Task {
  id?: number;
  job_id?: number;
  name: string | null;
  order: number;
  status_id: 1 | 2;
  updated_at?: string,
  created_at?: string,
}

export interface Invoice {
  id?: number;
  quantity: number;
  name: string;
  cost?: number;
  gst_cost: number;
  original_cost?: number;
  product_id?: number;
  product?: Product | null;
}

export interface Photo {
  S3Location: string;
  created_at: string;
  description: string | null;
  filename: string;
  full_url: string;
  group: string | null;
  id: number;
  isS3Moved: boolean;
  job_id: number;
  label: string | null;
  medium_url: string;
  thumbnail_url: string;
  updated_at: string;
}

export interface JobActivity {
  id: string;
  createdAt: number;
  description: string;
  subject: string;
  type: string;
}

export type JobDetailScrollTo =
  | 'invoice'
  | 'tasks';

export interface JobRecurrence {
  recurrence_logic: string
}
