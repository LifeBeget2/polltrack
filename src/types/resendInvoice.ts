import { Contact } from './contact';
import { InvoiceSettings } from './invoiceSettings';

interface Organisation {
  invoice_settings: InvoiceSettings,
}

interface Job {
  organisation: Organisation,
}

export interface ContactInvoice {
  id: number;
  address: string;
  contact: Contact;
  subject_message: string;
  contact_intro: string;
  job: Job;
  is_job_sheet_attached: boolean;
  is_invoice_sheet_attached: boolean;
  prefixed_invoice_no: string;
}
