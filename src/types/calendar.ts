import { User } from './user';

export interface CalendarEvent {
  id?: string;
  allDay: boolean;
  color?: string;
  description: string;
  rrule?: string;
  end: Date;
  start: Date;
  title: string;
  extendedProps?: object;
  submit?: boolean;
  resourceId?: string,
}

export interface CalendarResource {
  id?: string;
  title: string;
}

export interface ResourceLabelContentArg {
  fieldValue: string;
  resource: Resource;
}

export interface Resource {
  id: string;
}

export type CalendarView =
  | 'quickSchedule'
  | 'resourceTimelineWeek'
  | 'resourceTimeline'
  | 'resourceTimelineFortnite'
  | 'dayGridMonth'
  | 'timeGridWeek'
  | 'timeGridDay'
  | 'listWeek';

export interface QuickScheduleColumn {
  id: string;
  name: string;
  technician?: User | null;
  jobIds: string[] | [];
}
