import type { Organisation } from './organisation';

export interface User {
  id: number;
  organisation_id: number;
  avatar: string;
  email: string;
  name: string;
  organisation: Organisation
  [key: string]: any;
  roles: UserRole[];
  created_at: string;
  updated_at: string;
  first_name: string;
  last_name: string;
}

export interface UserRole {
  description: string | null;
  display_name: string;
  id: number;
  is_primary_role: boolean;
  name: string;
}

export interface VendUser {
  id: number;
  display_name: string;
}
