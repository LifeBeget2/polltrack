import { Image } from './image';
import { MessagingSettings } from './messagingSettings';
import { NotificationSettings } from './notificationSettings';
import { VendOrganisation } from './vend';

export interface Organisation {
  id: number;
  email: string;
  company_abn: string | null;
  company_address_street_one: string | null;
  company_address_street_two: string | null;
  company_address_country: string | null;
  company_address_state: string | null;
  company_address_city: string | null;
  company_address_postcode: string | null;
  company_phone: string | null;
  url: string | null;
  contact_email: string;
  gst_enabled: boolean;
  first_name: string;
  last_name: string;
  timezone: string;
  logo: Image | null;
  name: string;
  is_xero_enabled: boolean;
  is_vend_enabled: boolean;
  is_stripe_connected: boolean;
  has_active_vend_connection: boolean;
  analysis_preferences: AnalysisPreferences;
  stripe_connection: StripeConnection;
  messaging_settings: MessagingSettings;
  notification_settings: NotificationSettings;
  vend_organisation: VendOrganisation;
}

export interface AnalysisPreferences {
  id: number;
  organisation_id: number;
  balance_ch_enabled?: boolean;
  certificate_disabled?: boolean;
  default_algaecide?: string | null;
  default_clarifier?: string | null;
  default_ph_reducer?: string | null;
  email_reminder_enabled: boolean;
  email_reminder_days: number;
  email_reminder_template: string | null;
  email_template: string | null;
  email_water_test_template: string | null;
  is_liquid_acid_default?: boolean;
  is_manual_salt_enabled?: boolean;
  lsi_disabled?: boolean;
  maintenance_disabled?: boolean;
  no_report_images?: boolean;
  no_report_preview?: boolean;
  pool_reminder: number;
  printout_style?: number;
  salt_bag_size?: number;
  salt_in?: number;
  sms_reminder_enabled: boolean;
  sms_reminder_days: number;
  sms_tech_on_way_template: string | null;
  sms_water_test_template: string | null;
  spa_reminder: number;
  swim_spa_reminder: number;
  volume_to_switch?: number | null;
}

export interface StripeConnection {
  id: number;
  organisation_id: number;
  account_name: string;
  automate_fees: boolean;
  charge_processing_fees: boolean;
  connected_on: string;
  pay_now_payments_enabled: boolean;
  processing_fee: number;
  tech_payments_enabled: boolean;
  created_at: string;
  updated_at: string;
}

export interface Color {
  id?: number;
  organisation_id?: number;
  color_code: string;
  color_desc: string;
  created_at?: string;
  updated_at?: string;
  submit?: boolean;
}
