import type { Contact } from './contact';
import type { Job } from './job';
import type { Pool } from './pool';
import type { Product } from './product';
import {
  BATCH_STATUS,
  BATCHED_STATUS,
  ENTERED_STATUS,
  NOT_ENTERED_STATUS, OVERDUE_STATUS,
  PAID_STATUS,
  SENT_STATUS,
  UNPAID_STATUS,
  UNSENT_STATUS
} from '../constants/invoice';
import { ContactInvoice } from './batchQueue';

export interface Invoice {
  id: number;
  organisation_id: number;
  invoice_no: boolean;
  contact_id: number;
  contact: Contact;
  contact_first_name: string | null;
  contact_intro: string | null;
  contact_last_name: string | null;
  accounting_sync_message: string | null;
  accounting_sync_status: string | null;
  address: string | null;
  batch_invoice: BatchInvoice | null;
  batch_invoice_id: number | null;
  cc_emails: string | null;
  created_at: string | null;
  due_date: string | null;
  email: string | null;
  generated_notes: string | null;
  gst_enabled: boolean;
  invoice_pdf_sheet: string | null;
  invoice_sheet_url: string | null;
  isChemicalActionsDisp: boolean;
  isChemicalHistoryDisp: boolean;
  isInvoiceDisp: boolean;
  isTaskDisp: boolean;
  isWatertestDisp: boolean;
  is_batch: boolean;
  is_batched: boolean;
  is_invoice_entered_ac_software: boolean;
  is_invoice_paid: boolean;
  is_invoice_sent: boolean;
  is_invoice_sheet_attached: boolean;
  is_job_sheet_attached: boolean;
  is_s3_moved: boolean | number;
  job: Job;
  job_created_date: string | null;
  job_id: number;
  job_pdf_sheet: string | null;
  job_recurrence_id: number | null;
  job_sheet_invoice_notes_display: boolean;
  job_sheet_url: string | null;
  job_template_name: string | null;
  mailgun_status: MailgunStatus | null;
  po_number: string | null;
  pool: Pool;
  pool_id: number;
  prefixed_invoice_no: string | null;
  products: Product[],
  pronto_invoice_id: string | null;
  send_date: string | null;
  stripe_payment_id: string | null;
  stripe_receipt_url: string | null;
  subject_message: string | null;
  total: number | null;
  total_amount: number | null;
  updated_at: string | null;
  vend_invoice_id: string | null;
  vend_invoice_status: string | null;
  xero_invoice_id: string | null;
  xero_invoice_status: string | null;
}

export interface BatchInvoice {
  id: number;
  organisation_id: number;
  address: string;
  contact: Contact;
  contact_first_name: string;
  contact_last_name: string;
  contact_id: number;
  due_date: string;
  intro_message: string | null;
  invoice_pdf_location: string;
  is_individual_invoices_attached: boolean;
  is_invoice_entered_ac_software: boolean;
  is_invoice_paid: boolean;
  is_invoice_sent: boolean;
  is_job_sheets_attached: boolean;
  mailgun_status: MailgunStatus | null;
  prefix: string;
  prefixed_invoice_no: string;
  send_date: string;
  subject_message: string | null;
  total: number | null;
  vend_invoice_id: string | null;
  vend_invoice_status: string | null;
  xero_invoice_id: string | null;
  xero_invoice_status: string | null;
  created_at: string | null;
  updated_at: string | null;
  contact_invoices: ContactInvoice[];
}

export type InvoiceStatus =
  | typeof SENT_STATUS
  | typeof UNSENT_STATUS
  | typeof PAID_STATUS
  | typeof UNPAID_STATUS
  | typeof ENTERED_STATUS
  | typeof NOT_ENTERED_STATUS
  | typeof OVERDUE_STATUS
  | typeof BATCHED_STATUS
  | typeof BATCH_STATUS;

export interface StatusFilter {
  value: number | string;
  label: string;
}

export type MailgunStatus =
  | 'sending'
  | 'delivered'
  | 'dropped';
