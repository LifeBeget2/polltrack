import { Brand } from './product';

export interface Type {
  id: number;
  name: string;
}

export type PoolEquipment = {
  id: number;
  created_at: string;
  updated_at: string;
  organisation_id: number;
  pool_id: number;
  serial: string | null;

  brand_id: number;
  brand?: Brand;
  name: string;
  notes: string;
  photos: PoolEquipmentPhoto[];
  type_id: number;
  type?: Type;

  warranty_start_date: string;
  warranty_end_date: string;
};

export type PoolEquipmentPhoto = {
  id: number;
  equipment_id: number;

  created_at: string;
  filename: string;

  S3Location: string;
  full_url: string;
  medium_url: string;
  thumbnail_url: string;

  isS3Moved: boolean;
};
