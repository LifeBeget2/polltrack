import type { Contact } from './contact';
import { PoolEquipment } from './equipment';
import type { Note } from './note';

export interface Pool {
  id?: number,
  organisation_id: number,
  name: string | null,
  pool_volume: PoolVolume,
  pool_type_id: number,
  pool_type?: PoolType,
  surface_type_id: number | null | string,
  pool_surface_type: null | PoolSurfaceType,
  filter: string | null;
  balance_ch: string | null;
  ph_reducer: string | null;
  pool_length?: string | null;
  pool_depth?: string | null;
  pool_breath?: string | null;
  longitude?: string | null;
  latitude?: string | null;
  location_id: number | null | string;
  custom_exception_id: number | null | string;
  classification_id: number | null | string;
  ground_level_id: number | null | string;
  full_address?: string | null;
  address_street_one: string | null;
  address_street_two: string | null;
  address_postcode: string | null;
  address_state: string | null;
  address_city?: string | null;
  address_country: string | null;
  keys: string | null,
  notes: string | null,
  pool_notes?: Note[];
  technical_specification: string | null,
  visibility: boolean,
  contacts: Contact[],
  pool_sanitisers: PoolSanitiser[],
  pivot?: ContactPivot,
  updated_at?: string,
  created_at?: string,
  submit?: boolean,
  equipments: PoolEquipment[],
}

export interface PoolType {
  id: number;
  name: string;
}

export interface Classification {
  id: number;
  name: string;
}

export interface Location {
  id: number;
  name: string;
}

export interface GroundLevel {
  id: number;
  name: string;
}

export interface PoolSurfaceType {
  id?: number;
  name: string;
}

export interface PoolSanitiser {
  id?: number;
  name: string;
  sanitiser_classification_id: number;
  sanitiser_classification?: SanitiserClassification;
}

export interface CustomException {
  id: number;
  name: string;
}

export interface PoolParams {
  classifications: Classification[];
  poolTypes: PoolType[];
  locations: Location[];
  groundLevels: GroundLevel[];
}

export interface SanitiserClassification {
  id: number;
  name: string;
}

export interface ContactPivot {
  address_type_id: number,
  contact_id: number,
  pool_id: number,
}

export type PoolVolume = number | null | string;
