export interface Address {
  address_street_one: string | null;
  address_street_two?: string | null;
  address_postcode: string | null;
  address_state: string | null;
  address_city?: string | null;
  address_country: string | null;
}
