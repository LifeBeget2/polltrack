import { Contact } from './contact';
import { Product } from './product';

export interface ContactInvoice {
  id: number;
  due_date: string;
  send_date: string;
  invoice_no: string;
  job_created_date: string;
  job_template_name: string;
  prefix: string;
  technician: string;
  total: number;
  job_id?: number;
  job_pdf_sheet?: string;
  invoice_pdf_sheet?: string;
  total_amount?: number;
}

export interface ContactInvoiceDetailed {
  prefixed_invoice_no: string;
  job_template_name: string;
  send_date: string;
  total_amount: string;
  products: Product[]
}

export interface BatchQueue {
  contact_id: number;
  first_name: string;
  last_name: string;
  organisation_id: number;
  total: number;
  contact_invoices: ContactInvoice[];
  contact: Contact;
}
