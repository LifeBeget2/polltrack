export interface Column {
  id: string;
  displayName: string;
  description: string;
  required: boolean;
  matchHeaderName: string;
  type: 'string' | 'float' | 'boolean' | 'number'
}

export interface ResultRow {
  column: Column;
  value: string;
}

export interface ParsedRow {
  [header: string]: string;
}

// key - header from file
// value: expected column
export type ColumnMatch = { [fileColumnHeader: string]: Column };
