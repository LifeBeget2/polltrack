export interface Image {
  id: number;
  filename: string;
  thumbnail_url: string;
  medium_url: string;
  full_url: string;
}
