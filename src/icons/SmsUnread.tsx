import { Box } from '@mui/material';
import { MessageCircle as MessageCircleIcon } from 'react-feather';

const SmsUnread = () => (
  <Box style={{ position: 'relative' }}>
    <MessageCircleIcon />
    <Box
      sx={{
        position: 'absolute',
        top: 0,
        right: 0,
        width: 8,
        height: 8,
        backgroundColor: 'primary.main',
        borderColor: 'common.white',
        borderRadius: 4,
        borderWidth: 1,
        borderStyle: 'solid',
      }}
    />
  </Box>
);

export default SmsUnread;
