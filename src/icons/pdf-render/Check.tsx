import React, { FC, memo } from 'react';
import PropTypes from 'prop-types';

import { Path, Svg } from '@react-pdf/renderer';
import { Style } from '@react-pdf/types';

interface CheckProps {style?: Style, color?: string}

const Check: FC<CheckProps> = memo(({ style, color }) => (
  <Svg
    height="24px"
    viewBox="0 0 24 24"
    width="24px"
    // @ts-ignore
    fill={color}
    style={style}
  >
    <Path
      d="M0 0h24v24H0V0z"
      fill="none"
    />
    <Path d="M9 16.17L5.53 12.7c-.39-.39-1.02-.39-1.41 0-.39.39-.39 1.02 0 1.41l4.18 4.18c.39.39 1.02.39 1.41 0L20.29 7.71c.39-.39.39-1.02 0-1.41-.39-.39-1.02-.39-1.41 0L9 16.17z" />
  </Svg>
));

Check.propTypes = {
  style: PropTypes.object.isRequired,
  color: PropTypes.string,
};

Check.defaultProps = {
  color: '#000000'
};

export default Check;
