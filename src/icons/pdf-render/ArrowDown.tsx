import React, { FC, memo } from 'react';
import PropTypes from 'prop-types';

import { Path, Svg } from '@react-pdf/renderer';
import {
  Style,
} from '@react-pdf/types';

interface ArrowDownProps {style?: Style, color?: string}

const ArrowDown: FC<ArrowDownProps> = memo(({ style, color }) => (
  <Svg
    height="24px"
    viewBox="0 0 24 24"
    width="24px"
    // @ts-ignore
    fill={color}
    style={style}
  >
    <Path
      d="M0 0h24v24H0V0z"
      fill="none"
    />
    <Path d="M11 5v11.17l-4.88-4.88c-.39-.39-1.03-.39-1.42 0-.39.39-.39 1.02 0 1.41l6.59 6.59c.39.39 1.02.39 1.41 0l6.59-6.59c.39-.39.39-1.02 0-1.41-.39-.39-1.02-.39-1.41 0L13 16.17V5c0-.55-.45-1-1-1s-1 .45-1 1z" />
  </Svg>
));

ArrowDown.propTypes = {
  // @ts-ignore
  style: PropTypes.object.isRequired,
  color: PropTypes.string,
};

export default ArrowDown;
