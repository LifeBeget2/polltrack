import React, { FC, memo } from 'react';
import PropTypes from 'prop-types';

import { Path, Svg } from '@react-pdf/renderer';
import {
  Style,
} from '@react-pdf/types';

interface ArrowUpProps {style?: Style, color?: string}

const ArrowUp: FC<ArrowUpProps> = memo(({ style, color }) => (
  <Svg
    height="24px"
    viewBox="0 0 24 24"
    width="24px"
    // @ts-ignore
    fill={color}
    style={style}
  >
    <Path
      d="M0 0h24v24H0V0z"
      fill="none"
    />
    <Path d="M4 12l1.41 1.41L11 7.83V20h2V7.83l5.58 5.59L20 12l-8-8-8 8z" />

  </Svg>
));

ArrowUp.propTypes = {
  // @ts-ignore
  style: PropTypes.object.isRequired,
  color: PropTypes.string,
};

export default ArrowUp;
