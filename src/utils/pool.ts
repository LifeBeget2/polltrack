import { PoolSanitiser } from 'src/types/pool';

const getCommaSeparatedSanitisers = (sanitisers: PoolSanitiser[]) => sanitisers
  .map((v) => v.name)
  .join(', ');

export default getCommaSeparatedSanitisers;
