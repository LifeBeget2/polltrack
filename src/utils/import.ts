import papaparse, { ParseConfig, ParseResult } from 'papaparse';
import map from 'lodash/map';
import type { ParsedRow } from '../types/fileImporter';

export function promisifyPapaParse<T>(input, config: ParseConfig): Promise<ParseResult<T>> {
  return new Promise((resolve, reject) => {
    papaparse.parse(input, { ...config, complete: resolve, error: reject });
  });
}

export function transformExcelDataToReadableArray(importResult: any[], preview: number): ParsedRow[] {
  const excelHeader = importResult[0];
  const previewResults = importResult.slice(1, preview + 1);
  return map(previewResults, (previewResult) => Object.fromEntries(
    excelHeader.map((val, index) => [val, previewResult[index]])
  ));
}
