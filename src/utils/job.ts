import type { Product } from '../types/product';
import type { Invoice } from '../types/job';

export const transformProductToInvoice = (product: Product): Invoice => ({
  name: product.name,
  cost: product.cost,
  gst_cost: product.gst_cost,
  quantity: 1,
  product_id: product.id,
  product,
});
