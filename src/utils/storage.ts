export const setStorage = (key:string, value:string):void => window.localStorage.setItem(key, value);
export const removeItem = (key:string):void => window.localStorage.removeItem(key);
export const getItem = (key:string):string | null => window.localStorage.getItem(key) || localStorage.getItem(key);
