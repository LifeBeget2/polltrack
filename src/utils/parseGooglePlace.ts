import type { Address } from '../types/address';

function placeGet(byType, key, short = false) {
  if (!(key in byType)) return '';

  return short ? byType[key].short_name : byType[key].long_name;
}

const parsePlace = (place): Address => {
  place = place || {};

  const byType = (place.address_components || []).reduce((acc, data) => {
    data.types.forEach((type) => {
      acc[type] = data;
    });
    return acc;
  }, {});

  const addressStreetDetails = {
    streetNumber: placeGet(byType, 'street_number'),
    streetName: placeGet(byType, 'route'),
  };

  const result = {
    address_street_one: '',
    address_city: placeGet(byType, 'locality')
      || placeGet(byType, 'sublocality')
      || placeGet(byType, 'sublocality_level_1')
      || placeGet(byType, 'neighborhood')
      || placeGet(byType, 'administrative_area_level_3')
      || placeGet(byType, 'administrative_area_level_2'),
    address_state: placeGet(byType, 'administrative_area_level_1', true),
    address_country: placeGet(byType, 'country', true),
    address_postcode: placeGet(byType, 'postal_code'),
  };

  result.address_street_one = [
    addressStreetDetails.streetNumber,
    addressStreetDetails.streetName
  ].filter(Boolean).join(' ');

  return result;
};

export default parsePlace;
