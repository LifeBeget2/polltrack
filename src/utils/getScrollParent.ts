export default function getScrollParent(node: HTMLElement | null) : HTMLElement | null {
  if (node == null) {
    return null;
  }

  if (node.scrollHeight > node.clientHeight) {
    return node;
  }
  return getScrollParent(node.parentNode as HTMLElement | null);
}
