export function parseBoolByStr(val: string | boolean | number) {
  return Boolean(Number(val));
}
