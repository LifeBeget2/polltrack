import find from 'lodash/find';
import type { Phone, PhoneType, Contact, VendContact } from 'src/types/contact';
import { PHONE_TYPES } from '../constants/contact';

export const getCommaSeparatedPhones = (phones: Phone[]) => phones
  .map((v) => v.phone_number)
  .join(', ');

export const getPhoneTypeName = (phoneType: PhoneType) => find(PHONE_TYPES, { value: phoneType }).label;

export const uniquePhones = (phones: Phone[]): Phone[] => {
  const mapPhoneNumberToPhone: { [key: string]: Phone } = {};
  phones.forEach((phone) => {
    mapPhoneNumberToPhone[phone.phone_number] = phone;
  });
  return Object.values(mapPhoneNumberToPhone);
};

export const getFullName = (contact: Contact | VendContact) => `${contact.first_name} ${contact.last_name}`;
