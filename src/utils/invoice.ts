import type { Invoice } from '../types/job';
import type { Product } from '../types/product';

export const getSubTotal = (items: Invoice[] | Product[], gst: number) => {
  console.log(items);
  const total = items
    .map((i) => i.gst_cost)
    .reduce((a, b) => a + b, 0);

  return Math.round((total / (1 + gst)) * 100) / 100;
};

export const getGst = (items: Invoice[] | Product[], gst: number) => {
  const total = items
    .map((i) => i.gst_cost)
    .reduce((a, b) => a + b, 0);

  return Math.round((total * (gst / (1 + gst))) * 100) / 100;
};

export const getTotal = (items: Invoice[] | Product[]) => items
  .map((i) => i.gst_cost)
  .reduce((a, b) => a + b, 0);

export const getGstByTotalSum = (total: number, gst: number) => Math.round(total * (gst / (1 + gst)) * 100) / 100;
