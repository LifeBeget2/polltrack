import numeral from 'numeral';
import type { ClassicMetric } from '../types/dashboard';

const getMonthStat = (classicMetric: ClassicMetric) => `${classicMetric.ratio} ${classicMetric.ratio.includes('+') ? 'Up' : 'Down'} from ${numeral(classicMetric.last_month).format(classicMetric.is_currency ? '($0.00a)' : '0a')}`;

export default getMonthStat;
