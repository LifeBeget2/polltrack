import { createContext, useEffect, useReducer } from 'react';
import type { FC, ReactNode } from 'react';
import jwtDecode from 'jwt-decode';
import PropTypes from 'prop-types';
import type { Organisation } from 'src/types/organisation';
import axios from 'axios';
import type { User } from '../types/user';
import { apiConfig } from '../config';
import { useDispatch } from '../store';
import { getOrganisation, getInvoiceSettings, getQuoteSettings, getTradingTerms, getBatchSettings, getColors } from '../slices/account';
import { getBrands, getCategories, getMetrics } from '../slices/product';
import { getJobTypes } from '../slices/jobTemplate';
import { getCustomExceptions, getPoolParams, getPoolSanitisers, getPoolSurfaceTypes } from '../slices/poolSpecifications';
import { getUsers } from '../slices/user';
import { initializeNavSection } from 'src/slices/navSection';

interface State {
  isInitialised: boolean;
  isAuthenticated: boolean;
  user: User | null;
  organisation: Organisation | null;
}

interface AuthContextValue extends State {
  platform: 'JWT';
  login: (email: string, password: string) => Promise<void>;
  logout: () => Promise<void>;
  passwordRecovery: (username: string) => Promise<void>;
  passwordReset: (
    username: string,
    code: string,
    newPassword: string
  ) => Promise<void>;
}

interface AuthProviderProps {
  children: ReactNode;
}

type InitializeAction = {
  type: 'INITIALIZE';
  payload: {
    isAuthenticated: boolean;
    user: User | null;
    organisation: Organisation | null;
  };
};

type LoginAction = {
  type: 'LOGIN';
  payload: {
    user: User,
    organisation: Organisation,
  };
};

type LogoutAction = {
  type: 'LOGOUT';
};

type PasswordRecoveryAction = {
  type: 'PASSWORD_RECOVERY';
};

type PasswordResetAction = {
  type: 'PASSWORD_RESET';
};

type RegisterAction = {
  type: 'REGISTER';
  payload: {
    user: User,
    organisation: Organisation,
  };
};

type Action =
  | InitializeAction
  | LoginAction
  | LogoutAction
  | PasswordRecoveryAction
  | PasswordResetAction
  | RegisterAction;

const initialState: State = {
  isAuthenticated: false,
  isInitialised: false,
  user: null,
  organisation: null,
};

const isValidToken = (accessToken: string): boolean => {
  if (!accessToken) {
    return false;
  }

  const decoded: any = jwtDecode(accessToken);
  const currentTime = Date.now() / 1000;

  // TODO: implement refresh token logic

  return decoded.exp > currentTime;
};

const setSession = (accessToken: string | null, userId: number | null, organisationId: number | null): void => {
  if (accessToken && userId && organisationId) {
    localStorage.setItem('accessToken', accessToken);
    localStorage.setItem('userId', userId.toString(10));
    localStorage.setItem('organisationId', organisationId.toString(10));
    axios.defaults.headers.common.Authorization = `Bearer ${accessToken}`;
  } else {
    localStorage.removeItem('accessToken');
    localStorage.removeItem('userId');
    localStorage.removeItem('organisationId');
    delete axios.defaults.headers.common.Authorization;
  }
};

const handlers: Record<string, (state: State, action: Action) => State> = {
  INITIALIZE: (state: State, action: InitializeAction): State => {
    const { isAuthenticated, user, organisation } = action.payload;

    return {
      ...state,
      isAuthenticated,
      isInitialised: true,
      user,
      organisation
    };
  },
  LOGIN: (state: State, action: LoginAction): State => {
    const { user, organisation } = action.payload;

    return {
      ...state,
      isAuthenticated: true,
      user,
      organisation
    };
  },
  LOGOUT: (state: State): State => ({
    ...state,
    isAuthenticated: false,
    user: null,
    organisation: null
  }),
  REGISTER: (state: State, action: RegisterAction): State => {
    const { user, organisation } = action.payload;

    return {
      ...state,
      isAuthenticated: true,
      user,
      organisation
    };
  },
  PASSWORD_RECOVERY: (state: State): State => ({ ...state }),
  PASSWORD_RESET: (state: State): State => ({ ...state })
};

const reducer = (state: State, action: Action): State => (
  handlers[action.type] ? handlers[action.type](state, action) : state
);

export const AuthContext = createContext<AuthContextValue>({
  ...initialState,
  platform: 'JWT',
  login: () => Promise.resolve(),
  logout: () => Promise.resolve(),
  passwordRecovery: () => Promise.resolve(),
  passwordReset: () => Promise.resolve()
});

export const AuthProvider: FC<AuthProviderProps> = (props) => {
  const { children } = props;
  const [state, dispatch] = useReducer(reducer, initialState);
  const dispatchStore = useDispatch();

  useEffect(() => {
    const initialize = async (): Promise<void> => {
      dispatchStore(initializeNavSection());

      try {
        const accessToken = window.localStorage.getItem('accessToken');
        const userId = window.localStorage.getItem('userId');
        const organisationId = window.localStorage.getItem('organisationId');

        if (accessToken && isValidToken(accessToken) && userId && organisationId) {
          setSession(accessToken, parseInt(userId, 10), parseInt(organisationId, 10));

          const userResponse = await axios.get(`${apiConfig.apiV1Url}/organisations/${organisationId}/users/${userId}`);
          const organisationResponse = await axios.get(`${apiConfig.apiV1Url}/organisations/${organisationId}`);

          try {
            const orgId = parseInt(organisationId, 10);
            await Promise.all([
              dispatchStore(getOrganisation(orgId)),
              dispatchStore(getUsers(orgId)),
              dispatchStore(getInvoiceSettings(orgId)),
              dispatchStore(getBatchSettings(orgId)),
              dispatchStore(getQuoteSettings(orgId)),
              dispatchStore(getTradingTerms(orgId)),
              dispatchStore(getCategories(orgId)),
              dispatchStore(getMetrics(orgId)),
              dispatchStore(getBrands(orgId)),
              dispatchStore(getPoolSanitisers(orgId)),
              dispatchStore(getCustomExceptions(orgId)),
              dispatchStore(getPoolParams()),
              dispatchStore(getPoolSurfaceTypes(orgId)),
              dispatchStore(getJobTypes()),
              dispatchStore(getColors(orgId)),
            ]);
          } catch (err) {
            console.error(err);
          }

          const user = userResponse.data;
          const organisation = organisationResponse.data;

          dispatch({
            type: 'INITIALIZE',
            payload: {
              isAuthenticated: true,
              user,
              organisation
            }
          });
        } else {
          dispatch({
            type: 'INITIALIZE',
            payload: {
              isAuthenticated: false,
              user: null,
              organisation: null
            }
          });
        }
      } catch (err) {
        console.error(err);
        dispatch({
          type: 'INITIALIZE',
          payload: {
            isAuthenticated: false,
            user: null,
            organisation: null
          }
        });
      }
    };

    initialize();
  }, []);

  const login = async (email: string, password: string): Promise<void> => {
    const response = await axios.post<{ token: string }>(`${apiConfig.apiV1Url}/orglogin`, { email, password });
    const { token } = response.data;
    const accountDetailsResponse = await axios.post<{ user: User; organisation: Organisation }>(`${apiConfig.apiV1Url}/orglogin`, { email, password });
    const { user, organisation } = accountDetailsResponse.data;

    setSession(token, user.id, organisation.id);

    try {
      await Promise.all([
        dispatchStore(getOrganisation(organisation.id)),
        dispatchStore(getUsers(organisation.id)),
        dispatchStore(getInvoiceSettings(organisation.id)),
        dispatchStore(getBatchSettings(organisation.id)),
        dispatchStore(getTradingTerms(organisation.id)),
        dispatchStore(getQuoteSettings(organisation.id)),
        dispatchStore(getCategories(organisation.id)),
        dispatchStore(getMetrics(organisation.id)),
        dispatchStore(getBrands(organisation.id)),
        dispatchStore(getPoolSanitisers(organisation.id)),
        dispatchStore(getCustomExceptions(organisation.id)),
        dispatchStore(getPoolParams()),
        dispatchStore(getPoolSurfaceTypes(organisation.id)),
        dispatchStore(getJobTypes()),
        dispatchStore(getColors(organisation.id)),
      ]);
    } catch (err) {
      console.error(err);
    }

    dispatch({
      type: 'LOGIN',
      payload: {
        user,
        organisation
      }
    });
  };

  const logout = async (): Promise<void> => {
    setSession(null, null, null);
    dispatch({ type: 'LOGOUT' });
  };

  const passwordRecovery = async (email: string): Promise<void> => {
    await axios.post<{ user: User; organisation: Organisation }>(`${apiConfig.apiV1Url}/password/email`, { email });
    dispatch({
      type: 'PASSWORD_RECOVERY'
    });
  };

  const passwordReset = async (
    email: string,
    token: string,
    password: string
  ): Promise<void> => {
    await axios.post<{ user: User; organisation: Organisation }>(`${apiConfig.apiV1Url}/password/reset`, { email, token, password });
    dispatch({
      type: 'PASSWORD_RESET'
    });
  };

  return (
    <AuthContext.Provider
      value={{
        ...state,
        platform: 'JWT',
        login,
        logout,
        passwordRecovery,
        passwordReset
      }}
    >
      {children}
    </AuthContext.Provider>
  );
};

AuthProvider.propTypes = {
  children: PropTypes.node.isRequired
};

export const AuthConsumer = AuthContext.Consumer;
