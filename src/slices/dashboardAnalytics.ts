import { createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit';
import type { AppThunk } from 'src/store';
import axios from 'axios';
import { apiConfig } from 'src/config';
import type { ChartData, ClassicMetric, RangeType } from '../types/dashboard';

interface DashboardAnalyticsState {
  rangeType: RangeType,
  analyticsChartData: ChartData,
  classicMetrics: ClassicMetric[],
  classicMetricsInitialised: boolean,
  chartDataInitialised: boolean,
  chartDataLoading: boolean,
  classicMetricsLoading: boolean,
}

const initialState: DashboardAnalyticsState = {
  rangeType: 100,
  analyticsChartData: null,
  classicMetrics: [],
  classicMetricsInitialised: false,
  chartDataInitialised: false,
  chartDataLoading: true,
  classicMetricsLoading: true,
};

const slice = createSlice({
  name: 'dashboardAnalytics',
  initialState,
  reducers: {
    getChartsData(state: DashboardAnalyticsState, action: PayloadAction<{ analyticsChartData: ChartData }>) {
      const { analyticsChartData } = action.payload;

      state.analyticsChartData = analyticsChartData;
      state.chartDataLoading = false;
      state.chartDataInitialised = true;
    },
    changeRangeType(state: DashboardAnalyticsState, action: PayloadAction<{ rangeType: RangeType }>) {
      const { rangeType } = action.payload;

      state.rangeType = rangeType;
    },
    getClassicMetrics(state: DashboardAnalyticsState, action: PayloadAction<{ classicMetrics: ClassicMetric[] }>) {
      const { classicMetrics } = action.payload;

      state.classicMetrics = classicMetrics;
      state.classicMetricsLoading = false;
      state.classicMetricsInitialised = true;
    },
    setClassicMetricsLoading(state: DashboardAnalyticsState, action: PayloadAction<{ isLoading: boolean }>) {
      const { isLoading } = action.payload;

      state.classicMetricsLoading = isLoading;
    },
    setChartDataLoading(state: DashboardAnalyticsState, action: PayloadAction<{ isLoading: boolean }>) {
      const { isLoading } = action.payload;

      state.chartDataLoading = isLoading;
    },
  }
});

export const { reducer } = slice;

export const getClassicMetrics = (organisationId: number): AppThunk => async (dispatch) => {
  dispatch(slice.actions.setClassicMetricsLoading({ isLoading: true }));
  const response = await axios.get(`${apiConfig.apiV2Url}/organisations/${organisationId}/dashboard/analytics-classic-metrics`);

  dispatch(slice.actions.getClassicMetrics({ classicMetrics: response.data }));
};

export const changeRangeType = (rangeType: RangeType) => (dispatch) => {
  dispatch(slice.actions.changeRangeType({ rangeType }));
};

export const getChartsData = (organisationId: number, rangeType: RangeType): AppThunk => async (dispatch) => {
  dispatch(slice.actions.setChartDataLoading({ isLoading: true }));
  const response = await axios.get(`${apiConfig.apiV2Url}/organisations/${organisationId}/dashboard/analytics-line-chart-data?type=${rangeType}`);

  dispatch(slice.actions.getChartsData({ analyticsChartData: response.data }));
};

export default slice;
