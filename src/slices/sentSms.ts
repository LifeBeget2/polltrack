import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import axios from 'axios';
import { apiConfig } from 'src/config';
import { AppThunk } from 'src/store';
import { Meta } from 'src/types/pagination';
import {
  SMS_STATUS_DELIVERED,
  SMS_STATUS_ERROR,
  SMS_STATUS_FAILED,
  SMS_STATUS_LOW_BALANCE,
  SMS_STATUS_QUEUED,
  SMS_STATUS_SENT,
  SMS_STATUS_UNDELIVERED,
} from 'src/constants/sms';
import { Sms } from 'src/types/sms';

export const FILTER_STATUS_ALL = 'all_statuses';
export const FILTER_STATUS_DELIVERED = SMS_STATUS_DELIVERED;
export const FILTER_STATUS_UNDELIVERED = SMS_STATUS_UNDELIVERED;
export const FILTER_STATUS_QUEUED = SMS_STATUS_QUEUED;
export const FILTER_STATUS_SENT = SMS_STATUS_SENT;
export const FILTER_STATUS_FAILED = [
  SMS_STATUS_ERROR,
  SMS_STATUS_LOW_BALANCE,
  SMS_STATUS_FAILED,
].join(',');

export type StatusFilter =
  | typeof FILTER_STATUS_ALL
  | typeof FILTER_STATUS_DELIVERED
  | typeof FILTER_STATUS_UNDELIVERED
  | typeof FILTER_STATUS_QUEUED
  | typeof FILTER_STATUS_SENT
  | typeof FILTER_STATUS_FAILED;

interface SentSmsState {
  statusFilter?: StatusFilter;

  limit: number;
  page: number;
  total: number;

  sms: Sms[];
  isLoading: boolean;

  isEditingPhoneNumber: boolean;
  isResending: boolean;

  resendStatusMessage?: string;
}

const initialState: SentSmsState = {
  statusFilter: FILTER_STATUS_ALL,
  limit: 10,
  page: 1,
  total: 0,

  sms: [],
  isLoading: false,

  isEditingPhoneNumber: false,
  isResending: false,
  resendStatusMessage: null,
};

const slice = createSlice({
  name: 'sentSms',
  initialState,
  reducers: {
    reset() {
      return initialState;
    },
    startLoading(state: SentSmsState) {
      state.isLoading = true;
    },
    startResending(state: SentSmsState) {
      state.isResending = true;
    },
    stopResending(state: SentSmsState) {
      state.isResending = false;
    },
    getSms(
      state: SentSmsState,
      action: PayloadAction<{ sms: Sms[]; meta: Meta }>
    ) {
      const { sms, meta } = action.payload;

      state.sms = sms;
      state.total = meta.total;
      state.isLoading = false;
    },
    setLimit(state: SentSmsState, action: PayloadAction<{ limit: number }>) {
      const { limit } = action.payload;

      state.limit = limit;
      state.page = initialState.page;
      state.total = initialState.total;
    },
    setPage(state: SentSmsState, action: PayloadAction<{ page: number }>) {
      const { page } = action.payload;

      state.page = page;
    },
    setStatusFilter(
      state: SentSmsState,
      action: PayloadAction<{ status: StatusFilter }>
    ) {
      const { status } = action.payload;

      state.statusFilter = status;
    },
    updatePhoneNumber(state: SentSmsState) {
      state.isEditingPhoneNumber = true;
    },
    updatePhoneNumberFinished(state: SentSmsState) {
      state.isEditingPhoneNumber = false;
    },
    updatePhoneNumberBySmsId(state: SentSmsState, action: PayloadAction<{ smsId: number, phone: string }>) {
      const { smsId, phone } = action.payload;
      const index = state.sms.findIndex((smsItem) => smsItem.id === smsId);

      if (index === -1) {
        return;
      }
      state.sms[index].phone_number = phone;
    },
    updateStatusToQueued(state: SentSmsState, action: PayloadAction<{ smsId: number }>) {
      const { smsId } = action.payload;
      const smsIndex = state.sms.findIndex(({ id }) => id === smsId);
      if (smsIndex === -1) {
        return;
      }

      state.sms[smsIndex].status = SMS_STATUS_QUEUED;
    },
    setResendStatusMessage(state: SentSmsState, action: PayloadAction<{ message: string }>) {
      state.resendStatusMessage = action.payload.message;
    },
  },
});

export const { reducer } = slice;
export const { reset, setLimit, setPage, setStatusFilter, setResendStatusMessage } = slice.actions;

export const getSentSMS = (
  organisationId: number,
  limit: number,
  page: number,
  statusFilter: StatusFilter
): AppThunk => async (dispatch) => {
  const params = {
    orderBy: 'updated_at,desc',
    page,
    limit,
    status: statusFilter === FILTER_STATUS_ALL ? null : statusFilter.toString(),
  };

  dispatch(slice.actions.startLoading());
  const response = await axios.get(
    `${apiConfig.apiV1Url}/organisations/${organisationId}/smsnotifications`,
    { params }
  );
  dispatch(
    slice.actions.getSms({
      sms: response.data.data,
      meta: response.data.meta.pagination,
    })
  );
};

export const updatePhoneNumber = (organisationId: number, smsId: number, phone: string): AppThunk => async (dispatch) => {
  dispatch(slice.actions.updatePhoneNumber());
  try {
    await axios.put(
      `${apiConfig.apiV1Url}/organisations/${organisationId}/smsnotifications/${smsId}`,
      { phone_number: phone }
    );
    dispatch(slice.actions.updatePhoneNumberBySmsId({ smsId, phone }));
  } finally {
    dispatch(slice.actions.updatePhoneNumberFinished());
  }
};

export const resendSms = (organisationId: number, smsId: number): AppThunk => async (dispatch) => {
  dispatch(slice.actions.startResending());
  try {
    const response = await axios.post(
      `${apiConfig.apiV1Url}/organisations/${organisationId}/smsnotifications/${smsId}/resend`,
    );
    dispatch(slice.actions.updateStatusToQueued({ smsId }));
    dispatch(slice.actions.setResendStatusMessage({ message: response.data.success }));
  } finally {
    dispatch(slice.actions.stopResending());
  }
};
