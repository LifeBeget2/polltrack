import { createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit';
import type { AppThunk } from 'src/store';
import axios from 'axios';
import { apiConfig } from 'src/config';
import { SortDirection } from '@mui/material';
import type { ObservationGroup } from '../types/chemical';
import type { Meta } from '../types/pagination';

interface ObservationGroupState {
  searchText?: string,
  limit?: number,
  page?: number,
  total?: number,
  orderBy: string,
  order: SortDirection,
  observationGroups: ObservationGroup[],
}

const initialState: ObservationGroupState = {
  searchText: '',
  limit: 10,
  page: 0,
  total: 0,
  orderBy: 'name',
  order: 'asc',
  observationGroups: [],
};

const slice = createSlice({
  name: 'observationGroup',
  initialState,
  reducers: {
    getObservationGroups(state: ObservationGroupState, action: PayloadAction<{ observationGroups: ObservationGroup[]; meta: Meta }>) {
      const { observationGroups, meta } = action.payload;

      state.observationGroups = observationGroups;
      state.total = meta.total;
    },
    setLimit(state: ObservationGroupState, action: PayloadAction<{ limit: number }>) {
      const { limit } = action.payload;

      state.limit = limit;
      state.page = 0;
      state.total = 0;
    },
    setPage(state: ObservationGroupState, action: PayloadAction<{ page: number }>) {
      const { page } = action.payload;

      state.page = page;
    },
    setSearchText(state: ObservationGroupState, action: PayloadAction<{ searchText: string }>) {
      const { searchText } = action.payload;

      state.searchText = searchText;
      state.page = 0;
    },
    setOrder(state: ObservationGroupState, action: PayloadAction<{ orderBy: string; order: SortDirection }>) {
      const { orderBy, order } = action.payload;

      state.orderBy = orderBy;
      state.order = order;
    },
    deleteObservationGroup(state: ObservationGroupState, action: PayloadAction<{ observationGroupId: number; }>) {
      const { observationGroupId } = action.payload;

      state.observationGroups = state.observationGroups.filter((observationGroup) => observationGroup.id !== observationGroupId);
    }
  }
});

export const { reducer } = slice;

export const getObservationGroups = (
  organisationId: number,
  limit: number,
  page: number,
  filter: string = '',
  sortBy: string = '',
): AppThunk => async (dispatch) => {
  const response = await axios.get(`${apiConfig.apiV2Url}/organisations/${organisationId}/observation-group?filter=${filter}&limit=${limit}&order=${sortBy}&page=${page + 1}`);

  dispatch(slice.actions.getObservationGroups({ observationGroups: response.data.data, meta: response.data.meta }));
};

export const setLimit = (limit: number) => (dispatch) => {
  dispatch(slice.actions.setLimit({ limit }));
};

export const setPage = (page: number) => (dispatch) => {
  dispatch(slice.actions.setPage({ page }));
};

export const setSearchText = (searchText: string) => (dispatch) => {
  dispatch(slice.actions.setSearchText({ searchText }));
};

export const setOrder = (orderBy: string, order: SortDirection) => (dispatch) => {
  dispatch(slice.actions.setOrder({ orderBy, order }));
};

export const deleteObservationGroup = (organisationId: number, observationGroupId: number): AppThunk => async (dispatch) => {
  await axios.delete(`${apiConfig.apiV1Url}/organisations/${organisationId}/observationgroups/${observationGroupId}`);

  dispatch(slice.actions.deleteObservationGroup({ observationGroupId }));
};

export default slice;
