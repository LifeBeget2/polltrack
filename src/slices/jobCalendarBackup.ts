/* eslint-disable */
import { createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit';
import axios from 'axios';
import moment from 'moment/moment';
import type { AppThunk } from '../store';
import type { Job } from '../types/job';
import {apiConfig} from "../config";

interface JobsCalendarState {
  jobs: Job[];
  isModalOpen: boolean;
  isLoading: boolean;
  selectedJobId: string | null;
  fromDate: string;
  toDate: string;
  selectedRange: {
    start: number;
    end: number;
  } | null;
}

const initialState: JobsCalendarState = {
  jobs: [],
  isLoading: false,
  isModalOpen: false,
  selectedJobId: null,
  fromDate: moment().startOf('month').format('YYYY-MM-DD'),
  toDate: moment().endOf('month').format('YYYY-MM-DD'),
  selectedRange: null
};

const slice = createSlice({
  name: 'jobCalendar',
  initialState,
  reducers: {
    getJobs(
      state: JobsCalendarState,
      action: PayloadAction<{ jobs: Job[]; from: string; to: string }>
    ): void {
      const { jobs, from, to } = action.payload;

      state.jobs = jobs;
      state.fromDate = from;
      state.toDate = to;
      state.isLoading = false;
    },
    setIsLoading(
      state: JobsCalendarState,
      action: PayloadAction<{ isLoading: boolean }>
    ): void {
      const { isLoading } = action.payload;

      state.isLoading = isLoading;
    },
    createJob(
      state: JobsCalendarState,
      action: PayloadAction<{ job: Job }>
    ): void {
      const { job } = action.payload;

      state.jobs.push(job);
    },
    selectJob(
      state: JobsCalendarState,
      action: PayloadAction<{ jobId?: string }>
    ): void {
      const { jobId = null } = action.payload;

      state.isModalOpen = true;
      state.selectedJobId = jobId;
    },
    updateJob(
      state: JobsCalendarState,
      action: PayloadAction<{ job: Job }>
    ): void {
      const { job } = action.payload;

      state.jobs = state.jobs.map((_job) => {
        if (_job.id === job.id) {
          return job;
        }

        return _job;
      });
    },
    deleteJob(
      state: JobsCalendarState,
      action: PayloadAction<{ jobId: number }>
    ): void {
      const { jobId } = action.payload;

      state.jobs = state.jobs.filter((job) => job.id !== jobId);
    },
    selectRange(
      state: JobsCalendarState,
      action: PayloadAction<{ start: number; end: number }>
    ): void {
      const { start, end } = action.payload;

      state.isModalOpen = true;
      state.selectedRange = {
        start,
        end
      };
    },
    openModal(state: JobsCalendarState): void {
      state.isModalOpen = true;
    },
    closeModal(state: JobsCalendarState): void {
      state.isModalOpen = false;
      state.selectedJobId = null;
      state.selectedRange = null;
    }
  }
});

export const { reducer } = slice;

export const getJobs = (
  organisationId: number,
  from: string,
  to: string,
): AppThunk => async (dispatch) => {
  dispatch(slice.actions.setIsLoading({ isLoading: true }));
  const response = await axios.get(`${apiConfig.apiV2Url}/organisations/${organisationId}/jobs?from=${from}&to=${to}`);

  dispatch(slice.actions.getJobs({ jobs: response.data, from, to }));
};

export const createJob = (data: any): AppThunk => async (dispatch): Promise<void> => {
  const response = await axios.post<{ job: Job }>('/api/calendar/jobs/new', data);

  dispatch(slice.actions.createJob(response.data));
};

export const selectJob = (jobId?: string): AppThunk => async (dispatch): Promise<void> => {
  dispatch(slice.actions.selectJob({ jobId }));
};

export const updateJob = (
  jobId: string,
  update: any
): AppThunk => async (dispatch): Promise<void> => {
  const response = await axios.post<{ job: Job }>(
    '/api/calendar/jobs/update',
    {
      jobId,
      update
    }
  );

  dispatch(slice.actions.updateJob(response.data));
};

export const deleteJob = (jobId: number): AppThunk => async (dispatch): Promise<void> => {
  await axios.post(
    '/api/calendar/jobs/remove',
    {
      jobId
    }
  );

  dispatch(slice.actions.deleteJob({ jobId }));
};

export const selectRange = (
  start: Date,
  end: Date
): AppThunk => (dispatch): void => {
  dispatch(slice.actions.selectRange({
    start: start.getTime(),
    end: end.getTime()
  }));
};

export const openModal = (): AppThunk => (dispatch): void => {
  dispatch(slice.actions.openModal());
};

export const closeModal = (): AppThunk => (dispatch): void => {
  dispatch(slice.actions.closeModal());
};

export default slice;
