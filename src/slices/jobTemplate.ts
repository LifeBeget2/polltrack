import { createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit';
import type { AppThunk } from 'src/store';
import axios from 'axios';
import { apiConfig } from 'src/config';
import { SortDirection } from '@mui/material';
import { ALL_TYPE_FILTER, JobTemplate, JobType } from '../types/jobTemplate';
import type { Meta } from '../types/pagination';

interface JobTemplateState {
  searchText?: string,
  limit?: number,
  page?: number,
  total?: number,
  orderBy: string,
  order: SortDirection,
  jobTypeFilter?: string | number,
  jobTypes: JobType[],
  jobTemplates: JobTemplate[],
}

const initialState: JobTemplateState = {
  searchText: '',
  limit: 10,
  page: 0,
  total: 0,
  jobTypeFilter: ALL_TYPE_FILTER,
  orderBy: 'name',
  order: 'asc',
  jobTypes: [],
  jobTemplates: [],
};

const slice = createSlice({
  name: 'jobTemplate',
  initialState,
  reducers: {
    getTemplates(state: JobTemplateState, action: PayloadAction<{ jobTemplates: JobTemplate[]; meta: Meta }>) {
      const { jobTemplates, meta } = action.payload;

      state.jobTemplates = jobTemplates;
      state.total = meta.total;
    },
    getJobTypes(state: JobTemplateState, action: PayloadAction<{ jobTypes: JobType[]; }>) {
      const { jobTypes } = action.payload;

      state.jobTypes = jobTypes;
    },
    setLimit(state: JobTemplateState, action: PayloadAction<{ limit: number }>) {
      const { limit } = action.payload;

      state.limit = limit;
      state.page = 0;
      state.total = 0;
    },
    setPage(state: JobTemplateState, action: PayloadAction<{ page: number }>) {
      const { page } = action.payload;

      state.page = page;
    },
    setJobType(state: JobTemplateState, action: PayloadAction<{ jobType: string | number }>) {
      const { jobType } = action.payload;

      state.jobTypeFilter = jobType;
    },
    setSearchText(state: JobTemplateState, action: PayloadAction<{ searchText: string }>) {
      const { searchText } = action.payload;

      state.searchText = searchText;
      state.page = 0;
    },
    setOrder(state: JobTemplateState, action: PayloadAction<{ orderBy: string; order: SortDirection }>) {
      const { orderBy, order } = action.payload;

      state.orderBy = orderBy;
      state.order = order;
    },
    deleteTemplate(state: JobTemplateState, action: PayloadAction<{ templateId: number; }>) {
      const { templateId } = action.payload;

      state.jobTemplates = state.jobTemplates.filter((jobTemplate) => jobTemplate.id !== templateId);
    }
  }
});

export const { reducer } = slice;

export const getTemplates = (
  organisationId: number,
  limit: number,
  page: number,
  jobTypeFilter?: string | number,
  filter: string = '',
  sortBy: string = '',
): AppThunk => async (dispatch) => {
  const typeFilter = (Boolean(jobTypeFilter) && jobTypeFilter !== ALL_TYPE_FILTER) ? jobTypeFilter : '';
  const response = await axios.get(`${apiConfig.apiV2Url}/organisations/${organisationId}/job-template?job_type_id=${typeFilter}&filter=${filter}&limit=${limit}&order=${sortBy}&page=${page + 1}`);

  dispatch(slice.actions.getTemplates({ jobTemplates: response.data.data, meta: response.data.meta }));
};

export const getJobTypes = (): AppThunk => async (dispatch) => {
  const response = await axios.get(`${apiConfig.apiV1Url}/jobtypes`);

  dispatch(slice.actions.getJobTypes({ jobTypes: response.data }));
};

export const setLimit = (limit: number) => (dispatch) => {
  dispatch(slice.actions.setLimit({ limit }));
};

export const setPage = (page: number) => (dispatch) => {
  dispatch(slice.actions.setPage({ page }));
};

export const setJobType = (jobType: string | number) => (dispatch) => {
  dispatch(slice.actions.setJobType({ jobType }));
};

export const setSearchText = (searchText: string) => (dispatch) => {
  dispatch(slice.actions.setSearchText({ searchText }));
};

export const setOrder = (orderBy: string, order: SortDirection) => (dispatch) => {
  dispatch(slice.actions.setOrder({ orderBy, order }));
};

export const deleteTemplate = (organisationId: number, templateId: number): AppThunk => async (dispatch) => {
  await axios.delete(`${apiConfig.apiV2Url}/organisations/${organisationId}/job-template/${templateId}`);

  dispatch(slice.actions.deleteTemplate({ templateId }));
};

export default slice;
