import axios from 'axios';
import { createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit';

import { BatchQueue } from 'src/types/batchQueue';
import type { AppThunk } from '../store';
import { apiConfig } from '../config';

interface BatchQueueState {
  batchQueue: BatchQueue[];
  isLoading: boolean;

  searchQuery: string;
}

const initialState: BatchQueueState = {
  batchQueue: [],
  isLoading: false,

  searchQuery: '',
};

const slice = createSlice({
  name: 'batchQueue',
  initialState,
  reducers: {
    setBatchQueue(
      state: BatchQueueState,
      action: PayloadAction<{ batchQueue: BatchQueue[] }>
    ): void {
      const { batchQueue } = action.payload;
      state.batchQueue = batchQueue;
      state.isLoading = false;
    },
    getBatchQueue(
      state: BatchQueueState,
    ): void {
      state.isLoading = true;
    },

    setSearchQuery(state: BatchQueueState, action: PayloadAction<{ searchQuery: string }>) {
      state.searchQuery = action.payload.searchQuery;
    }
  }
});

export const { reducer } = slice;

export const { setSearchQuery } = slice.actions;

export const getBatchQueue = (
  organisationId: number,
): AppThunk => async (dispatch) => {
  dispatch(slice.actions.getBatchQueue());
  const response = await axios.get(`${apiConfig.apiV2Url}/organisations/${organisationId}/batch-invoice/batch-queue`);
  dispatch(slice.actions.setBatchQueue({ batchQueue: response.data }));
};

export default slice;
