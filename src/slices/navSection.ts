import { createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit';

interface NavSectionState {
  collapsedSectionTitles: string[];

}

const initialState: NavSectionState = {
  collapsedSectionTitles: [],
};

const LOCAL_STORAGE_KEY_COLLAPSED_SECTION_TITLES = 'collapsed_section_titles_1';

function persist(collapsedSectionTitles: string[]) {
  localStorage.setItem(LOCAL_STORAGE_KEY_COLLAPSED_SECTION_TITLES, JSON.stringify(collapsedSectionTitles));
}

const slice = createSlice({
  name: 'NavSection',
  initialState,
  reducers: {
    setCollapsedSectionTitles(state: NavSectionState, action: PayloadAction<{ collapsedSectionTitles: string[] }>) {
      state.collapsedSectionTitles = action.payload.collapsedSectionTitles;
    },
  }
});

export const { reducer } = slice;

export const initializeNavSection = () => (dispatch) => {
  try {
    const collapsedSectionTitles = JSON.parse(localStorage.getItem(LOCAL_STORAGE_KEY_COLLAPSED_SECTION_TITLES) || '[]');
    dispatch(slice.actions.setCollapsedSectionTitles({ collapsedSectionTitles }));
  } catch {
    dispatch(slice.actions.setCollapsedSectionTitles({ collapsedSectionTitles: [] }));
  }
};

export const expand = (
  sectionTitle: string,
  collapsedSectionTitles: string[],
) => (dispatch) => {
  const result = collapsedSectionTitles.filter((title) => sectionTitle !== title);
  dispatch(slice.actions.setCollapsedSectionTitles({ collapsedSectionTitles: result }));
  persist(result);
};

export const collapse = (sectionTitle: string, collapsedSectionTitles: string[]) => (dispatch) => {
  const result = [...collapsedSectionTitles, sectionTitle];
  dispatch(slice.actions.setCollapsedSectionTitles({ collapsedSectionTitles: result }));
  persist(result);
};

export const toggle = (sectionTitle: string, collapsedSectionTitles: string[]) => (dispatch) => {
  if (collapsedSectionTitles.includes(sectionTitle)) {
    dispatch(expand(sectionTitle, collapsedSectionTitles));
  } else {
    dispatch(collapse(sectionTitle, collapsedSectionTitles));
  }
};
