import { createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit';
import axios from 'axios';
import moment from 'moment/moment';
import type { AppThunk } from '../store';
import type { Job } from '../types/job';
import type { User } from '../types/user';
import { apiConfig } from '../config';
import type { CalendarView } from '../types/calendar';

interface JobCalendarState {
  view: CalendarView;
  jobs: Job[];
  unscheduledJobs: Job[];
  technicians: User[];
  isInitialised: boolean;
  isLoading: boolean;
  showMap: boolean;
  fromDate: string;
  toDate: string;
  selectedTechnicianId?: string;
}

const initialState: JobCalendarState = {
  view: 'quickSchedule',
  jobs: [],
  unscheduledJobs: [],
  technicians: [],
  isInitialised: false,
  isLoading: false,
  showMap: false,
  fromDate: moment().startOf('month').format('YYYY-MM-DD'),
  toDate: moment().endOf('month').format('YYYY-MM-DD'),
  selectedTechnicianId: null,
};

const slice = createSlice({
  name: 'jobCalendar',
  initialState,
  reducers: {
    init(
      state: JobCalendarState,
      action: PayloadAction<{ technicians: User[] }>
    ): void {
      const { technicians } = action.payload;
      state.technicians = technicians;
      state.isInitialised = true;
    },
    changeView(
      state: JobCalendarState,
      action: PayloadAction<{ view: CalendarView }>
    ): void {
      const { view } = action.payload;
      state.view = view;
    },
    getJobs(
      state: JobCalendarState,
      action: PayloadAction<{ jobs: Job[]; unscheduled: Job[], from: string; to: string }>
    ): void {
      const { jobs, unscheduled, from, to } = action.payload;

      state.jobs = jobs;
      state.unscheduledJobs = unscheduled;
      state.fromDate = from;
      state.toDate = to;
      state.isLoading = false;
    },
    setIsLoading(
      state: JobCalendarState,
      action: PayloadAction<{ isLoading: boolean }>
    ): void {
      const { isLoading } = action.payload;

      state.isLoading = isLoading;
    },
    setShowMap(
      state: JobCalendarState,
      action: PayloadAction<{ showMap: boolean }>
    ): void {
      const { showMap } = action.payload;

      state.showMap = showMap;
    },
    filterJobsByTechnicianId(state: JobCalendarState, action: PayloadAction<{ id?: string }>): void {
      state.selectedTechnicianId = action.payload.id;
    },
    updateJob(state: JobCalendarState, action: PayloadAction<{ job: Job }>) {
      const { job } = action.payload;

      state.jobs = state.jobs.filter((_job) => _job.id !== job.id);
      state.unscheduledJobs = state.unscheduledJobs.filter((_job) => _job.id !== job.id);

      // job should be unscheduled
      if (job.start_time == null && job.end_time == null) {
        state.unscheduledJobs = [...state.unscheduledJobs, job];
      } else {
        state.jobs = [...state.jobs, job];
      }
    }
  }
});

export const { reducer } = slice;
export const { updateJob } = slice.actions;

export const init = (
  organisationId: number,
): AppThunk => async (dispatch) => {
  const response = await axios.get(`${apiConfig.apiV1Url}/organisations/${organisationId}/users`);
  dispatch(slice.actions.init({ technicians: response.data }));
};

export const changeView = (view: CalendarView): AppThunk => (dispatch): void => {
  dispatch(slice.actions.changeView({ view }));
};

export const filterJobsByTechnicianId = (id?: string): AppThunk => (dispatch): void => {
  dispatch(slice.actions.filterJobsByTechnicianId({ id }));
};

export const setShowMap = (showMap: boolean): AppThunk => (dispatch): void => {
  dispatch(slice.actions.setShowMap({ showMap }));
};

export const getJobs = (
  organisationId: number,
  from: string,
  to: string,
): AppThunk => async (dispatch) => {
  dispatch(slice.actions.setIsLoading({ isLoading: true }));
  const promiseJobs = axios.get(`${apiConfig.apiV2Url}/organisations/${organisationId}/combined-jobs?from=${from}&to=${to}`);
  const promiseUnscheduled = axios.get(`${apiConfig.apiV2Url}/organisations/${organisationId}/unscheduled-jobs`);

  const [response, responseUnscheduled] = await Promise.all([promiseJobs, promiseUnscheduled]);
  dispatch(slice.actions.getJobs({ jobs: response.data, unscheduled: responseUnscheduled.data, from, to }));
};

export default slice;
