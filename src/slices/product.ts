import { createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit';
import type { AppThunk } from 'src/store';
import axios from 'axios';
import { apiConfig } from 'src/config';
import { SortDirection } from '@mui/material';
import { ALL_CAT_FILTER, Brand, Category, Metric, Product } from '../types/product';

interface ProductState {
  searchText?: string,
  limit?: number,
  page?: number,
  total?: number,
  orderBy: string,
  order: SortDirection,
  categoryFilter?: string | number,
  products: Product[],
  categories: Category[],
  metrics: Metric[],
  brands: Brand[],

  enabledColumns?: string[],
  enabledColumnsRestored: boolean,
}

const initialState: ProductState = {
  searchText: '',
  limit: 10,
  page: 0,
  total: 0,
  categoryFilter: ALL_CAT_FILTER,
  orderBy: 'name',
  order: 'asc',
  products: [],
  categories: [],
  metrics: [],
  brands: [],

  enabledColumns: null,
  enabledColumnsRestored: false,
};

const LOCALSTORAGE_KEY_ENABLED_COLUMNS = 'product_enabled_columns';

const slice = createSlice({
  name: 'product',
  initialState,
  reducers: {
    getCategories(state: ProductState, action: PayloadAction<{ categories: Category[]; }>) {
      const { categories } = action.payload;

      state.categories = categories;
    },
    getMetrics(state: ProductState, action: PayloadAction<{ metrics: Metric[]; }>) {
      const { metrics } = action.payload;

      state.metrics = metrics;
    },
    getBrands(state: ProductState, action: PayloadAction<{ brands: Brand[]; }>) {
      const { brands } = action.payload;

      state.brands = brands;
    },
    setLimit(state: ProductState, action: PayloadAction<{ limit: number }>) {
      const { limit } = action.payload;

      state.limit = limit;
      state.page = 0;
      state.total = 0;
    },
    setTotal(state: ProductState, action: PayloadAction<{ total: number }>) {
      const { total } = action.payload;

      state.total = total;
    },
    setPage(state: ProductState, action: PayloadAction<{ page: number }>) {
      const { page } = action.payload;

      state.page = page;
    },
    setCategory(state: ProductState, action: PayloadAction<{ category: string | number }>) {
      const { category } = action.payload;

      state.categoryFilter = category;
    },
    setSearchText(state: ProductState, action: PayloadAction<{ searchText: string }>) {
      const { searchText } = action.payload;

      state.searchText = searchText;
      state.page = 0;
    },
    setOrder(state: ProductState, action: PayloadAction<{ orderBy: string; order: SortDirection }>) {
      const { orderBy, order } = action.payload;

      state.orderBy = orderBy;
      state.order = order;
    },
    setEnabledColumns(state: ProductState, action: PayloadAction<{ enabledColumns: string[]; }>) {
      state.enabledColumns = action.payload.enabledColumns;
    },
    enabledColumnsRestored(state: ProductState) {
      state.enabledColumnsRestored = true;
    },
  }
});

export const { reducer } = slice;

export const getCategories = (organisationId: number): AppThunk => async (dispatch) => {
  const response = await axios.get(`${apiConfig.apiV1Url}/organisations/${organisationId}/productscategories`);

  dispatch(slice.actions.getCategories({ categories: response.data }));
};

export const getMetrics = (organisationId: number): AppThunk => async (dispatch) => {
  const response = await axios.get(`${apiConfig.apiV1Url}/organisations/${organisationId}/productsmetrics`);

  dispatch(slice.actions.getMetrics({ metrics: response.data }));
};

export const getBrands = (organisationId: number): AppThunk => async (dispatch) => {
  const response = await axios.get(`${apiConfig.apiV1Url}/organisations/${organisationId}/productsbrands`);

  dispatch(slice.actions.getBrands({ brands: response.data }));
};

export const setLimit = (limit: number) => (dispatch) => {
  dispatch(slice.actions.setLimit({ limit }));
};

export const setTotal = (total: number) => (dispatch) => {
  dispatch(slice.actions.setTotal({ total }));
};

export const setPage = (page: number) => (dispatch) => {
  dispatch(slice.actions.setPage({ page }));
};

export const setCategory = (category: string | number) => (dispatch) => {
  dispatch(slice.actions.setCategory({ category }));
};

export const setSearchText = (searchText: string) => (dispatch) => {
  dispatch(slice.actions.setSearchText({ searchText }));
};

export const setOrder = (orderBy: string, order: SortDirection) => (dispatch) => {
  dispatch(slice.actions.setOrder({ orderBy, order }));
};

export const setEnabledColumns = (enabledColumns: string[]) => (dispatch) => {
  dispatch(slice.actions.setEnabledColumns({ enabledColumns }));
  localStorage.setItem(LOCALSTORAGE_KEY_ENABLED_COLUMNS, JSON.stringify(enabledColumns));
};

export const restoreEnabledColumns = () => (dispatch) => {
  const enabledColumns = JSON.parse(localStorage.getItem(LOCALSTORAGE_KEY_ENABLED_COLUMNS));
  dispatch(slice.actions.setEnabledColumns({ enabledColumns }));
  dispatch(slice.actions.enabledColumnsRestored());
};

export default slice;
