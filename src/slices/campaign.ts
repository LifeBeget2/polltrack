import { createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit';
import type { AppThunk } from 'src/store';
import axios from 'axios';
import { apiConfig } from 'src/config';
import { SortDirection } from '@mui/material';
import type { Campaign } from '../types/campaign';
import type { Meta } from '../types/pagination';

interface CampaignState {
  searchText?: string,
  limit?: number,
  page?: number,
  total?: number,
  lastPage?: number,
  orderBy: string,
  order: SortDirection,
  campaigns: Campaign[],
}

const initialState: CampaignState = {
  searchText: '',
  limit: 9,
  page: 1,
  total: 0,
  lastPage: 0,
  orderBy: 'name',
  order: 'asc',
  campaigns: [],
};

const slice = createSlice({
  name: 'campaign',
  initialState,
  reducers: {
    getCampaigns(state: CampaignState, action: PayloadAction<{ campaigns: Campaign[]; meta: Meta }>) {
      const { campaigns, meta } = action.payload;

      state.campaigns = campaigns;
      state.total = meta.total;
      state.lastPage = meta.last_page;
    },
    setPage(state: CampaignState, action: PayloadAction<{ page: number }>) {
      const { page } = action.payload;

      state.page = page;
    },
    setSearchText(state: CampaignState, action: PayloadAction<{ searchText: string }>) {
      const { searchText } = action.payload;

      state.searchText = searchText;
      state.page = 0;
    },
    deleteCampaign(state: CampaignState, action: PayloadAction<{ campaignId: number; }>) {
      const { campaignId } = action.payload;

      state.campaigns = state.campaigns.filter((contact) => contact.id !== campaignId);
    }
  }
});

export const { reducer } = slice;

export const getCampaigns = (
  organisationId: number,
  limit: number,
  page: number,
  filter: string = '',
  sortBy: string = '',
): AppThunk => async (dispatch) => {
  const response = await axios.get(`${apiConfig.apiV2Url}/organisations/${organisationId}/campaign?filter=${filter}&limit=${limit}&order=${sortBy}&page=${page}`);

  dispatch(slice.actions.getCampaigns({ campaigns: response.data.data, meta: response.data.meta }));
};

export const setPage = (page: number) => (dispatch) => {
  dispatch(slice.actions.setPage({ page }));
};

export const setSearchText = (searchText: string) => (dispatch) => {
  dispatch(slice.actions.setSearchText({ searchText }));
};

export const updateCampaign = (
  organisationId: number,
  campaignId: number,
  data: any,
  limit: number,
  page: number,
  filter: string = '',
  sortBy: string = '',
): AppThunk => async (dispatch): Promise<void> => {
  await axios.put(`${apiConfig.apiV2Url}/organisations/${organisationId}/campaign/${campaignId}`, data);

  const response = await axios.get(`${apiConfig.apiV2Url}/organisations/${organisationId}/campaign?filter=${filter}&limit=${limit}&order=${sortBy}&page=${page}`);

  dispatch(slice.actions.getCampaigns({ campaigns: response.data.data, meta: response.data.meta }));
};

export const deleteCampaign = (organisationId: number, campaignId: number): AppThunk => async (dispatch) => {
  await axios.delete(`${apiConfig.apiV2Url}/organisations/${organisationId}/campaign/${campaignId}`);

  dispatch(slice.actions.deleteCampaign({ campaignId }));
};

export default slice;
