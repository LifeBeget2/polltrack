import { createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit';
import type { AppThunk } from 'src/store';
import axios from 'axios';
import { apiConfig } from 'src/config';
import type { PoolType, CustomException, PoolSanitiser, PoolParams, Location, GroundLevel, Classification, SanitiserClassification, PoolSurfaceType } from '../types/pool';

interface PoolSpecificationsState {
  poolSanitisers: PoolSanitiser[],
  customExceptions: CustomException[],
  poolTypes: PoolType[],
  locations: Location[],
  groundLevels: GroundLevel[],
  classifications: Classification[],
  sanitiserClassifications: SanitiserClassification[],
  surfaceTypes: PoolSurfaceType[];
}

const initialState: PoolSpecificationsState = {
  poolSanitisers: [],
  customExceptions: [],
  poolTypes: [],
  locations: [],
  groundLevels: [],
  classifications: [],
  sanitiserClassifications: [],
  surfaceTypes: [],
};

const slice = createSlice({
  name: 'poolSpecifications',
  initialState,
  reducers: {
    getPoolSanitisers(state: PoolSpecificationsState, action: PayloadAction<{ poolSanitisers: PoolSanitiser[] }>) {
      const { poolSanitisers } = action.payload;

      state.poolSanitisers = poolSanitisers;
    },
    getCustomExceptions(state: PoolSpecificationsState, action: PayloadAction<{ customExceptions: CustomException[] }>) {
      const { customExceptions } = action.payload;

      state.customExceptions = customExceptions;
    },
    getPoolParams(state: PoolSpecificationsState, action: PayloadAction<{ poolParams: PoolParams, sanitiserClassifications: SanitiserClassification[] }>) {
      const { poolParams, sanitiserClassifications } = action.payload;
      const { classifications, groundLevels, locations, poolTypes } = poolParams;

      state.poolTypes = poolTypes;
      state.classifications = classifications;
      state.groundLevels = groundLevels;
      state.locations = locations;
      state.sanitiserClassifications = sanitiserClassifications;
    },
    getSurfaceTypes(state: PoolSpecificationsState, action: PayloadAction<{ surfaceTypes: PoolSurfaceType[] }>) {
      const { surfaceTypes } = action.payload;

      state.surfaceTypes = surfaceTypes;
    },
  }
});

export const { reducer } = slice;

export const getPoolParams = (): AppThunk => async (dispatch) => {
  const response = await axios.get(`${apiConfig.apiV1Url}/pool-params`);
  const sanitiserClassifications = await axios.get(`${apiConfig.apiV1Url}/sanitiser-classifications`);

  dispatch(slice.actions.getPoolParams({ poolParams: response.data, sanitiserClassifications: sanitiserClassifications.data }));
};

export const getPoolSanitisers = (organisationId?: number): AppThunk => async (dispatch) => {
  const response = await axios.get(`${apiConfig.apiV1Url}/organisations/${organisationId}/sanitisers`);

  dispatch(slice.actions.getPoolSanitisers({ poolSanitisers: response.data }));
};

export const getCustomExceptions = (organisationId?: number): AppThunk => async (dispatch) => {
  const response = await axios.get(`${apiConfig.apiV1Url}/organisations/${organisationId}/customexceptions`);

  dispatch(slice.actions.getCustomExceptions({ customExceptions: response.data }));
};

export const getPoolSurfaceTypes = (organisationId?: number): AppThunk => async (dispatch) => {
  const response = await axios.get(`${apiConfig.apiV1Url}/organisations/${organisationId}/surfacetypes`);

  dispatch(slice.actions.getSurfaceTypes({ surfaceTypes: response.data }));
};

export default slice;
