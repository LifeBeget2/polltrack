import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import axios from 'axios';

import { apiConfig } from 'src/config';
import { AppThunk } from 'src/store';
import { SmsTier } from 'src/types/sms';

interface SmsTierState {
  tiers: SmsTier[];
  isLoading: boolean;
}

const initialState: SmsTierState = {
  tiers: [],
  isLoading: false,
};

const slice = createSlice({
  name: 'smsTier',
  initialState,
  reducers: {
    startLoading(state: SmsTierState) {
      state.isLoading = true;
    },
    stopLoading(state: SmsTierState) {
      state.isLoading = false;
    },
    getSmsTiers(
      state: SmsTierState,
      action: PayloadAction<{ tiers: SmsTier[] }>
    ) {
      const { tiers } = action.payload;

      state.tiers = tiers;
    },
  },
});

export const { reducer } = slice;

export const getSmsTiers = (organisationId: number): AppThunk => async (dispatch) => {
  dispatch(slice.actions.startLoading());
  try {
    const response = await axios.get(
      `${apiConfig.apiV1Url}/organisations/${organisationId}/sms-tier`
    );
    dispatch(slice.actions.getSmsTiers({ tiers: response.data.tiers }));
  } finally {
    dispatch(slice.actions.stopLoading());
  }
};

export default slice;
