import { createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit';
import type { AppThunk } from 'src/store';
import axios from 'axios';
import { apiConfig } from 'src/config';
import type {
  Organisation,
  Color,
  StripeConnection,
  AnalysisPreferences
} from 'src/types/organisation';
import type { InvoiceSettings } from '../types/invoiceSettings';
import type { TradingTerm } from '../types/contact';
import type { Image } from '../types/image';
import type { BatchSettings, QuoteSettings } from '../types/invoice';
import { MessagingSettings } from 'src/types/messagingSettings';
import { NotificationSettings } from 'src/types/notificationSettings';

interface AccountState {
  organisation?: Organisation;
  gst: number;
  stripeConnection?: StripeConnection;
  logo?: Image;
  analysisPreferences?: AnalysisPreferences;
  invoiceSettings?: InvoiceSettings;
  batchSettings?: BatchSettings;
  quoteSettings?: QuoteSettings;
  tradingTerms?: TradingTerm[];
  colors?: Color[];
  isStripeConnected: boolean;
  isVendConnected: boolean;
  isXeroConnected: boolean;
}

const initialState: AccountState = {
  organisation: null,
  gst: 0.1,
  stripeConnection: null,
  logo: null,
  analysisPreferences: null,
  invoiceSettings: null,
  batchSettings: null,
  quoteSettings: null,
  tradingTerms: [],
  colors: [],
  isStripeConnected: false,
  isVendConnected: false,
  isXeroConnected: false,
};

const slice = createSlice({
  name: 'account',
  initialState,
  reducers: {
    getOrganisation(state: AccountState, action: PayloadAction<{ organisation: Organisation; }>) {
      const { organisation } = action.payload;

      state.organisation = organisation;
      // TODO: we need to elaborate a dynamic taxation in case of moving to the US
      state.gst = organisation.company_address_country === 'NZ' ? 0.15 : 0.1;
      state.stripeConnection = organisation.stripe_connection;
      state.logo = organisation.logo;
      state.analysisPreferences = organisation.analysis_preferences;
      state.isXeroConnected = organisation.is_xero_enabled;
      state.isVendConnected = organisation.is_vend_enabled;
      state.isStripeConnected = organisation.is_stripe_connected;
    },
    updateOrganisation(state: AccountState, action: PayloadAction<{ organisation: Organisation; }>) {
      const { organisation } = action.payload;

      state.organisation = organisation;
      state.stripeConnection = organisation.stripe_connection;
      state.logo = organisation.logo;
      state.isXeroConnected = organisation.is_xero_enabled;
      state.isVendConnected = organisation.is_vend_enabled;
      state.isStripeConnected = organisation.is_stripe_connected;
    },
    updateOrganisationLogo(state: AccountState, action: PayloadAction<{ logo: Image; }>) {
      const { logo } = action.payload;

      state.logo = logo;
    },
    updateOrganisationMessagingSettings(state: AccountState, action: PayloadAction<{ messagingSettings: MessagingSettings; }>) {
      const { messagingSettings } = action.payload;
      state.organisation.messaging_settings = messagingSettings;
    },
    updateNotificationSettings(state: AccountState, action: PayloadAction<{ notificationSettings: NotificationSettings }>) {
      const { notificationSettings } = action.payload;
      state.organisation.notification_settings = notificationSettings;
    },
    getInvoiceSettings(state: AccountState, action: PayloadAction<{ invoiceSettings: InvoiceSettings; }>) {
      const { invoiceSettings } = action.payload;

      state.invoiceSettings = invoiceSettings;
    },
    getQuoteSettings(state: AccountState, action: PayloadAction<{ quoteSettings: QuoteSettings; }>) {
      const { quoteSettings } = action.payload;

      state.quoteSettings = quoteSettings;
    },
    getBatchSettings(state: AccountState, action: PayloadAction<{ batchSettings: BatchSettings; }>) {
      const { batchSettings } = action.payload;

      state.batchSettings = batchSettings;
    },
    getTradingTerms(state: AccountState, action: PayloadAction<{ tradingTerms: TradingTerm[]; }>) {
      const { tradingTerms } = action.payload;

      state.tradingTerms = tradingTerms;
    },
    createTradingTerm(
      state: AccountState,
      action: PayloadAction<{ tradingTerm: TradingTerm }>
    ): void {
      const { tradingTerm } = action.payload;

      state.tradingTerms.push(tradingTerm);
    },
    updateTradingTerm(
      state: AccountState,
      action: PayloadAction<{ tradingTerm: TradingTerm }>
    ): void {
      const { tradingTerm } = action.payload;

      state.tradingTerms = state.tradingTerms.map((_tradingTerm) => {
        if (_tradingTerm.id === tradingTerm.id) {
          return tradingTerm;
        }

        return _tradingTerm;
      });
    },
    deleteTradingTerm(
      state: AccountState,
      action: PayloadAction<{ tradingTermId: number }>
    ): void {
      const { tradingTermId } = action.payload;

      state.tradingTerms = state.tradingTerms.filter((tradingTerm) => tradingTerm.id !== tradingTermId);
    },
    getColors(state: AccountState, action: PayloadAction<{ colors: Color[]; }>) {
      const { colors } = action.payload;

      state.colors = colors;
    },
    createColor(
      state: AccountState,
      action: PayloadAction<{ color: Color }>
    ): void {
      const { color } = action.payload;

      state.colors.push(color);
    },
    updateColor(
      state: AccountState,
      action: PayloadAction<{ color: Color }>
    ): void {
      const { color } = action.payload;

      state.colors = state.colors.map((_color) => {
        if (_color.id === color.id) {
          return color;
        }

        return _color;
      });
    },
    deleteColor(
      state: AccountState,
      action: PayloadAction<{ colorId: number }>
    ): void {
      const { colorId } = action.payload;

      state.colors = state.colors.filter((color) => color.id !== colorId);
    },
  }
});

export const { reducer } = slice;

export const getOrganisation = (organisationId: number): AppThunk => async (dispatch) => {
  const response = await axios.get(`${apiConfig.apiV1Url}/organisations/${organisationId}`);

  dispatch(slice.actions.getOrganisation({ organisation: response.data }));
};

export const getInvoiceSettings = (organisationId: number): AppThunk => async (dispatch) => {
  const response = await axios.get(`${apiConfig.apiV1Url}/organisations/${organisationId}/invoice_settings`);

  dispatch(slice.actions.getInvoiceSettings({ invoiceSettings: response.data }));
};

export const saveInvoiceSettings = (organisationId: number, invoiceSettingsId: number, data: any): AppThunk => async (dispatch) => {
  const response = await axios.put(`${apiConfig.apiV1Url}/organisations/${organisationId}/invoice_settings/${invoiceSettingsId}`, data);

  dispatch(slice.actions.getInvoiceSettings({ invoiceSettings: response.data }));
};

export const getQuoteSettings = (organisationId: number): AppThunk => async (dispatch) => {
  const response = await axios.get(`${apiConfig.apiV1Url}/organisations/${organisationId}/quote_settings`);

  dispatch(slice.actions.getQuoteSettings({ quoteSettings: response.data }));
};

export const saveQuoteSettings = (organisationId: number, quoteSettingsId: number, data: any): AppThunk => async (dispatch) => {
  const response = await axios.put(`${apiConfig.apiV1Url}/organisations/${organisationId}/quote_settings/${quoteSettingsId}`, data);

  dispatch(slice.actions.getQuoteSettings({ quoteSettings: response.data }));
};

export const getBatchSettings = (organisationId: number): AppThunk => async (dispatch) => {
  const response = await axios.get(`${apiConfig.apiV1Url}/organisations/${organisationId}/batch-settings`);

  dispatch(slice.actions.getBatchSettings({ batchSettings: response.data }));
};

export const saveBatchSettings = (organisationId: number, data: any): AppThunk => async (dispatch) => {
  const response = await axios.put(`${apiConfig.apiV1Url}/organisations/${organisationId}/batch-settings`, data);

  dispatch(slice.actions.getBatchSettings({ batchSettings: response.data }));
};

export const saveLabSettings = (organisationId: number, data: any): AppThunk => async (dispatch) => {
  const response = await axios.put(`${apiConfig.apiV1Url}/organisations/${organisationId}/analysis-preferences`, data);

  dispatch(slice.actions.getOrganisation({ organisation: response.data }));
};

export const saveJobMessagingSettings = (organisationId: number, data: any): AppThunk => async (dispatch) => {
  const response = await axios.put(`${apiConfig.apiV1Url}/organisations/${organisationId}/messaging-settings`, data);

  dispatch(slice.actions.updateOrganisationMessagingSettings({ messagingSettings: response.data }));
};

export const saveNotificationSettings = (organisationId: number, data: any): AppThunk => async (dispatch) => {
  const response = await axios.put(`${apiConfig.apiV1Url}/organisations/${organisationId}/notification-settings`, data);

  dispatch(slice.actions.updateNotificationSettings({ notificationSettings: response.data }));
};

export const buyPack = (organisationId: number): AppThunk => async (dispatch) => {
  const data = {
    org_id: organisationId,
    type: 'buy-pack',
  };
  const response = await axios.post(`${apiConfig.apiV1Url}/organisations/${organisationId}/buy-pack`, data);

  dispatch(slice.actions.updateNotificationSettings({ notificationSettings: response.data }));
};

export const getTradingTerms = (organisationId: number): AppThunk => async (dispatch) => {
  const response = await axios.get(`${apiConfig.apiV1Url}/organisations/${organisationId}/trading-term`);

  dispatch(slice.actions.getTradingTerms({ tradingTerms: response.data }));
};

export const createTradingTerm = (organisationId: number, data: any): AppThunk => async (dispatch): Promise<void> => {
  const response = await axios.post(`${apiConfig.apiV1Url}/organisations/${organisationId}/trading-term`, data);

  dispatch(slice.actions.createTradingTerm({ tradingTerm: response.data }));
};

export const updateTradingTerm = (
  organisationId: number,
  tradingTermId: number,
  data: any
): AppThunk => async (dispatch): Promise<void> => {
  const response = await axios.put(`${apiConfig.apiV1Url}/organisations/${organisationId}/trading-term/${tradingTermId}`, data);

  dispatch(slice.actions.updateTradingTerm({ tradingTerm: response.data }));
};

export const updateDefaultTerm = (
  organisationId: number,
  tradingTerm: TradingTerm
): AppThunk => async (dispatch): Promise<void> => {
  await axios.put(`${apiConfig.apiV1Url}/organisations/${organisationId}/trading-term/${tradingTerm.id}`, {
    name: tradingTerm.name,
    type: tradingTerm.type,
    from_type: tradingTerm.from_type,
    quantity: tradingTerm.quantity,
    is_default: !tradingTerm.is_default
  });

  const response = await axios.get(`${apiConfig.apiV1Url}/organisations/${organisationId}/trading-term`);

  dispatch(slice.actions.getTradingTerms({ tradingTerms: response.data }));
};

export const deleteTradingTerm = (organisationId: number, tradingTermId: number): AppThunk => async (dispatch): Promise<void> => {
  await axios.delete(`${apiConfig.apiV1Url}/organisations/${organisationId}/trading-term/${tradingTermId}`);

  dispatch(slice.actions.deleteTradingTerm({ tradingTermId }));
};

export const updateOrganisation = (organisationId: number, data: any): AppThunk => async (dispatch) => {
  const response = await axios.put(`${apiConfig.apiV1Url}/organisations/${organisationId}`, data);
  dispatch(slice.actions.updateOrganisation({ organisation: response.data }));
};

export const updateOrganisationLogo = (organisationId: number, file: File): AppThunk => async (dispatch) => {
  const config = { headers: { 'Content-Type': 'multipart/form-data' } };
  const fd = new FormData();
  fd.append('logo', file);

  const response = await axios.post(`${apiConfig.apiV2Url}/organisations/${organisationId}/update-logo`, fd, config);
  dispatch(slice.actions.updateOrganisationLogo({ logo: response.data.logo }));
};

export const connectStripeAccount = (organisationId: number, code: string): AppThunk => async (dispatch) => {
  const response = await axios.post(`${apiConfig.apiV1Url}/organisations/${organisationId}/connect-stripe`, { code });
  dispatch(slice.actions.updateOrganisation({ organisation: response.data }));
};

export const updateStripeConnection = (organisationId: number, data: any): AppThunk => async (dispatch) => {
  const response = await axios.post(`${apiConfig.apiV1Url}/organisations/${organisationId}/update-stripe-connection`, data);
  dispatch(slice.actions.updateOrganisation({ organisation: response.data }));
};

export const disconnectStripeAccount = (organisationId: number): AppThunk => async (dispatch) => {
  const response = await axios.post(`${apiConfig.apiV1Url}/organisations/${organisationId}/disconnect-stripe`);
  dispatch(slice.actions.updateOrganisation({ organisation: response.data }));
};

export const getColors = (organisationId: number): AppThunk => async (dispatch) => {
  const response = await axios.get(`${apiConfig.apiV2Url}/organisations/${organisationId}/color`);

  dispatch(slice.actions.getColors({ colors: response.data }));
};

export const createColor = (organisationId: number, data: any): AppThunk => async (dispatch): Promise<void> => {
  const response = await axios.post(`${apiConfig.apiV2Url}/organisations/${organisationId}/color`, data);

  dispatch(slice.actions.createColor({ color: response.data }));
};

export const updateColor = (
  organisationId: number,
  colorId: number,
  data: any
): AppThunk => async (dispatch): Promise<void> => {
  const response = await axios.put(`${apiConfig.apiV2Url}/organisations/${organisationId}/color/${colorId}`, data);

  dispatch(slice.actions.updateColor({ color: response.data }));
};

export const deleteColor = (organisationId: number, colorId: number): AppThunk => async (dispatch): Promise<void> => {
  await axios.delete(`${apiConfig.apiV2Url}/organisations/${organisationId}/color/${colorId}`);

  dispatch(slice.actions.deleteColor({ colorId }));
};

export default slice;
