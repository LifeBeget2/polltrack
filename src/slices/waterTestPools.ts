import { createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit';
import type { AppThunk } from 'src/store';
import axios from 'axios';
import { apiConfig } from 'src/config';
import { SortDirection } from '@mui/material';
import type { Pool } from '../types/pool';
import type { Meta } from '../types/pagination';

interface WaterTestPoolsState {
  searchText?: string,
  expandedPoolId?: number | null,
  limit?: number,
  page?: number,
  total?: number,
  orderBy: string,
  order: SortDirection,
  pools: Pool[],

  enabledColumns?: string[],
  enabledColumnsRestored: boolean,
}

const initialState: WaterTestPoolsState = {
  searchText: '',
  expandedPoolId: null,
  limit: 10,
  page: 0,
  total: 0,
  orderBy: 'created_at',
  order: 'desc',
  pools: [],

  enabledColumns: null,
  enabledColumnsRestored: false,
};

const LOCALSTORAGE_KEY_ENABLED_COLUMNS = 'water_test_pools_enabled_columns';

const slice = createSlice({
  name: 'waterTestPools',
  initialState,
  reducers: {
    getPools(state: WaterTestPoolsState, action: PayloadAction<{ pools: Pool[]; meta: Meta }>) {
      const { pools, meta } = action.payload;

      state.pools = pools;
      state.total = meta.total;
    },
    setLimit(state: WaterTestPoolsState, action: PayloadAction<{ limit: number }>) {
      const { limit } = action.payload;

      state.limit = limit;
      state.page = 0;
      state.total = 0;
    },
    setPage(state: WaterTestPoolsState, action: PayloadAction<{ page: number }>) {
      const { page } = action.payload;

      state.page = page;
    },
    setExpandedRow(state: WaterTestPoolsState, action: PayloadAction<{ poolId: number | null }>) {
      const { poolId } = action.payload;

      state.expandedPoolId = poolId;
    },
    setSearchText(state: WaterTestPoolsState, action: PayloadAction<{ searchText: string }>) {
      const { searchText } = action.payload;

      state.searchText = searchText;
      state.page = 0;
    },
    setOrder(state: WaterTestPoolsState, action: PayloadAction<{ orderBy: string; order: SortDirection }>) {
      const { orderBy, order } = action.payload;

      state.orderBy = orderBy;
      state.order = order;
    },
    changeVisibility(state: WaterTestPoolsState, action: PayloadAction<{ poolId: number; visibility: boolean }>) {
      const { poolId, visibility } = action.payload;
      const pool = state.pools.find((pool) => pool.id === poolId);

      if (pool) {
        pool.visibility = visibility;
      }
    },
    deletePool(state: WaterTestPoolsState, action: PayloadAction<{ poolId: number; }>) {
      const { poolId } = action.payload;

      state.pools = state.pools.filter((pool) => pool.id !== poolId);
    },
    setEnabledColumns(state: WaterTestPoolsState, action: PayloadAction<{ enabledColumns: string[]; }>) {
      state.enabledColumns = action.payload.enabledColumns;
    },
    enabledColumnsRestored(state: WaterTestPoolsState) {
      state.enabledColumnsRestored = true;
    },
  }
});

export const { reducer } = slice;

export const getPools = (
  organisationId: number,
  limit: number,
  page: number,
  filter: string = '',
  sortBy: string = '',
): AppThunk => async (dispatch) => {
  const response = await axios.get(`${apiConfig.apiV2Url}/organisations/${organisationId}/pools?filter=${filter}&limit=${limit}&order=${sortBy}&page=${page + 1}`);

  dispatch(slice.actions.getPools({ pools: response.data.data, meta: response.data.meta }));
};

export const setLimit = (limit: number) => (dispatch) => {
  dispatch(slice.actions.setLimit({ limit }));
};

export const setPage = (page: number) => (dispatch) => {
  dispatch(slice.actions.setPage({ page }));
};

export const setExpandedRow = (poolId: number | null) => (dispatch) => {
  dispatch(slice.actions.setExpandedRow({ poolId }));
};

export const setSearchText = (searchText: string) => (dispatch) => {
  dispatch(slice.actions.setSearchText({ searchText }));
};

export const setOrder = (orderBy: string, order: SortDirection) => (dispatch) => {
  dispatch(slice.actions.setOrder({ orderBy, order }));
};

export const changeVisibility = (organisationId: number, poolId: number, visibility: boolean): AppThunk => async (dispatch) => {
  await axios.put(`${apiConfig.apiV1Url}/organisations/${organisationId}/pools/${poolId}/visibility`, { visibility: +visibility });

  dispatch(slice.actions.changeVisibility({ poolId, visibility }));
};

export const deletePool = (organisationId: number, poolId: number): AppThunk => async (dispatch) => {
  await axios.delete(`${apiConfig.apiV1Url}/organisations/${organisationId}/pools/${poolId}`);

  dispatch(slice.actions.deletePool({ poolId }));
};

export const setEnabledColumns = (enabledColumns: string[]) => (dispatch) => {
  dispatch(slice.actions.setEnabledColumns({ enabledColumns }));
  localStorage.setItem(LOCALSTORAGE_KEY_ENABLED_COLUMNS, JSON.stringify(enabledColumns));
};

export const restoreEnabledColumns = () => (dispatch) => {
  const enabledColumns = JSON.parse(localStorage.getItem(LOCALSTORAGE_KEY_ENABLED_COLUMNS));
  dispatch(slice.actions.setEnabledColumns({ enabledColumns }));
  dispatch(slice.actions.enabledColumnsRestored());
};

export default slice;
