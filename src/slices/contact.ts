import { createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit';
import type { AppThunk } from 'src/store';
import axios from 'axios';
import { apiConfig } from 'src/config';
import { SortDirection } from '@mui/material';
import { Contact } from '../types/contact';
import { Meta } from '../types/pagination';

interface ContactState {
  searchText?: string,
  expandedContactId?: number | null,
  limit?: number,
  page?: number,
  total?: number,
  orderBy: string,
  order: SortDirection,
  contacts: Contact[],

  enabledColumns?: string[],
  enabledColumnsRestored: boolean,
}

const initialState: ContactState = {
  searchText: '',
  expandedContactId: null,
  limit: 10,
  page: 0,
  total: 0,
  orderBy: 'first_name',
  order: 'asc',
  contacts: [],

  enabledColumns: null,
  enabledColumnsRestored: false,
};

const LOCALSTORAGE_KEY_ENABLED_COLUMNS = 'contact_enabled_columns_1';

const slice = createSlice({
  name: 'contact',
  initialState,
  reducers: {
    getContacts(state: ContactState, action: PayloadAction<{ contacts: Contact[]; meta: Meta }>) {
      const { contacts, meta } = action.payload;

      state.contacts = contacts;
      state.total = meta.total;
    },
    setLimit(state: ContactState, action: PayloadAction<{ limit: number }>) {
      const { limit } = action.payload;

      state.limit = limit;
      state.page = 0;
      state.total = 0;
    },
    setPage(state: ContactState, action: PayloadAction<{ page: number }>) {
      const { page } = action.payload;

      state.page = page;
    },
    setExpandedRow(state: ContactState, action: PayloadAction<{ contactId: number | null }>) {
      const { contactId } = action.payload;

      state.expandedContactId = contactId;
    },
    setSearchText(state: ContactState, action: PayloadAction<{ searchText: string }>) {
      const { searchText } = action.payload;

      state.searchText = searchText;
      state.page = 0;
    },
    setOrder(state: ContactState, action: PayloadAction<{ orderBy: string; order: SortDirection }>) {
      const { orderBy, order } = action.payload;

      state.orderBy = orderBy;
      state.order = order;
    },
    changeVisibility(state: ContactState, action: PayloadAction<{ contactId: number; visibility: boolean }>) {
      const { contactId, visibility } = action.payload;
      const contact = state.contacts.find((contact) => contact.id === contactId);

      if (contact) {
        contact.visibility = visibility;
      }
    },
    deleteContact(state: ContactState, action: PayloadAction<{ contactId: number; }>) {
      const { contactId } = action.payload;

      state.contacts = state.contacts.filter((contact) => contact.id !== contactId);
    },
    setEnabledColumns(state: ContactState, action: PayloadAction<{ enabledColumns: string[]; }>) {
      state.enabledColumns = action.payload.enabledColumns;
    },
    enabledColumnsRestored(state: ContactState) {
      state.enabledColumnsRestored = true;
    },
  }
});

export const { reducer } = slice;

export const getContacts = (
  organisationId: number,
  limit: number,
  page: number,
  filter: string = '',
  sortBy: string = '',
): AppThunk => async (dispatch) => {
  const response = await axios.get(`${apiConfig.apiV2Url}/organisations/${organisationId}/contacts?filter=${filter}&limit=${limit}&order=${sortBy}&page=${page + 1}&include=pools`);

  dispatch(slice.actions.getContacts({ contacts: response.data.data, meta: response.data.meta }));
};

export const setLimit = (limit: number) => (dispatch) => {
  dispatch(slice.actions.setLimit({ limit }));
};

export const setPage = (page: number) => (dispatch) => {
  dispatch(slice.actions.setPage({ page }));
};

export const setExpandedRow = (contactId: number | null) => (dispatch) => {
  dispatch(slice.actions.setExpandedRow({ contactId }));
};

export const setSearchText = (searchText: string) => (dispatch) => {
  dispatch(slice.actions.setSearchText({ searchText }));
};

export const setOrder = (orderBy: string, order: SortDirection) => (dispatch) => {
  dispatch(slice.actions.setOrder({ orderBy, order }));
};

export const setEnabledColumns = (enabledColumns: string[]) => (dispatch) => {
  dispatch(slice.actions.setEnabledColumns({ enabledColumns }));
  localStorage.setItem(LOCALSTORAGE_KEY_ENABLED_COLUMNS, JSON.stringify(enabledColumns));
};

export const restoreEnabledColumns = () => (dispatch) => {
  const enabledColumns = JSON.parse(localStorage.getItem(LOCALSTORAGE_KEY_ENABLED_COLUMNS));
  dispatch(slice.actions.setEnabledColumns({ enabledColumns }));
  dispatch(slice.actions.enabledColumnsRestored());
};

export const changeVisibility = (organisationId: number, contactId: number, visibility: boolean): AppThunk => async (dispatch) => {
  await axios.put(`${apiConfig.apiV1Url}/organisations/${organisationId}/contacts/${contactId}/visibility`, { visibility: +visibility });

  dispatch(slice.actions.changeVisibility({ contactId, visibility }));
};

export const deleteContact = (organisationId: number, contactId: number): AppThunk => async (dispatch) => {
  await axios.delete(`${apiConfig.apiV1Url}/organisations/${organisationId}/contacts/${contactId}`);

  dispatch(slice.actions.deleteContact({ contactId }));
};

export default slice;
