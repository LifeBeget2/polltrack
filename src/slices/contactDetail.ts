import { createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit';
import axios from 'axios';
import merge from 'lodash/merge';
import type { AppThunk } from '../store';
import type { Pool } from '../types/pool';
import type { Note } from '../types/note';
import type { Contact } from '../types/contact';
import { apiConfig } from '../config';
import { OWNER } from '../constants/poolContact';
import type { Invoice } from '../types/contactInvoice';
import type { Meta } from '../types/pagination';
import type { SortDirection } from '@mui/material';
import moment from 'moment/moment';

interface Range {
  from: string;
  to: string;
}

interface ContactDetailState {
  isLoaded: boolean;
  contactId?: number;
  contact?: Contact;
  pools: Pool[];
  notes?: Note[];
  isInvoicesLoading: boolean;
  invoices: Invoice[];
  selectedInvoicesRange: Range;
  invoicesLimit?: number,
  invoicesPage?: number,
  invoicesTotal?: number,
  invoicesOrderBy: string,
  invoicesOrder: SortDirection,
}

const initialState: ContactDetailState = {
  isLoaded: false,
  contactId: null,
  contact: null,
  pools: [],
  isInvoicesLoading: false,
  invoices: [],
  notes: [],
  selectedInvoicesRange: {
    from: moment().subtract(3, 'month').format('YYYY-MM-DD'),
    to: moment().format('YYYY-MM-DD')
  },
  invoicesLimit: 10,
  invoicesPage: 0,
  invoicesTotal: 0,
  invoicesOrderBy: 'created_at',
  invoicesOrder: 'desc',
};

const slice = createSlice({
  name: 'contactDetail',
  initialState,
  reducers: {
    reset(
      state: ContactDetailState,
    ): void {
      state.isLoaded = false;
      state.contactId = null;
      state.contact = null;
      state.pools = [];
      state.notes = [];
      state.isInvoicesLoading = false;
      state.invoices = [];
      state.invoicesPage = 0;
      state.invoicesTotal = 0;
      state.invoicesOrderBy = 'created_at';
      state.invoicesOrder = 'desc';
    },
    getContact(
      state: ContactDetailState,
      action: PayloadAction<{ contact: Contact | null }>
    ): void {
      const { contact } = action.payload;

      if (contact) {
        state.contact = contact;
        state.pools = contact.pools;
        state.notes = contact.contact_notes ? contact.contact_notes : [];
      } else {
        state.contactId = null;
        state.contact = null;
      }
      state.isLoaded = true;
    },
    updateContact(
      state: ContactDetailState,
      action: PayloadAction<{ contact: Contact }>
    ): void {
      const { contact } = action.payload;

      state.contact = contact;
      state.pools = contact.pools;
      state.notes = contact.contact_notes ? contact.contact_notes : [];
    },
    addPool(
      state: ContactDetailState,
      action: PayloadAction<{ pool: Pool }>
    ): void {
      const { pool } = action.payload;
      const exists = state.pools.find((_pool) => _pool.id === pool.id);

      if (!exists) {
        state.pools.push(merge({}, pool, {
          pivot: {
            address_type_id: OWNER,
            pool_id: pool.id
          },
        }));
      }
    },
    updatePool(
      state: ContactDetailState,
      action: PayloadAction<{ pool: Pool }>
    ): void {
      const { pool } = action.payload;

      state.pools = state.pools.map((_pool) => {
        if (_pool.id === pool.id) {
          return merge({}, pool, { pivot: _pool.pivot });
        }

        return _pool;
      });
    },
    updatePoolRelation(
      state: ContactDetailState,
      action: PayloadAction<{ poolId: number; relationId: number }>
    ): void {
      const { poolId, relationId } = action.payload;

      state.pools = state.pools.map((_pool) => {
        if (_pool.id === poolId) {
          _pool.pivot.address_type_id = relationId;
          return _pool;
        }

        return _pool;
      });
    },
    unlinkPool(
      state: ContactDetailState,
      action: PayloadAction<{ poolId: number }>
    ): void {
      const { poolId } = action.payload;

      state.pools = state.pools.filter((pool) => pool.id !== poolId);
    },
    getInvoices(state: ContactDetailState, action: PayloadAction<{ invoices: Invoice[]; meta: Meta }>) {
      const { invoices, meta } = action.payload;

      state.invoices = invoices;
      state.invoicesTotal = meta.total;
      state.isInvoicesLoading = false;
    },
    setInvoicesLimit(state: ContactDetailState, action: PayloadAction<{ limit: number }>) {
      const { limit } = action.payload;

      state.invoicesLimit = limit;
      state.invoicesPage = 0;
      state.invoicesTotal = 0;
    },
    setInvoicesPage(state: ContactDetailState, action: PayloadAction<{ page: number }>) {
      const { page } = action.payload;

      state.invoicesPage = page;
    },
    setIsInvoicesLoading(state: ContactDetailState, action: PayloadAction<{ isLoading: boolean }>) {
      const { isLoading } = action.payload;

      state.isInvoicesLoading = isLoading;
    },
    setInvoicesOrder(state: ContactDetailState, action: PayloadAction<{ orderBy: string; order: SortDirection }>) {
      const { orderBy, order } = action.payload;

      state.invoicesOrderBy = orderBy;
      state.invoicesOrder = order;
    },
    selectInvoicesRange(
      state: ContactDetailState,
      action: PayloadAction<{ from: string; to: string }>
    ): void {
      const { from, to } = action.payload;

      state.selectedInvoicesRange = {
        from,
        to
      };
    },
  }
});

export const { reducer } = slice;

export const reset = (): AppThunk => (dispatch): void => {
  dispatch(slice.actions.reset());
};

export const getContact = (organisationId?: number, contactId?: number): AppThunk => async (dispatch): Promise<void> => {
  if (organisationId && contactId) {
    const response = await axios.get(`${apiConfig.apiV1Url}/organisations/${organisationId}/contacts/${contactId}?include=pools,pools.contacts`);

    dispatch(slice.actions.getContact({ contact: response.data }));
  } else {
    dispatch(slice.actions.getContact({ contact: null }));
  }
};

export const updateContact = (
  organisationId: number,
  data: any,
  contactId?: number
): AppThunk => async (dispatch): Promise<void> => {
  let response;

  if (contactId) {
    response = await axios.put(`${apiConfig.apiV1Url}/organisations/${organisationId}/contacts/${contactId}`, data);
  } else {
    response = await axios.post(`${apiConfig.apiV1Url}/organisations/${organisationId}/contacts`, data);
  }

  dispatch(slice.actions.updateContact({ contact: response.data }));
};

export const addPool = (pool: Pool): AppThunk => (dispatch): void => {
  dispatch(slice.actions.addPool({ pool }));
};

export const unlinkPool = (poolId: number): AppThunk => (dispatch): void => {
  dispatch(slice.actions.unlinkPool({ poolId }));
};

export const updatePool = (pool: Pool): AppThunk => (dispatch): void => {
  dispatch(slice.actions.updatePool({ pool }));
};

export const updatePoolRelation = (poolId: number, relationId: number): AppThunk => (dispatch): void => {
  dispatch(slice.actions.updatePoolRelation({ poolId, relationId }));
};

export const getContactInvoices = (
  organisationId: number,
  contactId: number,
  selectedRange: Range,
  limit: number,
  page: number,
  sortBy: string = '',
): AppThunk => async (dispatch) => {
  dispatch(slice.actions.setIsInvoicesLoading({ isLoading: true }));
  const response = await axios.get(`${apiConfig.apiV2Url}/organisations/${organisationId}/contact-invoice?contact_id=${contactId}&from=${selectedRange.from}&to=${selectedRange.to}&limit=${limit}&order=${sortBy}&page=${page + 1}`);

  dispatch(slice.actions.getInvoices({ invoices: response.data.data, meta: response.data.meta }));
};

export const setInvoicesLimit = (limit: number) => (dispatch) => {
  dispatch(slice.actions.setInvoicesLimit({ limit }));
};

export const setInvoicesPage = (page: number) => (dispatch) => {
  dispatch(slice.actions.setInvoicesPage({ page }));
};

export const setInvoicesOrder = (orderBy: string, order: SortDirection) => (dispatch) => {
  dispatch(slice.actions.setInvoicesOrder({ orderBy, order }));
};

export const selectInvoicesRange = (
  from: Date,
  to: Date
): AppThunk => (dispatch): void => {
  dispatch(slice.actions.selectInvoicesRange({
    from: moment(from).format('YYYY-MM-DD'),
    to: moment(to).format('YYYY-MM-DD')
  }));
};

export default slice;
