import { createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit';
import type { AppThunk } from 'src/store';
import axios from 'axios';
import { apiConfig } from 'src/config';
import { SortDirection } from '@mui/material';
import type { ChemicalTest, ChemicalTestException } from '../types/chemical';

interface ChemicalTestState {
  searchText?: string,
  orderBy: string,
  order: SortDirection,
  isLoadingChemicalTests: boolean,
  chemicalTests: ChemicalTest[],
  isLoadingChemicalTest: boolean,
  chemicalTest: ChemicalTest,
  chemicalTestExceptions: ChemicalTestException[],
}

const initialState: ChemicalTestState = {
  searchText: '',
  orderBy: 'id',
  order: 'asc',
  isLoadingChemicalTests: true,
  chemicalTests: [],
  isLoadingChemicalTest: true,
  chemicalTest: null,
  chemicalTestExceptions: [],
};

const slice = createSlice({
  name: 'chemicalTest',
  initialState,
  reducers: {
    resetChemicalTest(
      state: ChemicalTestState,
    ): void {
      state.isLoadingChemicalTest = true;
      state.chemicalTest = null;
      state.chemicalTestExceptions = [];
    },
    getChemicalTests(state: ChemicalTestState, action: PayloadAction<{ chemicalTests: ChemicalTest[] }>) {
      const { chemicalTests } = action.payload;

      state.chemicalTests = chemicalTests;
      state.isLoadingChemicalTests = false;
    },
    setIsLoading(state: ChemicalTestState) {
      state.isLoadingChemicalTests = true;
    },
    getChemicalTest(state: ChemicalTestState, action: PayloadAction<{ chemicalTest: ChemicalTest }>) {
      const { chemicalTest } = action.payload;

      state.chemicalTest = chemicalTest;
      state.chemicalTestExceptions = chemicalTest.exceptions;
      state.isLoadingChemicalTest = false;
    },
    updateChemicalTest(state: ChemicalTestState, action: PayloadAction<{ chemicalTest: ChemicalTest }>) {
      const { chemicalTest } = action.payload;

      state.chemicalTest = chemicalTest;
      state.chemicalTestExceptions = chemicalTest.exceptions;
    },
    setSearchText(state: ChemicalTestState, action: PayloadAction<{ searchText: string }>) {
      const { searchText } = action.payload;

      state.searchText = searchText;
    },
    setOrder(state: ChemicalTestState, action: PayloadAction<{ orderBy: string; order: SortDirection }>) {
      const { orderBy, order } = action.payload;

      state.orderBy = orderBy;
      state.order = order;
    },
    changeDefault(state: ChemicalTestState, action: PayloadAction<{ chemicalTestId: number; isDefault: boolean }>) {
      const { chemicalTestId, isDefault } = action.payload;
      const changedChemicalTest = state.chemicalTests.find((chemicalTest) => chemicalTest.id === chemicalTestId);

      if (changedChemicalTest) {
        changedChemicalTest.is_default = isDefault;
      }
    },
    addException(state: ChemicalTestState, action: PayloadAction<ChemicalTestException>): void {
      const exception = action.payload;
      state.chemicalTestExceptions.push(exception);
    },
    updateException(
      state: ChemicalTestState,
      action: PayloadAction<ChemicalTestException>
    ): void {
      const exception = action.payload;
      state.chemicalTestExceptions = state.chemicalTestExceptions.map((_exception) => (_exception.id === exception.id ? exception : _exception));
    },
    removeException(state: ChemicalTestState, action: PayloadAction<number | string>): void {
      const exceptionId = action.payload;

      state.chemicalTestExceptions = state.chemicalTestExceptions.filter((exception) => exception.id !== exceptionId);
    }
  }
});

export const { reducer } = slice;

export const getChemicalTests = (
  organisationId: number,
  filter: string = '',
  sortBy: string = '',
): AppThunk => async (dispatch) => {
  dispatch(slice.actions.setIsLoading());

  const response = await axios.get(`${apiConfig.apiV2Url}/organisations/${organisationId}/chemical-test?filter=${filter}&order=${sortBy}`);

  dispatch(slice.actions.getChemicalTests({ chemicalTests: response.data }));
};

export const getChemicalTest = (
  organisationId: number,
  id: number,
): AppThunk => async (dispatch) => {
  const response = await axios.get(`${apiConfig.apiV2Url}/organisations/${organisationId}/chemical-test/${id}`);

  dispatch(slice.actions.getChemicalTest({ chemicalTest: response.data }));
};

export const resetChemicalTest = (): AppThunk => (dispatch): void => {
  dispatch(slice.actions.resetChemicalTest());
};

export const updateChemicalTest = (organisationId: number, chemicalTestId: number, data: any): AppThunk => async (dispatch) => {
  const response = await axios.put(`${apiConfig.apiV2Url}/organisations/${organisationId}/chemical-test/${chemicalTestId}`, data);

  dispatch(slice.actions.updateChemicalTest({ chemicalTest: response.data }));
};

export const setSearchText = (searchText: string) => (dispatch) => {
  dispatch(slice.actions.setSearchText({ searchText }));
};

export const setOrder = (orderBy: string, order: SortDirection) => (dispatch) => {
  dispatch(slice.actions.setOrder({ orderBy, order }));
};

export const changeDefault = (organisationId: number, chemicalTestId: number, isDefault: boolean): AppThunk => async (dispatch) => {
  await axios.put(`${apiConfig.apiV2Url}/organisations/${organisationId}/chemical-test/${chemicalTestId}/change-default`, { is_default: +isDefault });

  dispatch(slice.actions.changeDefault({ chemicalTestId, isDefault }));
};

export const addException = (exception: ChemicalTestException): AppThunk => (dispatch): void => {
  dispatch(slice.actions.addException(exception));
};

export const updateException = (exception: ChemicalTestException): AppThunk => (dispatch): void => {
  dispatch(slice.actions.updateException(exception));
};

export const removeException = (exceptionId: number | string): AppThunk => (dispatch): void => {
  dispatch(slice.actions.removeException(exceptionId));
};

export default slice;
