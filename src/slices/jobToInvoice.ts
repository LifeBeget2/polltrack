import { createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit';
import type { AppThunk } from 'src/store';
import axios from 'axios';
import { apiConfig } from 'src/config';
import moment from 'moment/moment';
import { SortDirection } from '@mui/material';
import { JOB_DONE } from 'src/constants/job';
import { ALL_JOBS_TO_INVOICE_FILTER, Job } from '../types/job';
import { Meta } from '../types/pagination';

interface Range {
  from: string;
  to: string;
}

interface JobToInvoiceState {
  searchText?: string,
  limit?: number,
  page?: number,
  total?: number,
  orderBy: string,
  order: SortDirection,
  isLoading: boolean,
  statusFilter: number | string,
  jobsToInvoice: Job[],
  selectedRange: Range;
}

const initialState: JobToInvoiceState = {
  searchText: '',
  limit: 10,
  page: 0,
  total: 0,
  orderBy: 'start_time',
  order: 'desc',
  statusFilter: ALL_JOBS_TO_INVOICE_FILTER,
  isLoading: false,
  jobsToInvoice: [],
  selectedRange: {
    from: moment().subtract(3, 'month').format('YYYY-MM-DD'),
    to: moment().format('YYYY-MM-DD')
  },
};

const slice = createSlice({
  name: 'jobToInvoice',
  initialState,
  reducers: {
    getJobsToInvoice(state: JobToInvoiceState, action: PayloadAction<{ jobs: Job[]; meta: Meta }>) {
      const { jobs, meta } = action.payload;

      state.jobsToInvoice = jobs;
      state.total = meta.total;
      state.isLoading = false;
    },
    setLimit(state: JobToInvoiceState, action: PayloadAction<{ limit: number }>) {
      const { limit } = action.payload;

      state.limit = limit;
      state.page = 0;
      state.total = 0;
    },
    setStatusFilter(state: JobToInvoiceState, action: PayloadAction<{ status: string | number }>) {
      const { status } = action.payload;

      state.statusFilter = status;
    },
    setPage(state: JobToInvoiceState, action: PayloadAction<{ page: number }>) {
      const { page } = action.payload;

      state.page = page;
    },
    setIsLoading(state: JobToInvoiceState, action: PayloadAction<{ isLoading: boolean }>) {
      const { isLoading } = action.payload;

      state.isLoading = isLoading;
    },
    setSearchText(state: JobToInvoiceState, action: PayloadAction<{ searchText: string }>) {
      const { searchText } = action.payload;

      state.searchText = searchText;
      state.page = 0;
    },
    setOrder(state: JobToInvoiceState, action: PayloadAction<{ orderBy: string; order: SortDirection }>) {
      const { orderBy, order } = action.payload;

      state.orderBy = orderBy;
      state.order = order;
    },
    selectRange(
      state: JobToInvoiceState,
      action: PayloadAction<{ from: string; to: string }>
    ): void {
      const { from, to } = action.payload;

      state.selectedRange = {
        from,
        to
      };
    },
  }
});

export const { reducer } = slice;

export const getJobsToInvoice = (
  organisationId: number,
  selectedRange: Range,
  limit: number,
  page: number,
  statusFilter: number | string,
  filter: string = '',
  sortBy: string = '',
): AppThunk => async (dispatch) => {
  dispatch(slice.actions.setIsLoading({ isLoading: true }));
  const isJobInvoiceVisible = (Boolean(statusFilter) && statusFilter !== ALL_JOBS_TO_INVOICE_FILTER) ? statusFilter : '';
  const response = await axios.get(`${apiConfig.apiV2Url}/organisations/${organisationId}/jobs?filter=${filter}&from=${selectedRange.from}&to=${selectedRange.to}&is_job_invoice_visible=${isJobInvoiceVisible}&job_status_id=${JOB_DONE}&limit=${limit}&order=${sortBy}&page=${page + 1}`);

  dispatch(slice.actions.getJobsToInvoice({ jobs: response.data.data, meta: response.data.meta }));
};

export const setLimit = (limit: number) => (dispatch) => {
  dispatch(slice.actions.setLimit({ limit }));
};

export const setPage = (page: number) => (dispatch) => {
  dispatch(slice.actions.setPage({ page }));
};

export const setSearchText = (searchText: string) => (dispatch) => {
  dispatch(slice.actions.setSearchText({ searchText }));
};

export const setStatusFilter = (status: string | number) => (dispatch) => {
  dispatch(slice.actions.setStatusFilter({ status }));
};

export const setOrder = (orderBy: string, order: SortDirection) => (dispatch) => {
  dispatch(slice.actions.setOrder({ orderBy, order }));
};

export const selectRange = (
  from: Date,
  to: Date
): AppThunk => (dispatch): void => {
  dispatch(slice.actions.selectRange({
    from: moment(from).format('YYYY-MM-DD'),
    to: moment(to).format('YYYY-MM-DD')
  }));
};

export default slice;
