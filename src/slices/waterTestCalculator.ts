/* eslint-disable */
import { createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit';
import type { AppThunk } from 'src/store';
import type { Job } from '../types/job';
import type { LabJob } from '../types/labJob';
import type { Pool } from '../types/pool';
import type { ChemicalTestResult, ObservationTestResult } from '../types/waterTest';
import type { ChemicalGroup, ObservationGroup, ChemicalTest, ObservationTest } from '../types/chemical';
import axios from 'axios';
import { apiConfig } from '../config';

interface WaterTestCalculatorState {
  isLoading: boolean;
  initialised: boolean;
  job: LabJob | Job;
  pool: Pool;
  chemicalGroups: ChemicalGroup[];
  observationGroups: ObservationGroup[];
  chemicalTests: ChemicalTest[];
  chemicalTestResults: ChemicalTestResult[];
  activeChemicalTestResults: ChemicalTestResult[];
  observationTests: ObservationTest[];
  observationTestResults: ObservationTestResult[];
  activeObservationTestResults: ObservationTestResult[];
  enabledObservationTests: ObservationTest[];
}

const initialState: WaterTestCalculatorState = {
  isLoading: true,
  initialised: false,
  job: null,
  pool: null,
  chemicalGroups: [],
  observationGroups: [],
  chemicalTests: [],
  chemicalTestResults: [],
  activeChemicalTestResults: [],
  observationTests: [],
  observationTestResults: [],
  activeObservationTestResults: [],
  enabledObservationTests: [],
};

const slice = createSlice({
  name: 'waterTestCalculator',
  initialState,
  reducers: {
    reset(
      state: WaterTestCalculatorState,
    ): void {
      state.isLoading = true;
      state.initialised = false;
      state.job = null;
      state.pool = null;
      state.chemicalGroups = [];
      state.observationGroups = [];
      state.chemicalTests = [];
      state.chemicalTestResults = [];
      state.observationTests = [];
      state.observationTestResults = [];
      state.enabledObservationTests = [];
    },
    init(state: WaterTestCalculatorState, action: PayloadAction<{
      job: LabJob | Job,
      pool: Pool,
      chemicalGroups: ChemicalGroup[],
      observationGroups: ObservationGroup[],
      chemicalTests: ChemicalTest[],
      observationTests: ObservationTest[],
    }>) {
      const { job, pool, chemicalGroups, observationGroups, chemicalTests, observationTests } = action.payload;

      state.job = job;
      state.pool = pool;
      state.chemicalGroups = chemicalGroups;
      state.observationGroups = observationGroups;
      state.chemicalTests = chemicalTests;
      state.chemicalTestResults = chemicalTests.map((chemicalTest) => {
        const existingResult = state.job.chemical_results.find((chemicalResult) => chemicalResult.chemical_test_id === chemicalTest.id);

        return {
          id: existingResult ? existingResult.id : chemicalTest.id,
          value: existingResult ? existingResult.value : '',
          min_value: existingResult ? existingResult.min_value : chemicalTest.minimum_value,
          max_value: existingResult ? existingResult.max_value : chemicalTest.maximum_value,
          target_value: existingResult ? existingResult.target_value : chemicalTest.target_value,
          show_on_report: existingResult ? existingResult.show_on_report : true,
          // TODO: apply the status logic
          status: 'ok',
          name: chemicalTest.name,
          action: 'Add 515.4 g of Lo ‘N’ Slo',
          description: 'Only add 100ml of hydrochloric acid / 100g of sodium bisulphate per 10,000L at a time.',
          enabled: chemicalTest.is_default,
          is_saved: Boolean(existingResult),
          chemical_test_id: chemicalTest.id,
          chemical_test: chemicalTest,
          chemical_group_id: existingResult ? existingResult.chemical_group_id : null,
        };
      });
      state.activeChemicalTestResults = state.chemicalTestResults.filter((chemicalTestResult: ChemicalTestResult) => chemicalTestResult.is_saved);
      state.observationTests = observationTests;
      state.observationTestResults = observationTests.map((observationTest) => {
        const existingResult = state.job.observation_results.find((observationResult) => observationResult.observation_test_id === observationTest.id);
        return {
          id: existingResult ? existingResult.id : observationTest.id,
          value: !!existingResult,
          show_on_report: existingResult ? existingResult.show_on_report : true,
          name: observationTest.name,
          action: 'Add 515.4 g of Lo ‘N’ Slo',
          description: 'Only add 100ml of hydrochloric acid / 100g of sodium bisulphate per 10,000L at a time.',
          enabled: observationTest.is_default,
          is_saved: Boolean(existingResult),
          observation_test_id: observationTest.id,
          observation_test: observationTest,
          observation_group_id: existingResult ? existingResult.observation_group_id : null,
        };
      });
      state.activeObservationTestResults = state.observationTestResults.filter((observationTestResult: ObservationTestResult) => observationTestResult.is_saved);
      state.initialised = true;
      state.isLoading = false;
    },
    updateChemicalTestResult(state: WaterTestCalculatorState, action: PayloadAction<{
      chemicalTestResult: ChemicalTestResult,
      value: string,
    }>) {
      const { chemicalTestResult, value } = action.payload;

      state.chemicalTestResults = state.chemicalTestResults.map((_chemicalTestResult) => {
        if (_chemicalTestResult.chemical_test.id === chemicalTestResult.chemical_test.id) {
          _chemicalTestResult.value = value;
        }

        return _chemicalTestResult;
      });

      state.activeChemicalTestResults = state.chemicalTestResults.filter(
        (chemicalTestResult: ChemicalTestResult) => chemicalTestResult.enabled && chemicalTestResult.value !== '' && parseFloat(chemicalTestResult.value) >= 0
      );
    },
    toggleChemicalTestResult(state: WaterTestCalculatorState, action: PayloadAction<{
      chemicalTestResult: ChemicalTestResult,
      enabled: boolean,
    }>) {
      const { chemicalTestResult, enabled } = action.payload;

      state.chemicalTestResults = state.chemicalTestResults.map((_chemicalTestResult) => {
        if (_chemicalTestResult.chemical_test.id === chemicalTestResult.chemical_test.id) {
          _chemicalTestResult.enabled = enabled;
        }

        return _chemicalTestResult;
      });
      state.activeChemicalTestResults = state.chemicalTestResults.filter(
        (chemicalTestResult: ChemicalTestResult) => chemicalTestResult.enabled && chemicalTestResult.value !== '' && parseFloat(chemicalTestResult.value) >= 0
      );
    },
    updateObservationTestResult(state: WaterTestCalculatorState, action: PayloadAction<{
      observationTestResult: ObservationTestResult,
      value: boolean,
    }>) {
      const { observationTestResult, value } = action.payload;

      state.observationTestResults = state.observationTestResults.map((_observationTestResult) => {
        if (_observationTestResult.observation_test.id === observationTestResult.observation_test.id) {
          _observationTestResult.value = value;
        }

        return _observationTestResult;
      });
      state.activeObservationTestResults = state.observationTestResults.filter((observationTestResult: ObservationTestResult) => observationTestResult.enabled && observationTestResult.value);
    },
    toggleObservationTestResult(state: WaterTestCalculatorState, action: PayloadAction<{
      observationTestResult: ObservationTestResult,
      enabled: boolean,
    }>) {
      const { observationTestResult, enabled } = action.payload;

      state.observationTestResults = state.observationTestResults.map((_observationTestResult) => {
        if (_observationTestResult.observation_test.id === observationTestResult.observation_test.id) {
          _observationTestResult.enabled = enabled;
        }

        return _observationTestResult;
      });
      state.activeObservationTestResults = state.observationTestResults.filter((observationTestResult: ObservationTestResult) => observationTestResult.enabled && observationTestResult.value);
    },
  }
});

export const { reducer } = slice;

export const init = (organisationId: number, job: LabJob | Job, pool: Pool): AppThunk => async (dispatch): Promise<void> => {
  const chemicalGroups = await axios.get(`${apiConfig.apiV2Url}/organisations/${organisationId}/chemical-group?limit=100&order=name&page=1`);
  const observationGroups = await axios.get(`${apiConfig.apiV2Url}/organisations/${organisationId}/observation-group?limit=100&order=name&page=1`);
  const chemicalTests = await axios.get(`${apiConfig.apiV2Url}/organisations/${organisationId}/chemical-test?order=name`);
  const observationTests = await axios.get(`${apiConfig.apiV2Url}/organisations/${organisationId}/observation-test?order=name`);

  dispatch(slice.actions.init({
    job,
    pool,
    chemicalGroups: chemicalGroups.data.data,
    observationGroups: observationGroups.data.data,
    chemicalTests: chemicalTests.data,
    observationTests: observationTests.data.data,
  }));
};

export const updateChemicalTestResult = (chemicalTestResult: ChemicalTestResult, value: string): AppThunk => (dispatch): void => {
  dispatch(slice.actions.updateChemicalTestResult({ chemicalTestResult, value }));
};

export const toggleChemicalTest = (chemicalTestResult: ChemicalTestResult, enabled: boolean): AppThunk => (dispatch): void => {
  dispatch(slice.actions.toggleChemicalTestResult({ chemicalTestResult, enabled }));
};

export const updateObservationTestResult = (observationTestResult: ObservationTestResult, value: boolean): AppThunk => (dispatch): void => {
  dispatch(slice.actions.updateObservationTestResult({ observationTestResult, value }));
};

export const toggleObservationTest = (observationTestResult: ObservationTestResult, enabled: boolean): AppThunk => (dispatch): void => {
  dispatch(slice.actions.toggleObservationTestResult({ observationTestResult, enabled }));
};

export const reset = (): AppThunk => (dispatch): void => {
  dispatch(slice.actions.reset());
};

export default slice;
