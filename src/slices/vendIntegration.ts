import axios from 'axios';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { apiConfig } from 'src/config';
import { AppThunk } from 'src/store';
import { Organisation } from 'src/types/organisation';

interface VendIntegrationState {
  steps: string[];
  activeStep: number;

  initializing: boolean;

  connecting: boolean;
  resettingConnection: boolean;
  connectionResetSuccess: boolean;
}

export const STEP_CONNECT = 0;
export const STEP_SYNC_CONTACTS = 1;
export const STEP_SYNC_PRODUCTS = 2;
export const STEP_MATCH_USERS = 3;

const initialState: VendIntegrationState = {
  steps: ['Connect to Vend', 'Sync contacts', 'Sync products', 'Finish setup'],
  activeStep: STEP_CONNECT,

  initializing: false,

  connecting: false,
  resettingConnection: false,
  connectionResetSuccess: false,
};

const slice = createSlice({
  name: 'vendIntegration',
  initialState,
  reducers: {
    reset() { return initialState; },
    startInitializing(state: VendIntegrationState) {
      state.initializing = true;
    },
    stopInitializing(state: VendIntegrationState) {
      state.initializing = false;
    },
    initialize(
      state: VendIntegrationState,
      action: PayloadAction<{ organisation: Organisation }>
    ) {
      const { organisation } = action.payload;

      if (!organisation?.has_active_vend_connection) {
        state.activeStep = STEP_CONNECT;
        return;
      }

      if (organisation.vend_organisation.last_contact_updated_at == null) {
        state.activeStep = STEP_SYNC_CONTACTS;
        return;
      }

      if (organisation.vend_organisation.last_product_updated_at == null) {
        state.activeStep = STEP_SYNC_PRODUCTS;
        return;
      }

      state.activeStep = STEP_MATCH_USERS;
    },
    startConnecting(state: VendIntegrationState) {
      state.connecting = true;
    },
    stopConnecting(state: VendIntegrationState) {
      state.connecting = false;
    },
    startResetting(state: VendIntegrationState) {
      state.resettingConnection = true;
    },
    stopResetting(state: VendIntegrationState) {
      state.resettingConnection = false;
    },
    connectionResetSuccess(state: VendIntegrationState) {
      state.connectionResetSuccess = true;
    },
    connectionResetHandled(state: VendIntegrationState) {
      state.connectionResetSuccess = false;
    },
    nextStep(state: VendIntegrationState) {
      const nextStep = state.activeStep + 1;
      if (nextStep < initialState.steps.length) {
        state.activeStep = nextStep;
      }
    },
  },
});

export const { reducer } = slice;
export const { reset, nextStep, connectionResetHandled } = slice.actions;

export default slice;

export const initialize = (organisationId: number): AppThunk => async (dispatch) => {
  dispatch(slice.actions.startInitializing());
  const response = await axios.get(`${apiConfig.apiV1Url}/organisations/${organisationId}`);
  const organisation = response.data as Organisation;
  dispatch(slice.actions.initialize({ organisation }));
  dispatch(slice.actions.stopInitializing());
};

export const connect = (): AppThunk => async (dispatch) => {
  try {
    dispatch(slice.actions.startConnecting());
    const response = await axios.post(
      `${apiConfig.apiV1Url}/vend/initiate`,
      null,
      {
        params: {
          redirectUrl: window.location.href,
        },
      }
    );
    window.location.href = `${apiConfig.apiV1Url}/vend/authorize?applicationToken=${response.data.applicationToken}`;
  } finally {
    dispatch(slice.actions.stopConnecting());
  }
};

export const resetConnection = (organisationId: number): AppThunk => async (dispatch) => {
  try {
    dispatch(slice.actions.startResetting());
    await axios.post(
      `${apiConfig.apiV1Url}/organisations/${organisationId}/vend/reset`,
    );
    dispatch(slice.actions.connectionResetSuccess());
  } finally {
    dispatch(slice.actions.stopResetting());
  }
};
