import { createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit';
import type { AppThunk } from 'src/store';
import axios from 'axios';
import { apiConfig } from 'src/config';
import { SortDirection } from '@mui/material';
import type { ObservationTest } from '../types/chemical';
import type { Meta } from '../types/pagination';

interface ObservationTestState {
  searchText?: string,
  limit?: number,
  page?: number,
  total?: number,
  orderBy: string,
  order: SortDirection,
  observationTests: ObservationTest[],
}

const initialState: ObservationTestState = {
  searchText: '',
  limit: 10,
  page: 0,
  total: 0,
  orderBy: 'name',
  order: 'asc',
  observationTests: [],
};

const slice = createSlice({
  name: 'observationTest',
  initialState,
  reducers: {
    getObservationTests(state: ObservationTestState, action: PayloadAction<{ observationTests: ObservationTest[]; meta: Meta }>) {
      const { observationTests, meta } = action.payload;

      state.observationTests = observationTests;
      state.total = meta.total;
    },
    setSearchText(state: ObservationTestState, action: PayloadAction<{ searchText: string }>) {
      const { searchText } = action.payload;

      state.searchText = searchText;
      state.page = 0;
    },
    setLimit(state: ObservationTestState, action: PayloadAction<{ limit: number }>) {
      const { limit } = action.payload;

      state.limit = limit;
      state.page = 0;
      state.total = 0;
    },
    setPage(state: ObservationTestState, action: PayloadAction<{ page: number }>) {
      const { page } = action.payload;

      state.page = page;
    },
    setOrder(state: ObservationTestState, action: PayloadAction<{ orderBy: string; order: SortDirection }>) {
      const { orderBy, order } = action.payload;

      state.orderBy = orderBy;
      state.order = order;
    },
    changeDefault(state: ObservationTestState, action: PayloadAction<{ observationTestId: number; isDefault: boolean }>) {
      const { observationTestId, isDefault } = action.payload;
      const changedObservationTest = state.observationTests.find((observationTest) => observationTest.id === observationTestId);

      if (changedObservationTest) {
        changedObservationTest.is_default = isDefault;
      }
    },
    deleteObservationTest(state: ObservationTestState, action: PayloadAction<{ observationTestId: number; }>) {
      const { observationTestId } = action.payload;

      state.observationTests = state.observationTests.filter((observationTest) => observationTest.id !== observationTestId);
    }
  }
});

export const { reducer } = slice;

export const getObservationTests = (
  organisationId: number,
  page: number,
  limit: number,
  filter: string = '',
  sortBy: string = '',
): AppThunk => async (dispatch) => {
  const response = await axios.get(`${apiConfig.apiV2Url}/organisations/${organisationId}/observation-test?filter=${filter}&limit=${limit}&order=${sortBy}&page=${page + 1}`);

  dispatch(slice.actions.getObservationTests({ observationTests: response.data.data, meta: response.data.meta }));
};

export const setSearchText = (searchText: string) => (dispatch) => {
  dispatch(slice.actions.setSearchText({ searchText }));
};

export const setLimit = (limit: number) => (dispatch) => {
  dispatch(slice.actions.setLimit({ limit }));
};

export const setPage = (page: number) => (dispatch) => {
  dispatch(slice.actions.setPage({ page }));
};

export const setOrder = (orderBy: string, order: SortDirection) => (dispatch) => {
  dispatch(slice.actions.setOrder({ orderBy, order }));
};

export const changeDefault = (organisationId: number, observationTestId: number, isDefault: boolean): AppThunk => async (dispatch) => {
  await axios.put(`${apiConfig.apiV2Url}/organisations/${organisationId}/observation-test/${observationTestId}/change-default`, { is_default: +isDefault });

  dispatch(slice.actions.changeDefault({ observationTestId, isDefault }));
};

export const deleteObservationTest = (organisationId: number, observationTestId: number): AppThunk => async (dispatch) => {
  await axios.delete(`${apiConfig.apiV1Url}/organisations/${organisationId}/observationtests/${observationTestId}`);

  dispatch(slice.actions.deleteObservationTest({ observationTestId }));
};

export default slice;
