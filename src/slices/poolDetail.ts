import { createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit';
import axios from 'axios';
import merge from 'lodash/merge';
import moment from 'moment/moment';
import type { AppThunk } from '../store';
import type { Pool } from '../types/pool';
import type { Contact } from '../types/contact';
import type { CombinedTestHistory } from '../types/testHistory';
import { apiConfig } from '../config';
import { OWNER } from '../constants/poolContact';
import type { Invoice } from '../types/contactInvoice';
import type { SortDirection } from '@mui/material';
import type { Meta } from '../types/pagination';
import type { Note } from '../types/note';

interface Range {
  from: string;
  to: string;
}

interface PoolDetailState {
  isLoaded: boolean;
  poolId?: number;
  pool?: Pool;
  contacts: Contact[];
  notes?: Note[];
  isHistoryLoading: boolean;
  chemicalHistoryItems: CombinedTestHistory[];
  selectedHistoryRange: Range;
  isInvoicesLoading: boolean;
  invoices: Invoice[];
  selectedInvoicesRange: Range;
  invoicesLimit?: number,
  invoicesPage?: number,
  invoicesTotal?: number,
  invoicesOrderBy: string,
  invoicesOrder: SortDirection,

  equipmentIdsDeletingInProgress: number[];
}

const initialState: PoolDetailState = {
  isLoaded: false,
  poolId: null,
  pool: null,
  contacts: [],
  notes: [],
  isHistoryLoading: false,
  chemicalHistoryItems: [],
  selectedHistoryRange: {
    from: moment().subtract(3, 'month').format('YYYY-MM-DD'),
    to: moment().format('YYYY-MM-DD')
  },
  isInvoicesLoading: false,
  invoices: [],
  selectedInvoicesRange: {
    from: moment().subtract(3, 'month').format('YYYY-MM-DD'),
    to: moment().format('YYYY-MM-DD')
  },
  invoicesLimit: 10,
  invoicesPage: 0,
  invoicesTotal: 0,
  invoicesOrderBy: 'created_at',
  invoicesOrder: 'desc',

  equipmentIdsDeletingInProgress: [],
};

const slice = createSlice({
  name: 'poolDetail',
  initialState,
  reducers: {
    reset(
      state: PoolDetailState,
    ): void {
      state.isLoaded = false;
      state.poolId = null;
      state.pool = null;
      state.contacts = [];
      state.notes = [];
      state.selectedHistoryRange = {
        from: moment().subtract(3, 'month').format('YYYY-MM-DD'),
        to: moment().format('YYYY-MM-DD')
      };
      state.chemicalHistoryItems = [];
      state.isHistoryLoading = false;
      state.isInvoicesLoading = false;
      state.invoices = [];
      state.invoicesPage = 0;
      state.invoicesTotal = 0;
      state.invoicesOrderBy = 'created_at';
      state.invoicesOrder = 'desc';
    },
    getPool(
      state: PoolDetailState,
      action: PayloadAction<{ pool: Pool | null }>
    ): void {
      const { pool } = action.payload;

      if (pool) {
        state.pool = pool;
        state.contacts = pool.contacts;
        state.notes = pool.pool_notes ? pool.pool_notes : [];
      } else {
        state.poolId = null;
        state.pool = null;
      }
      state.isLoaded = true;
    },
    addContact(
      state: PoolDetailState,
      action: PayloadAction<{ contact: Contact }>
    ): void {
      const { contact } = action.payload;
      const exists = state.contacts.find((_contact) => _contact.id === contact.id);

      if (!exists) {
        state.contacts.push(merge({}, contact, {
          pivot: {
            address_type_id: OWNER,
            contact_id: contact.id
          },
        }));
      }
    },
    updateContact(
      state: PoolDetailState,
      action: PayloadAction<{ contact: Contact }>
    ): void {
      const { contact } = action.payload;

      state.contacts = state.contacts.map((_contact) => {
        if (_contact.id === contact.id) {
          return merge({}, contact, { pivot: _contact.pivot });
        }

        return _contact;
      });
    },
    updateContactRelation(
      state: PoolDetailState,
      action: PayloadAction<{ contactId: number; relationId: number }>
    ): void {
      const { contactId, relationId } = action.payload;

      state.contacts = state.contacts.map((_contact) => {
        if (_contact.id === contactId) {
          _contact.pivot.address_type_id = relationId;
          return _contact;
        }

        return _contact;
      });
    },
    unlinkContact(
      state: PoolDetailState,
      action: PayloadAction<{ contactId: number }>
    ): void {
      const { contactId } = action.payload;

      state.contacts = state.contacts.filter((contact) => contact.id !== contactId);
    },
    getHistory(state: PoolDetailState, action: PayloadAction<{ history: CombinedTestHistory[] }>) {
      const { history } = action.payload;

      state.chemicalHistoryItems = history;
      state.isHistoryLoading = false;
    },
    setIsHistoryLoading(state: PoolDetailState, action: PayloadAction<{ isHistoryLoading: boolean }>) {
      const { isHistoryLoading } = action.payload;

      state.isHistoryLoading = isHistoryLoading;
    },
    selectHistoryRange(
      state: PoolDetailState,
      action: PayloadAction<{ from: string; to: string }>
    ): void {
      const { from, to } = action.payload;

      state.selectedHistoryRange = {
        from,
        to
      };
    },
    getInvoices(state: PoolDetailState, action: PayloadAction<{ invoices: Invoice[]; meta: Meta }>) {
      const { invoices, meta } = action.payload;

      state.invoices = invoices;
      state.invoicesTotal = meta.total;
      state.isInvoicesLoading = false;
    },
    setInvoicesLimit(state: PoolDetailState, action: PayloadAction<{ limit: number }>) {
      const { limit } = action.payload;

      state.invoicesLimit = limit;
      state.invoicesPage = 0;
      state.invoicesTotal = 0;
    },
    setInvoicesPage(state: PoolDetailState, action: PayloadAction<{ page: number }>) {
      const { page } = action.payload;

      state.invoicesPage = page;
    },
    setIsInvoicesLoading(state: PoolDetailState, action: PayloadAction<{ isLoading: boolean }>) {
      const { isLoading } = action.payload;

      state.isInvoicesLoading = isLoading;
    },
    setInvoicesOrder(state: PoolDetailState, action: PayloadAction<{ orderBy: string; order: SortDirection }>) {
      const { orderBy, order } = action.payload;

      state.invoicesOrderBy = orderBy;
      state.invoicesOrder = order;
    },
    selectInvoicesRange(
      state: PoolDetailState,
      action: PayloadAction<{ from: string; to: string }>
    ): void {
      const { from, to } = action.payload;

      state.selectedInvoicesRange = {
        from,
        to
      };
    },
    startDeletingEquipment(
      state: PoolDetailState,
      action: PayloadAction<{ equipmentId: number }>,
    ) {
      state.equipmentIdsDeletingInProgress = [...state.equipmentIdsDeletingInProgress, action.payload.equipmentId];
    },
    stopDeletingEquipment(
      state: PoolDetailState,
      action: PayloadAction<{ equipmentId: number }>,
    ) {
      state.equipmentIdsDeletingInProgress = state.equipmentIdsDeletingInProgress.filter((id) => id !== action.payload.equipmentId);
    },
    deleteEquipment(
      state: PoolDetailState,
      action: PayloadAction<{ equipmentId: number }>,
    ) {
      state.pool.equipments = state.pool.equipments.filter((equipment) => equipment.id !== action.payload.equipmentId);
    },
  }
});

export const { reducer } = slice;

export const reset = (): AppThunk => (dispatch): void => {
  dispatch(slice.actions.reset());
};

export const getPool = (organisationId?: number, poolId?: number): AppThunk => async (dispatch): Promise<void> => {
  if (organisationId && poolId) {
    const response = await axios.get(`${apiConfig.apiV1Url}/organisations/${organisationId}/pools/${poolId}?include=contacts,contacts.pools`);

    dispatch(slice.actions.getPool({ pool: response.data }));
  } else {
    dispatch(slice.actions.getPool({ pool: null }));
  }
};

export const addContact = (contact: Contact): AppThunk => (dispatch): void => {
  dispatch(slice.actions.addContact({ contact }));
};

export const unlinkContact = (contactId: number): AppThunk => (dispatch): void => {
  dispatch(slice.actions.unlinkContact({ contactId }));
};

export const updateContact = (contact: Contact): AppThunk => (dispatch): void => {
  dispatch(slice.actions.updateContact({ contact }));
};

export const updateContactRelation = (contactId: number, relationId: number): AppThunk => (dispatch): void => {
  dispatch(slice.actions.updateContactRelation({ contactId, relationId }));
};

export const getHistory = (
  organisationId: number,
  poolId: number,
  selectedRange: Range,
): AppThunk => async (dispatch) => {
  dispatch(slice.actions.setIsHistoryLoading({ isHistoryLoading: true }));

  const response = await axios.get(`${apiConfig.apiV2Url}/organisations/${organisationId}/pools/${poolId}/chemical-history?from=${selectedRange.from}&to=${selectedRange.to}`);

  dispatch(slice.actions.getHistory({ history: response.data }));
};

export const selectHistoryRange = (
  from: Date,
  to: Date
): AppThunk => (dispatch): void => {
  dispatch(slice.actions.selectHistoryRange({
    from: moment(from).format('YYYY-MM-DD'),
    to: moment(to).format('YYYY-MM-DD')
  }));
};

export const getPoolInvoices = (
  organisationId: number,
  poolId: number,
  selectedRange: Range,
  limit: number,
  page: number,
  sortBy: string = '',
): AppThunk => async (dispatch) => {
  dispatch(slice.actions.setIsInvoicesLoading({ isLoading: true }));
  const response = await axios.get(`${apiConfig.apiV2Url}/organisations/${organisationId}/contact-invoice?pool_id=${poolId}&from=${selectedRange.from}&to=${selectedRange.to}&limit=${limit}&order=${sortBy}&page=${page + 1}`);

  dispatch(slice.actions.getInvoices({ invoices: response.data.data, meta: response.data.meta }));
};

export const setInvoicesLimit = (limit: number) => (dispatch) => {
  dispatch(slice.actions.setInvoicesLimit({ limit }));
};

export const setInvoicesPage = (page: number) => (dispatch) => {
  dispatch(slice.actions.setInvoicesPage({ page }));
};

export const setInvoicesOrder = (orderBy: string, order: SortDirection) => (dispatch) => {
  dispatch(slice.actions.setInvoicesOrder({ orderBy, order }));
};

export const selectInvoicesRange = (
  from: Date,
  to: Date
): AppThunk => (dispatch): void => {
  dispatch(slice.actions.selectInvoicesRange({
    from: moment(from).format('YYYY-MM-DD'),
    to: moment(to).format('YYYY-MM-DD')
  }));
};

export default slice;

export const deleteEquipment = (organisationId: number, equipmentId: number): AppThunk => async (dispatch): Promise<void> => {
  dispatch(slice.actions.startDeletingEquipment({ equipmentId }));
  try {
    await axios.delete(`${apiConfig.apiV1Url}/organisations/${organisationId}/equipments/${equipmentId}`);
    dispatch(slice.actions.deleteEquipment({ equipmentId }));
  } finally {
    dispatch(slice.actions.stopDeletingEquipment({ equipmentId }));
  }
};
