import { createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit';
import type { AppThunk } from 'src/store';
import axios from 'axios';
import { apiConfig } from 'src/config';
import { SortDirection } from '@mui/material';
import moment from 'moment/moment';
import type { TestHistory } from '../types/testHistory';
import type { Meta } from '../types/pagination';

interface Range {
  from: string;
  to: string;
}

interface TestHistoryState {
  searchText?: string,
  limit?: number,
  page?: number,
  total?: number,
  orderBy: string,
  isHistoryLoading: boolean,
  order: SortDirection,
  history: TestHistory[],
  selectedRange: Range;
}

const initialState: TestHistoryState = {
  searchText: '',
  limit: 10,
  page: 0,
  total: 0,
  isHistoryLoading: false,
  selectedRange: {
    from: moment().subtract(3, 'month').format('YYYY-MM-DD'),
    to: moment().format('YYYY-MM-DD')
  },
  orderBy: 'start_time',
  order: 'desc',
  history: [],
};

const slice = createSlice({
  name: 'testHistory',
  initialState,
  reducers: {
    getHistory(state: TestHistoryState, action: PayloadAction<{ history: TestHistory[]; meta: Meta }>) {
      const { history, meta } = action.payload;

      state.history = history;
      state.isHistoryLoading = false;
      state.total = meta.total;
    },
    setLimit(state: TestHistoryState, action: PayloadAction<{ limit: number }>) {
      const { limit } = action.payload;

      state.limit = limit;
      state.page = 0;
      state.total = 0;
    },
    setPage(state: TestHistoryState, action: PayloadAction<{ page: number }>) {
      const { page } = action.payload;

      state.page = page;
    },
    setIsHistoryLoading(state: TestHistoryState, action: PayloadAction<{ isHistoryLoading: boolean }>) {
      const { isHistoryLoading } = action.payload;

      state.isHistoryLoading = isHistoryLoading;
    },
    setSearchText(state: TestHistoryState, action: PayloadAction<{ searchText: string }>) {
      const { searchText } = action.payload;

      state.searchText = searchText;
      state.page = 0;
    },
    setOrder(state: TestHistoryState, action: PayloadAction<{ orderBy: string; order: SortDirection }>) {
      const { orderBy, order } = action.payload;

      state.orderBy = orderBy;
      state.order = order;
    },
    selectRange(
      state: TestHistoryState,
      action: PayloadAction<{ from: string; to: string }>
    ): void {
      const { from, to } = action.payload;

      state.selectedRange = {
        from,
        to
      };
    },
  }
});

export const { reducer } = slice;

export const getHistory = (
  organisationId: number,
  selectedRange: Range,
  limit: number,
  page: number,
  filter: string = '',
  sortBy: string = '',
): AppThunk => async (dispatch) => {
  dispatch(slice.actions.setIsHistoryLoading({ isHistoryLoading: true }));

  const response = await axios.get(`${apiConfig.apiV2Url}/organisations/${organisationId}/water-test?filter=${filter}&from=${selectedRange.from}&to=${selectedRange.to}&limit=${limit}&order=${sortBy}&page=${page + 1}`);

  dispatch(slice.actions.getHistory({ history: response.data.data, meta: response.data.meta }));
};

export const setLimit = (limit: number) => (dispatch) => {
  dispatch(slice.actions.setLimit({ limit }));
};

export const setPage = (page: number) => (dispatch) => {
  dispatch(slice.actions.setPage({ page }));
};

export const setSearchText = (searchText: string) => (dispatch) => {
  dispatch(slice.actions.setSearchText({ searchText }));
};

export const setOrder = (orderBy: string, order: SortDirection) => (dispatch) => {
  dispatch(slice.actions.setOrder({ orderBy, order }));
};

export const selectRange = (
  from: Date,
  to: Date
): AppThunk => (dispatch): void => {
  dispatch(slice.actions.selectRange({
    from: moment(from).format('YYYY-MM-DD'),
    to: moment(to).format('YYYY-MM-DD')
  }));
};

export default slice;
