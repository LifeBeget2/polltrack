import { createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit';
import type { AppThunk } from 'src/store';
import axios from 'axios';
import { apiConfig } from 'src/config';
import { SortDirection } from '@mui/material';
import moment from 'moment/moment';
import type { TestHistory } from '../types/testHistory';
import type { Meta } from '../types/pagination';

interface Range {
  from: string;
  to: string;
}

interface WaterTestHistoryState {
  searchText?: string,
  limit?: number,
  page?: number,
  total?: number,
  orderBy: string,
  isHistoryLoading: boolean,
  order: SortDirection,
  history: TestHistory[],
  selectedRange: Range;
}

const initialState: WaterTestHistoryState = {
  searchText: '',
  limit: 10,
  page: 0,
  total: 0,
  isHistoryLoading: false,
  selectedRange: {
    from: moment().subtract(3, 'month').format('YYYY-MM-DD'),
    to: moment().format('YYYY-MM-DD')
  },
  orderBy: 'start_time',
  order: 'desc',
  history: [],
};

const slice = createSlice({
  name: 'waterTestHistory',
  initialState,
  reducers: {
    getHistory(state: WaterTestHistoryState, action: PayloadAction<{ history: TestHistory[]; meta: Meta }>) {
      const { history, meta } = action.payload;

      state.history = history;
      state.isHistoryLoading = false;
      state.total = meta.total;
    },
    setLimit(state: WaterTestHistoryState, action: PayloadAction<{ limit: number }>) {
      const { limit } = action.payload;

      state.limit = limit;
      state.page = 0;
      state.total = 0;
    },
    setPage(state: WaterTestHistoryState, action: PayloadAction<{ page: number }>) {
      const { page } = action.payload;

      state.page = page;
    },
    setIsHistoryLoading(state: WaterTestHistoryState, action: PayloadAction<{ isHistoryLoading: boolean }>) {
      const { isHistoryLoading } = action.payload;

      state.isHistoryLoading = isHistoryLoading;
    },
    setSearchText(state: WaterTestHistoryState, action: PayloadAction<{ searchText: string }>) {
      const { searchText } = action.payload;

      state.searchText = searchText;
      state.page = 0;
    },
    setOrder(state: WaterTestHistoryState, action: PayloadAction<{ orderBy: string; order: SortDirection }>) {
      const { orderBy, order } = action.payload;

      state.orderBy = orderBy;
      state.order = order;
    },
    selectRange(
      state: WaterTestHistoryState,
      action: PayloadAction<{ from: string; to: string }>
    ): void {
      const { from, to } = action.payload;

      state.selectedRange = {
        from,
        to
      };
    },
  }
});

export const { reducer } = slice;

export const getHistory = (
  organisationId: number,
  poolId: number,
  contactId: number,
  selectedRange: Range,
  limit: number,
  page: number,
  sortBy: string = '',
): AppThunk => async (dispatch) => {
  dispatch(slice.actions.setIsHistoryLoading({ isHistoryLoading: true }));

  const response = await axios.get(`${apiConfig.apiV2Url}/organisations/${organisationId}/water-test?pool_id=${poolId}&contact_id=${contactId}&from=${selectedRange.from}&to=${selectedRange.to}&limit=${limit}&order=${sortBy}&page=${page + 1}`);

  dispatch(slice.actions.getHistory({ history: response.data.data, meta: response.data.meta }));
};

export const setLimit = (limit: number) => (dispatch) => {
  dispatch(slice.actions.setLimit({ limit }));
};

export const setPage = (page: number) => (dispatch) => {
  dispatch(slice.actions.setPage({ page }));
};

export const setSearchText = (searchText: string) => (dispatch) => {
  dispatch(slice.actions.setSearchText({ searchText }));
};

export const setOrder = (orderBy: string, order: SortDirection) => (dispatch) => {
  dispatch(slice.actions.setOrder({ orderBy, order }));
};

export const selectRange = (
  from: Date,
  to: Date
): AppThunk => (dispatch): void => {
  dispatch(slice.actions.selectRange({
    from: moment(from).format('YYYY-MM-DD'),
    to: moment(to).format('YYYY-MM-DD')
  }));
};

export default slice;
