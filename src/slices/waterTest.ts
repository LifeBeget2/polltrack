import { createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit';
import type { AppThunk } from 'src/store';
import axios from 'axios';
import { apiConfig } from 'src/config';
import type { Pool } from '../types/pool';
import type { Contact } from '../types/contact';
import type { LabJob } from '../types/labJob';
import moment from 'moment/moment';
import { ChemicalTestResult, ObservationTestResult } from '../types/waterTest';

interface WaterTestState {
  isLoading: boolean;
  isSaving: boolean;
  contactId?: number;
  contact?: Contact;
  poolId?: number;
  pool?: Pool;
  labJob?: LabJob,
}

const initialState: WaterTestState = {
  isLoading: true,
  isSaving: false,
  poolId: null,
  pool: null,
  contactId: null,
  contact: null,
  labJob: null,
};

const slice = createSlice({
  name: 'waterTest',
  initialState,
  reducers: {
    reset(
      state: WaterTestState,
    ): void {
      state.isLoading = true;
      state.poolId = null;
      state.pool = null;
      state.contactId = null;
      state.contact = null;
      state.labJob = null;
    },
    saving(state: WaterTestState, action: PayloadAction<{ saving: boolean }>) {
      const { saving } = action.payload;
      state.isSaving = saving;
    },
    getLabJob(state: WaterTestState, action: PayloadAction<{ labJob: LabJob }>) {
      const { labJob } = action.payload;

      state.labJob = labJob;
      state.pool = labJob.pool;
      state.contact = labJob.contact;
      state.isLoading = false;
    },
    updatePool(state: WaterTestState, action: PayloadAction<{ pool: Pool }>) {
      const { pool } = action.payload;

      state.pool = pool;
    },
    updateContact(state: WaterTestState, action: PayloadAction<{ contact: Contact }>) {
      const { contact } = action.payload;

      state.contact = contact;
    },
    updateWaterTest(state: WaterTestState, action: PayloadAction<{ labJob: LabJob }>) {
      const { labJob } = action.payload;

      state.labJob = labJob;
      state.isSaving = false;
    },
  }
});

export const { reducer } = slice;

export const getLabJob = (organisationId: number, userId: number, poolId: number, contactId: number, labJobId?: number): AppThunk => async (dispatch): Promise<void> => {
  let response;
  if (labJobId) {
    response = await axios.get(`${apiConfig.apiV2Url}/organisations/${organisationId}/water-test/${labJobId}`);
  } else {
    response = await axios.post(`${apiConfig.apiV2Url}/organisations/${organisationId}/water-test`, {
      pool_id: poolId,
      contact_id: contactId,
      user_id: userId,
      start_time: moment().format('YYYY-MM-DD hh:mm:ss'),
    });
  }

  dispatch(slice.actions.getLabJob({ labJob: response.data }));
};

export const reset = (): AppThunk => (dispatch): void => {
  dispatch(slice.actions.reset());
};

export const updatePool = (pool: Pool): AppThunk => (dispatch): void => {
  dispatch(slice.actions.updatePool({ pool }));
};

export const updateContact = (contact: Contact): AppThunk => (dispatch): void => {
  dispatch(slice.actions.updateContact({ contact }));
};

export const saving = (saving: boolean): AppThunk => (dispatch): void => {
  dispatch(slice.actions.saving({ saving }));
};

export const updateWaterTest = (
  organisationId: number,
  labJobId: number,
  chemicalTestResults: ChemicalTestResult[],
  observationTestResults: ObservationTestResult[],
  reportData: any
): AppThunk => async (dispatch) => {
  const config = { headers: { 'Content-Type': 'multipart/form-data' } };
  const fd = new FormData();

  const extension = reportData.type.split('/')[1];
  fd.append('file', reportData, `filename.${extension}`);
  await axios.post(`${apiConfig.apiV2Url}/organisations/${organisationId}/water-test/${labJobId}/report`, fd, config);
  const resultsData = {
    chemical_results: chemicalTestResults.map((chemicalTestResult: ChemicalTestResult) => ({
      value: chemicalTestResult.value || 0,
      name: chemicalTestResult.name,
      action: chemicalTestResult.action,
      status: 'ok',
      min_value: chemicalTestResult.min_value,
      max_value: chemicalTestResult.max_value,
      target_value: chemicalTestResult.target_value,
      description: chemicalTestResult.description,
      short_description: chemicalTestResult.short_description || '',
      show_on_report: chemicalTestResult.show_on_report,
      chemical_test_id: chemicalTestResult.chemical_test_id,
      chemical_group_id: chemicalTestResult.chemical_group_id || null,
    })),
    observation_results: observationTestResults.map((observationTestResult: ObservationTestResult) => ({
      value: observationTestResult.value,
      name: observationTestResult.name,
      action: observationTestResult.action,
      description: observationTestResult.description,
      short_description: observationTestResult.short_description || '',
      show_on_report: observationTestResult.show_on_report,
      observation_test_id: observationTestResult.observation_test_id,
      observation_group_id: observationTestResult.observation_group_id || null,
    })),
  };

  const response = await axios.post(`${apiConfig.apiV2Url}/organisations/${organisationId}/water-test/${labJobId}/results`, resultsData);

  dispatch(slice.actions.updateWaterTest({ labJob: response.data }));
};

export default slice;
