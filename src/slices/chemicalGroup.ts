import { createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit';
import type { AppThunk } from 'src/store';
import axios from 'axios';
import { apiConfig } from 'src/config';
import { SortDirection } from '@mui/material';
import type { ChemicalGroup } from '../types/chemical';
import type { Meta } from '../types/pagination';

interface ChemicalGroupState {
  searchText?: string,
  limit?: number,
  page?: number,
  total?: number,
  orderBy: string,
  order: SortDirection,
  chemicalGroups: ChemicalGroup[],
}

const initialState: ChemicalGroupState = {
  searchText: '',
  limit: 10,
  page: 0,
  total: 0,
  orderBy: 'name',
  order: 'asc',
  chemicalGroups: [],
};

const slice = createSlice({
  name: 'chemicalGroup',
  initialState,
  reducers: {
    getChemicalGroups(state: ChemicalGroupState, action: PayloadAction<{ chemicalGroups: ChemicalGroup[]; meta: Meta }>) {
      const { chemicalGroups, meta } = action.payload;

      state.chemicalGroups = chemicalGroups;
      state.total = meta.total;
    },
    setLimit(state: ChemicalGroupState, action: PayloadAction<{ limit: number }>) {
      const { limit } = action.payload;

      state.limit = limit;
      state.page = 0;
      state.total = 0;
    },
    setPage(state: ChemicalGroupState, action: PayloadAction<{ page: number }>) {
      const { page } = action.payload;

      state.page = page;
    },
    setSearchText(state: ChemicalGroupState, action: PayloadAction<{ searchText: string }>) {
      const { searchText } = action.payload;

      state.searchText = searchText;
      state.page = 0;
    },
    setOrder(state: ChemicalGroupState, action: PayloadAction<{ orderBy: string; order: SortDirection }>) {
      const { orderBy, order } = action.payload;

      state.orderBy = orderBy;
      state.order = order;
    },
    deleteChemicalGroup(state: ChemicalGroupState, action: PayloadAction<{ chemicalGroupId: number; }>) {
      const { chemicalGroupId } = action.payload;

      state.chemicalGroups = state.chemicalGroups.filter((chemicalGroup) => chemicalGroup.id !== chemicalGroupId);
    }
  }
});

export const { reducer } = slice;

export const getChemicalGroups = (
  organisationId: number,
  limit: number,
  page: number,
  filter: string = '',
  sortBy: string = '',
): AppThunk => async (dispatch) => {
  const response = await axios.get(`${apiConfig.apiV2Url}/organisations/${organisationId}/chemical-group?filter=${filter}&limit=${limit}&order=${sortBy}&page=${page + 1}`);

  dispatch(slice.actions.getChemicalGroups({ chemicalGroups: response.data.data, meta: response.data.meta }));
};

export const setLimit = (limit: number) => (dispatch) => {
  dispatch(slice.actions.setLimit({ limit }));
};

export const setPage = (page: number) => (dispatch) => {
  dispatch(slice.actions.setPage({ page }));
};

export const setSearchText = (searchText: string) => (dispatch) => {
  dispatch(slice.actions.setSearchText({ searchText }));
};

export const setOrder = (orderBy: string, order: SortDirection) => (dispatch) => {
  dispatch(slice.actions.setOrder({ orderBy, order }));
};

export const deleteChemicalGroup = (organisationId: number, chemicalGroupId: number): AppThunk => async (dispatch) => {
  await axios.delete(`${apiConfig.apiV1Url}/organisations/${organisationId}/chemicalgroups/${chemicalGroupId}`);

  dispatch(slice.actions.deleteChemicalGroup({ chemicalGroupId }));
};

export default slice;
