import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import axios from 'axios';
import moment from 'moment';

import { PoolEquipment, PoolEquipmentPhoto, Type } from 'src/types/equipment';
import { Brand } from 'src/types/product';
import removeEmptyFieldsFromObject from 'src/utils/removeEmptyFieldsFromObject';

import { apiConfig } from '../config';
import { AppThunk } from '../store';

interface PoolEquipmentState {
  // must be true if `equipmentId` is not empty
  isEditingMode: boolean;
  equipmentNotFound: boolean;

  isLoading: boolean;

  isCreatingType: boolean;
  isCreatingBrand: boolean;

  isCreatingOrUpdatingEquipment: boolean;
  isCreatingOrUpdatingEquipmentSuccess: boolean;

  equipmentId: number | null;
  poolId: number | null;
  typeId: number | null;
  brandId: number | null;
  name: string;
  serial: string;
  notes: string;
  warrantyStartDate: string | null;
  warrantyEndDate: string | null;
  photos: PoolEquipmentPhoto[];
  photosBlobUrisToUploadOnSave: string[];

  equipmentTypes: Type[];
  productBrands: Brand[];
}

const initialState: PoolEquipmentState = {
  isEditingMode: false,
  equipmentNotFound: false,

  isLoading: false,

  isCreatingType: false,
  isCreatingBrand: false,

  isCreatingOrUpdatingEquipment: false,
  isCreatingOrUpdatingEquipmentSuccess: false,

  equipmentId: null,
  poolId: null,
  typeId: null,
  brandId: null,
  name: '',
  serial: '',
  notes: '',
  warrantyStartDate: null,
  warrantyEndDate: null,
  photos: [],
  photosBlobUrisToUploadOnSave: [],

  equipmentTypes: [],
  productBrands: [],
};

const slice = createSlice({
  name: 'poolEquipment',
  initialState,
  reducers: {
    reset(): PoolEquipmentState {
      return initialState;
    },
    startLoading(state: PoolEquipmentState) {
      state.isLoading = true;
    },
    stopLoading(state: PoolEquipmentState) {
      state.isLoading = false;
    },
    equipmentNotFound(state: PoolEquipmentState) {
      state.equipmentNotFound = true;
    },

    startCreatingType(state: PoolEquipmentState) {
      state.isCreatingType = true;
    },
    stopCreatingType(state: PoolEquipmentState) {
      state.isCreatingType = false;
    },

    startCreatingBrand(state: PoolEquipmentState) {
      state.isCreatingBrand = true;
    },
    stopCreatingBrand(state: PoolEquipmentState) {
      state.isCreatingBrand = false;
    },

    startCreatingOrUpdating(state: PoolEquipmentState) {
      state.isCreatingOrUpdatingEquipment = true;
    },
    stopCreatingOrUpdating(state: PoolEquipmentState) {
      state.isCreatingOrUpdatingEquipment = false;
    },

    creatingOrUpdatingSuccess(state: PoolEquipmentState) {
      state.isCreatingOrUpdatingEquipmentSuccess = true;
    },

    initialiseForCreating(
      state: PoolEquipmentState,
      action: PayloadAction<{
        poolId: number;
        equipmentTypes: Type[];
        productBrands: Brand[];
      }>
    ) {
      state.isEditingMode = false;
      state.poolId = action.payload.poolId;
      state.equipmentTypes = action.payload.equipmentTypes;
      state.productBrands = action.payload.productBrands;
      state.warrantyStartDate = null;
      state.warrantyEndDate = null;
    },
    initializeByEquipment(
      state: PoolEquipmentState,
      action: PayloadAction<{
        equipment: PoolEquipment;
        poolId: number;
        equipmentTypes: Type[];
        productBrands: Brand[];
      }>
    ): void {
      const { equipment, poolId, equipmentTypes, productBrands } = action.payload;

      state.isEditingMode = true;
      state.poolId = poolId;
      state.equipmentId = equipment.id;
      state.typeId = equipment.type_id;
      state.brandId = equipment.brand_id;
      state.name = equipment.name;
      state.serial = equipment.serial || '';
      state.notes = equipment.notes;
      state.warrantyStartDate = equipment.warranty_start_date;
      state.warrantyEndDate = equipment.warranty_end_date;
      state.photos = equipment.photos;

      state.equipmentTypes = equipmentTypes;
      state.productBrands = productBrands;
    },
    typeCreated(
      state: PoolEquipmentState,
      action: PayloadAction<{
        type: Type;
      }>
    ) {
      state.equipmentTypes = [action.payload.type, ...state.equipmentTypes];
      state.typeId = action.payload.type.id;
    },
    brandCreated(
      state: PoolEquipmentState,
      action: PayloadAction<{
        brand: Brand;
      }>
    ) {
      state.productBrands = [action.payload.brand, ...state.productBrands];
      state.brandId = action.payload.brand.id;
    },
    setTypeId(
      state: PoolEquipmentState,
      action: PayloadAction<{
        id: number;
      }>
    ) {
      state.typeId = action.payload.id;
    },
    setBrandId(
      state: PoolEquipmentState,
      action: PayloadAction<{
        id: number;
      }>
    ) {
      state.brandId = action.payload.id;
    },
    setName(
      state: PoolEquipmentState,
      action: PayloadAction<{
        name: string;
      }>
    ) {
      state.name = action.payload.name;
    },
    setSerial(
      state: PoolEquipmentState,
      action: PayloadAction<{
        serial: string;
      }>
    ) {
      state.serial = action.payload.serial;
    },
    setNotes(
      state: PoolEquipmentState,
      action: PayloadAction<{
        notes: string;
      }>
    ) {
      state.notes = action.payload.notes;
    },
    setWarrantyStartDate(
      state: PoolEquipmentState,
      action: PayloadAction<{
        startDate: string;
      }>
    ) {
      state.warrantyStartDate = action.payload.startDate;
    },
    setWarrantyEndDate(
      state: PoolEquipmentState,
      action: PayloadAction<{
        endDate: string;
      }>
    ) {
      state.warrantyEndDate = action.payload.endDate;
    },
    addPhotoBlobUrisToUploadOnSave(
      state: PoolEquipmentState,
      action: PayloadAction<{
        blobUris: string[];
      }>
    ) {
      state.photosBlobUrisToUploadOnSave = Array.from(
        new Set([
          ...state.photosBlobUrisToUploadOnSave,
          ...action.payload.blobUris,
        ])
      );
    },
    removePhotoBlobUri(
      state: PoolEquipmentState,
      action: PayloadAction<{
        blobUri: string;
      }>
    ) {
      state.photosBlobUrisToUploadOnSave = state.photosBlobUrisToUploadOnSave.filter(
        (uri) => uri !== action.payload.blobUri
      );
    },
    removePhoto(
      state: PoolEquipmentState,
      action: PayloadAction<{
        photoId: number;
      }>
    ) {
      state.photos = state.photos.filter(
        (photo) => photo.id !== action.payload.photoId
      );
    },
  },
});

export const { reducer } = slice;

export const {
  reset,
  initializeByEquipment,
  setBrandId,
  setTypeId,
  setName,
  setSerial,
  setWarrantyStartDate,
  setWarrantyEndDate,
  setNotes,
  addPhotoBlobUrisToUploadOnSave,
} = slice.actions;

export const initialize = (
  organisationId: number,
  equipmentId: string | null,
  poolId: string
): AppThunk => async (dispatch): Promise<void> => {
  dispatch(slice.actions.startLoading());
  const equipmentTypesPromise = axios.get(
    `${apiConfig.apiV1Url}/organisations/${organisationId}/equipment-types?orderBy=name,asc`
  );
  const productBrandsPromise = axios.get(
    `${apiConfig.apiV1Url}/organisations/${organisationId}/productsbrands?orderBy=name,asc`
  );

  try {
    if (equipmentId != null) {
      const [
        equipmentResponse,
        equipmentTypesResponse,
        productBrandsResponse,
      ] = await Promise.all([
        axios.get(
          `${apiConfig.apiV1Url}/organisations/${organisationId}/equipments/${equipmentId}`
        ),
        equipmentTypesPromise,
        productBrandsPromise,
      ]);
      dispatch(
        slice.actions.initializeByEquipment({
          equipment: equipmentResponse.data,
          poolId: parseInt(poolId, 10),
          equipmentTypes: equipmentTypesResponse.data,
          productBrands: productBrandsResponse.data,
        })
      );
    } else {
      const [equipmentTypesResponse, productBrandsResponse] = await Promise.all([equipmentTypesPromise, productBrandsPromise]);
      dispatch(
        slice.actions.initialiseForCreating({
          poolId: parseInt(poolId, 10),
          equipmentTypes: equipmentTypesResponse.data,
          productBrands: productBrandsResponse.data,
        })
      );
    }
  } catch (e) {
    if (e.response.status === 404) {
      dispatch(slice.actions.equipmentNotFound());
    }
    throw e;
  } finally {
    dispatch(slice.actions.stopLoading());
  }
};

export const createType = (organisationId: number, name: string, equipmentTypes: Type[]): AppThunk => async (dispatch): Promise<void> => {
  const nameSanitized = name.trim();

  if (nameSanitized.length === 0) {
    return;
  }

  const type = equipmentTypes.find((type) => type.name === nameSanitized);
  const typeAlreadyExists = type != null;

  if (typeAlreadyExists) {
    dispatch(slice.actions.setTypeId({ id: type.id }));
    return;
  }

  dispatch(slice.actions.startCreatingType());

  try {
    const newTypeResponse = await axios.post(
      `${apiConfig.apiV1Url}/organisations/${organisationId}/equipment-types`,
      { name: nameSanitized }
    );
    const newType = newTypeResponse.data as Type;
    dispatch(slice.actions.typeCreated({ type: newType }));
  } finally {
    dispatch(slice.actions.stopCreatingType());
  }
};

export const createBrand = (organisationId: number, name: string, brands: Brand[]): AppThunk => async (dispatch): Promise<void> => {
  const nameSanitized = name.trim();

  if (nameSanitized.length === 0) {
    return;
  }

  const brand = brands.find((b) => b.name === nameSanitized);
  const brandAlreadyExists = brand != null;

  if (brandAlreadyExists) {
    dispatch(slice.actions.setBrandId({ id: brand.id }));
    return;
  }

  dispatch(slice.actions.startCreatingBrand());

  try {
    const newBrandResponse = await axios.post(
      `${apiConfig.apiV1Url}/organisations/${organisationId}/productsbrands`,
      { name: nameSanitized }
    );
    const newBrand = newBrandResponse.data as Brand;
    dispatch(slice.actions.brandCreated({ brand: newBrand }));
  } finally {
    dispatch(slice.actions.stopCreatingBrand());
  }
};

export const createOrUpdateEquipment = (organisationId: number, state: PoolEquipmentState): AppThunk => async (dispatch): Promise<void> => {
  dispatch(slice.actions.startCreatingOrUpdating());

  const { warrantyStartDate, warrantyEndDate } = state;
  const warrantyStartDateFormatted = warrantyStartDate != null
    ? moment(warrantyStartDate).format('YYYY-MM-DD')
    : null;

  const warrantyEndDateFormatted = warrantyEndDate != null
    ? moment(warrantyEndDate).format('YYYY-MM-DD')
    : null;

  const payload = removeEmptyFieldsFromObject({
    pool_id: state.poolId,

    type_id: state.typeId,
    brand_id: state.brandId,
    name: state.name,
    serial: state.serial,
    warranty_start_date: warrantyStartDateFormatted,
    warranty_end_date: warrantyEndDateFormatted,
    notes: state.notes,
  });

  try {
    if (state.equipmentId == null) {
      await axios.post(
        `${apiConfig.apiV1Url}/organisations/${organisationId}/equipments`,
        payload
      );
    } else {
      await axios.put(
        `${apiConfig.apiV1Url}/organisations/${organisationId}/equipments/${state.equipmentId}`,
        payload
      );
    }
    dispatch(slice.actions.creatingOrUpdatingSuccess());
  } finally {
    dispatch(slice.actions.stopCreatingOrUpdating());
  }
};

export default slice;
