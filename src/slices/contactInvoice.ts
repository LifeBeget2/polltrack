import { createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit';
import type { AppThunk } from 'src/store';
import axios from 'axios';
import { apiConfig } from 'src/config';
import moment from 'moment/moment';
import { SortDirection } from '@mui/material';
import type { Invoice, InvoiceStatus } from '../types/contactInvoice';
import { Meta } from '../types/pagination';

interface Range {
  from: string;
  to: string;
}

interface ContactInvoiceState {
  searchText?: string,
  statusFilter?: string,
  limit?: number,
  page?: number,
  total?: number,
  orderBy: string,
  order: SortDirection,
  isLoading: boolean,
  invoices: Invoice[],
  selectedRange: Range;

  enabledColumns?: string[],
  enabledColumnsRestored: boolean,
}

const initialState: ContactInvoiceState = {
  searchText: '',
  statusFilter: 'all',
  limit: 10,
  page: 0,
  total: 0,
  orderBy: 'created_at',
  order: 'desc',
  isLoading: false,
  invoices: [],
  selectedRange: {
    from: moment().subtract(3, 'month').format('YYYY-MM-DD'),
    to: moment().format('YYYY-MM-DD')
  },
  enabledColumns: null,
  enabledColumnsRestored: false,
};

const LOCALSTORAGE_KEY_ENABLED_COLUMNS = 'invoice_enabled_columns';

const slice = createSlice({
  name: 'contactInvoice',
  initialState,
  reducers: {
    getContactInvoices(state: ContactInvoiceState, action: PayloadAction<{ invoices: Invoice[]; meta: Meta }>) {
      const { invoices, meta } = action.payload;

      state.invoices = invoices;
      state.total = meta.total;
      state.isLoading = false;
    },
    setLimit(state: ContactInvoiceState, action: PayloadAction<{ limit: number }>) {
      const { limit } = action.payload;

      state.limit = limit;
      state.page = 0;
      state.total = 0;
    },
    setPage(state: ContactInvoiceState, action: PayloadAction<{ page: number }>) {
      const { page } = action.payload;

      state.page = page;
    },
    setIsLoading(state: ContactInvoiceState, action: PayloadAction<{ isLoading: boolean }>) {
      const { isLoading } = action.payload;

      state.isLoading = isLoading;
    },
    setSearchText(state: ContactInvoiceState, action: PayloadAction<{ searchText: string }>) {
      const { searchText } = action.payload;

      state.searchText = searchText;
      state.page = 0;
    },
    setStatusFilter(state: ContactInvoiceState, action: PayloadAction<{ statusFilter: InvoiceStatus }>) {
      const { statusFilter } = action.payload;

      state.statusFilter = statusFilter;
    },
    setOrder(state: ContactInvoiceState, action: PayloadAction<{ orderBy: string; order: SortDirection }>) {
      const { orderBy, order } = action.payload;

      state.orderBy = orderBy;
      state.order = order;
    },
    deleteContactInvoice(state: ContactInvoiceState, action: PayloadAction<{ invoiceId: number; }>) {
      const { invoiceId } = action.payload;

      state.invoices = state.invoices.filter((invoice) => invoice.id !== invoiceId);
    },
    selectRange(
      state: ContactInvoiceState,
      action: PayloadAction<{ from: string; to: string }>
    ): void {
      const { from, to } = action.payload;

      state.selectedRange = {
        from,
        to
      };
    },
    setEnabledColumns(state: ContactInvoiceState, action: PayloadAction<{ enabledColumns: string[]; }>) {
      state.enabledColumns = action.payload.enabledColumns;
    },
    enabledColumnsRestored(state: ContactInvoiceState) {
      state.enabledColumnsRestored = true;
    },
  }
});

export const { reducer } = slice;

export const getContactInvoices = (
  organisationId: number,
  selectedRange: Range,
  limit: number,
  page: number,
  filter: string = '',
  statusFilter: string = '',
  sortBy: string = '',
): AppThunk => async (dispatch) => {
  dispatch(slice.actions.setIsLoading({ isLoading: true }));
  const response = await axios.get(`${apiConfig.apiV2Url}/organisations/${organisationId}/contact-invoice?filter=${filter}&from=${selectedRange.from}&to=${selectedRange.to}&status=${statusFilter}&limit=${limit}&order=${sortBy}&page=${page + 1}`);

  dispatch(slice.actions.getContactInvoices({ invoices: response.data.data, meta: response.data.meta }));
};

export const setLimit = (limit: number) => (dispatch) => {
  dispatch(slice.actions.setLimit({ limit }));
};

export const setPage = (page: number) => (dispatch) => {
  dispatch(slice.actions.setPage({ page }));
};

export const setSearchText = (searchText: string) => (dispatch) => {
  dispatch(slice.actions.setSearchText({ searchText }));
};

export const setStatusFilter = (statusFilter: InvoiceStatus) => (dispatch) => {
  dispatch(slice.actions.setStatusFilter({ statusFilter }));
};

export const setOrder = (orderBy: string, order: SortDirection) => (dispatch) => {
  dispatch(slice.actions.setOrder({ orderBy, order }));
};

export const deleteContactInvoice = (organisationId: number, invoiceId: number): AppThunk => async (dispatch) => {
  await axios.delete(`${apiConfig.apiV1Url}/organisations/${organisationId}/contactinvoice/${invoiceId}`);

  dispatch(slice.actions.deleteContactInvoice({ invoiceId }));
};

export const selectRange = (
  from: Date,
  to: Date
): AppThunk => (dispatch): void => {
  dispatch(slice.actions.selectRange({
    from: moment(from).format('YYYY-MM-DD'),
    to: moment(to).format('YYYY-MM-DD')
  }));
};

export const setEnabledColumns = (enabledColumns: string[]) => (dispatch) => {
  dispatch(slice.actions.setEnabledColumns({ enabledColumns }));
  localStorage.setItem(LOCALSTORAGE_KEY_ENABLED_COLUMNS, JSON.stringify(enabledColumns));
};

export const restoreEnabledColumns = () => (dispatch) => {
  const enabledColumns = JSON.parse(localStorage.getItem(LOCALSTORAGE_KEY_ENABLED_COLUMNS));
  dispatch(slice.actions.setEnabledColumns({ enabledColumns }));
  dispatch(slice.actions.enabledColumnsRestored());
};

export default slice;
