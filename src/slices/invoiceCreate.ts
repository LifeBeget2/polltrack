import { createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit';
import axios from 'axios';
import type { AppThunk } from '../store';
import type { Job, Invoice } from '../types/job';
import type { InvoiceRecipient } from '../types/invoice';
import { apiConfig } from '../config';

interface InvoiceCreateState {
  job: Job | null;
  invoices: Invoice[] | [];
  recipients: InvoiceRecipient[];
  isLoading: boolean;
}

const initialState: InvoiceCreateState = {
  job: null,
  invoices: [],
  recipients: [],
  isLoading: false
};

const slice = createSlice({
  name: 'invoiceCreate',
  initialState,
  reducers: {
    getJob(
      state: InvoiceCreateState,
      action: PayloadAction<{ job: Job }>
    ): void {
      const { job } = action.payload;

      state.job = job;
      state.invoices = job.invoices;
      state.recipients = [];
      state.isLoading = false;
    },
    addRecipient(
      state: InvoiceCreateState,
      action: PayloadAction<{ recipient: InvoiceRecipient }>
    ): void {
      const { recipient } = action.payload;
      const exists = state.recipients.find((_recipient: InvoiceRecipient) => _recipient.contact_id === recipient.contact_id);

      if (!exists) {
        recipient.number = state.recipients.length + 1;
        state.recipients.push(recipient);
      }
    },
    changeInvoiceRecipient(
      state: InvoiceCreateState,
      action: PayloadAction<{ invoice: Invoice, recipient: InvoiceRecipient }>
    ): void {
      const { invoice, recipient } = action.payload;
      state.recipients = state.recipients.map((_recipient: InvoiceRecipient) => {
        if (recipient.contact_id === _recipient.contact_id) {
          _recipient.products.push(invoice);
        } else if (_recipient.products.find((product) => product.id === invoice.id)) {
          _recipient.products = _recipient.products.filter((product) => product.id !== invoice.id);
        }

        return _recipient;
      });
    },
    deleteRecipient(
      state: InvoiceCreateState,
      action: PayloadAction<{ contactId: number }>
    ): void {
      const { contactId } = action.payload;

      const deleteRecipient = state.recipients.find((recipient) => recipient.contact_id === contactId);
      const recipients = state.recipients.filter((recipient) => recipient.contact_id !== contactId);

      state.recipients = recipients.map((recipient, index) => {
        recipient.number = index + 1;

        if (deleteRecipient.products.length && index === 0) {
          deleteRecipient.products.forEach((product) => {
            recipient.products.push(product);
          });
        }

        return recipient;
      });
    },
    setIsLoading(state: InvoiceCreateState, action: PayloadAction<{ isLoading: boolean }>) {
      const { isLoading } = action.payload;

      state.isLoading = isLoading;
    },
  }
});

export const { reducer } = slice;

export const getJob = (organisationId: number, jobId: number): AppThunk => async (dispatch): Promise<void> => {
  dispatch(slice.actions.setIsLoading({ isLoading: true }));

  const response = await axios.get(`${apiConfig.apiV1Url}/organisations/${organisationId}/jobs/${jobId}`);

  dispatch(slice.actions.getJob({ job: response.data }));
};

export const addRecipient = (recipient: InvoiceRecipient): AppThunk => (dispatch): void => {
  dispatch(slice.actions.addRecipient({ recipient }));
};

export const deleteRecipient = (contactId: number): AppThunk => (dispatch): void => {
  dispatch(slice.actions.deleteRecipient({ contactId }));
};

export const changeInvoiceRecipient = (invoice: Invoice, recipient: InvoiceRecipient): AppThunk => (dispatch): void => {
  dispatch(slice.actions.changeInvoiceRecipient({ invoice, recipient }));
};

export default slice;
