import { createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit';
import type { AppThunk } from 'src/store';
import axios from 'axios';
import { apiConfig } from 'src/config';
import { SortDirection } from '@mui/material';
import type { Quote } from '../types/quote';
import type { Meta } from '../types/pagination';

interface OpenedQuoteState {
  searchText?: string,
  limit?: number,
  page?: number,
  total?: number,
  orderBy: string,
  order: SortDirection,
  quotes: Quote[],
}

const initialState: OpenedQuoteState = {
  searchText: '',
  limit: 10,
  page: 0,
  total: 0,
  orderBy: 'created_at',
  order: 'desc',
  quotes: [],
};

const slice = createSlice({
  name: 'contact',
  initialState,
  reducers: {
    getOpenedQuotes(state: OpenedQuoteState, action: PayloadAction<{ quotes: Quote[]; meta: Meta }>) {
      const { quotes, meta } = action.payload;

      state.quotes = quotes;
      state.total = meta.total;
    },
    setLimit(state: OpenedQuoteState, action: PayloadAction<{ limit: number }>) {
      const { limit } = action.payload;

      state.limit = limit;
      state.page = 0;
      state.total = 0;
    },
    setPage(state: OpenedQuoteState, action: PayloadAction<{ page: number }>) {
      const { page } = action.payload;

      state.page = page;
    },
    setSearchText(state: OpenedQuoteState, action: PayloadAction<{ searchText: string }>) {
      const { searchText } = action.payload;

      state.searchText = searchText;
      state.page = 0;
    },
    setOrder(state: OpenedQuoteState, action: PayloadAction<{ orderBy: string; order: SortDirection }>) {
      const { orderBy, order } = action.payload;

      state.orderBy = orderBy;
      state.order = order;
    },
    deleteQuote(state: OpenedQuoteState, action: PayloadAction<{ quoteId: number; }>) {
      const { quoteId } = action.payload;

      state.quotes = state.quotes.filter((contact) => contact.id !== quoteId);
    }
  }
});

export const { reducer } = slice;

export const getOpenedQuotes = (
  organisationId: number,
  limit: number,
  page: number,
  filter: string = '',
  sortBy: string = '',
): AppThunk => async (dispatch) => {
  const response = await axios.get(`${apiConfig.apiV2Url}/organisations/${organisationId}/quotes?filter=${filter}&limit=${limit}&order=${sortBy}&page=${page + 1}&include=pool`);

  dispatch(slice.actions.getOpenedQuotes({ quotes: response.data.data, meta: response.data.meta }));
};

export const setLimit = (limit: number) => (dispatch) => {
  dispatch(slice.actions.setLimit({ limit }));
};

export const setPage = (page: number) => (dispatch) => {
  dispatch(slice.actions.setPage({ page }));
};

export const setSearchText = (searchText: string) => (dispatch) => {
  dispatch(slice.actions.setSearchText({ searchText }));
};

export const setOrder = (orderBy: string, order: SortDirection) => (dispatch) => {
  dispatch(slice.actions.setOrder({ orderBy, order }));
};

export const deleteQuote = (organisationId: number, quoteId: number): AppThunk => async (dispatch) => {
  await axios.delete(`${apiConfig.apiV1Url}/organisations/${organisationId}/jobs/${quoteId}`);

  dispatch(slice.actions.deleteQuote({ quoteId }));
};

export default slice;
