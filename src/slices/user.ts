import { createSlice } from '@reduxjs/toolkit';
import type { PayloadAction } from '@reduxjs/toolkit';
import type { AppThunk } from 'src/store';
import axios from 'axios';
import { apiConfig } from 'src/config';
import type { User } from '../types/user';

interface UserState {
  users: User[],
  technicians: User[],
}

const initialState: UserState = {
  users: [],
  technicians: [],
};

const slice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    getUsers(state: UserState, action: PayloadAction<{ users: User[] }>) {
      const { users } = action.payload;

      state.users = users;
      // TODO: filter techs based on a user role
      state.technicians = users;
    },
    deleteUser(state: UserState, action: PayloadAction<{ userId: number; }>) {
      const { userId } = action.payload;

      state.users = state.users.filter((user) => user.id !== userId);
    }
  }
});

export const { reducer } = slice;

export const getUsers = (
  organisationId: number,
): AppThunk => async (dispatch) => {
  const response = await axios.get(`${apiConfig.apiV1Url}/organisations/${organisationId}/users`);

  dispatch(slice.actions.getUsers({ users: response.data }));
};

export const deleteUser = (organisationId: number, userId: number): AppThunk => async (dispatch) => {
  await axios.delete(`${apiConfig.apiV1Url}/organisations/${organisationId}/users/${userId}`);

  dispatch(slice.actions.deleteUser({ userId }));
};

export default slice;
