import { useRef, useState } from 'react';
import type { FC } from 'react';
import PropTypes from 'prop-types';
import {
  AppBar,
  Avatar,
  Badge,
  Box,
  ButtonBase,
  IconButton,
  Toolbar,
  Tooltip
} from '@mui/material';
import { styled } from '@mui/material/styles';
import type { AppBarProps } from '@mui/material';
import { Menu as MenuIcon } from '../icons/menu-new';
import { AccountPopover } from '../components/account-popover';
// import { ContactsPopover } from '../components/contacts-popover';
import { ContentSearchDialog } from '../components/content-search-dialog';
// import { NotificationsPopover } from '../components/notifications-popover';
import { Bell as BellIcon } from '../icons/bell-new';
import { UserCircle as UserCircleIcon } from '../icons/user-circle-new';
import { Search as SearchIcon } from '../icons/search-new';
import { Users as UsersIcon } from '../icons/users-new';

interface MainNavbarProps extends AppBarProps {
  onOpenSidebar?: () => void;
}

// const MainNavbarRoot = styled(AppBar)(
//   ({ theme }) => ({
//     backgroundColor: theme.palette.background.paper,
//     ...(
//       theme.palette.mode === 'light'
//         ? {
//           boxShadow: theme.shadows[3]
//         }
//         : {
//           backgroundColor: theme.palette.background.paper,
//           borderBottomColor: theme.palette.divider,
//           borderBottomStyle: 'solid',
//           borderBottomWidth: 1,
//           boxShadow: 'none'
//         }
//     )
//   })
// );

const MainNavbarRoot = styled(AppBar)(
  ({ theme }) => (
    {
      ...(
        theme.palette.mode === 'light' && {
          backgroundColor: theme.palette.primary.main,
          boxShadow: 'none',
          color: theme.palette.primary.contrastText
        }
      ),
      ...(
        theme.palette.mode === 'dark' && {
          backgroundColor: theme.palette.background.paper,
          borderBottom: `1px solid ${theme.palette.divider}`,
          boxShadow: 'none'
        }
      ),
      zIndex: theme.zIndex.drawer + 100
    }
  )
);

const ContentSearchButton = () => {
  const [openDialog, setOpenDialog] = useState<boolean>(false);

  const handleOpenSearchDialog = (): void => {
    setOpenDialog(true);
  };

  const handleCloseSearchDialog = (): void => {
    setOpenDialog(false);
  };

  return (
    <>
      <Tooltip title="Search">
        <IconButton
          onClick={handleOpenSearchDialog}
          sx={{ ml: 1 }}
        >
          <SearchIcon fontSize="small" />
        </IconButton>
      </Tooltip>
      <ContentSearchDialog
        onClose={handleCloseSearchDialog}
        open={openDialog}
      />
    </>
  );
};

// const ContactsButton = () => {
//   const anchorRef = useRef<HTMLButtonElement | null>(null);
//   const [openPopover, setOpenPopover] = useState<boolean>(false);
//
//   const handleOpenPopover = (): void => {
//     setOpenPopover(true);
//   };
//
//   const handleClosePopover = (): void => {
//     setOpenPopover(false);
//   };
//
//   return (
//     <>
//       <Tooltip title="Contacts">
//         <IconButton
//           onClick={handleOpenPopover}
//           sx={{ ml: 1 }}
//           ref={anchorRef}
//         >
//           <UsersIcon fontSize="small" />
//         </IconButton>
//       </Tooltip>
//       <ContactsPopover
//         anchorEl={anchorRef.current}
//         onClose={handleClosePopover}
//         open={openPopover}
//       />
//     </>
//   );
// };

// const NotificationsButton = () => {
//   const anchorRef = useRef<HTMLButtonElement | null>(null);
//   const [unread, setUnread] = useState<number>(0);
//   const [openPopover, setOpenPopover] = useState<boolean>(false);
//   // Unread notifications should come from a context and be shared with both this component and
//   // notifications popover. To simplify the demo, we get it from the popover
//
//   const handleOpenPopover = (): void => {
//     setOpenPopover(true);
//   };
//
//   const handleClosePopover = (): void => {
//     setOpenPopover(false);
//   };
//
//   const handleUpdateUnread = (value: number): void => {
//     setUnread(value);
//   };
//
//   return (
//     <>
//       <Tooltip title="Notifications">
//         <IconButton
//           ref={anchorRef}
//           sx={{ ml: 1 }}
//           onClick={handleOpenPopover}
//         >
//           <Badge
//             color="error"
//             badgeContent={unread}
//           >
//             <BellIcon fontSize="small" />
//           </Badge>
//         </IconButton>
//       </Tooltip>
//       <NotificationsPopover
//         anchorEl={anchorRef.current}
//         onClose={handleClosePopover}
//         onUpdateUnread={handleUpdateUnread}
//         open={openPopover}
//       />
//     </>
//   );
// };

const AccountButton = () => {
  const anchorRef = useRef<HTMLButtonElement | null>(null);
  const [openPopover, setOpenPopover] = useState<boolean>(false);
  // To get the user from the authContext, you can use
  // `const { user } = useAuth();`
  const user = {
    avatar: '/static/mock-images/avatars/avatar-anika_visser.png',
    name: 'Anika Visser'
  };

  const handleOpenPopover = (): void => {
    setOpenPopover(true);
  };

  const handleClosePopover = (): void => {
    setOpenPopover(false);
  };

  return (
    <>
      <Box
        component={ButtonBase}
        onClick={handleOpenPopover}
        ref={anchorRef}
        sx={{
          alignItems: 'center',
          display: 'flex',
          ml: 2
        }}
      >
        <Avatar
          sx={{
            height: 40,
            width: 40
          }}
          src={user.avatar}
        >
          <UserCircleIcon fontSize="small" />
        </Avatar>
      </Box>
      <AccountPopover
        anchorEl={anchorRef.current}
        onClose={handleClosePopover}
        open={openPopover}
      />
    </>
  );
};

export const MainNavbar: FC<MainNavbarProps> = (props) => {
  const { onOpenSidebar, ...other } = props;

  return (
    <>
      <MainNavbarRoot {...other}>
        <Toolbar
          disableGutters
          sx={{
            minHeight: 64,
            left: 0,
            px: 2
          }}
        >
          <IconButton
            onClick={onOpenSidebar}
            sx={{
              display: {
                xs: 'inline-flex',
                lg: 'none'
              }
            }}
          >
            <MenuIcon fontSize="small" />
          </IconButton>
          <Box sx={{ flexGrow: 1 }} />
          <ContentSearchButton />
          {/* <ContactsButton /> */}
          {/* <NotificationsButton /> */}
          <AccountButton />
        </Toolbar>
      </MainNavbarRoot>
    </>
  );
};

MainNavbar.propTypes = {
  onOpenSidebar: PropTypes.func
};
