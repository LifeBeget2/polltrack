import type { FC, ReactNode } from 'react';
import PropTypes from 'prop-types';
import { Button, Collapse, List, ListSubheader } from '@mui/material';
import type { ListProps } from '@mui/material';
import { MainSidebarItem } from './main-sidebar-item';
import PlusIcon from '../icons/Plus';
import MinusIcon from '../icons/Minus';

interface Item {
  path?: string;
  icon?: ReactNode;
  chip?: ReactNode;
  info?: ReactNode;
  children?: Item[];
  title: string;
}

interface MainSidebarSectionProps extends ListProps {
  items: Item[];
  path: string;
  title: string;
  isCollapsed: boolean;
  onToggleSection: () => void;
}

const renderNavItems = ({
  depth = 0,
  items,
  path
}: {
  depth?: number;
  items: Item[];
  path: string;
}): JSX.Element => (
  <List disablePadding>
    {items.reduce(
      (acc, item) => reduceChildRoutes({
        acc,
        item,
        depth,
        path
      }),
      []
    )}
  </List>
);

const reduceChildRoutes = ({
  acc,
  item,
  depth,
  path
}: {
  acc: JSX.Element[];
  depth: number;
  item: Item;
  path: string;
}): Array<JSX.Element> => {
  const key = `${item.title}-${depth}`;
  const partialMatch = path.includes(item.path);
  const exactMatch = path === item.path;

  if (item.children) {
    acc.push(
      <MainSidebarItem
        active={partialMatch}
        chip={item.chip}
        depth={depth}
        icon={item.icon}
        info={item.info}
        key={key}
        open={partialMatch}
        path={item.path}
        title={item.title}
      >
        {renderNavItems({
          depth: depth + 1,
          items: item.children,
          path
        })}
      </MainSidebarItem>
    );
  } else {
    acc.push(
      <MainSidebarItem
        active={exactMatch}
        chip={item.chip}
        depth={depth}
        icon={item.icon}
        info={item.info}
        key={key}
        path={item.path}
        title={item.title}
      />
    );
  }

  return acc;
};

export const MainSidebarSection: FC<MainSidebarSectionProps> = (props) => {
  const { items, path, title, isCollapsed, onToggleSection, ...other } = props;

  return (
    <List
      subheader={(
        <Button
          endIcon={isCollapsed ? <PlusIcon fontSize="small" /> : <MinusIcon fontSize="small" />}
          sx={{
            color: 'text.primary',
            fontWeight: 'fontWeightMedium',
            justifyContent: 'flex-start',
            textAlign: 'left',
            px: 2,
            textTransform: 'none',
            width: '100%',
          }}
          variant="text"
          onClick={onToggleSection}
        >
          <ListSubheader
            disableGutters
            disableSticky
            sx={{
              color: 'text.primary',
              width: '100%',
              fontSize: '0.8rem',
              lineHeight: 2,
              fontWeight: 700,
              textTransform: 'uppercase'
            }}
          >
            {title}
          </ListSubheader>
        </Button>
      )}
      {...other}
    >
      <Collapse in={!isCollapsed}>
        {renderNavItems({
          items,
          path
        })}
      </Collapse>
    </List>
  );
};

MainSidebarSection.propTypes = {
  items: PropTypes.array.isRequired,
  path: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  isCollapsed: PropTypes.bool,
  onToggleSection: PropTypes.func,
};
