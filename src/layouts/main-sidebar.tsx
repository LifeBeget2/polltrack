import React, { ReactNode, useCallback, useEffect, useMemo } from 'react';
import type { FC } from 'react';
import NextLink from 'next/link';
import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import { useTranslation } from 'react-i18next';
import type { TFunction } from 'react-i18next';
import { Box, Button, Divider, Drawer, Typography, useMediaQuery } from '@mui/material';
import type { Theme } from '@mui/material';
import AccountIcon from 'src/icons/Account';
import MarketingIcon from 'src/icons/Marketing';
import IntegrationsIcon from 'src/icons/Integrations';
import DosagesIcon from 'src/icons/Dosages';
import PlanAndPaymentIcon from 'src/icons/PlanAndPayment';
import PoolCharacteristicsIcon from 'src/icons/PoolCharacteristics';
import TeamIcon from 'src/icons/Team';
import TestParametersIcon from 'src/icons/TestParameters';
import ToolsIcon from 'src/icons/Tools';
import ProductIcon from 'src/icons/Product';
import LabIcon from 'src/icons/Lab';
import ContactIcon from 'src/icons/Contact';
import QuoteIcon from 'src/icons/Quote';
import InvoiceIcon from 'src/icons/Invoice';
import JobIcon from 'src/icons/Job';
import PoolIcon from 'src/icons/Pool';
import Logo from 'src/components/Logo';
import { Scrollbar } from '../components/scrollbar';
import { MainSidebarSection } from './main-sidebar-section';
import DashboardIcon from '../icons/Dashboard';
import { useDispatch, useSelector } from '../store';
import { toggle } from '../slices/navSection';

interface MainSidebarProps {
  onClose: () => void;
  open: boolean;
}

interface Item {
  title: string;
  children?: Item[];
  chip?: ReactNode;
  icon?: ReactNode;
  path?: string;
}

interface Section {
  title: string;
  items: Item[];
}

const getSections = (t: TFunction): Section[] => [
  {
    title: t('Operations'),
    items: [
      {
        title: t('Dashboard'),
        path: '/dashboard',
        icon: <DashboardIcon fontSize="small" />,
        children: [
          {
            title: t('Overview'),
            path: '/dashboard/overview'
          },
          {
            title: t('Analytics'),
            path: '/dashboard/analytics'
          },
          {
            title: t('Finance'),
            path: '/dashboard/finance'
          }
        ]
      },
      {
        title: t('Lab'),
        path: '/lab',
        icon: <LabIcon fontSize="small" />,
        children: [
          {
            title: t('Water test'),
            path: '/lab/water-test/pool-list'
          },
          {
            title: t('Test history'),
            path: '/lab/test-history'
          },
        ]
      },
      {
        title: t('Jobs'),
        path: '/jobs',
        icon: <JobIcon fontSize="small" />,
        children: [
          {
            title: t('Calendar'),
            path: '/jobs/calendar'
          },
          {
            title: t('Job to invoice'),
            path: '/jobs/to-invoice-list'
          },
          {
            title: t('Recurrence dashboard'),
            path: '/jobs/recurrence-dashboard'
          },
        ]
      },
      {
        title: t('Quotes'),
        path: '/quotes',
        icon: <QuoteIcon fontSize="small" />,
        children: [
          {
            title: t('Opened'),
            path: '/quotes/opened'
          },
          {
            title: t('Archived'),
            path: '/quotes/archived'
          },
        ]
      },
      {
        title: t('Invoicing'),
        path: '/invoices',
        icon: <InvoiceIcon fontSize="small" />,
        children: [
          {
            title: t('Invoices'),
            path: '/invoices'
          },
          {
            title: t('Batch queue'),
            path: '/invoices/batch/queue'
          },
          {
            title: t('Batch invoices'),
            path: '/invoices/batch'
          },
        ]
      },
      {
        title: t('Contacts'),
        path: '/contacts',
        icon: <ContactIcon fontSize="small" />
      },
      {
        title: t('Pools'),
        path: '/pools',
        icon: <PoolIcon fontSize="small" />
      },
      {
        title: t('Products'),
        path: '/products',
        icon: <ProductIcon fontSize="small" />
      },
      {
        title: t('Campaigns'),
        path: '/campaigns',
        icon: <MarketingIcon fontSize="small" />
      },
    ],
  },
  {
    title: t('Setup'),
    items: [
      {
        title: t('My business'),
        path: '/setup/organisation',
        icon: <AccountIcon fontSize="small" />
      },
      {
        title: t('Plan & payment'),
        icon: <PlanAndPaymentIcon fontSize="small" />,
        path: '/setup/plan-and-payment',
      },
      {
        title: t('Team'),
        icon: <TeamIcon fontSize="small" />,
        path: '/setup/team',
      },
      {
        title: t('Quotes'),
        icon: <QuoteIcon fontSize="small" />,
        children: [
          {
            title: t('Email'),
            path: '/setup/quotes/email-settings',
          },
          {
            title: t('Settings'),
            path: '/setup/quotes/settings',
          },
        ]
      },
      {
        title: 'Jobs',
        icon: <JobIcon fontSize="small" />,
        children: [
          {
            title: t('Email'),
            path: '/setup/jobs/email-settings',
          },
          {
            title: t('SMS'),
            path: '/setup/jobs/sms-settings/configure',
          },
          {
            title: t('Job templates'),
            path: '/setup/jobs/job-templates',
          },
          {
            title: t('Job sheet'),
            path: '/setup/jobs/job-sheet-settings',
          },
          {
            title: t('Invoice'),
            path: '/setup/jobs/invoice-settings',
          },
          {
            title: t('Colors'),
            path: '/setup/jobs/colors',
          },
          {
            title: t('Trading terms'),
            path: '/setup/jobs/trading-terms',
          },
        ]
      },
      {
        title: t('Batch billing'),
        icon: <InvoiceIcon fontSize="small" />,
        children: [
          {
            title: t('Email'),
            path: '/setup/batch-billing/email-settings',
          },
          {
            title: t('Settings'),
            path: '/setup/batch-billing/settings',
          },
        ]
      },
      {
        title: t('Lab'),
        icon: <LabIcon fontSize="small" />,
        children: [
          {
            title: t('Email'),
            path: '/setup/lab/email-settings',
          },
          {
            title: t('SMS'),
            path: '/setup/lab/sms-settings',
          },
          {
            title: t('Settings'),
            path: '/setup/lab/settings',
          },
        ]
      },
      {
        title: t('Integrations'),
        icon: <IntegrationsIcon fontSize="small" />,
        children: [
          {
            title: t('Xero'),
            path: '/setup/integrations/xero',
          },
          {
            title: t('Stripe'),
            path: '/setup/integrations/stripe',
          },
          {
            title: t('Vend'),
            path: '/setup/integrations/vend',
          },
        ]
      },
      {
        title: t('Data cleaning'),
        icon: <ToolsIcon fontSize="small" />,
        children: [
          {
            title: t('Contacts'),
            path: '/setup/data-cleaning/contact/deduplicate',
          },
          {
            title: t('Pools'),
            path: '/setup/data-cleaning/pool/deduplicate',
          },
        ]
      },
      {
        title: t('Water testing'),
        icon: <TestParametersIcon fontSize="small" />,
        children: [
          {
            title: t('Chemical tests'),
            path: '/setup/water-testing/chemical-tests',
          },
          {
            title: t('Observation tests'),
            path: '/setup/water-testing/observation-tests',
          },
        ]
      },
      {
        title: t('Dosage groups'),
        icon: <DosagesIcon fontSize="small" />,
        children: [
          {
            title: t('Chemical groups'),
            path: '/setup/dosage-groups/chemical-groups',
          },
          {
            title: t('Observation groups'),
            path: '/setup/dosage-groups/observation-groups',
          },
        ]
      },
      {
        title: t('Pool characteristics'),
        path: '/setup/pool-characteristics/surface',
        icon: <PoolCharacteristicsIcon fontSize="small" />,
      },
    ],
  },
];

export const MainSidebar: FC<MainSidebarProps> = (props) => {
  const { onClose, open } = props;
  const router = useRouter();
  const { t } = useTranslation();
  const lgUp = useMediaQuery(
    (theme: Theme) => theme.breakpoints.up('lg'),
    {
      noSsr: true
    }
  );
  const dispatch = useDispatch();
  const collapsedSectionTitles = useSelector((state) => state.navSection.collapsedSectionTitles) || [];
  const mobileDevice = useMediaQuery((theme: Theme) => theme.breakpoints.down('sm'));
  const sections = useMemo(() => getSections(t), [t]);

  const onToggleSection = useCallback((sectionTitle: string) => {
    dispatch(toggle(sectionTitle, collapsedSectionTitles));
  }, [collapsedSectionTitles]);

  const handlePathChange = () => {
    if (!router.isReady) {
      return;
    }

    if (open) {
      onClose?.();
    }
  };

  useEffect(
    handlePathChange,
    [router.isReady, router.asPath]
  );

  const content = (
    <Box
      sx={{
        paddingTop: mobileDevice ? 0 : '64px',
        display: 'flex',
        flexDirection: 'column',
        height: '100%'
      }}
    >
      <Box
        sx={{
          display: 'flex',
          justifyContent: 'center',
        }}
      >
        <NextLink
          href="/"
          passHref
        >
          <a>
            <Logo
              sx={{
                height: 64,
                '& img': {
                  height: 64,
                  maxWidth: 238,
                  objectFit: 'contain'
                }
              }}
            />
          </a>
        </NextLink>
      </Box>
      <Scrollbar
        sx={{
          height: 'calc(100% - 64px)',
        }}
      >
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'column',
            height: '100%'
          }}
        >
          <Box sx={{ flexGrow: 1 }}>
            {sections.map((section) => (
              <MainSidebarSection
                key={section.title}
                path={router.asPath}
                isCollapsed={collapsedSectionTitles.includes(section.title)}
                onToggleSection={() => onToggleSection(section.title)}
                sx={{
                  mt: 2,
                  '& + &': {
                    mt: 2
                  }
                }}
                {...section}
              />
            ))}
          </Box>
          <Divider />
          <Box sx={{ p: 2 }}>
            <Typography
              color="textPrimary"
              variant="subtitle2"
            >
              {t('Need Help?')}
            </Typography>
            <Typography
              color="textSecondary"
              variant="body2"
            >
              {t('Check our docs')}
            </Typography>
            <NextLink
              href="/docs/welcome"
              passHref
            >
              <Button
                color="secondary"
                component="a"
                fullWidth
                sx={{ mt: 2 }}
                variant="contained"
              >
                {t('Documentation')}
              </Button>
            </NextLink>
          </Box>
        </Box>
      </Scrollbar>
    </Box>
  );

  if (lgUp) {
    return (
      <Drawer
        anchor="left"
        open
        PaperProps={{
          sx: {
            backgroundColor: 'background.paper',
            borderRightWidth: 1,
            width: 280
          }
        }}
        variant="permanent"
      >
        {content}
      </Drawer>
    );
  }

  return (
    <Drawer
      anchor="left"
      onClose={onClose}
      open={open}
      PaperProps={{
        sx: {
          backgroundColor: 'background.paper',
          width: 280
        }
      }}
      variant="temporary"
    >
      {content}
    </Drawer>
  );
};

MainSidebar.propTypes = {
  onClose: PropTypes.func,
  open: PropTypes.bool,
};
