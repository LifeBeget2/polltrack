export const PRODUCT_LIST_COLUMN_TITLE = 'title';
export const PRODUCT_LIST_COLUMN_SKU = 'sku';
export const PRODUCT_LIST_COLUMN_VOLUME = 'volume';
export const PRODUCT_LIST_COLUMN_UNITS = 'units';
export const PRODUCT_LIST_COLUMN_BRAND = 'brand';
export const PRODUCT_LIST_COLUMN_PRICE = 'price';
export const PRODUCT_LIST_COLUMN_ACTIVE = 'active';
