/* eslint-disable */
import React, { FC, useCallback, useMemo } from 'react';

import PropTypes from 'prop-types';

import { Theme } from '@mui/material';
import numeral from 'numeral';
import {
  Document,
  Image,
  Page,
  StyleSheet,
  Text,
  View,
} from '@react-pdf/renderer';

import type { LabJob } from 'src/types/labJob';
import type { Image as ImageType } from 'src/types/image';
import { Organisation } from 'src/types/organisation';
import { getEntityAddress } from 'src/utils/address';

import { initializePdfRender, useCommonStyles } from './commonStyles';
import PoolInfo from './PoolInfo';
import RecommendationItem from './RecommendationItem';
import ResultsTableHeader from './ResultsTableHeader';
import ResultsTableRow from './ResultsTableRow';
import type {
  ChemicalTestResult,
  ObservationTestResult,
} from 'src/types/waterTest';
import type { Pool } from 'src/types/pool';
import type { Contact } from 'src/types/contact';
import { getFullName } from '../../../utils/contact';
import getCommaSeparatedSanitisers from 'src/utils/pool';
import moment from 'moment/moment';

initializePdfRender();

interface WaterTestPdfProps {
  labJob: LabJob;
  contact: Contact;
  pool: Pool;
  theme: Theme;
  logo?: ImageType;
  organisation: Organisation;
  chemicalResults: ChemicalTestResult[] | [];
  observationResults: ObservationTestResult[] | [];
}

const createStyles = (theme: Theme) => StyleSheet.create({
  page: {
    backgroundColor: '#ffffff',
  },
  header: {
    padding: 24,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  headerInfo: {
    maxWidth: '50%',
  },
  headerLogo: {
    maxWidth: '50%',
  },
  footer: {
    padding: 16,
  },
  banner: {
    maxHeight: 60,
    paddingHorizontal: 24,
  },
  bannerImage: {
    height: 60,
    width: '100%'
  },
  content: {
    paddingTop: 12,
    paddingHorizontal: 24,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  contentRecommendationsColumn: {
    width: '50%',
  },
  contentTestResultsColumn: {
    width: '45%',
  },
  footerText: {
    textAlign: 'center',
    color: theme.palette.grey[600],
  },
  spacer: {
    flexGrow: 1,
    height: 100
  }
});

// TODO: Implement campaign banners

const WaterTestPdf: FC<WaterTestPdfProps> = (props) => {
  const { labJob, contact, pool, theme, logo, organisation, chemicalResults, observationResults } = props;
  console.log(observationResults);
  const organisationAddress = useMemo(
    () => getEntityAddress(organisation, 'company'),
    []
  );
  const commonStyles = useCommonStyles(theme);
  const styles = useMemo(() => createStyles(theme), [theme]);

  const currentDateText = useMemo(
    () => moment(labJob.start_time).format('DD MMM YYYY | HH:mm'),
    [labJob]
  );

  const getRange = useCallback(
    (chemicalResult: ChemicalTestResult) => `${chemicalResult.min_value} - ${chemicalResult.max_value}`,
    []
  );

  // const recommendations: TempTestRecommendation[] = useMemo(
  //   () => flow(
  //     map((x: TempTestResult) => x.recommendations),
  //     flatten
  //   )(results),
  //   [results]
  // );

  return (
    <Document>
      <Page
        size="A4"
        style={styles.page}
        wrap
      >
        <View
          fixed
          style={styles.header}
        >
          <View style={styles.headerInfo}>
            <Text style={commonStyles.h4}>Water Analysis</Text>
            <Text style={commonStyles.body1}>{currentDateText}</Text>
          </View>
          <Image
            style={styles.headerLogo}
            src={{
              uri: `https://cors.bridged.cc/${logo.full_url}`,
              method: 'GET',
              headers: {
                'Cache-Control': 'no-cache',
                'Access-Control-Allow-Origin': '*',
              },
              body: '',
            }}
          />
        </View>

        <PoolInfo
          theme={theme}
          to={getFullName(contact)}
          address={getEntityAddress(pool, 'pool')}
          type={pool.pool_type?.name || 'Unknown type'}
          volume={`${numeral(pool.pool_volume).format('0,0')} L`}
          surface={pool.pool_surface_type?.name || 'Unknown surface'}
          sanitiser={getCommaSeparatedSanitisers(pool.pool_sanitisers)}
        />

        <View style={styles.content}>
          <View style={styles.contentRecommendationsColumn}>
            <Text style={commonStyles.h6}>Recommendations</Text>
            {chemicalResults.map((chemicalResult: ChemicalTestResult) => (
              <RecommendationItem
                key={chemicalResult.id}
                theme={theme}
                title={chemicalResult.name}
                subtitle={chemicalResult.action}
                description={chemicalResult.description}
              />
            ))}
          </View>
          <View style={styles.contentTestResultsColumn}>
            <Text style={commonStyles.h6}>Test results</Text>
            <ResultsTableHeader theme={theme} />
            {chemicalResults.map((chemicalResult: ChemicalTestResult, index) => (
              <ResultsTableRow
                key={chemicalResult.id}
                status={chemicalResult.status}
                theme={theme}
                text={chemicalResult.name}
                range={getRange(chemicalResult)}
                actual={chemicalResult.value}
                highlighted={index % 2 === 0}
              />
            ))}
          </View>
        </View>

        <View
          fixed
          style={styles.spacer}
        />

        <View style={styles.banner}>
          <Image
            style={styles.bannerImage}
            src={{
              uri: 'https://cors.bridged.cc/https://s3.ap-southeast-2.amazonaws.com/pooltrackr-dev-images/1/campaigns/2021-09-07-03-42-03_6136df8be7a2f_2021-04-29-03-12-59_608a243b0f50b_Smarter-Sticks-Banner.jpeg',
              method: 'GET',
              headers: {
                'Cache-Control': 'no-cache',
                'Access-Control-Allow-Origin': '*',
              },
              body: '',
            }}
          />
        </View>

        <View
          fixed
          style={styles.footer}
        >
          <Text style={[commonStyles.body1, styles.footerText]}>
            {organisationAddress}
            {' '}
            |
            {' '}
            {organisation.email}
            {' '}
            |
            {' '}
            {organisation.company_phone}
          </Text>
        </View>
      </Page>
    </Document>
  );
};

WaterTestPdf.propTypes = {
  // @ts-ignore
  labJob: PropTypes.object.isRequired,
  // @ts-ignore
  pool: PropTypes.object.isRequired,
  // @ts-ignore
  contact: PropTypes.object.isRequired,
  chemicalResults: PropTypes.array,
  observationResults: PropTypes.array,
  // @ts-ignore
  theme: PropTypes.object.isRequired,
  // @ts-ignore
  logo: PropTypes.object,
  // @ts-ignore
  organisation: PropTypes.object.isRequired,
};

export default WaterTestPdf;
