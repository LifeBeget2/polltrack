import type { FC } from 'react';
import React, { useMemo, useState } from 'react';
import {
  Box,
  Divider,
  Card,
  CardContent,
  CardHeader,
  Grid,
  FormControlLabel,
  Switch,
  CardActions,
  Button,
} from '@mui/material';
import { useDispatch, useSelector } from 'src/store';
import { PlusCircle as PlusCircleIcon } from 'react-feather';
import { ObservationTestResult } from 'src/types/waterTest';
import { SelectListModal } from '../../widgets/modals';
import { toggleObservationTest, updateObservationTestResult } from 'src/slices/waterTestCalculator';

const CalculatorObservations: FC = (props) => {
  const { observationTestResults } = useSelector((state) => state.waterTestCalculator);
  const [viewModal, setViewModal] = useState<boolean>(false);
  const dispatch = useDispatch();

  const enabledObservationTestResults = useMemo(
    () => observationTestResults.filter((observationTestResult: ObservationTestResult) => observationTestResult.enabled),
    [observationTestResults],
  );

  const handleValueChange = (observationTestResult: ObservationTestResult, value): void => {
    dispatch(updateObservationTestResult(observationTestResult, value));
  };

  const handleToggleObservationTest = (observationTestResult: ObservationTestResult, enabled: boolean) => {
    dispatch(toggleObservationTest(observationTestResult, enabled));
  };

  const handleClose = () => {
    setViewModal(false);
  };

  return (
    <>
      <Card {...props}>
        <CardHeader title="Observations" />
        <Divider />
        <CardContent>
          <Grid
            container
            spacing={3}
          >
            {enabledObservationTestResults.map((observationTestResult) => (
              <Grid
                item
                md={4}
                xs={6}
                key={observationTestResult.id}
              >
                <Box
                  sx={{
                    px: 1
                  }}
                >
                  <FormControlLabel
                    control={(
                      <Switch
                        color="primary"
                        edge="start"
                        checked={observationTestResult.value}
                        onChange={(event): void => handleValueChange(observationTestResult, event.target.checked)}
                      />
                    )}
                    label={observationTestResult.name}
                  />
                </Box>
              </Grid>
            ))}
          </Grid>
        </CardContent>
        <CardActions
          sx={{
            p: 2
          }}
          disableSpacing
        >
          <Button
            type="button"
            startIcon={<PlusCircleIcon size={16} />}
            onClick={() => setViewModal(true)}
            variant="text"
          >
            Add observation tests
          </Button>
        </CardActions>
      </Card>
      <SelectListModal
        label="Add observation tests"
        searchPlaceholder="Search tests"
        keepMounted={false}
        open={viewModal}
        items={observationTestResults}
        onToggle={handleToggleObservationTest}
        onClose={handleClose}
      />
    </>
  );
};
export default CalculatorObservations;
