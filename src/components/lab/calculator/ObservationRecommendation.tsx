import type { FC } from 'react';
import React from 'react';
import {
  Box,
  Card,
  Theme,
  CardContent,
  Typography,
  FormControlLabel,
  Switch,
  Grid,
  TextField,
} from '@mui/material';
import type { SxProps } from '@mui/system';
import PropTypes from 'prop-types';
import type { ObservationGroup } from 'src/types/chemical';

interface ObservationRecommendationProps {
  observationGroups: ObservationGroup[];
  sx?: SxProps<Theme>;
}

const ObservationRecommendation: FC<ObservationRecommendationProps> = (props) => {
  const { observationGroups, ...rest } = props;

  return (
    <Card
      {...rest}
    >
      <CardContent>
        <Box
          sx={{
            alignItems: 'center',
            display: 'flex',
            justifyContent: 'space-between',
          }}
        >
          <Typography
            color="textPrimary"
            variant="h6"
          >
            Cloudy
          </Typography>
          <FormControlLabel
            control={(
              <Switch
                color="primary"
                edge="start"
              />
            )}
            label="Show on report"
          />
        </Box>
        <Box
          mt={2}
        >
          <Grid
            container
            spacing={3}
          >
            <Grid
              item
              sm={6}
              xs={12}
            >
              <TextField
                fullWidth
                label="Select Dosage Group"
                name="dosage_group"
                select
                required
                InputLabelProps={{ shrink: true }}
                SelectProps={{ native: true, displayEmpty: true }}
                variant="outlined"
                sx={{
                  backgroundColor: 'background.paper',
                }}
              >
                <option value="">Select</option>
                {observationGroups.map((observationGroup: ObservationGroup) => (
                  <option
                    key={observationGroup.id}
                    value={observationGroup.id}
                  >
                    {observationGroup.name}
                  </option>
                ))}
              </TextField>
            </Grid>
            <Grid
              item
              sm={6}
              xs={12}
            >
              <TextField
                fullWidth
                label="Customer action"
                name="customer_action"
                value="Dummy value"
                variant="outlined"
                sx={{
                  backgroundColor: 'background.paper',
                }}
              />
            </Grid>
          </Grid>
        </Box>
      </CardContent>
    </Card>
  );
};

ObservationRecommendation.propTypes = {
  // @ts-ignore
  observationGroups: PropTypes.array.isRequired,
  sx: PropTypes.object,
};

export default ObservationRecommendation;
