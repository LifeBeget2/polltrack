import type { FC } from 'react';
import React from 'react';
import {
  Box,
  Card,
  Theme,
  CardContent,
  Typography,
  FormControlLabel,
  Switch,
  Grid,
  TextField,
} from '@mui/material';
import { SxProps } from '@mui/system';
import PropTypes from 'prop-types';
import { ChemicalGroup } from 'src/types/chemical';
import ChemicalTestStatus from './ChemicalTestStatus';

interface ChemicalRecommendationProps {
  chemicalGroups: ChemicalGroup[];
  sx?: SxProps<Theme>;
}

const ChemicalRecommendation: FC<ChemicalRecommendationProps> = (props) => {
  const { chemicalGroups, ...rest } = props;

  return (
    <Card
      {...rest}
    >
      <CardContent>
        <Box
          sx={{
            alignItems: 'center',
            display: 'flex',
            justifyContent: 'space-between',
          }}
        >
          <Box
            sx={{
              alignItems: 'center',
              display: 'flex',
              justifyContent: 'flex-start',
            }}
          >
            <ChemicalTestStatus
              currentValue={3}
              minValue={4}
              maxValue={10}
            />
            <Typography
              color="textPrimary"
              variant="h6"
              sx={{
                ml: 2
              }}
            >
              Cyanuric Acid
            </Typography>
            <Typography
              color="textPrimary"
              variant="body2"
              sx={{
                ml: 1
              }}
            >
              |
            </Typography>
            <Typography
              sx={{
                ml: 1,
                color: 'error.main',
              }}
              variant="body2"
              fontWeight="bold"
            >
              Recorded: 6
            </Typography>
            <Typography
              color="textSecondary"
              variant="body2"
              sx={{
                ml: 1
              }}
            >
              Target: 8
            </Typography>
          </Box>
          <FormControlLabel
            control={(
              <Switch
                color="primary"
                edge="start"
              />
            )}
            label="Show on report"
          />
        </Box>
        <Box
          mt={2}
        >
          <Grid
            container
            spacing={3}
          >
            <Grid
              item
              sm={6}
              xs={12}
            >
              <TextField
                fullWidth
                label="Select Dosage Group"
                name="dosage_group"
                select
                required
                InputLabelProps={{ shrink: true }}
                SelectProps={{ native: true, displayEmpty: true }}
                variant="outlined"
                sx={{
                  backgroundColor: 'background.paper',
                }}
              >
                <option value="">Select</option>
                {chemicalGroups.map((chemicalGroup: ChemicalGroup) => (
                  <option
                    key={chemicalGroup.id}
                    value={chemicalGroup.id}
                  >
                    {chemicalGroup.name}
                  </option>
                ))}
              </TextField>
            </Grid>
            <Grid
              item
              sm={6}
              xs={12}
            >
              <TextField
                fullWidth
                label="Customer action"
                name="customer_action"
                value="Dummy value"
                variant="outlined"
                sx={{
                  backgroundColor: 'background.paper',
                }}
              />
            </Grid>
          </Grid>
        </Box>
      </CardContent>
    </Card>
  );
};

ChemicalRecommendation.propTypes = {
  // @ts-ignore
  chemicalGroups: PropTypes.array.isRequired,
  sx: PropTypes.object,
};

export default ChemicalRecommendation;
