/* eslint-disable */
import type { FC } from 'react';
import PropTypes from 'prop-types';
import { format } from 'date-fns';
import numeral from 'numeral';
import {
  Box,
  Grid,
  Paper,
  Table,
  TableBody,
  TableCell,
  TableHead,
  TableRow,
  Typography
} from '@mui/material';
import Logo from '../../Logo';
import { Scrollbar } from '../../scrollbar';
import type {LabJob} from "src/types/labJob";
import React from "react";

interface WaterTestReportPreviewProps {
  labJob: LabJob;
}

const WaterTestReportPreview: FC<WaterTestReportPreviewProps> = (props) => {
  const { labJob, ...other } = props;

  return (
    <Paper {...other}>
      <Scrollbar>
        <Box
          sx={{
            p: 3
          }}
        >
          <Grid
            container
            justifyContent="space-between"
          >
            <Grid item>
              <Typography
                color="primary"
                variant="h3"
                fontWeight="bold"
              >
                Water Analysis
              </Typography>
              <Typography
                color="textPrimary"
                variant="subtitle2"
              >
                Wednesday 4 August 20201 | 3:34pm
              </Typography>
            </Grid>
            <Grid item>
              <Logo
                sx={{
                  display: 'flex',
                  width: 300,
                  '& img': {
                    width: '100%'
                  }
                }}
              />
            </Grid>
          </Grid>
          <Box
            sx={{
              my: 4,
              mx: -3,
              px: 3,
              backgroundColor: 'background.default',
            }}
          >
            <Table>
              <TableBody>
                <TableRow
                  sx={{
                    'td': {
                      border: 0
                    }
                  }}
                >
                  <TableCell>
                    <Typography
                      color="textPrimary"
                      gutterBottom
                      variant="subtitle2"
                    >
                      To:
                    </Typography>
                  </TableCell>
                  <TableCell>
                    <Typography
                      color="textPrimary"
                      gutterBottom
                      variant="subtitle2"
                    >
                      Contact name
                    </Typography>
                  </TableCell>
                  <TableCell>
                    <Typography
                      color="textPrimary"
                      gutterBottom
                      variant="subtitle2"
                    >
                      Type
                    </Typography>
                  </TableCell>
                  <TableCell align="right">
                    Pool
                  </TableCell>
                  <TableCell>
                    <Typography
                      color="textPrimary"
                      gutterBottom
                      variant="subtitle2"
                    >
                      Surface
                    </Typography>
                  </TableCell>
                  <TableCell align="right">
                    Fiberglass
                  </TableCell>
                </TableRow>
                <TableRow
                  sx={{
                    'td': {
                      border: 0
                    }
                  }}
                >
                  <TableCell>
                    <Typography
                      color="textPrimary"
                      gutterBottom
                      variant="subtitle2"
                    >
                      To:
                    </Typography>
                  </TableCell>
                  <TableCell>
                    <Typography
                      color="textPrimary"
                      gutterBottom
                      variant="subtitle2"
                    >
                      Contact name
                    </Typography>
                  </TableCell>
                  <TableCell>
                    <Typography
                      color="textPrimary"
                      gutterBottom
                      variant="subtitle2"
                    >
                      Type
                    </Typography>
                  </TableCell>
                  <TableCell align="right">
                    Pool
                  </TableCell>
                  <TableCell>
                    <Typography
                      color="textPrimary"
                      gutterBottom
                      variant="subtitle2"
                    >
                      Surface
                    </Typography>
                  </TableCell>
                  <TableCell align="right">
                    Fiberglass
                  </TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </Box>
          <Box sx={{ my: 4 }}>
            <Grid
              container
              justifyContent="space-between"
            >
              <Grid item>
                <Typography
                  color="textPrimary"
                  gutterBottom
                  variant="subtitle2"
                >
                  Due date
                </Typography>
                <Typography
                  color="textPrimary"
                  variant="body2"
                >
                  {/*{format(invoice.dueDate, 'dd MMM yyyy')}*/}
                </Typography>
              </Grid>
              <Grid item>
                <Typography
                  color="textPrimary"
                  gutterBottom
                  variant="subtitle2"
                >
                  Date of issue
                </Typography>
                <Typography
                  color="textPrimary"
                  variant="body2"
                >
                  {/*{format(invoice.issueDate, 'dd MMM yyyy')}*/}
                </Typography>
              </Grid>
            </Grid>
          </Box>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>
                  Description
                </TableCell>
                <TableCell />
                <TableCell align="right">
                  Unit Price
                </TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {/*{invoice.items.map((items) => (*/}
              {/*  <TableRow key={items.id}>*/}
              {/*    <TableCell>*/}
              {/*      {items.description}*/}
              {/*    </TableCell>*/}
              {/*    <TableCell />*/}
              {/*    <TableCell align="right">*/}
              {/*      {numeral(items.unitAmount)*/}
              {/*        .format(`${items.currency}0,0.00`)}*/}
              {/*    </TableCell>*/}
              {/*  </TableRow>*/}
              {/*))}*/}
              <TableRow>
                <TableCell />
                <TableCell>
                  <Typography
                    color="textPrimary"
                    gutterBottom
                    variant="subtitle2"
                  >
                    Subtotal
                  </Typography>
                </TableCell>
                <TableCell align="right">
                  {/*{numeral(invoice.subtotalAmount)*/}
                  {/*  .format(`${invoice.currency}0,0.00`)}*/}
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell />
                <TableCell>
                  <Typography
                    color="textPrimary"
                    gutterBottom
                    variant="subtitle2"
                  >
                    Taxes
                  </Typography>
                </TableCell>
                <TableCell align="right">
                  {/*{numeral(invoice.taxAmount)*/}
                  {/*  .format(`${invoice.currency}0,0.00`)}*/}
                </TableCell>
              </TableRow>
              <TableRow>
                <TableCell />
                <TableCell>
                  <Typography
                    color="textPrimary"
                    gutterBottom
                    variant="subtitle2"
                  >
                    Total
                  </Typography>
                </TableCell>
                <TableCell align="right">
                  {/*{numeral(invoice.totalAmount)*/}
                  {/*  .format(`${invoice.currency}0,0.00`)}*/}
                </TableCell>
              </TableRow>
            </TableBody>
          </Table>
          <Box sx={{ mt: 2 }}>
            <Typography
              color="textPrimary"
              gutterBottom
              variant="h6"
            >
              Notes
            </Typography>
            <Typography
              color="textSecondary"
              variant="body2"
            >
              Please make sure you have the right bank registration number
              as I
              had issues before and make sure you guys cover transfer
              expenses.
            </Typography>
          </Box>
        </Box>
      </Scrollbar>
    </Paper>
  );
};

WaterTestReportPreview.propTypes = {
  // @ts-ignore
  labJob: PropTypes.object.isRequired
};

export default WaterTestReportPreview;
