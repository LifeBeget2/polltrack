import type { FC } from 'react';
import React, { memo, useMemo, useState } from 'react';
import {
  Divider,
  Card,
  CardContent,
  CardHeader,
  Grid,
  CardActions,
  Button,
} from '@mui/material';
import { useDispatch, useSelector } from 'src/store';
import { PlusCircle as PlusCircleIcon } from 'react-feather';
import { updateChemicalTestResult, toggleChemicalTest } from 'src/slices/waterTestCalculator';
import ChemicalTestResultInput from './ChemicalTestResultInput';
import type { ChemicalTestResult } from 'src/types/waterTest';
import { SelectListModal } from '../../widgets/modals';

const CalculatorTests: FC = (props) => {
  const { chemicalTestResults } = useSelector((state) => state.waterTestCalculator);
  const [viewModal, setViewModal] = useState<boolean>(false);
  const dispatch = useDispatch();

  const enabledChemicalTestResults = useMemo(
    () => chemicalTestResults.filter((chemicalTestResult: ChemicalTestResult) => chemicalTestResult.enabled),
    [chemicalTestResults],
  );

  const handleValueChange = (chemicalTestResult: ChemicalTestResult, value): void => {
    dispatch(updateChemicalTestResult(chemicalTestResult, value));
  };

  const handleToggleChemicalTest = (chemicalTestResult: ChemicalTestResult, enabled: boolean) => {
    dispatch(toggleChemicalTest(chemicalTestResult, enabled));
  };

  const handleClose = () => {
    setViewModal(false);
  };

  return (
    <>
      <Card {...props}>
        <CardHeader title="Water test" />
        <Divider />
        <CardContent>
          <Grid
            container
            spacing={3}
          >
            {enabledChemicalTestResults.map((chemicalTestResult) => (
              <Grid
                item
                xl={4}
                md={6}
                xs={12}
                key={chemicalTestResult.chemical_test.id}
              >
                <ChemicalTestResultInput
                  chemicalTestResult={chemicalTestResult}
                  onChange={handleValueChange}
                  onRemove={handleToggleChemicalTest}
                />
              </Grid>
            ))}
          </Grid>
        </CardContent>
        <CardActions
          sx={{
            p: 2
          }}
          disableSpacing
        >
          <Button
            type="button"
            startIcon={<PlusCircleIcon size={16} />}
            onClick={() => setViewModal(true)}
            variant="text"
          >
            Add chemical tests
          </Button>
        </CardActions>
      </Card>
      <SelectListModal
        label="Add chemical tests"
        searchPlaceholder="Search tests"
        keepMounted={false}
        open={viewModal}
        items={chemicalTestResults}
        onToggle={handleToggleChemicalTest}
        onClose={handleClose}
      />
    </>
  );
};

export default memo(CalculatorTests);
