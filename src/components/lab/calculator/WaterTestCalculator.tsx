/* eslint-disable */
import type { FC } from 'react';
import React, { useEffect, memo, useState } from 'react';
import {
  Box,
  Button,
  Grid,
  Dialog
} from '@mui/material';
import type { Pool } from 'src/types/pool';
import type { Contact } from 'src/types/contact';
import PropTypes from 'prop-types';
import type { LabJob } from 'src/types/labJob';
import type { Job } from 'src/types/job';
import CalculatorObservations from './CalculatorObservations';
import CalculatorRecommendations from './CalculatorRecommendations';
import { useDispatch, useSelector } from 'src/store';
import { init, reset } from 'src/slices/waterTestCalculator';
import LoadingScreen from 'src/components/LoadingScreen';
import CalculatorTests from './CalculatorTests';
import { PDFViewer, usePDF } from '@react-pdf/renderer';
import WaterTestPdf from '../water-test-pdf/WaterTestPdf';
import { useTheme } from '@mui/material/styles';
import ArrowLeftIcon from 'src/icons/ArrowLeft';
import { LoadingButton } from '@mui/lab';
import type {PdfInstance} from "src/types/common";
import debounce from "lodash/debounce";

interface WaterTestCalculatorProps {
  job: LabJob | Job;
  pool: Pool;
  contact: Contact;
  disabled: boolean;
  onGenerateReport: (pdfInstance: PdfInstance) => void;
}

const WaterTestCalculator: FC<WaterTestCalculatorProps> = (props) => {
  const { job, pool, contact, disabled, onGenerateReport } = props;
  const { organisation, logo } = useSelector((state) => state.account);
  const [viewPDF, setViewPDF] = useState<boolean>(false);
  const { isLoading, initialised, activeChemicalTestResults, activeObservationTestResults } = useSelector((state) => state.waterTestCalculator);
  const dispatch = useDispatch();
  const theme = useTheme();

  const [instance, updateInstance] = usePDF({ document:
    <WaterTestPdf
      labJob={job}
      pool={pool}
      contact={contact}
      theme={theme}
      logo={logo}
      organisation={organisation}
      chemicalResults={activeChemicalTestResults}
      observationResults={activeObservationTestResults}
    />
  });

  useEffect(() => () => dispatch(reset()), []);

  useEffect(() => {
    if (!initialised) {
      dispatch(init(organisation.id, job, pool));
    }
  }, [job, pool, organisation]);

  useEffect(() => {
    const handleChangeDebounce = debounce(() => {
      updateInstance();
    }, 2000);
    handleChangeDebounce();
  }, [pool, contact, activeChemicalTestResults, activeObservationTestResults]);

  if (isLoading) {
    return <LoadingScreen />;
  }

  return (
    <>
      <Grid
        container
        spacing={3}
      >
        <Grid
          item
          xs={12}
        >
          <CalculatorTests />
        </Grid>
        <Grid
          item
          xs={12}
        >
          <CalculatorObservations />
        </Grid>
        <Grid
          item
          xs={12}
        >
          <CalculatorRecommendations pool={pool} />
        </Grid>
      </Grid>
      <Box
        sx={{
          alignItems: 'center',
          justifyContent: 'center',
          display: 'flex',
          py: 2
        }}
      >
        <Button
          color="primary"
          onClick={(): void => setViewPDF(true)}
          variant="outlined"
          disabled={disabled}
        >
          Preview PDF
        </Button>
        <LoadingButton
          color="primary"
          sx={{ ml: 2 }}
          onClick={() => onGenerateReport(instance)}
          type="button"
          variant="contained"
          loading={disabled}
        >
          Generate report
        </LoadingButton>
      </Box>
      <Dialog
        fullScreen
        open={viewPDF}
      >
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'column',
            height: '100%'
          }}
        >
          <Box
            sx={{
              backgroundColor: 'background.default',
              p: 2
            }}
          >
            <Button
              color="primary"
              startIcon={<ArrowLeftIcon fontSize="small" />}
              onClick={(): void => setViewPDF(false)}
              variant="contained"
            >
              Back
            </Button>
          </Box>
          <Box sx={{ flexGrow: 1 }}>
            <PDFViewer
              height="100%"
              style={{ border: 'none' }}
              width="100%"
              showToolbar
            >
              <WaterTestPdf
                labJob={job}
                pool={pool}
                contact={contact}
                theme={theme}
                logo={logo}
                organisation={organisation}
                chemicalResults={activeChemicalTestResults}
                observationResults={activeObservationTestResults}
              />
            </PDFViewer>
          </Box>
        </Box>
      </Dialog>
    </>
  );
};

WaterTestCalculator.propTypes = {
  // @ts-ignore
  job: PropTypes.object.isRequired,
  // @ts-ignore
  pool: PropTypes.object.isRequired,
  // @ts-ignore
  contact: PropTypes.object.isRequired,
  disabled: PropTypes.bool.isRequired,
  onGenerateReport: PropTypes.func.isRequired,
};

export default memo(WaterTestCalculator);
