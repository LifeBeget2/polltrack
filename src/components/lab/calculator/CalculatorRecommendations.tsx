import type { FC } from 'react';
import React from 'react';
import {
  Divider,
  Card,
  CardHeader,
  CardContent,
  Theme,
} from '@mui/material';
import type { SxProps } from '@mui/system';
import type { Pool } from 'src/types/pool';
import PropTypes from 'prop-types';
import ObservationRecommendation from './ObservationRecommendation';
import ChemicalRecommendation from './ChemicalRecommendation';

interface CalculatorRecommendationsProps {
  pool: Pool;
  sx?: SxProps<Theme>;
}

const CalculatorRecommendations: FC<CalculatorRecommendationsProps> = (props) => {
  const { pool, ...rest } = props;

  return (
    <Card {...rest}>
      <CardHeader title="Recommendations" />
      <Divider />
      <CardContent>
        <ChemicalRecommendation
          sx={{
            backgroundColor: 'background.default',
          }}
          chemicalGroups={[]}
        />
        <ObservationRecommendation
          sx={{
            mt: 3,
            backgroundColor: 'background.default',
          }}
          observationGroups={[]}
        />
      </CardContent>
    </Card>
  );
};

CalculatorRecommendations.propTypes = {
  // @ts-ignore
  pool: PropTypes.object.isRequired,
  sx: PropTypes.object
};

export default CalculatorRecommendations;
