import React, { useMemo, useRef, useState } from 'react';
import type { FC } from 'react';
import PropTypes from 'prop-types';
import {
  Box,
  IconButton,
  InputAdornment,
  TextField,
  Tooltip,
  Typography,
  Popover,
} from '@mui/material';
import {
  Clock as ClockIcon,
  XCircle as XCircleIcon,
} from 'react-feather';
import ChemicalTestStatus from './ChemicalTestStatus';
import { PH_TEST, TEMPERATURE_TEST } from './constants';
import HistoryChart from './HistoryChart';
import type { ChemicalTestResult } from 'src/types/waterTest';

interface ChemicalTestResultInputProps {
  chemicalTestResult: ChemicalTestResult;
  onChange?: (chemicalTestResult: ChemicalTestResult, value: string) => void;
  onRemove?: (chemicalTestResult: ChemicalTestResult, enabled: boolean) => void;
}

const ChemicalTestResultInput: FC<ChemicalTestResultInputProps> = (props) => {
  const {
    chemicalTestResult,
    onChange,
    onRemove,
    ...other
  } = props;

  const anchorRef = useRef<HTMLButtonElement | null>(null);
  const [openHistory, setOpenHistory] = useState<boolean>(false);

  const currentValue = useMemo(
    () => (chemicalTestResult?.value ? parseFloat(chemicalTestResult.value) : ''),
    [chemicalTestResult.value],
  );

  const minValue = useMemo(
    () => chemicalTestResult.chemical_test.minimum_value || 0,
    [chemicalTestResult.chemical_test],
  );

  const maxValue = useMemo(
    () => chemicalTestResult.chemical_test.maximum_value || 0,
    [chemicalTestResult.chemical_test],
  );

  const tooltipInfo = useMemo(
    () => `Min: ${chemicalTestResult.chemical_test.minimum_value} | Max: ${chemicalTestResult.chemical_test.maximum_value} | Target: ${chemicalTestResult.chemical_test.target_value}`,
    [chemicalTestResult.chemical_test],
  );

  const handleOpenHistory = (): void => {
    setOpenHistory(true);
  };

  const handleRemoveTest = (): void => {
    onRemove(chemicalTestResult, false);
  };

  const handleCloseHistory = (): void => {
    setOpenHistory(false);
  };

  const testMeasurement = useMemo(
    () => {
      let measurement = 'ppm';
      switch (chemicalTestResult.chemical_test.name) {
        case TEMPERATURE_TEST:
          measurement = '°C';
          break;
        case PH_TEST:
          measurement = 'pH';
          break;
        default:
      }
      return measurement;
    },
    [chemicalTestResult.chemical_test],
  );

  return (
    <Box
      sx={{
        display: 'flex',
      }}
      {...other}
    >
      <Tooltip title={tooltipInfo}>
        <TextField
          fullWidth
          label={chemicalTestResult.chemical_test.name}
          name={chemicalTestResult.chemical_test.name}
          onChange={(event): void => onChange(
            chemicalTestResult,
            event.target.value
          )}
          type="number"
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                <Typography
                  color="textSecondary"
                  noWrap
                  variant="caption"
                >
                  {testMeasurement}
                </Typography>
              </InputAdornment>
            )
          }}
          value={chemicalTestResult.value}
        />
      </Tooltip>
      <Box
        sx={{
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center'
        }}
      >
        <Tooltip title="Test history">
          <IconButton
            sx={{
              color: 'primary.main',
            }}
            ref={anchorRef}
            onClick={handleOpenHistory}
          >
            <ClockIcon fontSize="small" />
          </IconButton>
        </Tooltip>
        {!chemicalTestResult.chemical_test?.is_default && (
          <IconButton
            sx={{
              color: 'error.main',
            }}
            onClick={handleRemoveTest}
          >
            <XCircleIcon fontSize="small" />
          </IconButton>
        )}
        <Popover
          anchorEl={anchorRef.current}
          anchorOrigin={{
            horizontal: 'left',
            vertical: 'bottom'
          }}
          onClose={handleCloseHistory}
          open={openHistory}
          PaperProps={{
            sx: { width: 500 }
          }}
        >
          <HistoryChart
            testName={chemicalTestResult.chemical_test.name}
            historyItems={null}
          />
        </Popover>
      </Box>
      <ChemicalTestStatus
        currentValue={currentValue}
        minValue={minValue}
        maxValue={maxValue}
      />
    </Box>
  );
};

ChemicalTestResultInput.propTypes = {
  // @ts-ignore
  chemicalTest: PropTypes.object.isRequired,
  onChange: PropTypes.func,
  onRemove: PropTypes.func,
};

export default ChemicalTestResultInput;
