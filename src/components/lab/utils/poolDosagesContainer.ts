/* eslint-disable */
import type { ChemicalGroup, ObservationGroup } from 'src/types/chemical';

export class PoolDosagesContainer {
  private readonly chemicalGroups: ChemicalGroup[];
  private readonly observationGroups: ObservationGroup[];

  constructor(chemicalGroups: ChemicalGroup[], observationGroups: ObservationGroup[]) {
    this.chemicalGroups = chemicalGroups;
    this.observationGroups = observationGroups;
  };

  getChemicalGroups(): ChemicalGroup[] {
    return this.chemicalGroups;
  };

  getObservationGroups(): ObservationGroup[] {
    return this.observationGroups;
  };
}
