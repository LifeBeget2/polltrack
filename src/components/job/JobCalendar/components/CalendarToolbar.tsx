import React, {
  FC,
  useCallback,
} from 'react';
import PropTypes from 'prop-types';
import {
  Box,
  Button,
  ButtonGroup,
  FormControlLabel,
  Switch,
  Typography,
  IconButton,
} from '@mui/material';
import ChevronLeftIcon from 'src/icons/ChevronLeft';
import ChevronRightIcon from 'src/icons/ChevronRight';

import type { CalendarView } from 'src/types/calendar';
import { useDispatch, useSelector } from 'src/store';
import { setShowMap } from 'src/slices/jobCalendar';

interface CalendarToolbarProps {
  title: string;
  onAddClick?: () => void;
  onDateNext?: () => void;
  onDatePrev?: () => void;
  onViewChange?: (view: CalendarView) => void;
  view: CalendarView;
}

interface ViewOption {
  label: string;
  value: CalendarView;
}

const viewOptions: ViewOption[] = [
  {
    label: 'Agenda',
    value: 'listWeek'
  },
  {
    label: 'Quick',
    value: 'quickSchedule'
  },
  {
    label: 'Day',
    value: 'resourceTimeline'
  },
  {
    label: 'Week',
    value: 'resourceTimelineWeek'
  },
  {
    label: 'Fortnite',
    value: 'resourceTimelineFortnite'
  },
  {
    label: 'Month',
    value: 'dayGridMonth'
  }
];

const CalendarToolbar: FC<CalendarToolbarProps> = (props) => {
  const { showMap } = useSelector((state) => state.jobCalendar);
  const dispatch = useDispatch();

  const {
    title,
    onDateNext,
    onDatePrev,
    onViewChange,
    view,
    ...other
  } = props;

  const handleViewChange = useCallback((newView: CalendarView): void => {
    if (onViewChange) {
      onViewChange(newView);
    }
  }, [onViewChange]);

  const handleShowMap = useCallback((showMap: boolean): void => {
    dispatch(setShowMap(showMap));
  }, []);

  return (
    <Box
      display="flex"
      justifyContent="space-between"
      flexDirection="row"
      alignItems="center"
      flexWrap="wrap"
      {...other}
    >
      <Box
        display="flex"
        flexDirection="row"
        alignItems="center"
      >
        <ButtonGroup>
          <IconButton onClick={onDatePrev}>
            <ChevronLeftIcon />
          </IconButton>
          <IconButton onClick={onDateNext}>
            <ChevronRightIcon />
          </IconButton>
        </ButtonGroup>

        <Typography
          sx={{
            ml: 1
          }}
          color="textPrimary"
          variant="h5"
        >
          {title}
        </Typography>
      </Box>
      <Box
        display="flex"
        flexDirection="row"
        alignItems="center"
        justifyContent="flex-end"
        flexWrap="wrap"
      >
        <ButtonGroup
          size="medium"
          sx={{ backgroundColor: 'common.white' }}
        >
          {viewOptions.map((viewOption) => (
            <Button
              variant={viewOption.value === view ? 'contained' : 'outlined'}
              key={viewOption.value}
              onClick={() => handleViewChange(viewOption.value)}
            >
              {viewOption.label.toUpperCase()}
            </Button>
          ))}
        </ButtonGroup>
        <FormControlLabel
          sx={{
            ml: 1,
            // minWidth: 150,
          }}
          control={(
            <Switch
              checked={showMap}
              color="primary"
              name="mapView"
              disabled={view !== 'resourceTimeline'}
              onChange={(event) => handleShowMap(event.target.checked)}
            />
          )}
          label="Map view"
        />
        {' '}
      </Box>
    </Box>
  );
};

CalendarToolbar.propTypes = {
  title: PropTypes.string.isRequired,
  onAddClick: PropTypes.func,
  onDateNext: PropTypes.func,
  onDatePrev: PropTypes.func,
  onViewChange: PropTypes.func,
  view: PropTypes.oneOf([
    'resourceTimeline',
    'resourceTimelineWeek',
    'resourceTimelineFortnite',
    'dayGridMonth',
    'listWeek',
  ])
};

export default CalendarToolbar;
