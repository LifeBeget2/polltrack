export interface IGetProducts {
  organisationId: number | string;
  limit: number | string;
  page: number;
  catFilter: string | number;
  filter: string;
  sortBy: string;
}

export interface IProduct {
  organisationId: number | string;
  id: number;
  body?: any;
}

export interface IChangeVisibility {
  organisationId: number | string;
  id: number;
  body: object;
}

export interface IExportProduct {
  organisationId: number | string;
}

export interface IDeleteProduct {
  organisationId: number | string;
  id: number;
}

export interface IEditProduct {
  organisationId: number | string;
  id: number;
  body:object;
}

export interface IAdd {
  organisationId: number | string;
  body:object;
}
