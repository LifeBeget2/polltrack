export interface IArg {
  url: string;
  method: 'POST' | 'PUT' | 'GET' | 'DELETE' | string;
  body?: any;
  responseType?: 'blob' | 'json' | 'buffer' | 'text';
  contentType?: string;
}

export interface IPaginatedResponse{
  data: any[];
  meta: IMeta;
}

export interface IMeta{
  current_page: number;
  from: number;
  last_page: number;
  links: any;
  path: string;
  per_page: string;
  to: number;
  total: number;
}
