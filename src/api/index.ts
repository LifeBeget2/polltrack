import {
  isRejectedWithValue,
  Middleware,
} from '@reduxjs/toolkit';
import { productApi } from './product';
import toast from 'react-hot-toast';

export const rtkQueryErrorLogger: Middleware = () => (next) => (action) => {
  if (isRejectedWithValue(action)) {
    try {
      if (action.meta.arg.type === 'mutation' && action.payload.data) {
        toast.error(action.payload.data);
      }
    } catch (e) {
      console.error(e.message);
    }
  }
  return next(action);
};

export const middlewareList = [
  productApi.middleware,
  rtkQueryErrorLogger
];

export const apiReducer = {
  [productApi.reducerPath]: productApi.reducer
};
