import { createApi } from '@reduxjs/toolkit/dist/query/react';
import { BaseQuery } from './BaseQuery';
import each from 'lodash/each';
import {
  IAdd,
  IChangeVisibility,
  IDeleteProduct,
  IExportProduct,
  IProduct,
  IGetProducts
} from './model/product.model';
import toast from 'react-hot-toast';

export const productApi = createApi({
  reducerPath: 'productApi',
  baseQuery: BaseQuery(),
  tagTypes: ['Product', 'Products'],
  endpoints: (builder) => ({
    getProducts: builder.query<any, IGetProducts>({
      query: (param:IGetProducts) => `v2/organisations/${param.organisationId}/products?category_id=${param.catFilter}&filter=${param.filter}&limit=${param.limit}&order=${param.sortBy}&page=${param.page + 1}`,
      providesTags: (result) => (result.data ? [
        ...result.data.map(({ id }) => ({ type: 'Products' as const, id })),
        { type: 'Products', id: 'LIST' },
      ] : [{ type: 'Products', id: 'LIST' }])
    }),
    getProduct: builder.query<any, IProduct>({
      query: (param:IProduct) => `v1/organisations/${param.organisationId}/products/${param.id}`,
      providesTags: (result, error, param) => [{ type: 'Product', id: param.id }]
    }),
    exportProduct: builder.mutation<any, IExportProduct>({
      query: (param:IExportProduct) => ({
        url: `v1/organisations/${param.organisationId}/export-products`,
        method: 'POST',
        responseType: 'text'
      })
    }),
    changeVisibility: builder.mutation<any, IChangeVisibility>({
      query: (param:IChangeVisibility) => ({
        url: `v1/organisations/${param.organisationId}/products/${param.id}/visibility`,
        method: 'PUT',
        body: param.body
      }),
      async onQueryStarted({ ...param }, { dispatch, queryFulfilled, getState }) {
        const result = productApi.util.selectInvalidatedBy(getState(), [{ type: 'Products', id: param.id }]);
        if (result && result.length) {
          each(result, async ({ endpointName, originalArgs }) => {
            if (endpointName && endpointName === 'getProducts' && originalArgs) {
              const patchResult = dispatch(
                // @ts-ignore
                productApi.util.updateQueryData('getProducts', originalArgs, (products) => {
                  products.data.forEach((product) => {
                    if (product.id === param.id) {
                      Object.assign(product, param.body);
                    }
                  });
                })
              );
              try {
                await queryFulfilled;
              } catch {
                patchResult.undo();
              }
            }
          });
        }
      },
      invalidatesTags: (result, error, param) => [{ type: 'Product', id: param.id }],
    }),
    deleteProduct: builder.mutation<any, IDeleteProduct>({
      query: (param:IDeleteProduct) => ({
        url: `v1/organisations/${param.organisationId}/products/${param.id}`,
        method: 'delete'
      }),
      transformResponse(response) {
        toast.success('Product successfully deleted');
        return response;
      },
      invalidatesTags: (result, error, param) => [
        { type: 'Product', id: param.id },
        { type: 'Products', id: param.id },
      ],
    }),
    editProduct: builder.mutation<any, IProduct>({
      query: (param:IProduct) => ({
        url: `v1/organisations/${param.organisationId}/products/${param.id}`,
        method: 'put',
        body: param.body
      }),
      async onQueryStarted({ ...patch }, { dispatch, queryFulfilled }) {
        const patchResult = dispatch(
          productApi.util.updateQueryData('getProduct', {
            organisationId: patch.organisationId,
            id: patch.id
          }, (draft) => {
            Object.assign(draft, patch);
          })
        );
        try {
          await queryFulfilled;
        } catch {
          patchResult.undo();
        }
      },
      transformResponse(response) {
        toast.success('Product successfully updated');
        return response;
      },
      invalidatesTags: (result, error, param) => [
        { type: 'Product', id: param.id },
        { type: 'Products', id: param.id },
      ],
    }),
    add: builder.mutation<any, IAdd>({
      query: (param:IAdd) => ({
        url: `v1/organisations/${param.organisationId}/products`,
        method: 'post',
        body: param.body
      }),
      transformResponse(response) {
        toast.success('Product successfully created');
        return response;
      },
      invalidatesTags: [{ type: 'Products', id: 'LIST' }],
    })
  })
});
export const {
  useChangeVisibilityMutation,
  useDeleteProductMutation,
  useAddMutation,
  useEditProductMutation,
  useExportProductMutation,
  useGetProductQuery,
  useGetProductsQuery
} = productApi;
