import { BaseQueryFn } from '@reduxjs/toolkit/query/react';
import { apiConfig } from '../config';
import { getItem } from '../utils/storage';
import { IArg } from './model/base.model';

export const BaseQuery = (
  { baseUrl }: { baseUrl: string } = { baseUrl: apiConfig.baseUrl }
): BaseQueryFn<
IArg | string,
unknown,
unknown
> => async (args:IArg | string) => {
  try {
    const parameter:IArg = typeof args === 'string' ? {
      url: args,
      method: 'GET',
      responseType: 'json',
      contentType: 'json'
    } : args;
    const { method, body, url, responseType, contentType } = parameter;
    const headers:Headers = new Headers();
    headers.append('Authorization', `Bearer ${getItem('accessToken')}`);
    headers.append('Content-Type', contentType !== undefined ? contentType : 'application/json');
    const result = await fetch(`${baseUrl}${url}`, {
      method,
      headers,
      body: JSON.stringify(body)
    });
    if (result?.status !== 200) {
      const error = await result.json();
      const { errors, message } = error;
      throw errors || message || error[0] || await result.text();
    }
    switch (responseType) {
      case 'blob':
        return { data: await result.blob() };
      case 'buffer':
        return { data: await result.arrayBuffer() };
      case 'json':
        return { data: await result.json() };
      case 'text':
        return { data: await result.text() };
      default:
        return { data: await result.json() };
    }
  } catch (errors) {
    return {
      error: { data: errors || '' },
    };
  }
};
